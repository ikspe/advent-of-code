import argparse
import dataclasses
import time


@dataclasses.dataclass
class Card:
    num: int
    winning_nums: set[int]
    my_nums: list[int]
    matches: int  # computed from count_matches()


def parse_line(line: str) -> Card:
    card_str, numbers = line.split(":")
    card_num = int(card_str.split()[1])
    winning_nums_str, my_nums_str = numbers.split("|")
    card = Card(num=card_num,
                winning_nums=set(map(int, winning_nums_str.strip().split())),
                my_nums=map(int, my_nums_str.strip().split()),
                matches=0)
    card.matches = count_matches(card)
    return card


def count_matches(card: Card):
    matches = 0
    for my_num in card.my_nums:
        if my_num in card.winning_nums:
            matches +=1
    return matches

def process_part2(list_cards: list[Card], verbose_level: int):
    count_of_cards = [1] * len(list_cards)  # original + copies
    for card in list_cards:
        if verbose_level >= 2:
            print(f"[DEBUG] card {card.num : 3}         count_of_cards = {count_of_cards}")
        count_this_card = count_of_cards[card.num - 1]
        list_new_card_nums = list(range(card.num + 1, card.num + card.matches + 1))
        if verbose_level >= 1:
            print(f"[DEBUG] card {card.num : 3} ({count_this_card:3} instances)=> found {len(list_new_card_nums):2} new cards : {list_new_card_nums}")
        for new_card_num in list_new_card_nums:
            count_of_cards[new_card_num - 1] += count_this_card
        if not list_new_card_nums:
            if verbose_level >= 1:
                print(f"[DEBUG] card {card.num : 3} => no new card => should NOT STOP as they may still have later cards returning copies...")
            # break
    if verbose_level >= 1:
        for idx, val in enumerate(count_of_cards):
            print(f"[DEBUG] final card {idx+1} => {val:3} instances")
    return sum(count_of_cards)

def main(is_part1: bool, input_file: str, verbose_level=0):
    list_cards: list[Card] = []
    with open(input_file) as f:
        for line in f:
            line = line.strip()
            if not line:
                continue
            list_cards.append(parse_line(line))
    if is_part1:
        sum_points = 0
        for card in list_cards:
            points = 0 if not card.matches else 2 ** (card.matches - 1)
            if verbose_level >= 1:
                print(f"[DEBUG] card {card.num: 3} => {points} points")
            sum_points += points
        return sum_points
    return process_part2(list_cards, verbose_level)


if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")

    # parse the arguments
    args = parser.parse_args()

    # run !
    res = None
    time_begin = time.time()
    try:
        res = main(args.part1, args.input, verbose_level=args.verbose)
    finally:
        time_end = time.time()
        print(f"Result = {res}")
        print(f"Computation time = {time_end - time_begin} s")
