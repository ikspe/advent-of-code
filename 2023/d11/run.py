import argparse
import time


class SpaceImage:
    def __init__(self, lines: list[str], verbose_level: int) -> None:
        self.lines = lines
        self.col_count = len(self.lines[0])
        self.line_count = len(self.lines)
        self.verbose_level = verbose_level
        self._empty_lines = set()
        self._empty_cols = set()
        for l_idx, line in enumerate(self.lines):
            if all([x == "." for x in line]):
                self._empty_lines.add(l_idx)
        for c_idx in range(self.col_count):
            if all([line[c_idx] == "." for line in self.lines]):
                self._empty_cols.add(c_idx)
        if self.verbose_level >= 1:
            print(f"[DEBUG] empty_lines = {self._empty_lines}")
            print(f"[DEBUG] empty_cols  = {self._empty_cols}")

    def display(self):
        # print(f"{self.line_count = } / {self.col_count = }")
        for l in self.lines:
            print(l)

    def _discover_galaxies(self):
        galaxies = set()
        for l_idx, line in enumerate(self.lines):
            for c_idx, space_point in enumerate(line):
                if space_point != ".":
                    galaxies.add((c_idx, l_idx))

        return galaxies

    def _adjust_galaxies(self, galaxies: set, adjust_factor: int):
        # part 1 : adjust_factor = 2
        # part 2 : example adjust_factor = 100 / real adjust_factor = 1000000

        mapping_lines_adjusted = {}
        curr_new_idx = 0
        for old_idx in range(self.line_count):
            mapping_lines_adjusted[old_idx] = curr_new_idx
            if old_idx in self._empty_lines:
                curr_new_idx += adjust_factor
            else:
                curr_new_idx += 1

        mapping_cols_adjusted = {}
        curr_new_idx = 0
        for old_idx in range(self.line_count):
            mapping_cols_adjusted[old_idx] = curr_new_idx
            if old_idx in self._empty_cols:
                curr_new_idx += adjust_factor
            else:
                curr_new_idx += 1

        adjusted_galaxies = set()
        for (c_idx, l_idx) in galaxies:
            adjusted_galaxies.add((mapping_cols_adjusted[c_idx], mapping_lines_adjusted[l_idx]))
        return adjusted_galaxies

    def compute(self, adjust_factor: int):
        #self._adjust_time_expand()
        galaxies = self._discover_galaxies()
        galaxies = self._adjust_galaxies(galaxies, adjust_factor)
        sum_shortest = 0
        while galaxies:
            g = galaxies.pop()
            for g2 in galaxies:
                sum_shortest += abs(g2[0] - g[0]) + abs(g2[1] - g[1])
        return sum_shortest


def main(is_part1: bool, input_file: str, verbose_level=0):
    with open(input_file) as f:
        space = SpaceImage([l for l in f.read().splitlines() if l], verbose_level)
    if is_part1:
        adjust_factor = 2
    else:
        adjust_factor = 1000000  # 100 for input example >_<
    return space.compute(adjust_factor)


if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")

    # parse the arguments
    args = parser.parse_args()

    # run !
    res = None
    time_begin = time.time()
    try:
        res = main(args.part1, args.input, verbose_level=args.verbose)
    finally:
        time_end = time.time()
        print(f"Result = {res}")
        print(f"Computation time = {time_end - time_begin} s")
