#!/usr/bin/env python3

import argparse
import os
import requests
import subprocess
import time
import yaml


class Config:
    CONFIG_FILE_NAME = "config.yml"

    def __init__(self, config_file) -> None:

        with open(config_file) as f:
            config = yaml.safe_load(f)

        self.this_year_folder = str(config["current_year_folder"])
        self.input_file_root_dir = os.path.expanduser(config["input_file_root_dir"])
        self.input_file_basename = config["input_file"]
        self.input_file_example_basename = config["input_file_example"]

    @classmethod
    def get_config(cls):
        return Config(cls.CONFIG_FILE_NAME)


def main():

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("day", metavar="dXX")  # required
    parser.add_argument("--part1", default=False, action="store_true")
    group = parser.add_argument_group("input")
    group.add_argument("--example", default=False, action="store_true")
    group.add_argument("--other-input", default=None)
    del group
    parser.add_argument("-v", "--verbose", default=0, action="count")
    parser.add_argument("--measure-time", default=False, action="store_true")
    parser.add_argument("--dry-run", default=False, action="store_true")

    parser.add_argument("--submit", metavar="INT_VAL", help="will not execute the solver", type=int)

    # parse the arguments
    args = parser.parse_args()

    config = Config.get_config()

    if args.other_input:
        input_file_name = args.other_input
    elif args.example:
        input_file_name = os.path.join(args.day, config.input_file_example_basename)
    else:
        input_file_name = os.path.join(config.input_file_root_dir, config.this_year_folder, args.day, config.input_file_basename)

    verbose_args = ["-" + "v" * args.verbose] if args.verbose else []
    part_1_or_2_args = ["--part1"] if args.part1 else []

    if args.submit:
        input_dir_this_year = os.path.join(config.input_file_root_dir, config.this_year_folder)
        if not args.day.startswith("d") or len(args.day) != 3:
            raise ValueError(f"format of day must be dXX : invalid {args.day}")
        day_numeric = int(args.day[1:])
        with open(os.path.join(input_dir_this_year, "web-cookie.txt")) as f:
            web_cookie = f.read().strip()
        url = f"https://adventofcode.com/{config.this_year_folder}/day/{day_numeric}/answer"
        post_content = {"level": "1" if args.part1 else "2", "answer": str(args.submit)}
        print(f"POSTING to url {url} with content = {post_content}")
        r = requests.post(url, headers={"Cookie": web_cookie}, files=post_content)
        print(f"Result : {r.status_code}")
        try:
            r.raise_for_status()
            print(f"FOUND IT !")
        except:
            print(f"ERROR :-(")
            print(r.text)
        return

    # run !
    command_to_launch = ["python3", os.path.join(args.day, "run.py"), *part_1_or_2_args, *verbose_args, "--input", input_file_name]
    if args.verbose >= 1:
        print(f"[DEBUG] run_one_day: Input file = {input_file_name}")
        print(f"[DEBUG] run_one_day: Executing : {command_to_launch} ...")
        print(f"[DEBUG] run_one_day: Executing : {' '.join(command_to_launch)} ...")
        import sys
        sys.stdout.flush()  # flushing since we run a subprocess
    if args.dry_run:
        print("DRY-RUN: stop before executing command")
        return

    time_begin = time.time()
    subprocess.check_call(command_to_launch)
    time_end = time.time()
    if args.measure_time:
        print(f"Execution time: {time_end - time_begin:.6f} seconds")


if __name__ == "__main__":
    main()
