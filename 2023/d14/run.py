import argparse
import enum
import time


class Direction(enum.IntEnum):
    NORTH = 1
    SOUTH = 2
    EAST = 3
    WEST = 4
    # vesoul ?


class World:
    def __init__(self, lines: list[str], verbose_level: int) -> None:
        assert all(lines) # check no empty line
        # self.lines = lines
        self.verbose_level = verbose_level
        self._col_count = len(lines[0])
        self._line_count = len(lines)
        self._obstacles: set[tuple] = set()
        self._rocks: set[tuple] = set()
        self._import_lines(lines)
        if self.verbose_level >= 2:
            print(f"[DEBUG] rock positions at init:")
            self._display_lines(lines)

    def _import_lines(self, lines):

        for l_idx, it_line in enumerate(lines):
            for c_idx, tile in enumerate(it_line):
                pos = (c_idx, l_idx)
                if tile == "#":
                    self._obstacles.add(pos)
                elif tile == "O":
                    self._rocks.add(pos)

    def _tilt(self, direction: Direction):
        # obstacles remain the same
        # look for all rocks moving to direction as they can
        new_rocks = set()
        
        # we need to move the rocks starting by upper lines first

        for l_idx in range(self._line_count):
            if direction == Direction.SOUTH:
                l_idx = self._line_count - 1 - l_idx
            for c_idx in range(self._col_count):
                if direction == Direction.EAST:
                    c_idx = self._col_count - 1 - c_idx
                pos = (c_idx, l_idx)
                if pos not in self._rocks:
                    continue  # no rock here
                if self.verbose_level >= 3:
                    print(f"[DEBUG] found rock at {pos} ...")
                last_correct_pos = None
                if direction == Direction.NORTH:
                    for tmp_l_idx in range(l_idx, -1, -1):
                        tmp_pos = (c_idx, tmp_l_idx)
                        if tmp_pos in self._obstacles or tmp_pos in new_rocks:
                            break
                        last_correct_pos = tmp_pos
                elif direction == Direction.SOUTH:
                    for tmp_l_idx in range(l_idx, self._line_count):
                        tmp_pos = (c_idx, tmp_l_idx)
                        if tmp_pos in self._obstacles or tmp_pos in new_rocks:
                            break
                        last_correct_pos = tmp_pos
                elif direction == Direction.WEST:
                    for tmp_c_idx in range(c_idx, -1, -1):
                        tmp_pos = (tmp_c_idx, l_idx)
                        if tmp_pos in self._obstacles or tmp_pos in new_rocks:
                            break
                        last_correct_pos = tmp_pos
                elif direction == Direction.EAST:
                    for tmp_c_idx in range(c_idx, self._col_count):
                        tmp_pos = (tmp_c_idx, l_idx)
                        if tmp_pos in self._obstacles or tmp_pos in new_rocks:
                            break
                        last_correct_pos = tmp_pos
                else:
                    raise ValueError(f"Incorrect direction : {direction}")
                assert last_correct_pos is not None
                if self.verbose_level >= 3:
                    print(f"[DEBUG]       rock at {pos} : moving to {last_correct_pos}")
                    #if last_correct_pos in new_rocks:
                    #    print(f"ERROR : already a rock     at {last_correct_pos}")
                    #if last_correct_pos in self._obstacles:
                    #    print(f"ERROR : already a obstacle at {last_correct_pos}")
                new_rocks.add(last_correct_pos)

        self._rocks = new_rocks

    def _export_lines(self):
        lines = [["." for _ in range(self._col_count)] for _ in range(self._line_count)]
        for c_idx, l_idx in self._obstacles:
            lines[l_idx][c_idx] = "#"
        for c_idx, l_idx in self._rocks:
            lines[l_idx][c_idx] = "O"
        return ["".join(l) for l in lines]

    @classmethod
    def _display_lines(self, lines):
        for l in lines:
            print(l)

    def _compute_load(self):
        total_load = 0
        for c_idx, l_idx in self._rocks:
            #print(f"Rock at line {l_idx} => load = {self._line_count + 1 - l_idx}")
            total_load += (self._line_count - l_idx)
        return total_load

    def compute_part1(self):
        self._tilt(Direction.NORTH)
        if self.verbose_level >= 2:
            print(f"[DEBUG] rock positions after tilt to north:")
            lines = self._export_lines()
            self._display_lines(lines)
        return self._compute_load()

    def compute_part2(self):
        current_cycle = 0
        rock_positions_at_cycle = [self._rocks]  # idx 0 = init ; idx 1 = after 1 cycle
        cycle_cycle_length = None
        while True:
            # one cycle
            self._tilt(Direction.NORTH)
            self._tilt(Direction.WEST)
            self._tilt(Direction.SOUTH)
            self._tilt(Direction.EAST)
            current_cycle += 1
            if self.verbose_level >= 1:
                print(f"[INFO] computed cycle {current_cycle:3}")
            if self.verbose_level >= 2:
                lines = self._export_lines()
                self._display_lines(lines)
            # check if rocks have same position as somewhere in the past => loop
            for prev_cycle_idx, prev_cycle_rocks in enumerate(rock_positions_at_cycle):
                if self._rocks == prev_cycle_rocks:
                    cycle_cycle_length = current_cycle - prev_cycle_idx
                    if self.verbose_level >= 1:
                        print(f"Found a loop : rocks at same position for cycle = {current_cycle} and cycle {prev_cycle_idx} => loop len = {cycle_cycle_length}")
                    break
            rock_positions_at_cycle.append(self._rocks)
            if cycle_cycle_length is not None:
                break
        remaining_iterations = (1000000000 - current_cycle) % cycle_cycle_length
        if self.verbose_level >= 1:
            print(f"Currently at cycle = {current_cycle} ; cycle cycle len = {cycle_cycle_length} => remaining_iterations = {remaining_iterations}")
        for _ in range(remaining_iterations):
            self._tilt(Direction.NORTH)
            self._tilt(Direction.WEST)
            self._tilt(Direction.SOUTH)
            self._tilt(Direction.EAST)
        if self.verbose_level >= 1:
            print(f"Finished remaining iterations")
        return self._compute_load()


def main(is_part1: bool, input_file: str, verbose_level=0):
    with open(input_file) as f:
        world = World(lines=f.read().splitlines(), verbose_level=verbose_level)
    if is_part1:
        return world.compute_part1()
    else:
        return world.compute_part2()


if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")

    # parse the arguments
    args = parser.parse_args()

    # run !
    res = None
    time_begin = time.time()
    try:
        res = main(args.part1, args.input, verbose_level=args.verbose)
    finally:
        time_end = time.time()
        print(f"Result = {res}")
        print(f"Computation time = {time_end - time_begin} s")
