import argparse
import time


TilePos = tuple[int, int]


class Maze:
    OFFSET_NORTH = (0, -1)
    OFFSET_SOUTH = (0, 1)
    OFFSET_WEST = (-1, 0)
    OFFSET_EAST = (1, 0)
    def __init__(self, all_lines: list[str], verbose_level: int) -> None:
        self.tiles: dict[tuple(int, int), str] = {}
        for line_idx, line in enumerate(all_lines):
            if not line:
                continue
            for col_idx, t in enumerate(line):
                self.tiles[(col_idx, line_idx)] = t
        self.col_count = len(all_lines[0])
        self.line_count = len(all_lines)
        self.verbose_level = verbose_level

    def _find_animal_pos_head(self):
        for (c_idx, l_idx), tile in self.tiles.items():
            if tile == "S":
                return (c_idx, l_idx)
        raise ValueError("Not found")

    def _are_tiles_connected(self, pos1, pos2):
        assert (pos2[0] - pos1[0], pos2[1] - pos1[0]) in \
            [(0, 1), (0, -1), (-1, 0), (1, 0)]
        tile1 = self.tiles[pos1[1]][pos1[0]]
        tile2 = self.tiles[pos2[1]][pos2[0]]
        raise NotImplementedError

    def _get_neighbors(self, pos: tuple):
        pos_c, pos_l = pos
        return [(pos_c+cc, pos_l+ll)
                for (cc, ll) in [self.OFFSET_NORTH, self.OFFSET_SOUTH, self.OFFSET_WEST, self.OFFSET_EAST]
                if 0<=pos_c+cc<self.col_count and 0<=pos_l+ll<self.line_count]

    def _is_possible_dest(self, pos_orig: tuple, tile: str, pos_dst: tuple):
        pos_c, pos_l = pos_orig
        if tile == "|":
            possible_offsets = [self.OFFSET_NORTH, self.OFFSET_SOUTH]
        elif tile == "-":
            possible_offsets = [self.OFFSET_WEST, self.OFFSET_EAST]
        elif tile == "L":
            possible_offsets = [self.OFFSET_NORTH, self.OFFSET_EAST]
        elif tile == "J":
            possible_offsets = [self.OFFSET_NORTH, self.OFFSET_WEST]
        elif tile == "7":
            possible_offsets = [self.OFFSET_SOUTH, self.OFFSET_WEST]
        elif tile == "F":
            possible_offsets = [self.OFFSET_SOUTH, self.OFFSET_EAST]
        elif tile == ".":
            possible_offsets = []
        elif tile == "S":
            possible_offsets = [self.OFFSET_NORTH, self.OFFSET_SOUTH, self.OFFSET_WEST, self.OFFSET_EAST]
        else:
            raise ValueError
        possible_dests = [(pos_c+cc, pos_l+ll)
                for (cc, ll) in possible_offsets
                if 0<=pos_c+cc<self.col_count and 0<=pos_l+ll<self.line_count]
        return pos_dst in possible_dests


    def _find_body_possible_next_tile(self, path: list):
        new_bodies = []
        last_pos = path[-1]
        tile_last_pos = self.tiles[last_pos]
        for next_tile in self._get_neighbors(last_pos):
            if next_tile in path:
                continue  # already included
            if not self._is_possible_dest(last_pos, tile_last_pos, next_tile):
                continue
            if not self._is_possible_dest(next_tile, self.tiles[next_tile], last_pos):
                continue
            new_bodies.append(path.copy() + [next_tile])
        if self.verbose_level >= 2:
            print(f"[DEBUG] path {path} : possible bodies = {new_bodies}")
        return new_bodies
        

    def find_animal_len_part1_incorrect(self):
            
        # find all positions from tail (=last tile)
        # for each of them :
        #   - find if tiles are connected (remove the one that are not)
        #   - check if tile already part of the animal : create new animal body
        # stop when no body has grown anymore
        
        pos_animal_head = self._find_animal_pos_head()
        current_paths = [[pos_animal_head]]
        current_path_len = 1
        while True:
            if self.verbose_level >= 1:
                print(f"[INFO] current_path_len = {current_path_len} / current_paths ({len(current_paths)}) = {current_paths}")
            next_paths = []
            for path in current_paths:
                next_paths.extend(self._find_body_possible_next_tile(path))
            # sanity check :
            for p in next_paths:
                assert len(p) == current_path_len + 1
            if not next_paths:
                if self.verbose_level >= 1:
                    print(f"[INFO] current_path_len = {current_path_len} / no next_paths => stopping")
                return current_path_len - 1
            current_path_len += 1
            current_paths = next_paths

    def _print_distances(self, tile_distances: dict):
        for line_idx in range(self.line_count):
            line_dist = ""
            for col_idx in range(self.col_count):
                d = tile_distances.get((col_idx, line_idx))
                if d is None:
                    line_dist += "."
                elif d > 9 :
                    line_dist += "X"
                else:
                    line_dist += str(d)
            print(line_dist)

    def find_furthest_tile_part1(self):
        pos_animal_head = self._find_animal_pos_head()
        tile_distances: dict[tuple[int, int], int] = {pos_animal_head: 0}
        if self.verbose_level >= 2:
            print(f"    Found pos {pos_animal_head} : distance = {0}")
        candidates = [pos_animal_head]
        max_dist = 0
        while candidates:
            new_candidates = []
            current_dist = max_dist + 1
            if self.verbose_level >= 2:
                print(f"starting loop (current_dist = {current_dist:2}) : candidates = {candidates}")
            for pos in candidates:
                tile_pos = self.tiles[pos]
                for next_pos in self._get_neighbors(pos):
                    if next_pos in new_candidates:
                        continue
                    if next_pos in tile_distances:
                        continue  # already found distqnce
                    if not self._is_possible_dest(pos, tile_pos, next_pos):
                        continue
                    if not self._is_possible_dest(next_pos, self.tiles[next_pos], pos):
                        continue
                    if self.verbose_level >= 2:
                        print(f"    Found pos {next_pos} : distance = {current_dist}")
                    new_candidates.append(next_pos)
                    tile_distances[next_pos] = current_dist
            candidates = new_candidates
            if self.verbose_level >= 2:
                print(f"ending   loop (current_dist = {current_dist:2}) : candidates = {candidates}")
            if candidates:
                max_dist = current_dist
        if self.verbose_level >= 1:
            self._print_distances(tile_distances)
        return max_dist

    def count_tiles_enclosed_main_loop(self):
        # TODO :
        # possible implementation :
        # insert intermediate lines/columns everywhere
        # => find the connected sets
        # => find the main loop (easy)
        # find tiles next to main loop but not connected to outside
        raise NotADirectoryError


def main(is_part1: bool, input_file: str, verbose_level=0):
    with open(input_file) as f:
        tiles = [line for line in f.read().splitlines() if line]
        maze = Maze(tiles, verbose_level)
        del tiles

    if is_part1:
        return maze.find_furthest_tile_part1()
    else:
        return maze.count_tiles_enclosed_main_loop()


if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")

    # parse the arguments
    args = parser.parse_args()

    # run !
    res = None
    time_begin = time.time()
    try:
        res = main(args.part1, args.input, verbose_level=args.verbose)
    finally:
        time_end = time.time()
        print(f"Result = {res}")
        print(f"Computation time = {time_end - time_begin} s")
