import argparse
import time


class MirrorPattern:
    def __init__(self, lines: list[str]) -> None:
        self.lines = lines

    def find_reflection(self, exclude_reflection: tuple):
        #print(f"Looking for reflection ... exclude_reflection = {exclude_reflection}")
        col_count = len(self.lines[0])
        line_count = len(self.lines)
        
        # is vertical reflection ?
        for mirror_idx in range(0, col_count - 1):
            if exclude_reflection[0] == mirror_idx:
                continue  # skipping original reflection, we want a different
            is_mirror = True
            for line in self.lines:
                mirr_len = min(mirror_idx + 1, col_count - mirror_idx - 1)
                line_part1 = line[:mirror_idx+1][-mirr_len:]
                line_part2 = line[mirror_idx+1:][:mirr_len]
                #print(f"[mirror_idx = {mirror_idx:2}] : comparing: {line_part1} / {line_part2}")
                if line_part1 != "".join(reversed(line_part2)):
                    is_mirror = False
                    break
            if is_mirror:
                #print(f"Found vertical mirror at index = {mirror_idx} => reflection = {mirror_idx+1}")
                return (mirror_idx, None)
            
        # is horizontal reflection ?
        for mirror_idx in range(0, line_count - 1):
            if exclude_reflection[1] == mirror_idx:
                continue  # skipping original reflection, we want a different
            is_mirror = True
            for line_idx in range(0, mirror_idx+1):
                line1 = self.lines[line_idx]
                other_line_idx = 2 * mirror_idx + 1 - line_idx
                if other_line_idx >= line_count:
                    continue  # out of area, skip comparing
                line2 = self.lines[other_line_idx]
                #print(f"[mirror_idx = {mirror_idx:2}] : comparing: {line1} / {line2}")
                if line1 != line2:
                    is_mirror = False
                    break
            if is_mirror:
                #print(f"Found horizontal mirror at index = {mirror_idx} => reflection = {100*(mirror_idx+1)}")
                return (None, mirror_idx)
            
        raise ValueError("No reflection found")

    def display(self):
        for l in self.lines:
            print(l)

class MirrorValley:
    def __init__(self, verbose_level: int) -> None:
        self.mirror_patterns: list[MirrorPattern] = [MirrorPattern([])]
        self.verbose_level = verbose_level

    def append_line(self, line: str):
        if not line:
            assert self.mirror_patterns
            assert self.mirror_patterns[-1].lines
            self.mirror_patterns.append(MirrorPattern([]))
            return
        self.mirror_patterns[-1].lines.append(line)

    def compute_part1(self):
        res = 0
        for mp in self.mirror_patterns:
            reflection = mp.find_reflection((None, None))
            if reflection[0] is not None:
                res += (reflection[0] + 1)
            else:
                res += 100 * (reflection[1] + 1)
        return res

    def _find_smudge_reflection(self, mp: MirrorPattern):
        orig_reflection = mp.find_reflection((None, None))
        col_count = len(mp.lines[0])
        line_count = len(mp.lines)
        for col_idx in range(col_count):
            for line_idx in range(line_count):
                if self.verbose_level >= 1:
                    print(f"Looking smudge reflection for                 {(col_idx, line_idx)}")
                new_mirror = MirrorPattern(mp.lines.copy())
                # alterate exactly one
                line = new_mirror.lines[line_idx]
                line = line[:col_idx] + ("." if line[col_idx] == "#" else "#") + line[col_idx+1:]
                new_mirror.lines[line_idx] = line
                if self.verbose_level >= 2:
                    new_mirror.display()
                try:
                    return new_mirror.find_reflection(orig_reflection)
                except ValueError:
                    continue
        raise ValueError("Not found smudge reflection")

    def compute_part2(self):
        res = 0
        for mirr_idx, mp in enumerate(self.mirror_patterns):
            if self.verbose_level >= 1:
                print(f"Looking smudge reflection for mirror {mirr_idx}")
            reflection = self._find_smudge_reflection(mp)
            if reflection[0] is not None:
                res += (reflection[0] + 1)
            else:
                res += 100 * (reflection[1] + 1)
        return res


def main(is_part1: bool, input_file: str, verbose_level=0):
    valley = MirrorValley(verbose_level=verbose_level)
    with open(input_file) as f:
        for line in f.read().splitlines():
            valley.append_line(line)

    if verbose_level >= 2:
        for mp in valley.mirror_patterns:
            mp.display()
            print()

    if is_part1:
        return valley.compute_part1()
    else:
        return valley.compute_part2()


if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")

    # parse the arguments
    args = parser.parse_args()

    # run !
    res = None
    time_begin = time.time()
    try:
        res = main(args.part1, args.input, verbose_level=args.verbose)
    finally:
        time_end = time.time()
        print(f"Result = {res}")
        print(f"Computation time = {time_end - time_begin} s")
