import argparse
import dataclasses
import time
import typing

@dataclasses.dataclass(slots=True)
class NumberPosition:
    num_str: int
    row: int
    col_start: int


NOT_SYMBOLS = [str(i) for i in (range(0, 10))] + ["."]


def _is_symbol(c: str):
    return c not in NOT_SYMBOLS


class World:
    def __init__(self, lines: list[str]) -> None:
        self.lines = lines
        self.col_count = len(self.lines[0])
        self.row_count = len(self.lines)
        self.numbers = self.find_numbers()
        self.indexed_numbers: dict[tuple[int, int]: NumberPosition] = {}

    def create_index_numbers(self):
        self.indexed_numbers = {(num.row, num.col_start): num for num in self.numbers}
        self.all_digits_to_number = {}  # map : position of each digit in world (row, col) to a number
        for num in self.numbers:
            for i in range(len(num.num_str)):
                self.all_digits_to_number[(num.col_start + i, num.row)] = num
    
    def find_numbers(self):
        numbers: list[NumberPosition] = []
        for row_idx, line in enumerate(self.lines):
            # line = line + "."  # "." is not a number, trick to avoid IndexError at EOL
            current_num: typing[NumberPosition] = None
            for col_idx, c in enumerate(line):
                if c.isdigit():
                    if current_num:
                        current_num.num_str += c
                    else:
                        current_num = NumberPosition(num_str=c, row=row_idx, col_start=col_idx)
                        numbers.append(current_num)
                else:
                    current_num = None
        return numbers

    def is_part_number(self, num: NumberPosition):
        num_len = len(num.num_str)
        adjacent_pos = [(num.col_start - 1, num.row), (num.col_start + num_len, num.row)]
        adjacent_pos += [(x, num.row - 1) for x in range(num.col_start - 1, num.col_start + num_len + 1)]
        adjacent_pos += [(x, num.row + 1) for x in range(num.col_start - 1, num.col_start + num_len + 1)]
        # print(f"[DEBUG] num = {num.num_str} / adjacent_pos = {adjacent_pos}")
        return any((0 <= c < self.col_count and 0 <= r < self.row_count and _is_symbol(self.lines[r][c])) for c, r in adjacent_pos)
    
    def get_gear_ratio(self, col_idx, row_idx):
        adjacent_numbers = {}
        pos = [(c, r)
               for c in range(col_idx -1, col_idx + 2)
               for r in range(row_idx -1, row_idx + 2)
               if 0 <= c < self.col_count and 0 <= r < self.row_count and (c, r) != (col_idx, row_idx)]
        for c, r in pos:
            if self.lines[r][c].isdigit():
                num = self.all_digits_to_number[(c, r)]
                adjacent_numbers[(num.col_start, num.row)] = num
        if len(adjacent_numbers) < 2:
            return 0
        res = 1
        for num in adjacent_numbers.values():
            res *= int(num.num_str)
        return res

def main(is_part1: bool, input_file: str, verbose_level=0):
    with open(input_file) as f:
        world = World([l for l in f.read().split("\n") if l])
    if not is_part1:
        world.create_index_numbers()
        sum_gear_ratio = 0
        for row_idx, line in enumerate(world.lines):
            for col_idx, c in enumerate(line):
                if c != "*":
                    continue
                sum_gear_ratio += world.get_gear_ratio(col_idx, row_idx)
        return sum_gear_ratio

    if verbose_level >= 2:
        for num in world.numbers:
            print(f"[DEBUG] Found '{num.num_str:3}' at row {num.row:2} / col {num.col_start}")

    sum_parts = 0
    for num in world.numbers:
        if world.is_part_number(num):
            if verbose_level >= 1:
                print(f"Found Part Number '{num.num_str:3}' at row {num.row:2} / col {num.col_start}")
            sum_parts += int(num.num_str)
    
    return sum_parts

if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")

    # parse the arguments
    args = parser.parse_args()

    # run !
    res = None
    time_begin = time.time()
    try:
        res = main(args.part1, args.input, verbose_level=args.verbose)
    finally:
        time_end = time.time()
        print(f"Result = {res}")
        print(f"Computation time = {time_end - time_begin} s")
