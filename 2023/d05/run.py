import argparse
import dataclasses
import time


@dataclasses.dataclass(slots=True)
class ConverterRange:
    dst_start: int
    src_start: int
    len: int

@dataclasses.dataclass(slots=True)
class DataRange:
    start: int
    len: int
    

class Almanac:
    def __init__(self, verbose_level: int):
        self.seeds: list[int] = []
        self.verbose_level = verbose_level
        self.all_mappings: dict[str, list[ConverterRange]] = {}

    def populate_from_line(self, context: str, line: str):
        if line.startswith("seeds:"):
            assert not context
            self.seeds = list(map(int, line.split(":")[1].strip().split()))
            return
        map_type, token = context.split()
        assert token == "map:"
        #key_type, value_type = map_type.split("-to-")
        mapping = self.all_mappings.get(map_type)
        if not mapping:
            mapping = []
            self.all_mappings[map_type] = mapping
        dst_range, src_range, range_len = map(int, line.split())
        ####oh my god, don't do this, memory will die !!
        #for i in range(range_len):
        #    mapping[src_range + i] = dst_range + i
        mapping.append(ConverterRange(dst_start=dst_range, src_start=src_range, len=range_len))

    def get_new_ranges(self, map_type: str, data_range: DataRange):
        
        mapping = self.all_mappings[map_type]
        pivot_values = []
        for conv_range in mapping:
            # we cut on both range start + end, no difference, so further algo are easier
            pivot_values.append(conv_range.src_start)
            pivot_values.append(conv_range.src_start + conv_range.len)

        if self.verbose_level >= 2:
            print(f"Splitting data_range(start={data_range.start}, len={data_range.len}) ...")
            
        remaining_range = data_range
        split_ranges = []
        for p in pivot_values:
            if remaining_range.start < p < remaining_range.start + remaining_range.len:
                split_ranges.append(DataRange(start=remaining_range.start, len=p - remaining_range.start))
                remaining_range = DataRange(start=p, len=remaining_range.len - p + remaining_range.start)
        # don't forget uncut range
        split_ranges.append(remaining_range)
        if self.verbose_level >= 2:
            print(f"    split ranges = " + "/".join(f"(start={r.start}, len={r.len})" for r in split_ranges))

        new_ranges = []
        for it_range in split_ranges:
            for conv_range in mapping:
                if conv_range.src_start <= it_range.start < conv_range.src_start + conv_range.len:
                    # no need to check it_range.len as we have split with pivots previously
                    new_ranges.append(DataRange(start=conv_range.dst_start - conv_range.src_start + it_range.start, len=it_range.len))
                    it_range = None
                    break
            # remaining range is kept unchanged
            if it_range is not None:
                new_ranges.append(it_range)
        
        if self.verbose_level >= 2:
            print(f"    converted ranges = " + "/".join(f"(start={r.start}, len={r.len})" for r in new_ranges))

        return new_ranges

    def sort_mappings(self):
        self.all_mappings = {k: sorted(v, key=lambda x: x.src_start) for k, v in self.all_mappings.items()}

ALL_DST_TYPES = ["soil", "fertilizer", "water", "light", "temperature", "humidity", "location"]

def process(almanac: Almanac, current_ranges: list[DataRange]):

    almanac.sort_mappings()

    current_type = "seed"
    
    for dst_type in ALL_DST_TYPES:
        new_ranges = []
        map_type = f"{current_type}-to-{dst_type}"
        for it_range in current_ranges:
            new_ranges += almanac.get_new_ranges(map_type, it_range)
        if almanac.verbose_level >= 1:
            print(f"Finished converting {current_type} to {dst_type}")
            print(f"        ", end="")
            for it_range in current_ranges:
                print(f"({it_range.start}, {it_range.len})", end="")
            print(f" ->", end="")
            for it_range in new_ranges:
                print(f"({it_range.start}, {it_range.len})", end="")
            print()
        current_ranges = new_ranges
        current_type = dst_type

    assert current_type == "location"
    res = min([x.start for x in new_ranges])
    if almanac.verbose_level >= 1:
        print(f"Final data ({current_type}) : {new_ranges} -> result = {res}")
    return res

def main(is_part1: bool, input_file: str, verbose_level=0):
    almanac = Almanac(verbose_level=verbose_level)
    
    with open(input_file) as f:
        context = ""
        for line in f:
            line = line.strip()
            if not line:
                continue
            if line.endswith("map:"):
                context = line
                continue
            almanac.populate_from_line(context, line)
            continue

    if is_part1:
        # part 1 is particular case of part 2 with all ranges of len = 1
        current_ranges: list[DataRange] = [DataRange(start=x, len=1) for x in almanac.seeds]
    else:
        current_ranges: list[DataRange] = []
        for i in range(0, len(almanac.seeds), 2):
            current_ranges.append(DataRange(start=almanac.seeds[i], len=almanac.seeds[i+1]))
    return process(almanac, current_ranges)


if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")

    # parse the arguments
    args = parser.parse_args()

    # run !
    res = None
    time_begin = time.time()
    try:
        res = main(args.part1, args.input, verbose_level=args.verbose)
    finally:
        time_end = time.time()
        print(f"Result = {res}")
        print(f"Computation time = {time_end - time_begin} s")
