import argparse
import dataclasses
import time


@dataclasses.dataclass
class Hailstone:
    x: int
    y: int
    z: int
    vx: int
    vy: int
    vz: int

    @classmethod
    def from_line(cls, line: str):
        positions, velocity = line.split("@")
        x, y, z = map(int, positions.split(","))
        vx, vy, vz = map(int, velocity.split(","))
        return Hailstone(x,y,z,vx,vy,vz)

    def __str__(self) -> str:
        return ", ".join(map(str, [self.x, self.y, self.z])) + " @ " + ", ".join(map(str, [self.vx, self.vy, self.vz]))


class World:
    def __init__(self, lines: list[str], verbose_level: int) -> None:
        self.hailstones = [Hailstone.from_line(l) for l in lines]
        self.verbose_level = verbose_level

    def _find_crossing1(self, a: Hailstone, b: Hailstone):

        print()
        print(f"Hailstone A: {a}")
        print(f"Hailstone B: {b}")
        
        assert a.vz == b.vz  # assert for part1, TODO part 2
        assert a.z == b.z  # assert for part1, TODO part 2

        # xa(ta) = a.x + a.vx * ta
        # ya(ta) = a.x + a.vx * ta
        # za(ta) = a.z + a.vz * ta
        # xb(tb) = b.x + b.vx * tb
        # yb(tb) = b.x + b.vx * tb
        # zb(tb) = b.z + b.vz * tb
        #
        # question : find ta/tb for xa(ta) = xb(tb) and ya(ta) = yb(tb)
        #
        # ta = (b.x - a.x + b.vx * tb) / a.vx
        # tb = ((b.y - a.y) - a.vy * (b.x - a.x) / a.vx) / (b.vy - a.vy * (b.vx - a.vx))
        if a.vx * b.x == b.vx + a.x and a.x != b.x:
            return None  # parallel + no intersection
        if a.vy * b.y == b.vy * a.y  and a.y != b.y:
            return None  # parallel + no intersection
        if a.vz * b.z == b.vz * a.z and a.z != b.z:
            return None  # parallel + no intersection
        
        if b.vx - b.vx == a.vy - b.vy:
            raise NotImplementedError
        
        tb = ((b.y - a.y) - a.vy * (b.x - a.x) / a.vx) / (b.vy - a.vy * (b.vx - a.vx))
        ta = (b.x - a.x + b.vx * tb) / a.vx

        # TODO : check za(ta) == zb(tb)
        xa_ta = a.x + a.vx * ta
        ya_ta = a.y + a.vy * ta
        za_ta = a.z + a.vz * ta
        return xa_ta, ya_ta, za_ta

    def _find_crossing(self, a: Hailstone, b: Hailstone):

        print()
        print(f"Hailstone A: {a}")
        print(f"Hailstone B: {b}")
        
        assert a.vz == b.vz  # assert for part1, TODO part 2
        assert a.z == b.z  # assert for part1, TODO part 2

        coeff_a =  b.vx  # coeff to apply to B to get a common X speed
        coeff_b = a.vx  # coeff to apply to B to get a common X speed
        # we could use a single coeff : a.vx / b.vx but dealing with floats is a pain
        if coeff_a * a.vx == coeff_b.b.vx and coeff_a * a.vy == coeff_b * b.vy and coeff_a * a.vz == coeff_b:

    def compute_part1(self, x_limits, y_limits):

        # ignore Z
        for h in self.hailstones:
            h.z = 0
            h.vz = 0

        res_crossing = 0

        for idx_a, a in enumerate(self.hailstones):
            for b in self.hailstones[idx_a+1:]:

                crossing = self._find_crossing(a, b)
                print(f"Cross A/B : {crossing}")
                if crossing is None:
                    continue
                if not (x_limits[0] <= crossing[0] <= x_limits[1]):
                    continue
                if not (y_limits[0] <= crossing[1] <= y_limits[1]):
                    continue
                res_crossing += 1
        return res_crossing



def main(is_part1: bool, input_file: str, verbose_level=0):
    with open(input_file) as f:
        world = World(f.read().splitlines(), verbose_level)
    
    if is_part1:
        if input_file.endswith("input-example.txt"):
            x_limits = y_limits = (7, 27)
        else:
            x_limits = y_limits = (200000000000000, 400000000000000)
        return world.compute_part1(x_limits, y_limits)
    else:
        raise NotImplementedError


if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")

    # parse the arguments
    args = parser.parse_args()

    # run !
    res = None
    time_begin = time.time()
    try:
        res = main(args.part1, args.input, verbose_level=args.verbose)
    finally:
        time_end = time.time()
        print(f"Result = {res}")
        print(f"Computation time = {time_end - time_begin} s")
