import argparse
import time


class HistoryValue:
    def __init__(self, values: list[int]) -> None:
        self.values = values

    def _print_step(self, step_idx, step):
        indent = " " * step_idx
        print(indent + " ".join(str(x) for x in step))

    def _find_steps(self):
        all_steps = [self.values.copy()]
        #self._print_step(len(all_steps) - 1, all_steps[-1])
        # iterate to find all zeroes
        while True:
            previous_step = all_steps[-1]
            next_step = []
            for i in range(len(previous_step) - 1):
                next_step.append(previous_step[i+1] - previous_step[i])
            all_steps.append(next_step)
            #self._print_step(len(all_steps) - 1, all_steps[-1])
            if all([x == 0 for x in next_step]):
                break
        return all_steps

    def find_next_value(self):
        all_steps = self._find_steps()
        # complete each line
        all_steps[-1].append(0)
        for step_idx in range(len(all_steps) - 2, -1, -1):
            step = all_steps[step_idx]
            step.append(step[-1] + all_steps[step_idx + 1][-1])
        #for step_idx, step in reversed(list(enumerate(all_steps))):
        #    self._print_step(step_idx, step)
        return all_steps[0][-1]

    def find_backward_value(self):
        all_steps = self._find_steps()
        # complete each line
        all_steps[-1].insert(0, 0)
        for step_idx in range(len(all_steps) - 2, -1, -1):
            step = all_steps[step_idx]
            step.insert(0, step[0] - all_steps[step_idx + 1][0])
            
        #for step_idx, step in reversed(list(enumerate(all_steps))):
        #    self._print_step(step_idx, step)
        return all_steps[0][0]

def process_part1(list_history_values: list[HistoryValue]):
    res = 0
    for hv in list_history_values:
        res += hv.find_next_value()
    return res

def process_part2(list_history_values: list[HistoryValue]):
    res = 0
    for hv in list_history_values:
        res += hv.find_backward_value()
    return res
    

def main(is_part1: bool, input_file: str, verbose_level=0):
    list_history_values: list[HistoryValue] = []
    with open(input_file) as f:
        for line in f.readlines():
            if not line:
                continue
            list_history_values.append(HistoryValue(values=[int(x) for x in line.split()]))
    if is_part1:
        return process_part1(list_history_values)
    else:
        return process_part2(list_history_values)


if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")

    # parse the arguments
    args = parser.parse_args()

    # run !
    res = None
    time_begin = time.time()
    try:
        res = main(args.part1, args.input, verbose_level=args.verbose)
    finally:
        time_end = time.time()
        print(f"Result = {res}")
        print(f"Computation time = {time_end - time_begin} s")
