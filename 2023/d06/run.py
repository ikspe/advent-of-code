import argparse
import math
import time


# debug function
def find_distance(hold_time: int, total_duration: int):
    # units : millisec / millimeter / millimeter per millisec
    #assert 0 <= hold_time <= total_duration
    # speed = hold_time
    # move_duration = max(0, total_duration - hold_time)
    # distance = speed * move_duration

    # simplification 
    # move_duration = total_duration - hold_time
    return hold_time * total_duration - hold_time * hold_time

# debug function
def compare_distance_with_record(hold_time: int, total_duration: int, record: int):
    distance = find_distance(hold_time, total_duration)
    return distance - record


def process_races(list_durations: list[int], list_distances: list[int], verbose_level: int):
    res = 1
    for duration, record in zip(list_durations, list_distances):
        # we need to find x for which :
        # find_distance(x, duration) > record
        # ==>   (x * duration - x ** 2 > record)
        # ===> x ** 2 - x * duration + record < 0
        # a=1 / b = -duration / c = record
        # delta = b**2 - 4 * a *c
        # bounds = - (b +/- sqrt(delta)) / (2 * a)
        delta_sqrt = math.sqrt(duration ** 2 - 4 * record)
        lower_bound = (duration - delta_sqrt) / 2
        upper_bound = (duration + delta_sqrt) / 2
        if lower_bound == math.floor(lower_bound):
            solution_count = int(upper_bound - lower_bound - 1)  # we exclude the 2 exact solutions => not beating the record
        else:
            solution_count = math.floor(upper_bound) - math.ceil(lower_bound) + 1
        if verbose_level >= 1:
            print(f"duration {duration:4} / record {record:4} => bounds {lower_bound} / {upper_bound}   (delta_sqrt = {delta_sqrt}) => solution_count = {solution_count}")
        if verbose_level >= 2:
            for x in range(math.ceil(lower_bound), math.floor(upper_bound) + 1):
                d = find_distance(x, duration)
                print(f"duration {duration} / record {record} => x = {x:4}  =>  {d:4}    (record : {record:4})  => comparison = {compare_distance_with_record(x, duration, record):4}")
        res *= solution_count
    return res


def main(is_part1: bool, input_file: str, verbose_level=0):
    with open(input_file) as f:
        lines = f.read().splitlines()
        if is_part1:
            list_durations = list(map(int, lines[0].split(":")[1].strip().split()))
            list_distances = list(map(int, lines[1].split(":")[1].strip().split()))
        else:
            list_durations = [int(lines[0].split(":")[1].strip().replace(" ", ""))]
            list_distances = [int(lines[1].split(":")[1].strip().replace(" ", ""))]

    if verbose_level >= 1:
        print(f"[DEBUG] {list_durations = }")
        print(f"[DEBUG] {list_distances = }")
    return process_races(list_durations, list_distances, verbose_level)


if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")

    # parse the arguments
    args = parser.parse_args()

    # run !
    res = None
    time_begin = time.time()
    try:
        res = main(args.part1, args.input, verbose_level=args.verbose)
    finally:
        time_end = time.time()
        print(f"Result = {res}")
        print(f"Computation time = {time_end - time_begin} s")
