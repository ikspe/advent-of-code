import argparse
import time


class SpringLine:
    def __init__(self, line: str, part1: bool) -> None:
        self.springs, str_groups = line.split()
        self.groups_desc: list[int] = list(map(int, str_groups. split(",")))
        if not part1:
            self.springs = "?".join([self.springs] * 5)
            self.groups_desc = sum([self.groups_desc] * 5, start=[])
        self._orig_line = line

    def __str__(self) -> str:
        return self._orig_line

    @classmethod
    def extract_damaged_groups_before_unknown(cls, springs: str):
        """
        springs : operational = .
                  damaged = #
                  unknown = ?
        """
        first_known_springs = springs.split("?")[0]
        return [len(t) for t in first_known_springs.split(".") if t]

class SpringMap:
    def __init__(self, verbose_level: int) -> None:
        self.lines: list[SpringLine] = []
        self.verbose_level = verbose_level

    def compute_part1(self):
        res = 0
        for line in self.lines:
            arrangements = self._count_spring_arrangements(line)
            if self.verbose_level >= 1:
                print(f"{str(line):30} => arrangements count = {arrangements:4}")
            res += arrangements
        return res

    def _count_spring_arrangements(self, spring_line: SpringLine):
        candidates = [spring_line.springs]
        while True:
            first_candidate = candidates[0]
            if len([x for x in first_candidate if x == '?']) == 0:
                break
            new_candidates: list[str] = []
            for c in candidates:
                new_candidates.append(c.replace("?", ".", 1))
                new_candidates.append(c.replace("?", "#", 1))
            is_last_replace = "?" not in new_candidates[0]
            if self.verbose_level >= 3:
                print(f"[DEBUG] ({spring_line})     is_last_replace = {str(is_last_replace):5} / new_candidates = {new_candidates}")
            # prune inconsistent candidates
            candidates = []
            for c in new_candidates:
                first_groups = SpringLine.extract_damaged_groups_before_unknown(c)
                if self.verbose_level >= 2:
                    print(f"[DEBUG] ({spring_line})         candidate {c:20} => first_groups = {first_groups}")
                if sum(first_groups) > sum(spring_line.groups_desc):
                    continue
                if is_last_replace:
                    if first_groups != spring_line.groups_desc:
                        continue
                else:
                    if len(first_groups) > len(spring_line.groups_desc):
                        continue
                    # all values should match, eventually the last one may differ but should be smaller or equal
                    are_same_groups = True
                    for idx in range(len(first_groups) - 1):
                        if first_groups[idx] != spring_line.groups_desc[idx]:
                            are_same_groups = False
                            break
                    if not are_same_groups:
                        continue
                    last_idx = len(first_groups) - 1
                    if last_idx >= 0 and first_groups[last_idx] > spring_line.groups_desc[last_idx]:
                        continue
                candidates.append(c)
            if self.verbose_level >= 3:
                print(f"[DEBUG] ({spring_line})     candidates = {candidates}")
            #raise NotImplementedError # TODO
        return len(candidates)


def main(is_part1: bool, input_file: str, verbose_level=0):
    spring_map = SpringMap(verbose_level)
    
    with open(input_file) as f:
        for line in f.read().splitlines():
            if not line:
                continue
            spring_map.lines.append(SpringLine(line, is_part1))
    if verbose_level >= 1:
        for s in spring_map.lines:
            wildcard_count = len([x for x in s.springs if x == '?'])
            print(f"[DEBUG] : {len(s.springs):2} ; number of '?' : {wildcard_count:2} => number of combinaisons = {2 ** wildcard_count :6}")
    return spring_map.compute_part1()


if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")

    # parse the arguments
    args = parser.parse_args()

    # run !
    res = None
    time_begin = time.time()
    try:
        res = main(args.part1, args.input, verbose_level=args.verbose)
    finally:
        time_end = time.time()
        print(f"Result = {res}")
        print(f"Computation time = {time_end - time_begin} s")
