import argparse
import time


def hash_1a(input_str: str) -> int:
    val = 0
    for c in input_str:
        val = ((val + ord(c)) * 17) % 256
    return val


def compute_part1(full_input: str) -> int:
    return sum(hash_1a(t) for t in full_input.replace("\n", "").split(","))


def _apply_box_operation(boxes: list[dict], label, operator, quantity):
    box_idx = hash_1a(label)
    box = boxes[box_idx]
    if operator == "-":
        box.pop(label, None)
    elif operator == "=":
        box[label] = quantity

def _print_boxes(boxes: list[dict]):
    for box_idx, box in enumerate(boxes):
        if not box:
            continue
        print(f"Box {box_idx:3}: " + " ".join(f"[{k}: {v}]" for k, v in box.items()))

def _compute_focusing_power(boxes: list[dict]):
    res = 0
    for box_idx, box in enumerate(boxes):
        for slot_idx, (k, v) in enumerate(box.items()):
            res += (box_idx + 1) * (slot_idx + 1) * int(v)
    return res

def compute_part2(full_input: str) -> int:
    boxes = [{} for _ in range(256)]
    for it_str in full_input.replace("\n", "").split(","):
        if "=" in it_str:
            operator = "="
            label, quantity = it_str.split("=")
        elif "-" in it_str:
            operator = "-"
            label, quantity = it_str.split("-")
        else:
            raise ValueError
        #print(f"[DEBUG] {it_str:20} => tokens = {label:6} / operator {operator}  / quantity {quantity:4}    => box idx = {hash_1a(label)}")
        _apply_box_operation(boxes, label, operator, quantity)

        # debug print
        #print(f"After \"{it_str}\":")
        #_print_boxes(boxes)
        #print()
    return _compute_focusing_power(boxes)


def main(is_part1: bool, input_file: str, verbose_level=0):
    with open(input_file) as f:
        full_input = f.read()
    if is_part1:
        return compute_part1(full_input)
    else:
        return compute_part2(full_input)


if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")

    # parse the arguments
    args = parser.parse_args()

    # run !
    res = None
    time_begin = time.time()
    try:
        res = main(args.part1, args.input, verbose_level=args.verbose)
    finally:
        time_end = time.time()
        print(f"Result = {res}")
        print(f"Computation time = {time_end - time_begin} s")
