import argparse
import time


DICT_HUMAN_DIGITS = {x: idx + 1 for idx, x in enumerate(["one", "two", "three", "four", "five", "six", "seven", "eight", "nine"])}


def find_digit_in_line(line: str, from_start: bool, possible_digits: dict):
    while True:
        if not line:
            raise ValueError(f"empty line")
        for digit_str, digit_val in possible_digits.items():
            if from_start and line.startswith(digit_str):
                return str(digit_val)
            if not from_start and line.endswith(digit_str):
                return str(digit_val)
        if from_start:
            line = line[1:]
        else:
            line = line[:-1]


def main(is_part1: bool, input_file: str, verbose_level=0):
    possible_digits_dict = {str(x): x for x in range(0, 10)}
    if not is_part1:
        possible_digits_dict.update(DICT_HUMAN_DIGITS)
    with open(input_file) as f:
        calibration_values = []
        for line in f:
            line = line.strip()
            first_digit = find_digit_in_line(line, True, possible_digits=possible_digits_dict)
            last_digit = find_digit_in_line(line, False, possible_digits=possible_digits_dict)
            this_cal_val = int(first_digit + last_digit)
            if verbose_level:
                print(f"[DEBUG] found calibration value : {this_cal_val}  (line = {line})")
            calibration_values.append(this_cal_val)
    sum_cal_val = sum(calibration_values)
    if verbose_level:
        print(f"[DEBUG] sum : {sum_cal_val}")
    return sum_cal_val


if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")

    # parse the arguments
    args = parser.parse_args()

    # run !
    res = None
    time_begin = time.time()
    try:
        res = main(args.part1, args.input, verbose_level=args.verbose)
    finally:
        time_end = time.time()
        print(f"Result = {res}")
        print(f"Computation time = {time_end - time_begin} s")
