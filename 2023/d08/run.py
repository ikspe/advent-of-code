import argparse
import time


class World:
    def __init__(self, instructions: str, element_map: dict, verbose_level: int) -> None:
        self.instructions = instructions
        self.element_map = element_map
        self.verbose_level = verbose_level

    def process_part_1(self):
        if self.verbose_level:
            print(f"process_part_1() : instructions = {self.instructions}")
            for pos, (left, right) in self.element_map.items():
                print(f"            '{pos}' = ({left}, {right})")
        pos = "AAA"
        return self._find_iteration_count(pos, lambda p: p == "ZZZ")

    def _find_iteration_count(self, pos, condition: callable):
        iter_num = 0
        while True:
            
            for it_instr in self.instructions:
                if condition(pos):
                    return iter_num
                dst_l, dst_r = self.element_map[pos]
                pos = (dst_l if it_instr == "L" else dst_r)
                iter_num += 1

    def process_part_2(self):
        import math  # least common integer multiple
        all_positions = [p for p in self.element_map if p[-1] == "A"]
        if self.verbose_level:
            print(f"process_part_2() : all_positions = {all_positions}")
        all_positions_num_iters = []
        for pos in all_positions:
            all_positions_num_iters.append(self._find_iteration_count(pos, lambda p: p[-1] == "Z"))
        if self.verbose_level:
            print(f"process_part_2() : all_positions_num_iters = {all_positions_num_iters}")
        return math.lcm(*all_positions_num_iters)


def main(is_part1: bool, input_file: str, verbose_level=0):
    with open(input_file) as f:
        all_lines = f.read().splitlines()
        instructions = all_lines[0]
        element_map = {}
        for line in all_lines[2:]:
            if not line:
                continue
            lhs, rhs = line.split("=")
            rhs_tokens = rhs.strip().replace("(", "").replace(")", "")
            element_map[lhs.strip()] = [x.strip() for x in rhs_tokens.split(",")]

    world = World(instructions, element_map, verbose_level)

    if is_part1:
        return world.process_part_1()
    else:
        return world.process_part_2()


if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")

    # parse the arguments
    args = parser.parse_args()

    # run !
    res = None
    time_begin = time.time()
    try:
        res = main(args.part1, args.input, verbose_level=args.verbose)
    finally:
        time_end = time.time()
        print(f"Result = {res}")
        print(f"Computation time = {time_end - time_begin} s")
