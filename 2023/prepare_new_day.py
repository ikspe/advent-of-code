#!/usr/bin/env python3

import argparse
import os
import shutil
import requests
import yaml


class Config:
    CONFIG_FILE_NAME = "config.yml"

    def __init__(self, config_file) -> None:

        with open(config_file) as f:
            config = yaml.safe_load(f)

        self.this_year_folder = str(config["current_year_folder"])
        self.input_file_root_dir = os.path.expanduser(config["input_file_root_dir"])
        self.input_file_basename = config["input_file"]
        self.input_file_example_basename = config["input_file_example"]

    @classmethod
    def get_config(cls):
        return Config(cls.CONFIG_FILE_NAME)


def main():

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("day", metavar="dXX")  # required

    # parse the arguments
    args = parser.parse_args()

    config = Config.get_config()
    dest_dir: str = args.day
    if not dest_dir.startswith("d") or len(dest_dir) != 3:
        raise ValueError(f"format of desination dir must be dXX : invalid {dest_dir}")
    if not dest_dir[1:].isdecimal():
        raise ValueError(f"format of desination dir must be dXX : invalid {dest_dir}")

    print(f"Instatiating day directory {dest_dir} from template...")
    shutil.copytree("template", dest_dir)
    print("... done")

    print(f"Fetching input...")
    input_dir_this_year = os.path.join(config.input_file_root_dir, config.this_year_folder)
    input_dir = os.path.join(input_dir_this_year, dest_dir)
    input_file = os.path.join(input_dir, config.input_file_basename)
    day_numeric = int(dest_dir[1:])
    with open(os.path.join(input_dir_this_year, "web-cookie.txt")) as f:
        web_cookie = f.read().strip()
    url = f"https://adventofcode.com/{config.this_year_folder}/day/{day_numeric}/input"
    print(f"... downloading url {url} to {input_file} ...")
    os.makedirs(input_dir, exist_ok=True)
    r = requests.get(url, headers={"Cookie": web_cookie})
    print(f"... request finished, status = {r.status_code}")
    r.raise_for_status()
    with open(input_file, "w") as f:
        f.write(r.text)
    print("... done")


if __name__ == "__main__":
    main()
