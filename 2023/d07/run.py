import argparse
import enum
import time


CARDS_STRENGHTS = {x.strip(): idx for idx, x in enumerate(reversed("A, K, Q, J, T, 9, 8, 7, 6, 5, 4, 3, 2".split(",")))}

class HandType(enum.IntEnum):
    # values are arbitrary, but ordered
    FIVE_OF_A_KIND = 10
    FOUR_OF_A_KIND = 9
    FULL_HOUSE = 8
    THREE_OF_A_KIND = 7
    TWO_PAIRS = 6
    ONE_PAIR = 5
    HIGH_CARD = 4


class PlayerHand:
    def __init__(self, cards: str, bid: int) -> None:
        self.cards = cards
        self.bid = bid
        self.cards_values = [CARDS_STRENGHTS[c] for c in self.cards]

    def __str__(self) -> str:
        return f"PlayerHand({self.cards} , bid = {self.bid})"

    @classmethod
    def _compute_group_cards(cls, cards: str) -> dict[str, int]:
        grouped_cards = {}
        for c in cards:
            if c not in grouped_cards:
                grouped_cards[c] = 1
            else:
                grouped_cards[c] += 1
        return grouped_cards

    def compute_order_part1(self):
        #print(f"{self}.compute_order_part1()")
        grouped_cards = self._compute_group_cards(self.cards)
        #print(f"{self}.compute_order_part1() : grouped_cards = {grouped_cards}")
        self.hand_type = self._compute_hand_type(grouped_cards)

    def compute_order_part2(self):
        # DON'T : try various combination, it's wasting time...
        # better do static analysis :
        # 5 J => FIVE_OF_A_KIND
        # 4 J => FIVE_OF_A_KIND
        # 3 J => try to complete :
        #   if 1*2 identical => FIVE_OF_A_KIND
        #   else : FOUR_OF_A_KIND
        # 2 J => try to complete :
        #   if 1*3 identical => FIVE_OF_A_KIND
        #   if 1*2 identical => FOUR_OF_A_KIND
        #   else: THREE_OF_A_KIND
        # 1 J : try to complete :
        #   if 1*4 identical => FIVE_OF_A_KIND
        #   if 1*3 identical => FOUR_OF_A_KIND
        #   if 2*2 identical => FULL_HOUSE
        #   if 1*2 identical => THREE_OF_A_KIND
        #   else => ONE_PAIR
        # else : normal computation
        grouped_cards = self._compute_group_cards(self.cards)
        jokers_count = grouped_cards.get("J", 0)

        # get hand_type ignoring jokers
        grouped_cards["J"] = 0
        hand_type_without_jokers = self._compute_hand_type(grouped_cards)
        if jokers_count == 5 or jokers_count == 4:
            self.hand_type = HandType.FIVE_OF_A_KIND
        elif jokers_count == 3:
            if hand_type_without_jokers == HandType.ONE_PAIR:
                self.hand_type = HandType.FIVE_OF_A_KIND
            else:
                self.hand_type = HandType.FOUR_OF_A_KIND
        elif jokers_count == 2:
            if hand_type_without_jokers == HandType.THREE_OF_A_KIND:
                self.hand_type = HandType.FIVE_OF_A_KIND
            elif hand_type_without_jokers == HandType.ONE_PAIR:
                self.hand_type = HandType.FOUR_OF_A_KIND
            else:
                self.hand_type = HandType.THREE_OF_A_KIND
        elif jokers_count == 1:
            if hand_type_without_jokers == HandType.FOUR_OF_A_KIND:
                self.hand_type = HandType.FIVE_OF_A_KIND
            elif hand_type_without_jokers == HandType.THREE_OF_A_KIND:
                self.hand_type = HandType.FOUR_OF_A_KIND
            elif hand_type_without_jokers == HandType.TWO_PAIRS:
                self.hand_type = HandType.FULL_HOUSE
            elif hand_type_without_jokers == HandType.ONE_PAIR:
                self.hand_type = HandType.THREE_OF_A_KIND
            else:
                self.hand_type = HandType.ONE_PAIR
        else:
            self.hand_type = hand_type_without_jokers
        
    @classmethod
    def _compute_hand_type(cls, grouped_cards: dict) -> HandType:
        max_of_one_kind = max(grouped_cards.values())
        if max_of_one_kind == 5:
            return HandType.FIVE_OF_A_KIND
        elif max_of_one_kind == 4:
            return HandType.FOUR_OF_A_KIND
        elif max_of_one_kind == 3:
            if any(n == 2 for n in grouped_cards.values()):
                return HandType.FULL_HOUSE
            else:
                return HandType.THREE_OF_A_KIND
        elif max_of_one_kind == 2:
            if len([n for n in grouped_cards.values() if n == 2]) == 2:
                return HandType.TWO_PAIRS
            else:
                return HandType.ONE_PAIR
        else:
            return HandType.HIGH_CARD

    @property
    def key(self):
        return self.hand_type.value, self.cards_values

    @classmethod
    def from_line(cls, line: str):
        cards, bid = line.strip().split()
        player = PlayerHand(cards, int(bid))
        return player

def process(all_players: list[PlayerHand], is_part1:bool, verbose_level: int):
    if is_part1:
        for p in all_players:
            p.compute_order_part1()
    else:
        for p in all_players:
            p.compute_order_part2()
    ordered_hands = sorted(all_players, key=lambda p: p.key)
    if verbose_level >= 1:
        print(f"Finished ordering :")
        for p in ordered_hands:
            print(f"{p} => key = {p.key}")
    res = 0
    for idx, p in enumerate(ordered_hands):
        if verbose_level >= 1:
            print(f"{p} : val = {idx + 1} * {p.bid} = {(idx + 1) * p.bid}")
        res += (idx + 1) * p.bid
    return res

def main(is_part1: bool, input_file: str, verbose_level=0):
    if not is_part1:
        del CARDS_STRENGHTS["J"]
        CARDS_STRENGHTS["J"] = min(CARDS_STRENGHTS.values()) - 1 # weakest card
    if verbose_level >= 1:
        print(f"CARDS_STRENGHTS = {CARDS_STRENGHTS}")
    with open(input_file) as f:
        all_players: list[PlayerHand] = []
        for line in f.read().splitlines():
            if not line:
                continue
            all_players.append(PlayerHand.from_line(line))
    return process(all_players, is_part1, verbose_level)


if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")

    # parse the arguments
    args = parser.parse_args()

    # run !
    res = None
    time_begin = time.time()
    try:
        res = main(args.part1, args.input, verbose_level=args.verbose)
    finally:
        time_end = time.time()
        print(f"Result = {res}")
        print(f"Computation time = {time_end - time_begin} s")
