import argparse
import time


MAX_COLORS_PART_1 = {"red": 12, "green": 13, "blue": 14}


def _val_color_str_to_tuple(val_color: str):
    v, c = val_color.strip().split()
    return c, int(v)


def is_possible_game(game_dict: dict, max_colors: dict):
    return all(val <= max_colors[color] for color, val in game_dict.items())

def find_power_of_smallest_game(list_games_dict: list[dict[str, int]]):
    power = 1
    for color in MAX_COLORS_PART_1:
        val = max(game.get(color, 0) for game in list_games_dict)
        power *= val
    return power

def parse_line(line: str):
    game_id_str, games_list = line.split(":", maxsplit=1)
    game_id = int(game_id_str.split()[1])
    list_games_dict = [dict(_val_color_str_to_tuple(x) for x in one_game.split(",")) for one_game in games_list.split(";")]
    return game_id, list_games_dict


def main(is_part1: bool, input_file: str, verbose_level=0):

    sum_ids = 0
    sum_powers = 0

    with open(input_file) as f:
        for line in f:
            line = line.strip()
            if not line:
                continue
            game_id, list_games_dict = parse_line(line)
            if is_part1:
                if all(is_possible_game(game_dict, MAX_COLORS_PART_1) for game_dict in list_games_dict):
                    if verbose_level:
                        print(f"[DEBUG] found matching game : {game_id}")
                    sum_ids += game_id
            else:
                power = find_power_of_smallest_game(list_games_dict)
                if verbose_level:
                    print(f"[DEBUG] game {game_id:2} power = {power}")
                sum_powers += power

    if is_part1:
        if verbose_level:
            print(f"[DEBUG] sum_ids = {sum_ids}")
        return sum_ids
    else:
        if verbose_level:
            print(f"[DEBUG] sum_powers = {sum_powers}")
        return sum_ids


if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")

    # parse the arguments
    args = parser.parse_args()

    # run !
    res = None
    time_begin = time.time()
    try:
        res = main(args.part1, args.input, verbose_level=args.verbose)
    finally:
        time_end = time.time()
        print(f"Result = {res}")
        print(f"Computation time = {time_end - time_begin} s")
