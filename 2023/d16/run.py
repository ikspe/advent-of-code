import argparse
import enum
import time


class Direction:
    NORTH = 1
    WEST = 2
    SOUTH = 3
    EAST = 4

"""
MATRIX_ROTATION_CLOCKWISE = {
    Direction.NORTH: Direction.EAST,
    Direction.EAST: Direction.SOUTH,
    Direction.SOUTH: Direction.WEST,
    Direction.WEST: Direction.NORTH,
    }
MATRIX_ROTATION_ANTI_CLOCKWISE = {v: k for k, v in MATRIX_ROTATION_CLOCKWISE.items()}
"""
MATRIX_ROTATION_SLASH = {
    Direction.NORTH: Direction.EAST,
    Direction.EAST: Direction.NORTH,
    Direction.WEST: Direction.SOUTH,
    Direction.SOUTH: Direction.WEST,
    }
MATRIX_ROTATION_BACKSLASH = {
    Direction.EAST: Direction.SOUTH,
    Direction.SOUTH: Direction.EAST,
    Direction.NORTH: Direction.WEST,
    Direction.WEST: Direction.NORTH,
    }


def _register_beam_head(beam_heads: set, bh):
    if bh in beam_heads:
        return  # do nothing
    beam_heads.add(bh)


class Layout:
    def __init__(self, lines: list[str], verbose_level: int) -> None:
        self.lines = lines
        self._col_count = len(self.lines[0])
        self._line_count = len(self.lines)
        self.verbose_level = verbose_level

    def _compute_energy(self, start_beam: tuple[int, int, Direction]) -> int:
        beam_heads: set[tuple[int, int, Direction]] = {start_beam}
        energized_tiles = set()
        iter_count = 0
        memory_all_beam_heads = set()
        while bool(beam_heads):
            if self.verbose_level >= 2:
                print(f"[DEBUG] iter {iter_count:3} [BEFORE] {len(energized_tiles) = } / {beam_heads = }")
            new_beam_heads = set()
            for col_idx, line_idx, dir_beam in beam_heads:
                if dir_beam == Direction.EAST:
                    new_col_idx, new_line_idx = col_idx + 1, line_idx
                elif dir_beam == Direction.WEST:
                    new_col_idx, new_line_idx = col_idx - 1, line_idx
                elif dir_beam == Direction.SOUTH:
                    new_col_idx, new_line_idx = col_idx, line_idx + 1
                elif dir_beam == Direction.NORTH:
                    new_col_idx, new_line_idx = col_idx, line_idx - 1
                if not ((0 <= new_col_idx < self._col_count) and (0 <= new_line_idx < self._line_count)):
                    continue  # out of bounds
                new_tile_val = self.lines[new_line_idx][new_col_idx]
                if new_tile_val == ".":
                    _register_beam_head(new_beam_heads,  (new_col_idx, new_line_idx, dir_beam))
                elif new_tile_val == "/":
                    _register_beam_head(new_beam_heads,  (new_col_idx, new_line_idx, MATRIX_ROTATION_SLASH[dir_beam]))
                elif new_tile_val == "\\":
                    _register_beam_head(new_beam_heads,  (new_col_idx, new_line_idx, MATRIX_ROTATION_BACKSLASH[dir_beam]))
                elif new_tile_val in ("-", "|"):
                    if      (dir_beam in (Direction.EAST, Direction.WEST) and new_tile_val == "-") or \
                            (dir_beam in (Direction.NORTH, Direction.SOUTH) and new_tile_val == "|"):
                        _register_beam_head(new_beam_heads,  (new_col_idx, new_line_idx, dir_beam))  # like empty space
                    else:
                        # split into 2 new beams
                        _register_beam_head(new_beam_heads,  (new_col_idx, new_line_idx, MATRIX_ROTATION_SLASH[dir_beam]))
                        _register_beam_head(new_beam_heads,  (new_col_idx, new_line_idx, MATRIX_ROTATION_BACKSLASH[dir_beam]))
                else:
                    raise ValueError()
            beam_heads = new_beam_heads
            del new_beam_heads
            energized_tiles.update([x[:2] for x in beam_heads])
            beam_heads.difference_update(memory_all_beam_heads)
            #at_least_one_new_head = bool(set(beam_heads).difference(memory_all_beam_heads))
            if self.verbose_level >= 3:
                print(f"[DEBUG] iter {iter_count:3} [AFTER ] {len(energized_tiles) = } / {beam_heads = }")
            memory_all_beam_heads.update(beam_heads)
            iter_count += 1
            if iter_count >= 2000:
                raise NotImplementedError
                
        return len(energized_tiles)

    def compute_part1(self):
        return self._compute_energy((-1, 0, Direction.EAST))

    def compute_part2(self):
        res = -1
        for l in range(self._line_count):
            for start_beam in [(-1, l, Direction.EAST), (self._col_count, l, Direction.WEST)]:
                this_energy = self._compute_energy(start_beam)
                if this_energy > res:
                    if self.verbose_level >= 1:
                        print(f"Found better energy = {this_energy} at {start_beam} (prev = {res})")
                    res = this_energy
        for c in range(self._col_count):
            for start_beam in [(c, -1, Direction.SOUTH), (c, self._line_count, Direction.NORTH)]:
                this_energy = self._compute_energy(start_beam)
                if this_energy > res:
                    if self.verbose_level >= 1:
                        print(f"Found better energy = {this_energy} at {start_beam} (prev = {res})")
                    res = this_energy
        return res


def main(is_part1: bool, input_file: str, verbose_level=0):
    with open(input_file) as f:
        layout = Layout(lines=f.read().splitlines(), verbose_level=verbose_level)

    if is_part1:
        return layout.compute_part1()
    else:
        return layout.compute_part2()


if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")

    # parse the arguments
    args = parser.parse_args()

    # run !
    res = None
    time_begin = time.time()
    try:
        res = main(args.part1, args.input, verbose_level=args.verbose)
    finally:
        time_end = time.time()
        print(f"Result = {res}")
        print(f"Computation time = {time_end - time_begin} s")
