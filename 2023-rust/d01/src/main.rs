use clap::Parser;
use log::{debug, error};

// https://doc.rust-lang.org/cargo/reference/environment-variables.html
//const PKG_VERSION: &str = env!("CARGO_PKG_VERSION");
const PKG_NAME: &str = env!("CARGO_PKG_NAME");
//const PKG_REPOSITORY: &str = env!("CARGO_PKG_REPOSITORY");

fn default_input_path() -> String {
    // TODO : this function should be moved to crate "common"
    format!("~/git/advent-of-code-inputs/2023/{}/input.txt", PKG_NAME).to_string()
}

/// compute puzzle of the day
#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
#[command(after_help = "To select log level, use env var RUST_LOG=level")]
struct Args {
    /// run only part 1 or part 2
    #[arg(short, long)]
    part: Option<usize>,

    /// Input file (default to input file downloaded in input repo)
    #[arg(short, long)]
    input: Option<String>,
}

fn run_part1(file_path: &str) -> i64 {
    debug!("starting part 1");
    //todo!();
    debug!("finished part 1");
    todo!()
}

fn run_part2(file_path: &str) -> i64 {
    debug!("starting part2");
    //todo!();
    debug!("finished part 2");
    todo!()
}

fn main() {
    env_logger::init();
    let args = Args::parse();
    let input_file = args.input.unwrap_or_else(default_input_path);
    println!("input_file = {input_file:?}");
    if args.part == None || args.part == Some(1) {
        let res = run_part1(&input_file);
        println!("Result part1 : {res}");
    }
    if args.part == None || args.part == Some(2) {
        let res = run_part2(&input_file);
        println!("Result part2 : {res}");
    }
}

#[cfg(test)]
mod tests {
    // print output :
    // cargo test -- --show-output

    use super::*;

    const TEST_FILE_1: &str = "example1.txt";
    const TEST_FILE_2: &str = "example2.txt";

    const TEST_RESULT_PART1_FILE_1: i64 = 2256;
    const TEST_RESULT_PART2_FILE_2: i64 = 74229;

    #[test]
    fn test_part1() {
        let result = run_part1(TEST_FILE_1);
        println!("Result part1 on example : {result}");
        assert_eq!(result, TEST_RESULT_PART1_FILE_1);
    }

    #[test]
    fn test_part2() {
        let result = run_part2(TEST_FILE_2);
        println!("Result part2 on example : {result}");
        assert_eq!(result, TEST_RESULT_PART2_FILE_2);
    }
}
