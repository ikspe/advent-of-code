use std::collections::HashMap;

use aoc_common::log::{debug, trace};
use aoc_common::read_file::read_lines_as_reader;
use clap::Parser;

// https://doc.rust-lang.org/cargo/reference/environment-variables.html
//const PKG_VERSION: &str = env!("CARGO_PKG_VERSION");
const AOC_YEAR: u32 = 2024;
const PKG_NAME: &str = env!("CARGO_PKG_NAME");
//const PKG_REPOSITORY: &str = env!("CARGO_PKG_REPOSITORY");

fn default_input_path() -> String {
    // TODO : this function should be moved to crate "common"
    format!(
        "{}/git/advent-of-code-inputs/{}/{}/input.txt",
        std::env::var("HOME").expect("error getting variable HOME"),
        AOC_YEAR,
        PKG_NAME
    )
    .to_string()
}

/// compute puzzle of the day
#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
#[command(after_help = "To select log level, use env var RUST_LOG=level")]
struct Args {
    /// run only part 1 or part 2
    #[arg(short, long)]
    part: Option<usize>,

    /// Input file (default to input file downloaded in input repo)
    #[arg(short, long)]
    input: Option<String>,
}

#[derive(Debug)]
struct Market {
    buyers: Vec<u32>, // modulo 16113920 = 0x1000000 : u32 is enough
}

impl Market {
    pub fn parse_file(file_path: &str) -> Self {
        let reader: std::io::Lines<std::io::BufReader<std::fs::File>> =
            read_lines_as_reader(file_path).expect("error reading file");
        let buyers: Vec<_> = reader.map(|x| x.unwrap().parse::<u32>().unwrap()).collect();
        Self { buyers }
    }
    pub fn gen_next_pseudo_random_numbers(val0: u32) -> u32 {
        // prune: modulo 16777216 = 0x1000000 <==> AND 0xFFFFFF

        // multiplying the secret number by 64.
        // Then, mix this result into the secret number.
        // Finally, prune
        // multiply by 64 <==> N << 6
        let val1 = (val0.wrapping_shl(6) ^ val0) & 0xFFFFFF;
        // let val1 = ((val0 * 64) ^ val0) & 0xFFFFFF;
        // let val1 = ((val0 * 64) ^ val0) % 0x1000000;

        // dividing the secret number by 32.
        // Round the result down to the nearest integer.
        // Then, mix this result into the secret number. Finally, prune
        // divide by 32 <==> N >> 5
        let val2 = (val1.wrapping_shr(5) ^ val1) & 0xFFFFFF;

        // multiplying the secret number by 2048.
        // Then, mix this result into the secret number.
        // Finally, prune
        // multiply by 2048 <==> N << 11
        let val3 = (val2.wrapping_shl(11) ^ val2) & 0xFFFFFF;
        val3
    }
    pub fn compute_part1(&self) -> u64 {
        let mut res = 0u64;
        for it_buyer in self.buyers.iter() {
            let mut val = *it_buyer;
            for _ in 0..2000 {
                val = Market::gen_next_pseudo_random_numbers(val);
            }
            res += val as u64;
        }
        res
    }
    pub fn compute_part2(&self) -> u32 {
        // global best solution over all buyers
        let mut hash_variation_to_sum_bananas: HashMap<(i8, i8, i8, i8), u32> = HashMap::new();

        let global_key: (i8, i8, i8, i8) = (-2, 1, -1, 3);

        for it_buyer in self.buyers.iter() {
            let mut val = *it_buyer;
            let mut prev_price = (val % 10) as i8;
            let mut price_and_variations: Vec<(i8, i8)> = vec![(prev_price, 0)]; //the first price has no associated change
            let mut hash_variations_to_price: HashMap<(i8, i8, i8, i8), i8> = HashMap::new();

            for _ in 0..2000 {
                val = Market::gen_next_pseudo_random_numbers(val);
                trace!("    {} : {}", *it_buyer, val);
                let price = (val % 10) as i8;
                // println!(
                //     "    {} : price = {} (prev_price = {})",
                //     *it_buyer, price, prev_price
                // );
                price_and_variations.push((price, price - prev_price));
                prev_price = price;
                if price_and_variations.len() >= 4 {
                    let last4 = &price_and_variations[price_and_variations.len() - 4..];
                    let key = (last4[0].1, last4[1].1, last4[2].1, last4[3].1);
                    // println!("    {} : key = {:?}", *it_buyer, key);
                    // if key == global_key {
                    //     println!(
                    //         "    {} : key = {:?} => found ! (now = {})",
                    //         *it_buyer, key, price
                    //     );
                    // }
                    let prev_variation = hash_variations_to_price.get(&key);
                    trace!(
                        "    {} : last 4 variations = {:?} (stored : {:?} / now = {:?})",
                        *it_buyer,
                        key,
                        prev_variation,
                        price
                    );
                    if prev_variation.is_none() {
                        debug!(
                            "    {} : last 4 variations = {:?} ; storing = {:?}",
                            *it_buyer, key, price
                        );
                        hash_variations_to_price.insert(key, price);
                        if let Some(global_price) = hash_variation_to_sum_bananas.get_mut(&key) {
                            (*global_price) += price as u32;
                        } else {
                            hash_variation_to_sum_bananas.insert(key, price as u32);
                        }
                    }
                }
            }
            trace!("{} : {:?}", *it_buyer, price_and_variations);
            let price_for_global_key = hash_variations_to_price.get(&global_key);
            // println!(
            //     "    {} : global_key = {:?} => price_for_global_key = {:?}",
            //     *it_buyer, global_key, price_for_global_key
            // );
        }
        // println!(
        //     "global_key = {:?} : sum bananas => {:?}",
        //     global_key,
        //     hash_variation_to_sum_bananas.get(&global_key)
        // );
        let mut best_solution: Option<u32> = None;
        for &it_val in hash_variation_to_sum_bananas.values() {
            if best_solution.is_none() {
                best_solution = Some(it_val);
                continue;
            }
            if best_solution.unwrap() < it_val {
                best_solution = Some(it_val);
                continue;
            }
        }
        best_solution.unwrap()
    }
}

fn run_part1(file_path: &str) -> i64 {
    debug!("starting part 1");
    let market = Market::parse_file(file_path);
    let result = market.compute_part1();
    debug!("finished part 1");

    result as i64
}

fn run_part2(file_path: &str) -> i64 {
    debug!("starting part 2");
    let market = Market::parse_file(file_path);
    let result = market.compute_part2();
    debug!("finished part 2");
    result as i64
}

fn main() {
    env_logger::init();
    let args = Args::parse();
    let input_file = args.input.unwrap_or_else(default_input_path);
    println!("input_file = {input_file:?}");
    if args.part == None || args.part == Some(1) {
        let res = run_part1(&input_file);
        println!("Result part1 : {res}");
    }
    if args.part == None || args.part == Some(2) {
        let res = run_part2(&input_file);
        println!("Result part2 : {res}");
    }
}

#[cfg(test)]
mod tests {
    // print output :
    // cargo test -- --show-output

    use super::*;

    const TEST_FILE_1: &str = "example1.txt";
    const TEST_FILE_2: &str = "example2.txt";

    fn get_result_part1_file1() -> i64 {
        37327623
    }
    fn get_result_part2_file2() -> i64 {
        23
    }

    #[test]
    fn test_basic_compute() {
        println!("Init value: 123");
        let mut res: Vec<u32> = vec![123];
        for _ in 0..10 {
            res.push(Market::gen_next_pseudo_random_numbers(*res.last().unwrap()));
        }
        println!("next values: {:?}", &res[1..11]);
        let expected_val: Vec<u32> = [
            15887950, 16495136, 527345, 704524, 1553684, 12683156, 11100544, 12249484, 7753432,
            5908254,
        ]
        .to_vec();
        println!("expected values: {:?}", expected_val);
        assert_eq!(res[1..11].to_vec(), expected_val);
    }

    #[test]
    fn test_multi_iter() {
        for (init_val, expected_res) in [
            (1u32, 8685429u32),
            (10u32, 4700978u32),
            (100u32, 15273692u32),
            (2024u32, 8667524u32),
        ] {
            let mut val = init_val;
            for _ in 0..2000 {
                val = Market::gen_next_pseudo_random_numbers(val);
            }
            println!(
                "init_val = {} / after 2000 iterations : {} / expected = {}",
                init_val, val, expected_res
            );
            assert_eq!(val, expected_res);
        }
    }

    #[test]
    fn test_part1() {
        let result = run_part1(TEST_FILE_1);
        let expected_result = get_result_part1_file1();
        println!("Result part1 on example : {result} / expected result = {expected_result}");
        assert_eq!(result, expected_result);
    }

    #[test]
    fn test_part2() {
        let result = run_part2(TEST_FILE_2);
        let expected_result = get_result_part2_file2();
        println!("Result part2 on example : {result} / expected result = {expected_result}");
        assert_eq!(result, expected_result);
    }
}
