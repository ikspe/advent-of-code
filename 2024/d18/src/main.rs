use std::{
    collections::{HashMap, HashSet},
    u16,
};

use aoc_common::direction::Direction;
use aoc_common::read_file::read_lines_as_reader;
use clap::Parser;
use log::{debug, error, info, trace, warn};

// https://doc.rust-lang.org/cargo/reference/environment-variables.html
//const PKG_VERSION: &str = env!("CARGO_PKG_VERSION");
const AOC_YEAR: u32 = 2024;
const PKG_NAME: &str = env!("CARGO_PKG_NAME");
//const PKG_REPOSITORY: &str = env!("CARGO_PKG_REPOSITORY");

fn default_input_path() -> String {
    // TODO : this function should be moved to crate "common"
    format!(
        "{}/git/advent-of-code-inputs/{}/{}/input.txt",
        std::env::var("HOME").expect("error getting variable HOME"),
        AOC_YEAR,
        PKG_NAME
    )
    .to_string()
}

/// compute puzzle of the day
#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
#[command(after_help = "To select log level, use env var RUST_LOG=level")]
struct Args {
    /// run only part 1 or part 2
    #[arg(short, long)]
    part: Option<usize>,

    /// Input file (default to input file downloaded in input repo)
    #[arg(short, long)]
    input: Option<String>,

    /// Grid size : default 70x70 (real input) but 6x6 on example
    #[arg(short, long, default_value_t = 70)]
    grid_size: usize,

    /// example part 1 , 12 bytes / real life : 1024
    #[arg(short, long, default_value_t = 1024)]
    count_fallen_bytes_part1: usize,
}

type Position = (i16, i16);

#[derive(Debug)]
struct MemoryLayout {
    corrupted: HashSet<Position>,
    grid_size: i16,
}

impl MemoryLayout {
    pub fn read_falling_bytes(file_path: &str) -> Vec<Position> {
        let reader: std::io::Lines<std::io::BufReader<std::fs::File>> =
            read_lines_as_reader(file_path).expect("error reading file");
        let mut result: Vec<Position> = vec![];
        for line in reader {
            let line = line.unwrap();
            let tokens: Vec<_> = line
                .splitn(2, ",")
                .map(|x| x.parse::<u16>().unwrap() as i16)
                .collect();
            result.push((tokens[0], tokens[1]));
        }
        result
    }
    pub fn build_layout(falling_bytes: &Vec<Position>, grid_size: usize, mem_amout: usize) -> Self {
        let mut corrupted: HashSet<Position> = HashSet::new();
        for pos in falling_bytes[0..mem_amout].iter() {
            corrupted.insert(*pos);
        }
        Self {
            corrupted,
            // Wow, grid 6x6 in reality means 7x7 ... WTF
            grid_size: (grid_size + 1) as i16,
        }
    }

    /// return the node in openSet having the lowest fScore[] value
    fn get_node_lowest_score(
        open_set: &HashSet<Position>,
        f_score: &HashMap<Position, usize>,
    ) -> Position {
        let mut result_node: Option<Position> = None;
        let mut lowest_score: Option<usize> = None;
        for it_node in open_set.iter() {
            let it_score = f_score[it_node];
            let is_lowest = if let Some(current_lowest_score) = lowest_score {
                it_score < current_lowest_score
            } else {
                true
            };
            if is_lowest {
                result_node = Some(it_node.clone());
                lowest_score = Some(it_score);
            }
        }
        result_node.unwrap()
    }

    // A* finds a path from start to goal.
    // h is the heuristic function. h(n) estimates the cost to reach goal from node n.
    pub fn find_shortest_path(&self) -> Option<usize> {
        // \o/   Dijkstra is back => let's go for A*
        let start_pos = (0 as i16, 0 as i16);
        let end_pos = (self.grid_size - 1, self.grid_size - 1);
        //let mut current_path = vec![(0 as i16, 0 as i16)];

        // openSet := {start}
        let mut open_set: HashSet<Position> = HashSet::new();
        open_set.insert(start_pos);

        // cameFrom := an empty map
        let mut came_from: HashMap<Position, Position> = HashMap::new();

        // For node n, gScore[n] is the currently known cost of the cheapest path from start to n.
        // gScore := map with default value of Infinity
        // gScore[start] := 0
        let mut g_score: HashMap<Position, usize> = HashMap::new();
        g_score.insert(start_pos, 0);

        // For node n, fScore[n] := gScore[n] + h(n). fScore[n] represents our current best guess as to
        // how cheap a path could be from start to finish if it goes through n.
        // fScore := map with default value of Infinity
        // fScore[start] := h(start)
        let mut f_score: HashMap<Position, usize> = HashMap::new();
        f_score.insert(
            start_pos,
            // very rough estimation of fScore using Manhattan distance
            (end_pos.0.abs_diff(start_pos.0) + (end_pos.1).abs_diff(start_pos.1)) as usize,
        );

        let max_pos_i32 = (self.grid_size as i32, self.grid_size as i32);

        while !open_set.is_empty() {
            // current := the node in openSet having the lowest fScore[] value
            let current = Self::get_node_lowest_score(&open_set, &f_score);
            trace!("");
            trace!(
                "current = {:?} from open_set (len = {})",
                current,
                open_set.len()
            );

            // if current = goal
            //     return reconstruct_path(cameFrom, current)
            if current == end_pos {
                return Some(g_score[&current]);
            }
            open_set.remove(&current);

            let current_i32 = (current.0 as i32, current.1 as i32);
            let g_score_current = g_score[&current];
            trace!(
                "current = {:?} / g_score_current = {}",
                current,
                g_score_current
            );
            // for each neighbor of current
            for direction in Direction::iterator() {
                let neighbor =
                    if let Some(neighbor_i32) = direction.move_pos(&current_i32, &max_pos_i32) {
                        (neighbor_i32.0 as i16, neighbor_i32.1 as i16)
                    } else {
                        continue; //out of map
                    };
                if self.corrupted.contains(&neighbor) {
                    trace!(
                        "current = {:?} / direction = {:?} => neighbor = {:?} - ignoring corrupted",
                        current,
                        direction,
                        neighbor
                    );
                    continue; // corrupted : unreachable
                }

                // d(current,neighbor) is the weight of the edge from current to neighbor
                // tentative_gScore is the distance from start to the neighbor through current
                // tentative_gScore := gScore[current] + d(current, neighbor)
                let tentative_g_score = g_score_current + 1;
                // if tentative_gScore < gScore[neighbor]
                //     // This path to neighbor is better than any previous one. Record it!
                let needs_to_record = if let Some(previous_score_neighbor) = g_score.get(&neighbor)
                {
                    tentative_g_score < *previous_score_neighbor
                } else {
                    true
                };
                if needs_to_record {
                    // cameFrom[neighbor] := current
                    // gScore[neighbor] := tentative_gScore
                    // fScore[neighbor] := tentative_gScore + h(neighbor)
                    // if neighbor not in openSet
                    //     openSet.add(neighbor)
                    trace!(
                        "current = {:?} / direction = {:?} => neighbor = {:?} - recording best finding (tentative_g_score = {})",
                        current,
                        direction,
                        neighbor,
                        tentative_g_score
                    );
                    came_from.insert(neighbor, current);
                    g_score.insert(neighbor, tentative_g_score);
                    f_score.insert(neighbor,
                        tentative_g_score +
                        // very rough estimation of fScore using Manhattan distance
                        (end_pos.0.abs_diff(start_pos.0) + (end_pos.1).abs_diff(start_pos.1)) as usize
                    );
                    if !open_set.contains(&neighbor) {
                        open_set.insert(neighbor);
                    }
                } else {
                    trace!(
                        "current = {:?} / direction = {:?} => neighbor = {:?} - ignored (already better score - tentative_g_score = {})",
                        current,
                        direction,
                        neighbor,
                        tentative_g_score
                    );
                }
            }
        }
        warn!("did not find solution");
        None
    }
}

fn run_part1(file_path: &str, grid_size: usize, count_fallen_bytes: usize) -> i64 {
    debug!("starting part 1");
    let all_falling_bytes = MemoryLayout::read_falling_bytes(file_path);
    let mem_layout = MemoryLayout::build_layout(&all_falling_bytes, grid_size, count_fallen_bytes);
    debug!("mem_layout = {:?}", mem_layout);
    let result = mem_layout.find_shortest_path().unwrap();
    debug!("finished part 1");
    result as i64
}

fn run_part2(file_path: &str, grid_size: usize) -> String {
    debug!("starting part 2");
    let all_falling_bytes: Vec<(i16, i16)> = MemoryLayout::read_falling_bytes(file_path);
    // FIXME : we can shortcut starting real data iteration at 1024
    for mem_amount in 1..all_falling_bytes.len() + 1 {
        let mem_layout = MemoryLayout::build_layout(&all_falling_bytes, grid_size, mem_amount);
        let last_byte = &all_falling_bytes[mem_amount - 1];
        info!(
            "mem_layout with mem_amount = {} (last byte = {:?}) ...",
            mem_amount, last_byte
        );
        let result = mem_layout.find_shortest_path();
        if result.is_none() {
            info!(
                "Finished part 2 : no path for mem_amount = {} : found obstructing byte = {:?} ...",
                mem_amount, last_byte
            );
            return format!("{},{}", last_byte.0, last_byte.1).to_string();
        }
    }
    error!("finished part 2 with no result");
    panic!("we should not reach that point")
}

fn main() {
    env_logger::init();
    let args = Args::parse();
    let input_file = args.input.unwrap_or_else(default_input_path);
    println!("input_file = {input_file:?}");
    if args.part == None || args.part == Some(1) {
        let res = run_part1(&input_file, args.grid_size, args.count_fallen_bytes_part1);
        println!("Result part1 : {res}");
    }
    if args.part == None || args.part == Some(2) {
        let res = run_part2(&input_file, args.grid_size);
        println!("Result part2 : {res}");
    }
}

#[cfg(test)]
mod tests {
    // print output :
    // cargo test -- --show-output

    use super::*;

    const TEST_GRID_SIZE: usize = 6; // 6x6
    const TEST_COUNT_FALLEN_BYTES_PART1: usize = 12;
    const TEST_FILE_1: &str = "example1.txt";
    // const TEST_FILE_2: &str = "example2.txt";

    const TEST_RESULT_PART1_FILE_1: i64 = 22;
    const TEST_RESULT_PART2_FILE_1: &str = "6,1";

    #[test]
    fn test_part1() {
        let result = run_part1(TEST_FILE_1, TEST_GRID_SIZE, TEST_COUNT_FALLEN_BYTES_PART1);
        println!("Result part1 on example : {result}");
        assert_eq!(result, TEST_RESULT_PART1_FILE_1);
    }

    #[test]
    fn test_part2() {
        let result = run_part2(TEST_FILE_1, TEST_GRID_SIZE);
        println!("Result part2 on example : {result}");
        assert_eq!(result, TEST_RESULT_PART2_FILE_1);
    }
}
