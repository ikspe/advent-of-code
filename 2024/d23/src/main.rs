use std::collections::{HashMap, HashSet};

use aoc_common::log::{debug, info, trace};
use aoc_common::read_file::read_lines_as_reader;
use clap::Parser;

// https://doc.rust-lang.org/cargo/reference/environment-variables.html
//const PKG_VERSION: &str = env!("CARGO_PKG_VERSION");
const AOC_YEAR: u32 = 2024;
const PKG_NAME: &str = env!("CARGO_PKG_NAME");
//const PKG_REPOSITORY: &str = env!("CARGO_PKG_REPOSITORY");

fn default_input_path() -> String {
    // TODO : this function should be moved to crate "common"
    format!(
        "{}/git/advent-of-code-inputs/{}/{}/input.txt",
        std::env::var("HOME").expect("error getting variable HOME"),
        AOC_YEAR,
        PKG_NAME
    )
    .to_string()
}

/// compute puzzle of the day
#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
#[command(after_help = "To select log level, use env var RUST_LOG=level")]
struct Args {
    /// run only part 1 or part 2
    #[arg(short, long)]
    part: Option<usize>,

    /// Input file (default to input file downloaded in input repo)
    #[arg(short, long)]
    input: Option<String>,
}

#[derive(Debug)]
struct NetworkMap {
    connections: HashMap<String, HashSet<String>>,
}

impl NetworkMap {
    pub fn parse_file(file_path: &str) -> Self {
        let reader: std::io::Lines<std::io::BufReader<std::fs::File>> =
            read_lines_as_reader(file_path).expect("error reading file");
        let mut connections: HashMap<String, HashSet<String>> = HashMap::new();
        for line in reader {
            let line = line.unwrap();
            let mut line_split = line.split("-");
            let pc1 = line_split.next().unwrap();
            let pc2 = line_split.next().unwrap();
            assert!(line_split.next().is_none());
            for (key, val) in [(pc1, pc2), (pc2, pc1)] {
                if let Some(set_vals) = connections.get_mut(&key.to_string()) {
                    set_vals.insert(val.to_string());
                } else {
                    let mut set_vals: HashSet<String> = HashSet::new();
                    set_vals.insert(val.to_string());
                    connections.insert(key.into(), set_vals);
                }
            }
        }
        Self { connections }
    }
    pub fn compute_part1(&self) -> usize {
        // now that part 2 is implemented,
        // we could simply exec the same algo as part 2
        // but only for steps 2 and 3 and filter in result with computers starting with "t"

        // Start by looking for sets of three computers
        // where each computer in the set is connected to the other two computers.
        let mut sets_interconnected: HashSet<(String, String, String)> = HashSet::new();
        for (it_src, it_dst) in self.connections.iter() {
            for it_src2 in it_dst.iter() {
                for it_src3 in it_dst.iter() {
                    if it_src2 == it_src3 {
                        continue;
                    }
                    if self.connections[it_src2].contains(it_src3) {
                        let mut this_set = [it_src, it_src2, it_src3];
                        this_set.sort();
                        if !this_set.iter().any(|&x| x.starts_with("t")) {
                            continue;
                        }
                        sets_interconnected.insert((
                            this_set[0].clone(),
                            this_set[1].clone(),
                            this_set[2].clone(),
                        ));
                    }
                }
            }
        }
        sets_interconnected.len()
    }
    pub fn compute_part2(&self) -> String {
        // init : build pairs of computed computers (sorted)
        let mut interconnection_n_2: HashSet<String> = HashSet::new();
        let mut current_step = 2;
        for (it_src, it_dst) in self.connections.iter() {
            for it_src2 in it_dst {
                let mut this_set = vec![&it_src[..], &it_src2[..]];
                this_set.sort();
                interconnection_n_2.insert(this_set.join(",").to_string());
            }
        }
        let mut curr_interconnection = interconnection_n_2;
        loop {
            info!(
                "curr_interconnection (step = {}) : count = {:?}",
                current_step,
                curr_interconnection.len()
            );
            if curr_interconnection.len() == 1 {
                info!(
                    "FOUND result : (step = {}) : count = {:?} => {:?}",
                    current_step,
                    curr_interconnection.len(),
                    curr_interconnection
                );
                return curr_interconnection.iter().next().unwrap().clone();
            }
            curr_interconnection = self.compute_bigger_interconnection(curr_interconnection);
            current_step += 1;
            assert!(!curr_interconnection.is_empty());
        }
    }
    pub fn compute_bigger_interconnection(
        &self,
        previous_interconnection: HashSet<String>,
    ) -> HashSet<String> {
        let mut new_interconnection: HashSet<String> = HashSet::new();
        for it_interc in previous_interconnection.iter() {
            let vec_it_interc: Vec<_> = it_interc.split(",").map(|x| x.to_string()).collect();
            let first_node = &vec_it_interc[0];
            for it_candidate in self.connections[first_node].iter() {
                if vec_it_interc.contains(it_candidate) {
                    continue;
                }
                let is_matching_all = {
                    let mut is_matching_all = true;
                    for it_node in vec_it_interc.iter() {
                        if !self.connections[it_node].contains(it_candidate) {
                            is_matching_all = false;
                            break;
                        }
                    }
                    is_matching_all
                };
                if is_matching_all {
                    let mut this_set = vec_it_interc.clone();
                    this_set.push(it_candidate.clone());
                    this_set.sort();
                    new_interconnection.insert(this_set.join(",").to_string());
                }
            }
        }
        new_interconnection
    }
}

fn run_part1(file_path: &str) -> i64 {
    debug!("starting part 1");
    let network_map = NetworkMap::parse_file(file_path);
    debug!("network_map = {:?}", network_map);
    let result = network_map.compute_part1();
    debug!("finished part 1");
    result as i64
}

fn run_part2(file_path: &str) -> String {
    debug!("starting part 2");
    let network_map = NetworkMap::parse_file(file_path);
    debug!("network_map = {:?}", network_map);
    let result = network_map.compute_part2();
    debug!("finished part 2");
    result
    // er-fh-fi-ir-kk-lo-lp-qi-ti-vb-xf-ys-yu =>
}

fn main() {
    env_logger::init();
    let args = Args::parse();
    let input_file = args.input.unwrap_or_else(default_input_path);
    println!("input_file = {input_file:?}");
    if args.part == None || args.part == Some(1) {
        let res = run_part1(&input_file);
        println!("Result part1 : {res}");
    }
    if args.part == None || args.part == Some(2) {
        let res = run_part2(&input_file);
        println!("Result part2 : {res}");
    }
}

#[cfg(test)]
mod tests {
    // print output :
    // cargo test -- --show-output

    use super::*;

    const TEST_FILE_1: &str = "example1.txt";
    // const TEST_FILE_2: &str = "example2.txt";

    fn get_result_part1_file1() -> i64 {
        7
    }
    fn get_result_part2_file1() -> String {
        "co,de,ka,ta".to_string()
    }

    #[test]
    fn test_part1() {
        let result = run_part1(TEST_FILE_1);
        let expected_result = get_result_part1_file1();
        println!("Result part1 on example : {result} / expected result = {expected_result}");
        assert_eq!(result, expected_result);
    }

    #[test]
    fn test_part2() {
        let result = run_part2(TEST_FILE_1);
        let expected_result = get_result_part2_file1();
        println!("Result part2 on example : {result} / expected result = {expected_result}");
        assert_eq!(result, expected_result);
    }
}
