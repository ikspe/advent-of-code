use std::{
    cmp::min,
    collections::{HashMap, HashSet},
};

use aoc_common::direction::Direction;
use aoc_common::read_file::read_lines_as_reader;
use clap::Parser;
use log::{debug, error, trace};

// https://doc.rust-lang.org/cargo/reference/environment-variables.html
//const PKG_VERSION: &str = env!("CARGO_PKG_VERSION");
const AOC_YEAR: u32 = 2024;
const PKG_NAME: &str = env!("CARGO_PKG_NAME");
//const PKG_REPOSITORY: &str = env!("CARGO_PKG_REPOSITORY");

fn default_input_path() -> String {
    // TODO : this function should be moved to crate "common"
    format!(
        "{}/git/advent-of-code-inputs/{}/{}/input.txt",
        std::env::var("HOME").expect("error getting variable HOME"),
        AOC_YEAR,
        PKG_NAME
    )
    .to_string()
}

/// compute puzzle of the day
#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
#[command(after_help = "To select log level, use env var RUST_LOG=level")]
struct Args {
    /// run only part 1 or part 2
    #[arg(short, long)]
    part: Option<usize>,

    /// Input file (default to input file downloaded in input repo)
    #[arg(short, long)]
    input: Option<String>,
}
type Position = (i32, i32);
#[derive(Debug, Clone)]
enum CellContent {
    Wall,
    Box,
    BoxRight,
}
#[derive(Debug)]

struct LanternfishWarehouse {
    cells: HashMap<Position, CellContent>,
    pos_max: Position,
    pos_robot: Position,
    moves: Vec<Direction>,
}

impl LanternfishWarehouse {
    pub fn parse_file(file_path: &str, part1: bool) -> Self {
        let mut reader: std::io::Lines<std::io::BufReader<std::fs::File>> =
            read_lines_as_reader(file_path).expect("error reading file");
        let mut lines_grid: Vec<String> = vec![];
        loop {
            let line = if let Some(line) = reader.next() {
                line.unwrap()
            } else {
                panic!();
            };
            if line.is_empty() {
                break;
            }
            lines_grid.push(line);
        }
        let mut lines_moves: Vec<String> = vec![];
        loop {
            let line = if let Some(line) = reader.next() {
                line.unwrap()
            } else {
                break;
            };
            if line.is_empty() {
                break;
            }
            lines_moves.push(line);
        }
        let pos_max = (lines_grid[0].len() as i32, lines_grid.len() as i32);
        let pos_max = if part1 {
            pos_max
        } else {
            (pos_max.0 * 2, pos_max.1)
        };

        let (cells, pos_robot) = Self::parse_cells(lines_grid, part1);
        let moves = Self::parse_moves(lines_moves);
        Self {
            cells,
            pos_max,
            pos_robot,
            moves,
        }
    }

    fn parse_cells(
        lines_grid: Vec<String>,
        part1: bool,
    ) -> (HashMap<Position, CellContent>, Position) {
        let mut pos_robot: Option<Position> = None;
        let mut cells: HashMap<Position, CellContent> = HashMap::new();
        for (it_y, line) in lines_grid.iter().enumerate() {
            for (it_x, val) in line.chars().enumerate() {
                let pos = if part1 {
                    (it_x as i32, it_y as i32)
                } else {
                    ((it_x * 2) as i32, it_y as i32)
                };
                let content = match val {
                    '#' => CellContent::Wall,
                    'O' | '[' => CellContent::Box,
                    //']' => CellContent::BoxRight,
                    ']' => continue,
                    '.' => continue,
                    '@' => {
                        assert!(pos_robot.is_none());
                        pos_robot = Some(pos);
                        continue;
                    }
                    _ => panic!(),
                };
                if part1 {
                    cells.insert(pos, content);
                } else {
                    match content {
                        CellContent::Wall => {
                            cells.insert(pos, content.clone());
                            cells.insert((pos.0 + 1, pos.1), content);
                        }
                        CellContent::Box => {
                            cells.insert(pos, content);
                            cells.insert((pos.0 + 1, pos.1), CellContent::BoxRight);
                        }
                        _ => panic!(),
                    }
                }
            }
        }
        (cells, pos_robot.unwrap())
    }
    fn parse_moves(lines_moves: Vec<String>) -> Vec<Direction> {
        let mut moves: Vec<Direction> = vec![];
        for line in lines_moves {
            for val in line.chars() {
                let dir = match val {
                    '^' => Direction::North,
                    '>' => Direction::East,
                    'v' => Direction::South,
                    '<' => Direction::West,
                    _ => panic!(),
                };
                moves.push(dir);
            }
        }
        moves
    }
    pub fn compute_part1(&mut self) -> u64 {
        for it_direction in self.moves.clone() {
            self.exec_one_move_part1(&it_direction);
        }
        self.compute_sum_coordinates_part1()
    }
    fn exec_one_move_part1(&mut self, direction: &Direction) {
        debug!("move : direction = {:?}", direction);
        let mut attempt_moves: Vec<(Position, Position)> = vec![];
        loop {
            let last_cell = if let Some((_, last_cell)) = attempt_moves.last() {
                last_cell
            } else {
                &self.pos_robot
            };
            let next_cell = direction.move_pos(&last_cell, &self.pos_max).unwrap();
            if let Some(next_cell_type) = self.cells.get(&next_cell) {
                match next_cell_type {
                    CellContent::Wall => {
                        trace!(
                            "    next_cell = {:?} : found wall, stop (not moving)",
                            next_cell
                        );
                        return;
                    } //not possible to move
                    CellContent::Box => {
                        trace!(
                            "    next_cell = {:?} : found box, checking next...",
                            next_cell
                        );
                        attempt_moves.push((last_cell.clone(), next_cell));
                        continue;
                    }
                    _ => panic!("not expected in part 1"),
                };
            } else {
                trace!(
                    "    next_cell = {:?} : found free cell => OK (moving)",
                    next_cell
                );
                attempt_moves.push((last_cell.clone(), next_cell));
                break;
            };
        }
        trace!(
            "move : direction = {:?} => attempt_moves = {:?}",
            direction,
            attempt_moves
        );
        while !attempt_moves.is_empty() {
            let (prev_cell, next_cell) = attempt_moves.pop().unwrap();
            if attempt_moves.is_empty() {
                // robot moving
                self.pos_robot = next_cell;
            } else {
                // normal box move
                self.cells.remove(&prev_cell);
                self.cells.insert(next_cell, CellContent::Box);
            }
        }
    }
    fn compute_sum_coordinates_part1(&self) -> u64 {
        let mut result: u64 = 0;
        for (it_pos, it_type) in self.cells.iter() {
            match *it_type {
                CellContent::Wall => continue,
                CellContent::Box => {}
                _ => panic!("unexpected in part 1"),
            };
            result += it_pos.0 as u64 + 100 * it_pos.1 as u64;
        }
        result
    }
    pub fn compute_part2(&mut self) -> u64 {
        // TODO : same as part 1 except the attempt_moves is now a vector of (prev: Pos, next: Pos)
        // and each step needs to compute all moves from previous step, and ALL need to pass
        // (either free cell, either box, but not wall)
        for it_direction in self.moves.clone() {
            self.exec_one_move_part2(&it_direction);
        }
        self.compute_sum_coordinates_part2()
    }
    fn compute_sum_coordinates_part2(&self) -> u64 {
        let mut result: u64 = 0;
        for (it_pos, it_type) in self.cells.iter() {
            match *it_type {
                CellContent::Wall => continue,
                CellContent::Box => {}
                CellContent::BoxRight => continue, //managed by Box with x+1
            };
            // WTF, this should be like this if following the real problem description
            // let delta_x = min(it_pos.0 as i32, self.pos_max.0 - it_pos.0 - 2);
            // let delta_y = min(it_pos.1 as i32, self.pos_max.1 - it_pos.1 - 1);
            // however it does not work so we follow the rules from part1 :
            let delta_x = it_pos.0;
            let delta_y = it_pos.1;
            result += delta_x as u64 + 100 * delta_y as u64;
        }
        result
    }
    fn exec_one_move_part2(&mut self, direction: &Direction) {
        debug!("move : direction = {:?}", direction);
        let mut attempt_moves: Vec<HashSet<Position>> = vec![];
        {
            let mut current_cells_to_move: HashSet<Position> = HashSet::new();
            current_cells_to_move.insert(self.pos_robot);
            attempt_moves.push(current_cells_to_move);
        }
        loop {
            let current_cells_to_move = attempt_moves.last().unwrap();
            if current_cells_to_move.is_empty() {
                break;
            }
            let mut next_cells_to_move: HashSet<Position> = HashSet::new();
            for it_cell in current_cells_to_move.iter() {
                let next_cell = direction.move_pos(&it_cell, &self.pos_max).unwrap();
                let next_cell_type = if let Some(next_cell_type) = self.cells.get(&next_cell) {
                    next_cell_type
                } else {
                    continue;
                };
                match next_cell_type {
                    CellContent::Wall => {
                        trace!(
                            "    next_cell = {:?} : found wall, stop (not moving)",
                            next_cell
                        );
                        return;
                    }
                    CellContent::Box => {
                        trace!(
                            "    next_cell = {:?} : found box, checking next...",
                            next_cell
                        );
                        if matches!(direction, Direction::North | Direction::South) {
                            next_cells_to_move.insert(next_cell);
                            next_cells_to_move.insert((next_cell.0 + 1, next_cell.1));
                        } else {
                            next_cells_to_move.insert(next_cell);
                        }
                    }
                    CellContent::BoxRight => {
                        trace!(
                            "    next_cell = {:?} : found boxright, checking next...",
                            next_cell
                        );
                        if matches!(direction, Direction::North | Direction::South) {
                            next_cells_to_move.insert((next_cell.0 - 1, next_cell.1));
                            next_cells_to_move.insert(next_cell);
                        } else {
                            next_cells_to_move.insert(next_cell);
                        }
                    }
                }
            }
            attempt_moves.push(next_cells_to_move);
        }
        trace!(
            "move : direction = {:?} => attempt_moves = {:?}",
            direction,
            attempt_moves
        );
        while !attempt_moves.is_empty() {
            let current_cells_to_move = attempt_moves.pop().unwrap();
            if attempt_moves.is_empty() {
                // robot moving
                assert_eq!(current_cells_to_move.len(), 1);
                let next_cell = direction.move_pos(&self.pos_robot, &self.pos_max).unwrap();
                self.pos_robot = next_cell;
            } else {
                // normal box move
                for it_cell in current_cells_to_move {
                    let next_cell = direction.move_pos(&it_cell, &self.pos_max).unwrap();
                    let cell_type = self.cells[&it_cell].clone();
                    self.cells.remove(&it_cell);
                    self.cells.insert(next_cell, cell_type);
                }
            }
        }
    }
}

fn run_part1(file_path: &str) -> i64 {
    debug!("starting part 1");
    let mut warehouse = LanternfishWarehouse::parse_file(file_path, true);
    debug!("warehouse = {:?}", warehouse);
    let result = warehouse.compute_part1();
    debug!("finished part 1");
    result as i64
}

fn run_part2(file_path: &str) -> i64 {
    debug!("starting part 2");
    let mut warehouse = LanternfishWarehouse::parse_file(file_path, false);
    debug!("warehouse = {:?}", warehouse);
    let result = warehouse.compute_part2();
    debug!("finished part 2");
    result as i64
}

fn main() {
    env_logger::init();
    let args = Args::parse();
    let input_file = args.input.unwrap_or_else(default_input_path);
    println!("input_file = {input_file:?}");
    if args.part == None || args.part == Some(1) {
        let res = run_part1(&input_file);
        println!("Result part1 : {res}");
    }
    if args.part == None || args.part == Some(2) {
        let res = run_part2(&input_file);
        println!("Result part2 : {res}");
    }
}

#[cfg(test)]
mod tests {
    // print output :
    // cargo test -- --show-output

    use super::*;

    const TEST_FILE_0: &str = "example0.txt";
    const TEST_FILE_1: &str = "example1.txt";
    // const TEST_FILE_2: &str = "example2.txt";
    const TEST_FILE1_FINAL_PART2: &str = "example1_final_part2.txt";

    const TEST_RESULT_PART1_FILE_0: i64 = 2028;
    const TEST_RESULT_PART1_FILE_1: i64 = 10092;
    const TEST_RESULT_PART2_FILE_1: i64 = 9021;

    #[test]
    fn test_example0_part1() {
        let result = run_part1(TEST_FILE_0);
        println!("Result part1 on example : {result}");
        assert_eq!(result, TEST_RESULT_PART1_FILE_0);
    }

    #[test]
    fn test_example1_score_part2() {
        let warehouse = LanternfishWarehouse::parse_file(TEST_FILE1_FINAL_PART2, true);
        let result = warehouse.compute_sum_coordinates_part2() as i64;
        println!("Result score part 2 on example : {result}");
        assert_eq!(result, TEST_RESULT_PART2_FILE_1);
    }

    #[test]
    fn test_part1() {
        let result = run_part1(TEST_FILE_1);
        println!("Result part1 on example : {result}");
        assert_eq!(result, TEST_RESULT_PART1_FILE_1);
    }

    #[test]
    fn test_part2() {
        let result = run_part2(TEST_FILE_1);
        println!("Result part2 on example : {result}");
        assert_eq!(result, TEST_RESULT_PART2_FILE_1);
    }
}
