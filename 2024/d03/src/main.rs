use aoc_common::read_file::read_lines_as_reader;
use clap::Parser;
use log::{debug, error, trace};

// https://doc.rust-lang.org/cargo/reference/environment-variables.html
//const PKG_VERSION: &str = env!("CARGO_PKG_VERSION");
const AOC_YEAR: u32 = 2024;
const PKG_NAME: &str = env!("CARGO_PKG_NAME");
//const PKG_REPOSITORY: &str = env!("CARGO_PKG_REPOSITORY");

fn default_input_path() -> String {
    // TODO : this function should be moved to crate "common"
    format!(
        "{}/git/advent-of-code-inputs/{}/{}/input.txt",
        std::env::var("HOME").expect("error getting variable HOME"),
        AOC_YEAR,
        PKG_NAME
    )
    .to_string()
}

/// compute puzzle of the day
#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
#[command(after_help = "To select log level, use env var RUST_LOG=level")]
struct Args {
    /// run only part 1 or part 2
    #[arg(short, long)]
    part: Option<usize>,

    /// Input file (default to input file downloaded in input repo)
    #[arg(short, long)]
    input: Option<String>,
}

fn compute_mul(mut line: String) -> u32 {
    let mut sum_mul: u32 = 0;
    trace!("    compute_mul() - line:    {}", line);
    loop {
        let idx = line.find("mul(");
        if idx == None {
            trace!("        not finding substr \"mul(\" : break");
            break;
        }
        trace!("        found substr \"mul(\" at idx {:?}", idx);
        line = line.split_at(idx.unwrap() + 4).1.to_string();
        trace!("        line:    {}", line);
        let idx = line.find(")");
        if idx == None {
            continue;
        }
        let idx = idx.unwrap();
        trace!("        found substr \")\" at idx {}", idx);
        let operators = line[..idx].to_string();
        trace!("        operators = {}", operators);
        let idx_comma = operators.find(",");
        if let Some(idx_comma) = idx_comma {
            let num1 = operators.split_at(idx_comma).0.parse::<u32>();
            let num2 = operators.split_at(idx_comma + 1).1.parse::<u32>();
            match (num1, num2) {
                (Ok(num1), Ok(num2)) => {
                    trace!("        computing {} * {}", num1, num2);
                    sum_mul += num1 * num2;
                    line = line.split_at(idx + 1).1.to_string();
                }
                // maybe there were other substr here
                _ => {}
            }
        }
        trace!("    line = {}", line);
    }
    sum_mul
}

fn run_part1(file_path: &str) -> i64 {
    debug!("starting part 1");
    let reader = read_lines_as_reader(file_path).expect("error reading file");
    let mut sum_mul: u32 = 0;
    for line in reader {
        sum_mul += compute_mul(line.unwrap());
    }
    debug!("finished part 1");
    sum_mul as i64
}

const LEN_STR_DO: usize = 4; //"do()"
const LEN_STR_DONT: usize = 7; //"don't()"

fn run_part2(file_path: &str) -> i64 {
    debug!("starting part 2");
    let reader = read_lines_as_reader(file_path).expect("error reading file");
    let mut sum_mul: u32 = 0;
    let mut do_enabled = true; // At the beginning of the program, mul instructions are enabled.
    for line in reader {
        let mut line = line.unwrap();
        trace!("sum_mul:    {}", sum_mul);
        trace!("line:    {}", line);
        loop {
            trace!("    do_enabled = {} , line:    {}", do_enabled, line);
            if do_enabled {
                if let Some(idx_next_dont) = line.find("don't()") {
                    sum_mul += compute_mul(line.split_at(idx_next_dont).0.to_string());
                    line = line.split_at(idx_next_dont + LEN_STR_DONT).1.to_string();
                    do_enabled = false;
                } else {
                    sum_mul += compute_mul(line);
                    break;
                }
            } else {
                if let Some(idx_next_do) = line.find("do()") {
                    line = line.split_at(idx_next_do + LEN_STR_DO).1.to_string();
                    do_enabled = true;
                } else {
                    break;
                }
            }
        }
    }
    // 87543713 => That's not the right answer; your answer is too high.
    // 79845780 => OK
    debug!("finished part 2");
    sum_mul as i64
}

fn main() {
    env_logger::init();
    let args = Args::parse();
    let input_file = args.input.unwrap_or_else(default_input_path);
    println!("input_file = {input_file:?}");
    if args.part == None || args.part == Some(1) {
        let res = run_part1(&input_file);
        println!("Result part1 : {res}");
    }
    if args.part == None || args.part == Some(2) {
        let res = run_part2(&input_file);
        println!("Result part2 : {res}");
    }
}

#[cfg(test)]
mod tests {
    // print output :
    // cargo test -- --show-output

    use super::*;

    const TEST_FILE_1: &str = "example1.txt";
    const TEST_FILE_2: &str = "example2.txt";

    const TEST_RESULT_PART1_FILE_1: i64 = 161;
    const TEST_RESULT_PART2_FILE_2: i64 = 48;

    #[test]
    fn test_part1() {
        let result = run_part1(TEST_FILE_1);
        println!("Result part1 on example : {result}");
        assert_eq!(result, TEST_RESULT_PART1_FILE_1);
    }

    #[test]
    fn test_part2() {
        let result = run_part2(TEST_FILE_2);
        println!("Result part2 on example : {result}");
        assert_eq!(result, TEST_RESULT_PART2_FILE_2);
    }
}
