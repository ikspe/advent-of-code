import argparse

class Aoc2024D03:

    def __init__(self, verbose: bool) -> None:
        self._verbose = verbose

    def _parse_mul(self, line: str) -> int:
        tokens_line = line.split("mul(")
        sum_line = 0
        for it_token in tokens_line:
            operators = it_token.split(")", maxsplit=1)[0]
            operators = operators.split(",", maxsplit=1)
            if len(operators) != 2:
                continue
            try:
                val1 = int(operators[0])
                val2 = int(operators[1])
            except ValueError:
                continue
            sum_line += (val1 * val2)
        return sum_line

    def run_part1(self, file_path: str) -> int:
        with open(file_path) as f:
            sum_val = 0
            for line in f:
                line = line.strip()
                val = self._parse_mul(line)
                sum_val += val
        return sum_val

    def run_part2(self, file_path: str) -> int:
        with open(file_path) as f:
            sum_val = 0
            do_enabled = True
            for line in f:
                line = line.strip()
                val_line = 0
                while True:
                    if do_enabled:
                        tokens = line.split("don't()", maxsplit=1)
                        val = self._parse_mul(tokens[0])
                        val_line += val
                        if len(tokens) == 2:
                            line = tokens[1]
                            do_enabled = False
                        else:
                            break
                    else:
                        tokens = line.split("do()", maxsplit=1)
                        if len(tokens) == 2:
                            line = tokens[1]
                            do_enabled = True
                        else:
                            break
                sum_val += val_line
        # 109350076 : That's not the right answer; your answer is too high.
        # 87543713  : That's not the right answer; your answer is too high.
        # 79845780  : OK
        return sum_val


UNIT_TEST_EXPECTED_PART1 = 161
UNIT_TEST_EXPECTED_PART2 = 48

def main():
    parser = argparse.ArgumentParser()
    args = parser.add_argument("--unit-tests", help="Run unit-tests ; this is incompatible with --input", default=False, action="store_true")
    args = parser.add_argument("--part", type=int)
    args = parser.add_argument("--input")
    args = parser.add_argument("-v", "--verbose", default=False, action="store_true")

    args = parser.parse_args()

    aoc = Aoc2024D03(args.verbose)

    if args.unit_tests:
        if args.input:
            raise ValueError("Please don't specify value --input for --unit-tests")
        res_part1 = aoc.run_part1("./example1.txt")
        res_part2 = aoc.run_part2("./example2.txt")
        print(f"[UNIT TEST] part 1 result = {res_part1} / expected = {UNIT_TEST_EXPECTED_PART1}")
        print(f"[UNIT TEST] part 2 result = {res_part2} / expected = {UNIT_TEST_EXPECTED_PART2}")
        return
    
    if args.part not in (None, 1, 2):
        raise ValueError("Incorrect argument: --part")

    if args.part in (None, 1):
        res_part1 = aoc.run_part1(args.input)
        print(f"part 1 result = {res_part1}")
    if args.part in (None, 2):
        res_part2 = aoc.run_part2(args.input)
        print(f"part 2 result = {res_part2}")


if __name__ == "__main__":
    main()
