// extracted from 2024/d18

use crate::direction::Direction;
use log::{trace, warn};
use std::collections::{HashMap, HashSet};

pub type Position = (i32, i32);

#[derive(Debug)]
pub struct AStarSolver<'w> {
    walls: &'w HashSet<Position>,
    width: i32,
    height: i32,
}

impl<'w> AStarSolver<'w> {
    pub fn new(walls: &'w HashSet<Position>, width: i32, height: i32) -> Self {
        Self {
            walls,
            width,
            height,
        }
    }

    /// return the node in openSet having the lowest fScore[] value
    fn get_node_lowest_score(
        open_set: &HashSet<Position>,
        f_score: &HashMap<Position, usize>,
    ) -> Position {
        let mut result_node: Option<Position> = None;
        let mut lowest_score: Option<usize> = None;
        for it_node in open_set.iter() {
            let it_score = f_score[it_node];
            let is_lowest = if let Some(current_lowest_score) = lowest_score {
                it_score < current_lowest_score
            } else {
                true
            };
            if is_lowest {
                result_node = Some(it_node.clone());
                lowest_score = Some(it_score);
            }
        }
        result_node.unwrap()
    }

    // A* finds a path from start to goal.
    // h is the heuristic function. h(n) estimates the cost to reach goal from node n.
    pub fn find_shortest_path(&self, start_pos: &Position, end_pos: &Position) -> Option<usize> {
        // \o/   Dijkstra is back => let's go for A*
        let start_pos = *start_pos;
        let end_pos = *end_pos;
        //let mut current_path = vec![(0 as i16, 0 as i16)];

        // openSet := {start}
        let mut open_set: HashSet<Position> = HashSet::new();
        open_set.insert(start_pos);

        // cameFrom := an empty map
        let mut came_from: HashMap<Position, Position> = HashMap::new();

        // For node n, gScore[n] is the currently known cost of the cheapest path from start to n.
        // gScore := map with default value of Infinity
        // gScore[start] := 0
        let mut g_score: HashMap<Position, usize> = HashMap::new();
        g_score.insert(start_pos, 0);

        // For node n, fScore[n] := gScore[n] + h(n). fScore[n] represents our current best guess as to
        // how cheap a path could be from start to finish if it goes through n.
        // fScore := map with default value of Infinity
        // fScore[start] := h(start)
        let mut f_score: HashMap<Position, usize> = HashMap::new();
        f_score.insert(
            start_pos,
            // very rough estimation of fScore using Manhattan distance
            (end_pos.0.abs_diff(start_pos.0) + (end_pos.1).abs_diff(start_pos.1)) as usize,
        );

        let max_pos_i32 = (self.width, self.height);

        while !open_set.is_empty() {
            // current := the node in openSet having the lowest fScore[] value
            let current = Self::get_node_lowest_score(&open_set, &f_score);
            trace!("");
            trace!(
                "current = {:?} from open_set (len = {})",
                current,
                open_set.len()
            );

            // if current = goal
            //     return reconstruct_path(cameFrom, current)
            if current == end_pos {
                return Some(g_score[&current]);
            }
            open_set.remove(&current);

            let current_i32 = (current.0 as i32, current.1 as i32);
            let g_score_current = g_score[&current];
            trace!(
                "current = {:?} / g_score_current = {}",
                current,
                g_score_current
            );
            // for each neighbor of current
            for direction in Direction::iterator() {
                let neighbor =
                    if let Some(neighbor_i32) = direction.move_pos(&current_i32, &max_pos_i32) {
                        neighbor_i32
                    } else {
                        continue; //out of map
                    };
                if self.walls.contains(&neighbor) {
                    trace!(
                        "current = {:?} / direction = {:?} => neighbor = {:?} - ignoring corrupted",
                        current,
                        direction,
                        neighbor
                    );
                    continue; // corrupted : unreachable
                }

                // d(current,neighbor) is the weight of the edge from current to neighbor
                // tentative_gScore is the distance from start to the neighbor through current
                // tentative_gScore := gScore[current] + d(current, neighbor)
                let tentative_g_score = g_score_current + 1;
                // if tentative_gScore < gScore[neighbor]
                //     // This path to neighbor is better than any previous one. Record it!
                let needs_to_record = if let Some(previous_score_neighbor) = g_score.get(&neighbor)
                {
                    tentative_g_score < *previous_score_neighbor
                } else {
                    true
                };
                if needs_to_record {
                    // cameFrom[neighbor] := current
                    // gScore[neighbor] := tentative_gScore
                    // fScore[neighbor] := tentative_gScore + h(neighbor)
                    // if neighbor not in openSet
                    //     openSet.add(neighbor)
                    trace!(
                        "current = {:?} / direction = {:?} => neighbor = {:?} - recording best finding (tentative_g_score = {})",
                        current,
                        direction,
                        neighbor,
                        tentative_g_score
                    );
                    came_from.insert(neighbor, current);
                    g_score.insert(neighbor, tentative_g_score);
                    f_score.insert(neighbor,
                        tentative_g_score +
                        // very rough estimation of fScore using Manhattan distance
                        (end_pos.0.abs_diff(start_pos.0) + (end_pos.1).abs_diff(start_pos.1)) as usize
                    );
                    if !open_set.contains(&neighbor) {
                        open_set.insert(neighbor);
                    }
                } else {
                    trace!(
                        "current = {:?} / direction = {:?} => neighbor = {:?} - ignored (already better score - tentative_g_score = {})",
                        current,
                        direction,
                        neighbor,
                        tentative_g_score
                    );
                }
            }
        }
        warn!("did not find solution");
        None
    }
}
