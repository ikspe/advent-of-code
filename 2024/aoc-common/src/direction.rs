/// Definition of type representing a direction
use std::slice::Iter;

/// direction... with diagonals :D
#[derive(Debug)]
pub enum DirectionWithDiagonals {
    Right,
    BottomRight,
    Bottom,
    BottomLeft,
    Left,
    TopLeft,
    Top,
    TopRight,
}

impl DirectionWithDiagonals {
    pub fn iterator() -> Iter<'static, DirectionWithDiagonals> {
        static DIRECTIONS: [DirectionWithDiagonals; 8] = [
            DirectionWithDiagonals::Right,
            DirectionWithDiagonals::BottomRight,
            DirectionWithDiagonals::Bottom,
            DirectionWithDiagonals::BottomLeft,
            DirectionWithDiagonals::Left,
            DirectionWithDiagonals::TopLeft,
            DirectionWithDiagonals::Top,
            DirectionWithDiagonals::TopRight,
        ];
        DIRECTIONS.iter()
    }
}

/// direction... without diagonals :D
/// PartialEq, Hash, Eq : required for HashSet
#[derive(Debug, PartialEq, Hash, Eq, Clone, Copy)]
pub enum Direction {
    East,
    South,
    West,
    North,
}

impl Direction {
    pub fn iterator() -> Iter<'static, Direction> {
        static DIRECTIONS: [Direction; 4] = [
            Direction::East,
            Direction::South,
            Direction::West,
            Direction::North,
        ];
        DIRECTIONS.iter()
    }
    pub fn next(&self) -> Self {
        match self {
            Direction::East => Direction::South,
            Direction::South => Direction::West,
            Direction::West => Direction::North,
            Direction::North => Direction::East,
        }
    }
    pub fn move_pos(&self, pos: &(i32, i32), max: &(i32, i32)) -> Option<(i32, i32)> {
        let new_pos = match self {
            Direction::East => (pos.0 + 1, pos.1),
            Direction::South => (pos.0, pos.1 + 1),
            Direction::West => (pos.0 - 1, pos.1),
            Direction::North => (pos.0, pos.1 - 1),
        };
        if new_pos.0 < 0 || new_pos.1 < 0 || new_pos.0 >= max.0 || new_pos.1 >= max.1 {
            return None;
        }
        Some(new_pos)
    }
}
