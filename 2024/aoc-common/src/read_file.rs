use std::{
    fs::{self, File},
    io::{self, BufRead},
    path::Path,
};

pub fn read_file_full(file_path: &str) -> String {
    return fs::read_to_string(file_path).expect("Should have been able to read the file");
}

pub fn read_file_lines(file_path: &str) -> Vec<String> {
    // File hosts.txt must exist in the current path
    let buf_lines =
        read_lines_as_reader(file_path).expect("Should have been able to read the file");
    // flatten(): Consumes the iterator, returns an (Optional) String
    return buf_lines.flatten().collect();
}

// The output is wrapped in a Result to allow matching on errors.
// Returns an Iterator to the Reader of the lines of the file.
pub fn read_lines_as_reader<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where
    P: AsRef<Path>,
{
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}

#[cfg(test)]
mod tests {
    // print output :
    // cargo test -- --show-output

    use super::*;

    const TEST_DATA_FILE_01: &str = "test-data/file-01-simple.txt";
    const TEST_DATA_FILE_02: &str = "test-data/file-02-multilines.txt";

    #[test]
    fn test_read_file_full() {
        println!("reading file {} ...", TEST_DATA_FILE_01);
        let result = read_file_full(TEST_DATA_FILE_01);
        println!("result = {result}");
        assert_eq!(result, "this is a very simple file");

        println!("reading file {} ...", TEST_DATA_FILE_02);
        let result = read_file_full(TEST_DATA_FILE_02);
        println!("result = {result}");
        assert!(result.starts_with("I'm nobody! Who are you?\nAre you"));
        assert!(result.ends_with("don't tell!\n[...]"));
    }

    #[test]
    fn test_read_file_lines() {
        println!("reading file {} ...", TEST_DATA_FILE_01);
        let result = read_file_lines(TEST_DATA_FILE_01);
        println!("result = {result:?}");
        assert_eq!(result.len(), 1);
        assert_eq!(result[0], "this is a very simple file");

        println!("reading file {} ...", TEST_DATA_FILE_02);
        let result = read_file_lines(TEST_DATA_FILE_02);
        println!("result = {result:?}");
        assert_eq!(result.len(), 5);
        assert_eq!(result[0], "I'm nobody! Who are you?");
        assert_eq!(result[2], "");
        assert_eq!(result[4], "[...]");
    }
}
