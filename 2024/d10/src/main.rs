use std::collections::{HashMap, HashSet};

use aoc_common::direction::Direction;
use aoc_common::read_file::read_lines_as_reader;
use clap::Parser;
use log::{debug, info, trace};

// https://doc.rust-lang.org/cargo/reference/environment-variables.html
//const PKG_VERSION: &str = env!("CARGO_PKG_VERSION");
const AOC_YEAR: u32 = 2024;
const PKG_NAME: &str = env!("CARGO_PKG_NAME");
//const PKG_REPOSITORY: &str = env!("CARGO_PKG_REPOSITORY");

fn default_input_path() -> String {
    // TODO : this function should be moved to crate "common"
    format!(
        "{}/git/advent-of-code-inputs/{}/{}/input.txt",
        std::env::var("HOME").expect("error getting variable HOME"),
        AOC_YEAR,
        PKG_NAME
    )
    .to_string()
}

/// compute puzzle of the day
#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
#[command(after_help = "To select log level, use env var RUST_LOG=level")]
struct Args {
    /// run only part 1 or part 2
    #[arg(short, long)]
    part: Option<usize>,

    /// Input file (default to input file downloaded in input repo)
    #[arg(short, long)]
    input: Option<String>,
}

type Position = (i32, i32);
// arbitrary decided the first direction = East / last = North
const LAST_DIRECTION: Direction = Direction::North;
static TRAIL_HEAD: u16 = 0;
static TRAIL_END: u16 = 9;

#[derive(Debug, Clone)]
struct TopographicMap {
    // signed integers are more practical to detect out of map
    positions: HashMap<Position, u16>,
    pub max: Position,
}

fn debug_current_trail(trail: &Vec<(Position, Direction)>) -> Vec<Position> {
    trail.iter().map(|pos_dir| pos_dir.0).collect()
}

enum ScoreOrRating {
    Score,
    Rating,
}

impl TopographicMap {
    pub fn parse_file(file_path: &str) -> Self {
        let reader: std::io::Lines<std::io::BufReader<std::fs::File>> =
            read_lines_as_reader(file_path).expect("error reading file");
        let mut list_lines: Vec<String> = vec![];
        for line in reader {
            list_lines.push(line.unwrap());
        }
        TopographicMap::parse_lines(list_lines)
    }
    fn parse_lines(lines: Vec<String>) -> Self {
        const RADIX: u32 = 10;
        let width = lines[0].len() as i32;
        let height = lines.len() as i32;
        let mut positions: HashMap<(i32, i32), u16> = HashMap::new();
        for (it_y, it_line) in lines.iter().enumerate() {
            for (it_x, val) in it_line.chars().enumerate() {
                positions.insert(
                    (it_x as i32, it_y as i32),
                    // '.' is a debug value
                    if val == '.' {
                        99
                    } else {
                        val.to_digit(RADIX).unwrap() as u16
                    },
                );
            }
        }
        Self {
            positions,
            max: (width, height),
        }
    }

    ////////////////
    ///
    /// YES...
    /// I KNOW...
    /// THIS IS A TERRIBLE DESIGN AND
    /// IT DESERVES TO BE REFACTORED.
    /// BUT... IT WORKS :-)
    ///
    ////////////////
    fn compute_sum_scores_one_trailhead(
        &self,
        pos_trailhead: &(i32, i32),
        score_or_rating: &ScoreOrRating,
    ) -> u32 {
        let mut trailend: HashSet<Position> = HashSet::new();
        let mut trailend_count: u32 = 0;
        // each element : position + next direction to take
        let mut explored_pos: HashSet<Position> = HashSet::new();
        let mut current_trail: Vec<(Position, Direction)> = vec![(*pos_trailhead, Direction::East)];
        explored_pos.insert(current_trail[0].0);
        loop {
            trace!(
                "len = {}, {:?}",
                current_trail.len(),
                debug_current_trail(&current_trail)
            );
            let current_pos = current_trail.last_mut().unwrap();
            trace!(
                "pos_trailhead = {:?} ; current_pos = {:?} : val = {}",
                pos_trailhead,
                current_pos,
                self.positions[&current_pos.0]
            );
            let rewind = if self.positions[&current_pos.0] == TRAIL_END {
                debug!(
                    "pos_trailhead = {:?} ; current_pos = {:?} : FOUND TRAILEND",
                    pos_trailhead, current_pos
                );
                trailend.insert(current_pos.0);
                trailend_count += 1;
                true // rewind
            } else {
                let next_pos = current_pos.1.move_pos(&current_pos.0, &self.max);
                trace!(
                    "pos_trailhead = {:?} ; current_pos = {:?} : next_pos = {:?}",
                    pos_trailhead,
                    current_pos,
                    next_pos,
                );
                let next_pos = if let Some(next_pos) = next_pos {
                    if self.positions[&next_pos] != self.positions[&current_pos.0] + 1 {
                        None // next position : incorrect height
                    } else {
                        match *score_or_rating {
                            ScoreOrRating::Score => {
                                if explored_pos.contains(&next_pos) {
                                    None // already tried position : try another direction
                                } else {
                                    Some(next_pos)
                                }
                            }
                            ScoreOrRating::Rating => Some(next_pos),
                        }
                    }
                } else {
                    None // out of map : try another direction
                };
                if let Some(next_pos) = next_pos {
                    current_trail.push((next_pos, Direction::East)); // first direction
                    explored_pos.insert(next_pos);
                    false
                } else {
                    // next position is invalid : try alternative if possible
                    match current_pos.1 {
                        Direction::North => {
                            true // already tried last possible directions => rewind
                        }
                        _ => {
                            current_pos.1 = current_pos.1.next(); // try another direction
                            false
                        }
                    }
                }
            };
            if rewind {
                trace!(
                    "    REWIND start, trail = {:?}",
                    debug_current_trail(&current_trail)
                );
                loop {
                    trace!(
                        "    REWIND      , trail = {:?}",
                        debug_current_trail(&current_trail)
                    );
                    if current_trail.is_empty() {
                        break;
                    }
                    if self.positions[&current_trail.last().unwrap().0] == TRAIL_END {
                        current_trail.pop();
                        continue;
                    }
                    let current_pos = current_trail.last_mut().unwrap();
                    match current_pos.1 {
                        Direction::North => {
                            // already tried last possible directions => continue rewind
                            current_trail.pop();
                        }
                        _ => {
                            current_pos.1 = current_pos.1.next();
                            break;
                        }
                    }
                }
                trace!(
                    "    REWIND end  , trail = {:?}",
                    debug_current_trail(&current_trail)
                );
                if current_trail.is_empty() {
                    break;
                }
            }
        }
        info!(
            "trailhead at {:?} : score = {} / rating = {}",
            pos_trailhead,
            trailend.len(),
            trailend_count,
        );
        match score_or_rating {
            ScoreOrRating::Score => trailend.len() as u32,
            ScoreOrRating::Rating => trailend_count,
        }
    }

    pub fn compute_sum_scores_all_trailheads(&self, score_or_rating: ScoreOrRating) -> u32 {
        let mut result: u32 = 0;
        for (pos, val) in self.positions.iter() {
            if *val == TRAIL_HEAD {
                result += self.compute_sum_scores_one_trailhead(pos, &score_or_rating);
            }
        }
        result
    }
}

fn run_part1(file_path: &str) -> i64 {
    debug!("starting part 1");
    let topo_map = TopographicMap::parse_file(file_path);
    let result: u32 = topo_map.compute_sum_scores_all_trailheads(ScoreOrRating::Score);
    debug!("finished part 1");
    result as i64
}

fn run_part2(file_path: &str) -> i64 {
    debug!("starting part 2");
    let topo_map = TopographicMap::parse_file(file_path);
    let result: u32 = topo_map.compute_sum_scores_all_trailheads(ScoreOrRating::Rating);
    debug!("finished part 2");
    result as i64
}

fn main() {
    env_logger::init();
    let args = Args::parse();
    let input_file = args.input.unwrap_or_else(default_input_path);
    println!("input_file = {input_file:?}");
    if args.part == None || args.part == Some(1) {
        let res = run_part1(&input_file);
        println!("Result part1 : {res}");
    }
    if args.part == None || args.part == Some(2) {
        let res = run_part2(&input_file);
        println!("Result part2 : {res}");
    }
}

#[cfg(test)]
mod tests {
    // print output :
    // cargo test -- --show-output

    use super::*;

    const TEST_FILE_1: &str = "example1.txt";
    const TEST_FILE_2: &str = "example2.txt";

    const TEST_RESULT_PART1_FILE_1: i64 = 36;
    const TEST_RESULT_PART2_FILE_1: i64 = 81;
    const TEST_RESULT_PART2_FILE_2: i64 = 3;

    #[test]
    fn test_part1() {
        let result = run_part1(TEST_FILE_1);
        println!("Result part1 on example : {result}");
        assert_eq!(result, TEST_RESULT_PART1_FILE_1);
    }

    #[test]
    fn test_part2() {
        let result = run_part2(TEST_FILE_1);
        println!("Result part2 on example : {result}");
        assert_eq!(result, TEST_RESULT_PART2_FILE_1);
    }

    #[test]
    fn test_example2_part2() {
        let result = run_part2(TEST_FILE_2);
        println!("Result part2 on example : {result}");
        assert_eq!(result, TEST_RESULT_PART2_FILE_2);
    }
}
