use std::collections::HashSet;

use aoc_common::algos::a_star::{AStarSolver, Position};
use aoc_common::log::{debug, error, trace};
use aoc_common::read_file::read_lines_as_reader;
use clap::Parser;

// https://doc.rust-lang.org/cargo/reference/environment-variables.html
//const PKG_VERSION: &str = env!("CARGO_PKG_VERSION");
const AOC_YEAR: u32 = 2024;
const PKG_NAME: &str = env!("CARGO_PKG_NAME");
//const PKG_REPOSITORY: &str = env!("CARGO_PKG_REPOSITORY");

fn default_input_path() -> String {
    // TODO : this function should be moved to crate "common"
    format!(
        "{}/git/advent-of-code-inputs/{}/{}/input.txt",
        std::env::var("HOME").expect("error getting variable HOME"),
        AOC_YEAR,
        PKG_NAME
    )
    .to_string()
}

/// compute puzzle of the day
#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
#[command(after_help = "To select log level, use env var RUST_LOG=level")]
struct Args {
    /// run only part 1 or part 2
    #[arg(short, long)]
    part: Option<usize>,

    /// Input file (default to input file downloaded in input repo)
    #[arg(short, long)]
    input: Option<String>,
}

#[derive(Debug)]
struct MapRacetrack {
    walls: HashSet<Position>, // includes outer bounds as it's necessary for A* algo
    start: Position,
    end: Position,
    size: Position,
}

impl MapRacetrack {
    pub fn parse_file(file_path: &str) -> Self {
        let reader: std::io::Lines<std::io::BufReader<std::fs::File>> =
            read_lines_as_reader(file_path).expect("error reading file");
        let lines: Vec<_> = reader.map(|x| x.unwrap()).collect();
        let mut pos_start: Option<Position> = None;
        let mut pos_end: Option<Position> = None;
        let mut walls: HashSet<Position> = HashSet::new();
        let size = (lines[0].len() as i32, lines.len() as i32);
        for (it_y, line) in lines.iter().enumerate() {
            for (it_x, val) in line.chars().enumerate() {
                let pos = (it_x as i32, it_y as i32);
                match val {
                    '#' => walls.insert(pos),
                    '.' => continue,
                    'S' => {
                        assert!(pos_start.is_none());
                        pos_start = Some(pos);
                        continue;
                    }
                    'E' => {
                        assert!(pos_end.is_none());
                        pos_end = Some(pos);
                        continue;
                    }
                    _ => panic!(),
                };
            }
        }
        Self {
            walls,
            start: pos_start.unwrap(),
            end: pos_end.unwrap(),
            size: size,
        }
    }
    pub fn find_best_path(&self) -> usize {
        let a_star_solver = AStarSolver::new(&self.walls, self.size.0, self.size.1);
        a_star_solver
            .find_shortest_path(&self.start, &&self.end)
            .unwrap()
    }
    pub fn compute_part1(&self, threshold_at_least_savings: usize) -> usize {
        let normal_best_path = self.find_best_path();
        let mut walls_cloned = self.walls.clone();
        let mut count_at_least_savings: usize = 0;
        debug!("find_shortest_path = {}", normal_best_path);
        for it_pos in self.walls.iter() {
            if it_pos.0 == 0
                || it_pos.1 == 0
                || it_pos.0 == self.size.0 - 1
                || it_pos.1 == self.size.1
            {
                continue;
            }
            // remove 1 element
            walls_cloned.remove(it_pos);
            let a_star_solver = AStarSolver::new(&walls_cloned, self.size.0, self.size.1);
            let cheating_best_path = a_star_solver
                .find_shortest_path(&self.start, &self.end)
                .unwrap();
            // add back element for next iteration
            walls_cloned.insert(*it_pos);
            let cheat_savings = normal_best_path - cheating_best_path;
            trace!(
                "    removed {:?} : cheating_best_path = {} => saving {}",
                it_pos,
                cheating_best_path,
                cheat_savings
            );
            if cheat_savings >= threshold_at_least_savings {
                count_at_least_savings += 1;
            }
        }
        count_at_least_savings
    }
}

fn run_part1(file_path: &str) -> i64 {
    debug!("starting part 1");
    let racetrack = MapRacetrack::parse_file(file_path);
    let result = racetrack.compute_part1(100);
    debug!("finished part 1");
    result as i64
}

fn run_part2(file_path: &str) -> i64 {
    debug!("starting part 2");
    let mut result: u32 = 0;
    todo!();
    debug!("finished part 2");
    result as i64
}

fn main() {
    env_logger::init();
    let args = Args::parse();
    let input_file = args.input.unwrap_or_else(default_input_path);
    println!("input_file = {input_file:?}");
    if args.part == None || args.part == Some(1) {
        let res = run_part1(&input_file);
        println!("Result part1 : {res}");
    }
    if args.part == None || args.part == Some(2) {
        let res = run_part2(&input_file);
        println!("Result part2 : {res}");
    }
}

#[cfg(test)]
mod tests {
    // print output :
    // cargo test -- --show-output

    use super::*;

    const TEST_FILE_1: &str = "example1.txt";
    // const TEST_FILE_2: &str = "example2.txt";

    const TEST_BASIC_BEST_PATH_FILE_1: usize = 84;

    fn get_result_part1_file1() -> i64 {
        0
    }
    fn get_result_part1_file1_threshold_1() -> usize {
        44
        // 14 + 14 + 2 + 4 + 2 + 3 + 1 + 1 + 1 + 1 + 1
    }
    fn get_result_part2_file1() -> i64 {
        todo!()
    }

    #[test]
    fn test_basic_best_path() {
        let racetrack = MapRacetrack::parse_file(TEST_FILE_1);
        let result = racetrack.find_best_path();
        let expected_result = TEST_BASIC_BEST_PATH_FILE_1;
        println!("Result best path on example : {result} / expected result = {expected_result}");
        assert_eq!(result, TEST_BASIC_BEST_PATH_FILE_1);
    }

    #[test]
    fn test_part1() {
        let result = run_part1(TEST_FILE_1);
        let expected_result = get_result_part1_file1();
        println!("Result part1 on example : {result} / expected result = {expected_result}");
        assert_eq!(result, expected_result);
    }

    #[test]
    fn test_part1_threshold_1() {
        let racetrack = MapRacetrack::parse_file(TEST_FILE_1);
        let result = racetrack.compute_part1(1);
        let expected_result = get_result_part1_file1_threshold_1();
        println!(
            "Result at least 1 savings on example : {result} / expected result = {expected_result}"
        );
        assert_eq!(result, expected_result);
    }

    #[test]
    fn test_part2() {
        let result = run_part2(TEST_FILE_1);
        let expected_result = get_result_part2_file1();
        println!("Result part2 on example : {result} / expected result = {expected_result}");
        assert_eq!(result, expected_result);
    }
}
