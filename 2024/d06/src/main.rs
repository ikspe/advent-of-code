use aoc_common::read_file::read_lines_as_reader;
use clap::Parser;
use log::{debug, error, trace};
use std::collections::HashSet;

// https://doc.rust-lang.org/cargo/reference/environment-variables.html
//const PKG_VERSION: &str = env!("CARGO_PKG_VERSION");
const AOC_YEAR: u32 = 2024;
const PKG_NAME: &str = env!("CARGO_PKG_NAME");
//const PKG_REPOSITORY: &str = env!("CARGO_PKG_REPOSITORY");

fn default_input_path() -> String {
    // TODO : this function should be moved to crate "common"
    format!(
        "{}/git/advent-of-code-inputs/{}/{}/input.txt",
        std::env::var("HOME").expect("error getting variable HOME"),
        AOC_YEAR,
        PKG_NAME
    )
    .to_string()
}

#[derive(Debug, Clone)]
struct MapPositions {
    obstructions: HashSet<(usize, usize)>,
    pub width: usize,
    pub height: usize,
}

#[derive(Hash, PartialEq, Eq, Debug, Clone)]
enum Direction {
    Right,
    Bottom,
    Left,
    Top,
}

// The easiest way to use HashSet with a custom type is to derive Eq and Hash.
// We must also derive PartialEq, which is required if Eq is derived.
#[derive(Hash, Eq, PartialEq, Debug, Clone)]
struct GuardPosition {
    x: usize,
    y: usize,
    direction: Direction,
}

impl MapPositions {
    pub fn parse_file(file_path: &str) -> (Self, GuardPosition) {
        let reader: std::io::Lines<std::io::BufReader<std::fs::File>> =
            read_lines_as_reader(file_path).expect("error reading file");
        let mut list_lines: Vec<String> = vec![];
        for line in reader {
            list_lines.push(line.unwrap());
        }
        MapPositions::parse_map(list_lines)
    }
    pub fn parse_map(lines: Vec<String>) -> (Self, GuardPosition) {
        let width = lines[0].len();
        let height = lines.len();
        let mut obstructions = HashSet::new();
        let mut guard_position: Option<GuardPosition> = None;
        for (it_y, it_line) in lines.iter().enumerate() {
            for (it_x, val) in it_line.chars().enumerate() {
                if val == '#' {
                    obstructions.insert((it_x, it_y));
                } else if val == '^' {
                    if guard_position.is_some() {
                        panic!(
                            "Guard detected at 2 positions : existing = {:?} , new = ({}, {})",
                            guard_position, it_x, it_y
                        )
                    }
                    guard_position = Some(GuardPosition {
                        x: it_x,
                        y: it_y,
                        direction: Direction::Top,
                    });
                }
            }
        }
        assert!(guard_position.is_some());
        (
            Self {
                obstructions: obstructions,
                width: width,
                height: height,
            },
            guard_position.unwrap(),
        )
    }
    pub fn move_guard(&self, guard: GuardPosition) -> Option<GuardPosition> {
        // practical to use negative values to detect if out of the map
        let start_x = guard.x as i32;
        let start_y = guard.y as i32;
        let new_pos = match guard.direction {
            Direction::Top => (start_x, start_y - 1),
            Direction::Right => (start_x + 1, start_y),
            Direction::Bottom => (start_x, start_y + 1),
            Direction::Left => (start_x - 1, start_y),
        };
        if new_pos.0 < 0
            || new_pos.1 < 0
            || new_pos.0 as usize >= self.width
            || new_pos.1 as usize >= self.height
        {
            return None; // leaved map
        }
        if self
            .obstructions
            .contains(&(new_pos.0 as usize, new_pos.1 as usize))
        {
            Some(GuardPosition {
                x: guard.x,
                y: guard.y,
                direction: match guard.direction {
                    Direction::Right => Direction::Bottom,
                    Direction::Bottom => Direction::Left,
                    Direction::Left => Direction::Top,
                    Direction::Top => Direction::Right,
                },
            })
        } else {
            Some(GuardPosition {
                x: new_pos.0 as usize,
                y: new_pos.1 as usize,
                direction: guard.direction,
            })
        }
    }

    fn compute_guard_path(
        &self,
        guard_position: &GuardPosition,
        check_if_loop: bool,
    ) -> (HashSet<GuardPosition>, bool) {
        let mut all_guard_positions = HashSet::new();
        let mut guard_position = guard_position.to_owned();
        trace!("map_positions = {:?}", self);
        trace!("guard_position = {:?}", guard_position);
        all_guard_positions.insert(guard_position.clone());
        loop {
            let new_guard_pos = if let Some(new_guard_pos) = self.move_guard(guard_position) {
                new_guard_pos
            } else {
                trace!(
                    "STOP : new guard pos = None => out of map (all_guard_positions.len() = {})",
                    all_guard_positions.len()
                );
                return (all_guard_positions, false);
            };
            if check_if_loop {
                if all_guard_positions.contains(&new_guard_pos) {
                    trace!(
                        "STOP : found a loop at guard pos = {:?} (all_guard_positions.len() = {})",
                        new_guard_pos,
                        all_guard_positions.len()
                    );
                    return (all_guard_positions, true);
                }
            }
            trace!("moving guard : position = {:?}", new_guard_pos);
            guard_position = new_guard_pos.clone();
            all_guard_positions.insert(new_guard_pos);
        }
    }
}

/// compute puzzle of the day
#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
#[command(after_help = "To select log level, use env var RUST_LOG=level")]
struct Args {
    /// run only part 1 or part 2
    #[arg(short, long)]
    part: Option<usize>,

    /// Input file (default to input file downloaded in input repo)
    #[arg(short, long)]
    input: Option<String>,
}

fn run_part1(file_path: &str) -> i64 {
    debug!("starting part 1");
    let (map_positions, guard_position) = MapPositions::parse_file(file_path);
    let (all_guard_positions, is_loop) = map_positions.compute_guard_path(&guard_position, false);
    debug!(
        "Finished moving guard : we have been visiting all_guard_positions.len() = {}",
        all_guard_positions.len()
    );
    assert_eq!(is_loop, false);
    // now we want all the positions on the map the guard has been, no matter the direction !
    let visited_positions: HashSet<_> = all_guard_positions
        .iter()
        .map(|pos| (pos.x, pos.y))
        .collect();
    debug!("visited_positions.len() = {}", visited_positions.len());
    debug!("finished part 1");
    visited_positions.len() as i64
}

fn run_part2(file_path: &str) -> i64 {
    debug!("starting part 2");
    let (map_positions, guard_position) = MapPositions::parse_file(file_path);
    let mut all_positions: HashSet<(usize, usize)> = HashSet::new();
    for y in (0..map_positions.height) {
        all_positions.extend((0..map_positions.width).into_iter().map(|x| (x, y)));
    }
    // let candidate_obstructions = {
    //     debug!("all_positions.len() = {}", all_positions.len());
    //     debug!(
    //         "map_positions.obstructions.len() = {}",
    //         map_positions.obstructions.len()
    //     );
    //     debug!("pos_guard is alone => len() = 1");
    //     let mut candidate_obstructions = all_positions;
    //     for it_pos in map_positions.obstructions.iter() {
    //         candidate_obstructions.remove(&it_pos);
    //     }
    //     candidate_obstructions.remove(&(guard_position.x, guard_position.y));
    //     debug!(
    //         "candidate_obstructions.len() = {}",
    //         candidate_obstructions.len()
    //     );
    //     candidate_obstructions
    // };
    let candidate_obstructions = {
        // compute default guard path
        let (all_guard_positions, is_loop) =
            map_positions.compute_guard_path(&guard_position, false);
        assert_eq!(is_loop, false);
        // now we want all the positions on the map the guard has been, no matter the direction !
        let visited_positions: HashSet<_> = all_guard_positions
            .iter()
            .map(|pos| (pos.x, pos.y))
            .collect();
        visited_positions
    };
    log::info!(
        "candidate_obstructions.len() = {}",
        candidate_obstructions.len()
    );
    let mut valid_obstructions = 0;
    let mut map_positions_candidate = map_positions.clone(); //optimization : avoid cloning for each candidate
    for it_pos_obs in candidate_obstructions {
        debug!("Computing obstruction = {:?}", it_pos_obs);
        map_positions_candidate.obstructions.insert(it_pos_obs);
        let (_, is_loop) = map_positions_candidate.compute_guard_path(&guard_position, true);
        map_positions_candidate.obstructions.remove(&it_pos_obs);
        debug!("... done : is_loop = {}", is_loop);
        if is_loop {
            valid_obstructions += 1;
        }
    }

    debug!("finished part 2");
    valid_obstructions as i64
}

fn main() {
    env_logger::init();
    let args = Args::parse();
    let input_file = args.input.unwrap_or_else(default_input_path);
    println!("input_file = {input_file:?}");
    if args.part == None || args.part == Some(1) {
        let res = run_part1(&input_file);
        println!("Result part1 : {res}");
    }
    if args.part == None || args.part == Some(2) {
        let res = run_part2(&input_file);
        println!("Result part2 : {res}");
    }
}

#[cfg(test)]
mod tests {
    // print output :
    // cargo test -- --show-output

    use super::*;

    const TEST_FILE_1: &str = "example1.txt";
    // const TEST_FILE_2: &str = "example2.txt";

    const TEST_RESULT_PART1_FILE_1: i64 = 41;
    const TEST_RESULT_PART2_FILE_2: i64 = 6;

    #[test]
    fn test_part1() {
        let result = run_part1(TEST_FILE_1);
        println!("Result part1 on example : {result}");
        assert_eq!(result, TEST_RESULT_PART1_FILE_1);
    }

    #[test]
    fn test_part2() {
        let result = run_part2(TEST_FILE_1);
        println!("Result part2 on example : {result}");
        assert_eq!(result, TEST_RESULT_PART2_FILE_2);
    }
}
