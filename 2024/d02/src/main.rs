use aoc_common::read_file::read_lines_as_reader;
use clap::Parser;
use log::{debug, error, trace};

// https://doc.rust-lang.org/cargo/reference/environment-variables.html
//const PKG_VERSION: &str = env!("CARGO_PKG_VERSION");
const AOC_YEAR: u32 = 2024;
const PKG_NAME: &str = env!("CARGO_PKG_NAME");
//const PKG_REPOSITORY: &str = env!("CARGO_PKG_REPOSITORY");

fn default_input_path() -> String {
    // TODO : this function should be moved to crate "common"
    format!(
        "{}/git/advent-of-code-inputs/{}/{}/input.txt",
        std::env::var("HOME").expect("error getting variable HOME"),
        AOC_YEAR,
        PKG_NAME
    )
    .to_string()
}

/// compute puzzle of the day
#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
#[command(after_help = "To select log level, use env var RUST_LOG=level")]
struct Args {
    /// run only part 1 or part 2
    #[arg(short, long)]
    part: Option<usize>,

    /// Input file (default to input file downloaded in input repo)
    #[arg(short, long)]
    input: Option<String>,
}

fn is_line_safe<'a, T>(mut tokens: T) -> bool
where
    T: Iterator<Item = &'a str>,
{
    let mut cur_val = tokens.next().unwrap().parse::<i32>().unwrap();
    trace!("cur_val = {}", cur_val);
    let mut is_increase: Option<bool> = None;
    for it_tok in tokens {
        let new_val = it_tok.parse::<i32>().unwrap();
        trace!("new_val = {}", new_val);
        let diff = new_val.abs_diff(cur_val);
        trace!("diff = {}", diff);
        if !(1 <= diff && diff <= 3) {
            trace!("abs(diff) not in [1,3], stopping line");
            return false;
        }

        let new_direction = (new_val - cur_val) > 0;
        match is_increase {
            None => is_increase = Some(new_direction),
            Some(cur_direction) => {
                if cur_direction != new_direction {
                    trace!(
                        "tokens not all increasing/dicreasing (cur_direction = {} / new_direction = {})",
                        cur_direction,
                        new_direction
                    );
                    return false;
                }
            }
        }
        cur_val = new_val;
    }
    true
}

fn run_part1(file_path: &str) -> i64 {
    debug!("starting part 1");
    let reader = read_lines_as_reader(file_path).expect("error reading file");
    let mut count_line_safe: u32 = 0;
    for line in reader {
        let line = line.unwrap();
        debug!("line = {}", line);
        let tokens = line.split_whitespace();
        if is_line_safe(tokens) {
            debug!("Safe");
            count_line_safe += 1;
        }
        debug!("Unsafe");
    }
    debug!("finished part 1");
    count_line_safe as i64
}

fn run_part2(file_path: &str) -> i64 {
    debug!("starting part 2");
    let reader = read_lines_as_reader(file_path).expect("error reading file");
    let mut count_line_safe: u32 = 0;
    'loop_lines: for line in reader {
        let line = line.unwrap();
        debug!("line = {}", line);
        let tokens: Vec<_> = line.split_whitespace().collect();
        let iterator_tokens = tokens.clone().into_iter();
        if is_line_safe(iterator_tokens) {
            debug!("Safe without removing any level");
            count_line_safe += 1;
            continue;
        } else {
            for i in 0..=tokens.len() - 1 {
                let mut tokens_one_less = tokens.clone();
                let removed = tokens_one_less.remove(i);
                debug!("    try line minus 1 token = {:?}", tokens_one_less);
                let iterator_tokens = tokens_one_less.into_iter();
                if is_line_safe(iterator_tokens) {
                    debug!("Safe by removing the {}-th level, {}.", i, removed);
                    count_line_safe += 1;
                    continue 'loop_lines;
                }
            }
            debug!("Unsafe regardless of which level is removed.");
        }
    }
    // 327 => That's not the right answer; your answer is too low.
    debug!("finished part 2");
    count_line_safe as i64
}

fn main() {
    env_logger::init();
    let args = Args::parse();
    let input_file = args.input.unwrap_or_else(default_input_path);
    println!("input_file = {input_file:?}");
    if args.part == None || args.part == Some(1) {
        let res = run_part1(&input_file);
        println!("Result part1 : {res}");
    }
    if args.part == None || args.part == Some(2) {
        let res = run_part2(&input_file);
        println!("Result part2 : {res}");
    }
}

#[cfg(test)]
mod tests {
    // print output :
    // cargo test -- --show-output

    use super::*;

    const TEST_FILE_1: &str = "example1.txt";
    // const TEST_FILE_2: &str = "example2.txt";

    const TEST_RESULT_PART1_FILE_1: i64 = 2;
    const TEST_RESULT_PART2_FILE_1: i64 = 4;

    #[test]
    fn test_part1() {
        let result = run_part1(TEST_FILE_1);
        println!("Result part1 on example : {result}");
        assert_eq!(result, TEST_RESULT_PART1_FILE_1);
    }

    #[test]
    fn test_part2() {
        let result = run_part2(TEST_FILE_1);
        println!("Result part2 on example : {result}");
        assert_eq!(result, TEST_RESULT_PART2_FILE_1);
    }
}
