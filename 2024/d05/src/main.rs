use std::collections::{HashMap, HashSet};

use aoc_common::read_file::read_lines_as_reader;
use clap::Parser;
use log::{debug, info, trace};

// https://doc.rust-lang.org/cargo/reference/environment-variables.html
//const PKG_VERSION: &str = env!("CARGO_PKG_VERSION");
const AOC_YEAR: u32 = 2024;
const PKG_NAME: &str = env!("CARGO_PKG_NAME");
//const PKG_REPOSITORY: &str = env!("CARGO_PKG_REPOSITORY");

fn default_input_path() -> String {
    // TODO : this function should be moved to crate "common"
    format!(
        "{}/git/advent-of-code-inputs/{}/{}/input.txt",
        std::env::var("HOME").expect("error getting variable HOME"),
        AOC_YEAR,
        PKG_NAME
    )
    .to_string()
}

/// compute puzzle of the day
#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
#[command(after_help = "To select log level, use env var RUST_LOG=level")]
struct Args {
    /// run only part 1 or part 2
    #[arg(short, long)]
    part: Option<usize>,

    /// Input file (default to input file downloaded in input repo)
    #[arg(short, long)]
    input: Option<String>,
}

#[derive(Debug)]
struct ElfInput {
    section1: Vec<(u32, u32)>,
    ordering_rules_direct: HashMap<u32, HashSet<u32>>,
    update_sequence_req: Vec<Vec<u32>>,
}

impl ElfInput {
    pub fn from_lines(lines: Vec<String>) -> Self {
        let mut section1: Vec<(u32, u32)> = vec![];
        let mut update_sequence_req: Vec<Vec<u32>> = vec![];

        let mut state_in_ordering_rules = true;
        for line in lines {
            if state_in_ordering_rules {
                if line.is_empty() {
                    state_in_ordering_rules = false;
                    continue;
                }
                let mut split_line = line.split("|");
                let lhs = split_line.next().unwrap();
                let rhs = split_line.next().unwrap();
                assert!(split_line.next().is_none());
                section1.push((lhs.parse().unwrap(), rhs.parse().unwrap()));
            } else {
                if line.is_empty() {
                    continue;
                }
                update_sequence_req
                    .push(line.split(",").map(|x| x.parse::<u32>().unwrap()).collect());
            }
        }
        let ordering_rules_direct = Self::build_ordering_rules_direct(&section1);
        Self {
            section1,
            ordering_rules_direct,
            update_sequence_req,
        }
    }
    fn build_ordering_rules_direct(section1: &Vec<(u32, u32)>) -> HashMap<u32, HashSet<u32>> {
        // note : this is pretty naive approach, not taking into consideration all indirect so it's likely not enough
        let mut ordering_rules_direct: HashMap<u32, HashSet<u32>> = HashMap::new();
        for it_basic_rule in section1 {
            if let Some(next_pages) = ordering_rules_direct.get_mut(&it_basic_rule.0) {
                next_pages.insert(it_basic_rule.1);
            } else {
                let mut next_pages: HashSet<u32> = HashSet::new();
                next_pages.insert(it_basic_rule.1);
                ordering_rules_direct.insert(it_basic_rule.0, next_pages);
            }
        }
        ordering_rules_direct
    }
    pub fn is_update_matching_rules(&self, candidate_update: &[u32]) -> bool {
        if candidate_update.len() == 1 {
            return true;
        }
        let candidate_first = &candidate_update[0];
        let candidate_others = &candidate_update[1..];
        let maybe_rules_first = self.ordering_rules_direct.get(candidate_first);
        for it_others in candidate_others {
            if let Some(rules_first) = maybe_rules_first {
                if rules_first.contains(it_others) {
                    continue;
                }
            }
            if let Some(rules_others) = self.ordering_rules_direct.get(it_others) {
                if rules_others.contains(candidate_first) {
                    return false;
                }
            }
        }
        return self.is_update_matching_rules(candidate_others);
    }
    pub fn reorganize_update(&self, incorrect_sequence: &[u32]) -> Vec<u32> {
        let mut remaining_numbers = incorrect_sequence.to_vec();
        let mut fixed_sequence: Vec<u32> = vec![];
        loop {
            if remaining_numbers.is_empty() {
                break;
            }
            let mut idx_to_remove: Option<usize> = None;
            for (pos_first, it_first) in remaining_numbers.iter().enumerate() {
                let maybe_rules_first = self.ordering_rules_direct.get(it_first);
                let mut is_first_ok = true;
                for (pos_other, it_other) in remaining_numbers.iter().enumerate() {
                    if pos_first == pos_other {
                        continue;
                    }
                    if let Some(rules_first) = maybe_rules_first {
                        if rules_first.contains(it_other) {
                            continue;
                        }
                    }
                    if let Some(rules_other) = self.ordering_rules_direct.get(it_other) {
                        if rules_other.contains(it_first) {
                            is_first_ok = false;
                            break;
                        }
                    }
                }
                if is_first_ok {
                    fixed_sequence.push(*it_first);
                    idx_to_remove = Some(pos_first);
                    break;
                }
            }
            if let Some(idx_to_remove) = idx_to_remove {
                remaining_numbers.remove(idx_to_remove);
            } else {
                unreachable!();
            }
        }
        fixed_sequence
    }
}

fn run_part1(file_path: &str) -> i64 {
    debug!("starting part 1");
    let mut result: u32 = 0;
    let mut lines: Vec<String> = vec![];
    let reader: std::io::Lines<std::io::BufReader<std::fs::File>> =
        read_lines_as_reader(file_path).expect("error reading file");

    for line in reader {
        lines.push(line.unwrap());
    }
    let elf_input = ElfInput::from_lines(lines);
    for it_update in elf_input.update_sequence_req.iter() {
        debug!("================");
        debug!("Treating update candidate {:?} ...", it_update);
        if elf_input.is_update_matching_rules(&it_update) {
            let middle_num = it_update[(it_update.len() - 1) / 2];
            trace!(
                "Found valid ordering : {:?} => using middle number = {}",
                it_update,
                middle_num
            );
            result += middle_num;
        }
    }
    debug!("finished part 1");
    result as i64
    // 9566 => That's not the right answer; your answer is too high.
}

fn run_part2(file_path: &str) -> i64 {
    debug!("starting part 2");
    let mut result: u32 = 0;
    let mut lines: Vec<String> = vec![];
    let reader: std::io::Lines<std::io::BufReader<std::fs::File>> =
        read_lines_as_reader(file_path).expect("error reading file");

    for line in reader {
        lines.push(line.unwrap());
    }
    let elf_input = ElfInput::from_lines(lines);
    for it_update in elf_input.update_sequence_req.iter() {
        debug!("================");
        debug!("Treating update candidate {:?} ...", it_update);
        if elf_input.is_update_matching_rules(&it_update) {
            debug!("Found valid ordering : {:?} => SKIPPING", it_update,);
            continue;
        }
        let fixed_sequence = elf_input.reorganize_update(&it_update);
        let middle_num = fixed_sequence[(fixed_sequence.len() - 1) / 2];
        trace!(
            "Reorganize into valid ordering : {:?} => using middle number = {}",
            fixed_sequence,
            middle_num
        );
        result += middle_num;
    }
    debug!("finished part 2");
    result as i64
}

fn main() {
    env_logger::init();
    let args = Args::parse();
    let input_file = args.input.unwrap_or_else(default_input_path);
    println!("input_file = {input_file:?}");
    if args.part == None || args.part == Some(1) {
        let res = run_part1(&input_file);
        println!("Result part1 : {res}");
    }
    if args.part == None || args.part == Some(2) {
        let res = run_part2(&input_file);
        println!("Result part2 : {res}");
    }
}

#[cfg(test)]
mod tests {
    // print output :
    // cargo test -- --show-output

    use super::*;

    const TEST_FILE_1: &str = "example1.txt";
    // const TEST_FILE_2: &str = "example2.txt";

    const TEST_RESULT_PART1_FILE_1: i64 = 143;
    const TEST_RESULT_PART2_FILE_1: i64 = 123;

    #[test]
    fn test_part1() {
        let result = run_part1(TEST_FILE_1);
        println!("Result part1 on example : {result}");
        assert_eq!(result, TEST_RESULT_PART1_FILE_1);
    }

    #[test]
    fn test_part2() {
        let result = run_part2(TEST_FILE_1);
        println!("Result part2 on example : {result}");
        assert_eq!(result, TEST_RESULT_PART2_FILE_1);
    }
}
