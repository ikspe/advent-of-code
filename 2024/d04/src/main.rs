use aoc_common::direction::DirectionWithDiagonals;
use aoc_common::read_file::read_lines_as_reader;
use clap::Parser;
use log::{debug, info};
use std::collections::{HashMap, HashSet};

// https://doc.rust-lang.org/cargo/reference/environment-variables.html
//const PKG_VERSION: &str = env!("CARGO_PKG_VERSION");
const AOC_YEAR: u32 = 2024;
const PKG_NAME: &str = env!("CARGO_PKG_NAME");
//const PKG_REPOSITORY: &str = env!("CARGO_PKG_REPOSITORY");

fn default_input_path() -> String {
    // TODO : this function should be moved to crate "common"
    format!(
        "{}/git/advent-of-code-inputs/{}/{}/input.txt",
        std::env::var("HOME").expect("error getting variable HOME"),
        AOC_YEAR,
        PKG_NAME
    )
    .to_string()
}

fn gen_direction_indexes(direction: &DirectionWithDiagonals) -> &[(i32, i32); PATTERN_LEN] {
    // it's easier if we can use negative numbers for checking out of map
    match direction {
        DirectionWithDiagonals::Right => &[(0, 0), (1, 0), (2, 0), (3, 0)],
        DirectionWithDiagonals::BottomRight => &[(0, 0), (1, 1), (2, 2), (3, 3)],
        DirectionWithDiagonals::Bottom => &[(0, 0), (0, 1), (0, 2), (0, 3)],
        DirectionWithDiagonals::BottomLeft => &[(0, 0), (-1, 1), (-2, 2), (-3, 3)],
        DirectionWithDiagonals::Left => &[(0, 0), (-1, 0), (-2, 0), (-3, 0)],
        DirectionWithDiagonals::TopLeft => &[(0, 0), (-1, -1), (-2, -2), (-3, -3)],
        DirectionWithDiagonals::Top => &[(0, 0), (0, -1), (0, -2), (0, -3)],
        DirectionWithDiagonals::TopRight => &[(0, 0), (1, -1), (2, -2), (3, -3)],
    }
}

/// compute puzzle of the day
#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
#[command(after_help = "To select log level, use env var RUST_LOG=level")]
struct Args {
    /// run only part 1 or part 2
    #[arg(short, long)]
    part: Option<usize>,

    /// Input file (default to input file downloaded in input repo)
    #[arg(short, long)]
    input: Option<String>,
}

const PATTERN_TO_MATCH: &str = "XMAS";
const PATTERN_LEN: usize = 4;

struct GridLetters {
    letters: HashMap<(i32, i32), char>,
    // it's easier if we can use negative numbers for checking out of map
    pub width: i32,
    pub height: i32,
}

impl GridLetters {
    pub fn new(lines: Vec<String>) -> Self {
        let width = lines[0].len();
        let height = lines.len();
        let mut letters: HashMap<(i32, i32), char> = HashMap::new();
        for (y, line) in lines.iter().enumerate() {
            for (x, val) in line.chars().enumerate() {
                letters.insert((x as i32, y as i32), val);
            }
        }
        Self {
            letters: letters,
            width: width as i32,
            height: height as i32,
        }
    }

    pub fn count_xmas_word_at_position(&self, pos: &(i32, i32), val: char) -> usize {
        if val != 'X' {
            return 0;
        }
        // let mut word_positions: HashSet<(i32, i32)> = HashSet::new();
        let mut result = 0;
        'loop_directons: for direction in DirectionWithDiagonals::iterator() {
            let direction_indexes = gen_direction_indexes(direction);
            let (end_x, end_y) = direction_indexes
                .last()
                .and_then(|pos_end| Some((pos.0 + pos_end.0, pos.1 + pos_end.1)))
                .unwrap();
            if end_x < 0 || end_x >= self.width || end_y < 0 || end_y >= self.height {
                continue 'loop_directons;
            }
            // idx 0 already checked => start at 1
            for (i, val) in PATTERN_TO_MATCH[1..].chars().enumerate() {
                let dir_idx = direction_indexes[i + 1];
                let dir_idx = (pos.0 + dir_idx.0, pos.1 + dir_idx.1);
                if *self.letters.get(&dir_idx).unwrap() != val {
                    continue 'loop_directons;
                }
            }
            debug!(
                "    Found matching string at ({:?}) for direction = {:?}",
                pos, direction
            );
            // for (inc_x, inc_y) in direction_indexes {
            //     let computed_pos = (pos.0 + inc_x, pos.1 + inc_y);
            //     debug!(
            //         "    Found matching string at ({:?}) for direction = {:?} => saving pos = {:?}",
            //         pos, direction, computed_pos
            //     );
            //     word_positions.insert(computed_pos);
            // }
            result += 1;
        }
        // Some(word_positions)
        result
    }

    pub fn count_cross_mas_puzzle(&self, pos_and_val: &(&(i32, i32), &char)) -> usize {
        if *pos_and_val.1 != 'A' {
            return 0;
        }
        // 1st diagonal
        {
            let pos1 = (pos_and_val.0 .0 - 1, pos_and_val.0 .1 - 1);
            if pos1.0 < 0 || pos1.1 < 0 {
                // out of map
                return 0;
            }
            let pos2 = (pos_and_val.0 .0 + 1, pos_and_val.0 .1 + 1);
            if pos2.0 >= self.width || pos2.1 >= self.height {
                // out of map
                return 0;
            }
            let val1 = self.letters[&pos1];
            let val2 = self.letters[&pos2];
            if !(val1 == 'M' && val2 == 'S' || val1 == 'S' && val2 == 'M') {
                return 0;
            }
        }
        // 2nd diagonal
        {
            let pos1 = (pos_and_val.0 .0 - 1, pos_and_val.0 .1 + 1);
            let pos2 = (pos_and_val.0 .0 + 1, pos_and_val.0 .1 - 1);
            // no need to check if pos3 and pos4 are on the map : they are because previous pos1 and pos2 are !
            let val1 = self.letters[&pos1];
            let val2 = self.letters[&pos2];
            if !(val1 == 'M' && val2 == 'S' || val1 == 'S' && val2 == 'M') {
                return 0;
            }
        }
        debug!("    Found matching X-MAS at ({:?})", pos_and_val.0);
        1
    }
}

fn run_part1(file_path: &str) -> i64 {
    debug!("starting part 1");
    let reader: std::io::Lines<std::io::BufReader<std::fs::File>> =
        read_lines_as_reader(file_path).expect("error reading file");
    let mut list_lines: Vec<String> = vec![];
    for line in reader {
        list_lines.push(line.unwrap());
    }
    let grid_letters = GridLetters::new(list_lines);
    let mut result: usize = 0;
    for it_letter in grid_letters.letters.iter() {
        result += grid_letters.count_xmas_word_at_position(it_letter.0, *it_letter.1);
    }
    debug!("finished part 1");
    result as i64
}

fn run_part2(file_path: &str) -> i64 {
    debug!("starting part 2");
    let reader: std::io::Lines<std::io::BufReader<std::fs::File>> =
        read_lines_as_reader(file_path).expect("error reading file");
    let mut list_lines: Vec<String> = vec![];
    for line in reader {
        list_lines.push(line.unwrap());
    }
    let grid_letters = GridLetters::new(list_lines);
    let mut result: usize = 0;
    for it_letter in grid_letters.letters.iter() {
        result += grid_letters.count_cross_mas_puzzle(&it_letter);
    }
    debug!("finished part 2");
    result as i64
}

fn main() {
    env_logger::init();
    let args = Args::parse();
    let input_file = args.input.unwrap_or_else(default_input_path);
    println!("input_file = {input_file:?}");
    if args.part == None || args.part == Some(1) {
        let res = run_part1(&input_file);
        println!("Result part1 : {res}");
    }
    if args.part == None || args.part == Some(2) {
        let res = run_part2(&input_file);
        println!("Result part2 : {res}");
    }
}

#[cfg(test)]
mod tests {
    // print output :
    // cargo test -- --show-output

    use super::*;

    const TEST_FILE_1: &str = "example1.txt";
    // const TEST_FILE_2: &str = "example2.txt";

    const TEST_RESULT_PART1_FILE_1: i64 = 18;
    const TEST_RESULT_PART2_FILE_1: i64 = 9;

    #[test]
    fn test_access_string() {
        let tmp = "XMAS".to_string();
        println!("{}", tmp);
        assert_eq!(tmp.chars().nth(0).unwrap(), 'X');
        assert_eq!(tmp.chars().nth(1).unwrap(), 'M');
        assert_eq!(tmp.chars().nth(2).unwrap(), '?');
    }

    #[test]
    fn test_part1() {
        let result = run_part1(TEST_FILE_1);
        println!("Result part1 on example : {result}");
        assert_eq!(result, TEST_RESULT_PART1_FILE_1);
    }

    #[test]
    fn test_part2() {
        let result = run_part2(TEST_FILE_1);
        println!("Result part2 on example : {result}");
        assert_eq!(result, TEST_RESULT_PART2_FILE_1);
    }
}
