use aoc_common::read_file::read_lines_as_reader;
use clap::Parser;
use log::{debug, trace};

// https://doc.rust-lang.org/cargo/reference/environment-variables.html
//const PKG_VERSION: &str = env!("CARGO_PKG_VERSION");
const AOC_YEAR: u32 = 2024;
const PKG_NAME: &str = env!("CARGO_PKG_NAME");
//const PKG_REPOSITORY: &str = env!("CARGO_PKG_REPOSITORY");

fn default_input_path() -> String {
    // TODO : this function should be moved to crate "common"
    format!(
        "{}/git/advent-of-code-inputs/{}/{}/input.txt",
        std::env::var("HOME").expect("error getting variable HOME"),
        AOC_YEAR,
        PKG_NAME
    )
    .to_string()
}

/// compute puzzle of the day
#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
#[command(after_help = "To select log level, use env var RUST_LOG=level")]
struct Args {
    /// run only part 1 or part 2
    #[arg(short, long)]
    part: Option<usize>,

    /// Input file (default to input file downloaded in input repo)
    #[arg(short, long)]
    input: Option<String>,
}

const MAX_BUTTON_PRESSED_PART_1: u32 = 100;
const COST_BUTTON_A: i64 = 3;
const COST_BUTTON_B: i64 = 1;
const PRIZE_CONVERSION_ERROR_PART2: i64 = 10000000000000;

type Position = (i64, i64); // int instead of unisged is preventing panic for substract with overflow

#[derive(Debug, Clone)]
struct ClawMachine {
    button_a: Position,
    button_b: Position,
    prize: Position,
}

impl ClawMachine {
    pub fn compute_min_tokens_win_prize(&self) -> Option<i64> {
        debug!("Computing for {:?}", self);
        // we could be clever and do some computation
        // but we know there is at most 100 attempts to win a prize,
        // so let's not try it to hard and let's brute force it, YOLO !
        let mut maybe_min_cost: Option<i64> = None;
        for times_button_a in 0..=MAX_BUTTON_PRESSED_PART_1 {
            let remaining_dist = (
                self.prize.0 - times_button_a as i64 * self.button_a.0,
                self.prize.1 - times_button_a as i64 * self.button_a.1,
            );
            let times_button_b: u32 = if remaining_dist == (0, 0) {
                0
            } else {
                // check if button B can bring us to remaining distance :
                if remaining_dist.0 * self.button_b.1 != remaining_dist.1 * self.button_b.0 {
                    continue;
                }
                if self.button_b.0 != 0 {
                    (remaining_dist.0 / self.button_b.0) as u32
                } else {
                    (remaining_dist.1 / self.button_b.1) as u32
                }
            };
            if times_button_b > MAX_BUTTON_PRESSED_PART_1 {
                debug!(
                    "        times_button_b ({}) > MAX_BUTTON_PRESSED_PART_1",
                    times_button_b
                );
                continue;
            }
            let remaining_dist = (
                remaining_dist.0 - times_button_b as i64 * self.button_b.0,
                remaining_dist.1 - times_button_b as i64 * self.button_b.1,
            );
            if remaining_dist != (0, 0) {
                continue;
            }
            let curr_cost =
                times_button_a as i64 * COST_BUTTON_A + times_button_b as i64 * COST_BUTTON_B;
            if let Some(min_cost) = maybe_min_cost {
                if curr_cost < min_cost {
                    maybe_min_cost = Some(curr_cost);
                }
            } else {
                maybe_min_cost = Some(curr_cost);
            }
            /*
            if times_button_a as i32 * self.button_a.0 > self.prize.0
                || times_button_a as i32 * self.button_a.1 > self.prize.1
            {
                break;
            }
            for times_button_b in 0..=MAX_BUTTON_PRESSED_PART_1 {
                let final_pos = (
                    times_button_a as i32 * self.button_a.0
                        + times_button_b as i32 * self.button_b.0,
                    times_button_a as i32 * self.button_a.1
                        + times_button_b as i32 * self.button_b.1,
                );
                if final_pos == self.prize {
                    debug!(
                        "    Found final_pos (= {:?}) == prize (= {:?}) for : times_button_a = {} / times_button_b = {}",
                        final_pos, self.prize, times_button_a, times_button_b
                    );
                    let curr_cost = COST_BUTTON_A * times_button_a + COST_BUTTON_B * times_button_b;
                    if let Some(min_cost) = maybe_min_cost {
                        if curr_cost < min_cost {
                            maybe_min_cost = Some(curr_cost);
                        } else {
                            maybe_min_cost = Some(min_cost)
                        }
                    } else {
                        maybe_min_cost = Some(curr_cost);
                    }
                }
            }
             */
        }
        debug!("    {:?} => found min cost = {:?}", self, maybe_min_cost);
        maybe_min_cost
    }
    pub fn compute_min_tokens_win_prize_correct_parsing_error(
        &self,
        prize_parsing_error: i64,
    ) -> Option<i64> {
        // //original equations
        // tba * bax + tbb * bbx = px
        // tba * bay + tbb * bby = py
        //
        // // step 1
        // // if bax != 0
        // tba = (px - tbb * bbx) / bax
        // tba * bay + tbb * bby = py
        //
        // // step 2
        // tba = (px - tbb * bbx) / bax
        // (px - tbb * bbx) / bax * bay + tbb * bby = py
        //
        // // step 3
        // tba = (px - tbb * bbx) / bax
        // tbb * (bby - bbx / bax * bay) = py - px / bax * bay
        //
        // // final : step 4
        // tba = (px - tbb * bbx) / bax
        // tbb = (py - px / bax * bay) / (bby - bbx / bax * bay)
        let prize = (
            self.prize.0 + prize_parsing_error,
            self.prize.1 + prize_parsing_error,
        );
        debug!("{:?} / prize = {:?}", self, prize);
        if self.button_a.0 * self.button_b.1 == self.button_a.1 * self.button_b.0 {
            // particular case : button A and button B have proportional values
            // if they are also aligned with prize, so there are multiple solutions
            unimplemented!(
                "{:?} / prize = {:?} => buttons A and B are proportional !!",
                self,
                prize
            )
        } else {
            if self.button_a.0 == 0 {
                unimplemented!()
            } else {
                // cf final = step 4
                let times_button_b = (prize.1 - prize.0 / self.button_a.0 * self.button_a.1)
                    / (self.button_b.1 - self.button_b.0 / self.button_a.0 * self.button_a.1);
                let times_button_a = (prize.0 - times_button_b * self.button_b.0) / self.button_a.0;
                if self.button_a.0 * times_button_a + self.button_b.0 * times_button_b != prize.0
                    || self.button_a.1 * times_button_a + self.button_b.1 * times_button_b
                        != prize.1
                {
                    return None;
                }
                return Some(COST_BUTTON_A * times_button_a + COST_BUTTON_B * times_button_b);
            }
        }
        // panic!()
    }
}

#[derive(Debug, Clone)]
struct ResortLobby {
    claw_machines: Vec<ClawMachine>,
}

impl ResortLobby {
    fn parse_pos_line(line: String, expected_prefix: &str, expected_modifier: char) -> Position {
        let mut line_split = line.splitn(2, ": ");
        assert_eq!(line_split.next().unwrap(), expected_prefix);
        let positions = line_split.next().unwrap();
        assert!(line_split.next().is_none());
        let mut positions_split = positions.splitn(2, ", ");
        let pos_x = positions_split.next().unwrap();
        let pos_y = positions_split.next().unwrap();
        assert!(positions_split.next().is_none());
        assert_eq!(pos_x.chars().nth(0).unwrap(), 'X');
        assert_eq!(pos_x.chars().nth(1).unwrap(), expected_modifier);
        assert_eq!(pos_y.chars().nth(0).unwrap(), 'Y');
        assert_eq!(pos_y.chars().nth(1).unwrap(), expected_modifier);
        (
            pos_x[2..].parse::<i64>().unwrap(),
            pos_y[2..].parse::<i64>().unwrap(),
        )
    }
    pub fn parse_file(file_path: &str) -> Self {
        let mut reader: std::io::Lines<std::io::BufReader<std::fs::File>> =
            read_lines_as_reader(file_path).expect("error reading file");
        let mut claw_machines: Vec<ClawMachine> = vec![];
        loop {
            let line1 = if let Some(line1) = reader.next() {
                line1.unwrap()
            } else {
                break;
            };
            let line2 = reader.next().unwrap().unwrap();
            let line3 = reader.next().unwrap().unwrap();

            let button_a = Self::parse_pos_line(line1, "Button A", '+');
            let button_b = Self::parse_pos_line(line2, "Button B", '+');
            let pos_priz = Self::parse_pos_line(line3, "Prize", '=');
            claw_machines.push(ClawMachine {
                button_a,
                button_b,
                prize: pos_priz,
            });
            if let Some(line_break) = reader.next() {
                assert!(line_break.unwrap().is_empty())
            } else {
                break;
            }
        }
        Self { claw_machines }
    }

    pub fn compute_min_tokens_win_all_prizes(&self) -> i64 {
        self.claw_machines
            .iter()
            .map(|x| x.compute_min_tokens_win_prize().unwrap_or(0))
            .sum()
    }
    pub fn compute_min_tokens_win_all_prizes_part2(&self) -> i64 {
        self.claw_machines
            .iter()
            .map(|x| {
                x.compute_min_tokens_win_prize_correct_parsing_error(PRIZE_CONVERSION_ERROR_PART2)
                    .unwrap_or(0)
            })
            .sum()
    }
}

fn run_part1(file_path: &str) -> i64 {
    debug!("starting part 1");
    let resort_lobby = ResortLobby::parse_file(file_path);
    trace!("resort_lobby = {:?}", resort_lobby);
    let result = resort_lobby.compute_min_tokens_win_all_prizes();
    debug!("finished part 1");
    result as i64
}

fn run_part2(file_path: &str) -> i64 {
    debug!("starting part 2");
    let resort_lobby = ResortLobby::parse_file(file_path);
    trace!("resort_lobby = {:?}", resort_lobby);
    let result = resort_lobby.compute_min_tokens_win_all_prizes_part2();
    debug!("finished part 2");
    result as i64
}

fn main() {
    env_logger::init();
    let args = Args::parse();
    let input_file = args.input.unwrap_or_else(default_input_path);
    println!("input_file = {input_file:?}");
    if args.part == None || args.part == Some(1) {
        let res = run_part1(&input_file);
        println!("Result part1 : {res}");
    }
    if args.part == None || args.part == Some(2) {
        let res = run_part2(&input_file);
        println!("Result part2 : {res}");
    }
}

#[cfg(test)]
mod tests {
    // print output :
    // cargo test -- --show-output

    use super::*;

    const TEST_FILE_1: &str = "example1.txt";
    // const TEST_FILE_2: &str = "example2.txt";

    const TEST_RESULT_PART1_FILE_1: i64 = 480;
    // const TEST_RESULT_PART2_FILE_1: i64 = 48;

    #[test]
    fn test_part1() {
        let result = run_part1(TEST_FILE_1);
        println!("Result part1 on example : {result}");
        assert_eq!(result, TEST_RESULT_PART1_FILE_1);
    }

    #[test]
    fn test_part2() {
        let result = run_part2(TEST_FILE_1);
        println!("Result part2 on example : {result}");
        // assert_eq!(result, TEST_RESULT_PART2_FILE_1);

        let resort_lobby = ResortLobby::parse_file(TEST_FILE_1);
        trace!("resort_lobby = {:?}", resort_lobby);
        let result = resort_lobby.claw_machines[0]
            .compute_min_tokens_win_prize_correct_parsing_error(PRIZE_CONVERSION_ERROR_PART2);
        assert!(result.is_none());
        let result = resort_lobby.claw_machines[1]
            .compute_min_tokens_win_prize_correct_parsing_error(PRIZE_CONVERSION_ERROR_PART2);
        assert!(result.is_some());
        let result = resort_lobby.claw_machines[2]
            .compute_min_tokens_win_prize_correct_parsing_error(PRIZE_CONVERSION_ERROR_PART2);
        assert!(result.is_none());
        let result = resort_lobby.claw_machines[3]
            .compute_min_tokens_win_prize_correct_parsing_error(PRIZE_CONVERSION_ERROR_PART2);
        assert!(result.is_some());
    }
}
