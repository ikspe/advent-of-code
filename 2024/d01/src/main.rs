use std::collections::HashMap;

use aoc_common::read_file::read_lines_as_reader;
use clap::Parser;
use log::{debug, error};

// https://doc.rust-lang.org/cargo/reference/environment-variables.html
//const PKG_VERSION: &str = env!("CARGO_PKG_VERSION");
const AOC_YEAR: u32 = 2024;
const PKG_NAME: &str = env!("CARGO_PKG_NAME");
//const PKG_REPOSITORY: &str = env!("CARGO_PKG_REPOSITORY");

fn default_input_path() -> String {
    // TODO : this function should be moved to crate "common"
    format!(
        "{}/git/advent-of-code-inputs/{}/{}/input.txt",
        std::env::var("HOME").expect("error getting variable HOME"),
        AOC_YEAR,
        PKG_NAME
    )
    .to_string()
}

/// compute puzzle of the day
#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
#[command(after_help = "To select log level, use env var RUST_LOG=level")]
struct Args {
    /// run only part 1 or part 2
    #[arg(short, long)]
    part: Option<usize>,

    /// Input file (default to input file downloaded in input repo)
    #[arg(short, long)]
    input: Option<String>,
}

fn read_value_as_two_lists(file_path: &str) -> (Vec<u32>, Vec<u32>) {
    let reader = read_lines_as_reader(file_path).expect("error reading file");
    let mut vec1: Vec<u32> = vec![];
    let mut vec2: Vec<u32> = vec![];
    for line in reader {
        let line = line.unwrap();
        let mut tokens = line.split_whitespace();
        vec1.push(tokens.next().unwrap().parse::<u32>().unwrap());
        vec2.push(tokens.next().unwrap().parse::<u32>().unwrap());
    }
    assert_eq!(vec1.len(), vec2.len());
    (vec1, vec2)
}

fn run_part1(file_path: &str) -> i64 {
    debug!("starting part 1");
    let (mut vec1, mut vec2) = read_value_as_two_lists(file_path);
    vec1.sort();
    vec2.sort();
    assert_eq!(vec1.len(), vec2.len());
    let mut sum_distances = 0;
    for (val1, val2) in vec1.iter().zip(vec2.iter()) {
        sum_distances += val1.abs_diff(*val2);
    }
    debug!("finished part 1");
    sum_distances as i64
}

fn run_part2(file_path: &str) -> i64 {
    debug!("starting part 2");
    let (vec1, vec2) = read_value_as_two_lists(file_path);
    let mut count_vec2: HashMap<u32, u32> = Default::default();
    for val2 in vec2 {
        if let Some(count_val2) = count_vec2.get_mut(&val2) {
            *count_val2 += 1;
        } else {
            count_vec2.insert(val2, 1);
        }
    }
    let sum_similarity: u32 = vec1
        .iter()
        .map(|val1| val1 * count_vec2.get(val1).unwrap_or(&0))
        .sum();
    // let mut sum_similarity: u32 = 0;
    // for val1 in vec1 {
    //     sum_similarity += val1 * count_vec2.get(&val1).unwrap_or(&0);
    // }
    debug!("finished part 2");
    sum_similarity as i64
}

fn main() {
    env_logger::init();
    let args = Args::parse();
    let input_file = args.input.unwrap_or_else(default_input_path);
    println!("input_file = {input_file:?}");
    if args.part == None || args.part == Some(1) {
        let res = run_part1(&input_file);
        println!("Result part1 : {res}");
    }
    if args.part == None || args.part == Some(2) {
        let res = run_part2(&input_file);
        println!("Result part2 : {res}");
    }
}

#[cfg(test)]
mod tests {
    // print output :
    // cargo test -- --show-output

    use super::*;

    const TEST_FILE_1: &str = "example1.txt";
    //const TEST_FILE_2: &str = "example2.txt";

    const TEST_RESULT_PART1_FILE_1: i64 = 11;
    const TEST_RESULT_PART2_FILE_1: i64 = 31;

    #[test]
    fn test_part1() {
        let result = run_part1(TEST_FILE_1);
        println!("Result part1 on example : {result}");
        assert_eq!(result, TEST_RESULT_PART1_FILE_1);
    }

    #[test]
    fn test_part2() {
        let result = run_part2(TEST_FILE_1);
        println!("Result part2 on example : {result}");
        assert_eq!(result, TEST_RESULT_PART2_FILE_1);
    }
}
