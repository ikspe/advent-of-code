use std::collections::{HashMap, HashSet};

use aoc_common::read_file::read_lines_as_reader;
use clap::Parser;
use log::{debug, trace};

// https://doc.rust-lang.org/cargo/reference/environment-variables.html
//const PKG_VERSION: &str = env!("CARGO_PKG_VERSION");
const AOC_YEAR: u32 = 2024;
const PKG_NAME: &str = env!("CARGO_PKG_NAME");
//const PKG_REPOSITORY: &str = env!("CARGO_PKG_REPOSITORY");

fn default_input_path() -> String {
    // TODO : this function should be moved to crate "common"
    format!(
        "{}/git/advent-of-code-inputs/{}/{}/input.txt",
        std::env::var("HOME").expect("error getting variable HOME"),
        AOC_YEAR,
        PKG_NAME
    )
    .to_string()
}

/// compute puzzle of the day
#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
#[command(after_help = "To select log level, use env var RUST_LOG=level")]
struct Args {
    /// run only part 1 or part 2
    #[arg(short, long)]
    part: Option<usize>,

    /// Input file (default to input file downloaded in input repo)
    #[arg(short, long)]
    input: Option<String>,
}

#[derive(Debug, Clone)]
struct MapCity {
    // signed integers are more practical to detect out of map
    antennas: HashMap<char, Vec<(i32, i32)>>,
    pub width: i32,
    pub height: i32,
}

impl MapCity {
    pub fn parse_file(file_path: &str) -> Self {
        let reader: std::io::Lines<std::io::BufReader<std::fs::File>> =
            read_lines_as_reader(file_path).expect("error reading file");
        let mut list_lines: Vec<String> = vec![];
        for line in reader {
            list_lines.push(line.unwrap());
        }
        MapCity::parse_lines(list_lines)
    }
    fn parse_lines(lines: Vec<String>) -> Self {
        let width = lines[0].len() as i32;
        let height = lines.len() as i32;
        let mut antennas: HashMap<char, Vec<(i32, i32)>> = HashMap::new();
        for (it_y, it_line) in lines.iter().enumerate() {
            for (it_x, val) in it_line.chars().enumerate() {
                if val == '.' {
                    continue;
                }
                let pos = (it_x as i32, it_y as i32);
                if let Some(antennas_this_frequency) = antennas.get_mut(&val) {
                    antennas_this_frequency.push(pos);
                } else {
                    let antennas_this_frequency: Vec<(i32, i32)> = vec![pos];
                    antennas.insert(val, antennas_this_frequency);
                }
            }
        }
        Self {
            antennas,
            width,
            height,
        }
    }
    fn find_all_antinodes_for_antennas(
        &self,
        pos1: &(i32, i32),
        pos2: &(i32, i32),
        stop_after_first: bool,
    ) -> Vec<(i32, i32)> {
        let diff = (pos2.0 - pos1.0, pos2.1 - pos1.1);
        trace!("    {:?} / {:?} : diff = {:?}", pos1, pos2, diff);
        assert_ne!(
            diff,
            (0, 0),
            "the two antennas are at same location : impossible"
        );
        let mut result: Vec<(i32, i32)> = if stop_after_first {
            vec![]
        } else {
            vec![*pos1, *pos2]
        };
        for (current_pos, coeff) in vec![(*pos1, -1), (*pos2, 1)] {
            let mut current_pos = current_pos;
            loop {
                current_pos = (
                    current_pos.0 + coeff * diff.0,
                    current_pos.1 + coeff * diff.1,
                );
                if current_pos.0 < 0
                    || current_pos.0 >= self.width
                    || current_pos.1 < 0
                    || current_pos.1 >= self.height
                {
                    break;
                }
                result.push(current_pos);
                if stop_after_first {
                    break;
                }
            }
        }
        result
    }
    pub fn find_antinodes(&self, stop_after_first: bool) -> HashSet<(i32, i32)> {
        let mut antinodes: HashSet<(i32, i32)> = HashSet::new();
        for (frequency, list_antennas) in self.antennas.iter() {
            debug!("Frequency {} : start", frequency);
            for (antenna_idx, it_antenna_first) in list_antennas.iter().enumerate() {
                trace!(
                    "    antenna {:?} : start (idx {})",
                    it_antenna_first,
                    antenna_idx
                );
                for other_idx in (antenna_idx + 1)..list_antennas.len() {
                    let it_antenna_other = &list_antennas[other_idx];
                    let curr_antinodes = self.find_all_antinodes_for_antennas(
                        &it_antenna_first,
                        &it_antenna_other,
                        stop_after_first,
                    );

                    debug!(
                        "    {:?} / {:?} : found antinodes => {:?}",
                        it_antenna_first, it_antenna_other, curr_antinodes
                    );
                    antinodes.extend(curr_antinodes);
                    debug!(
                        "    {:?} / {:?} : all antinodes.len = {} => {:?}",
                        it_antenna_first,
                        it_antenna_other,
                        antinodes.len(),
                        antinodes
                    );
                }
            }
        }
        antinodes
    }
}

fn run_part1(file_path: &str) -> i64 {
    debug!("starting part 1");
    let map_city = MapCity::parse_file(file_path);
    debug!("map_city = {:?}", map_city);
    let result = map_city.find_antinodes(true).len();
    debug!("finished part 1");
    result as i64
}

fn run_part2(file_path: &str) -> i64 {
    debug!("starting part 2");
    let map_city = MapCity::parse_file(file_path);
    debug!("map_city = {:?}", map_city);
    let result = map_city.find_antinodes(false).len();
    debug!("finished part 2");
    result as i64
}

fn main() {
    env_logger::init();
    let args = Args::parse();
    let input_file = args.input.unwrap_or_else(default_input_path);
    println!("input_file = {input_file:?}");
    if args.part == None || args.part == Some(1) {
        let res = run_part1(&input_file);
        println!("Result part1 : {res}");
    }
    if args.part == None || args.part == Some(2) {
        let res = run_part2(&input_file);
        println!("Result part2 : {res}");
    }
}

#[cfg(test)]
mod tests {
    // print output :
    // cargo test -- --show-output

    use super::*;

    const TEST_FILE_1: &str = "example1.txt";
    const TEST_FILE_2: &str = "example2.txt";
    const TEST_FILE_3: &str = "example3.txt";

    const TEST_RESULT_PART1_FILE_1: i64 = 14;
    const TEST_RESULT_PART1_FILE_2: i64 = 4;
    const TEST_RESULT_PART2_FILE_1: i64 = 34;
    const TEST_RESULT_PART2_FILE_3: i64 = 9;

    #[test]
    fn test_part1() {
        let result = run_part1(TEST_FILE_1);
        println!("Result part1 on example : {result}");
        assert_eq!(result, TEST_RESULT_PART1_FILE_1);
    }

    #[test]
    fn test_example2_part1() {
        // this is because I am too supid and I took x = pos2.1 + diff.0 instead of pos2.0 diff.0
        let map_city = MapCity::parse_file(TEST_FILE_2);
        let antinodes = map_city.find_antinodes(true);
        let expected_antinodes = {
            let mut expected_antinodes: HashSet<(i32, i32)> = HashSet::new();
            expected_antinodes.extend(vec![(3, 1), (0, 2), (2, 6), (6, 7)]);
            expected_antinodes
        };
        println!("{:?}", antinodes);
        let result = antinodes.len() as i64;
        println!("Result part1 on example : {result}");
        assert_eq!(antinodes, expected_antinodes);
        assert_eq!(result, TEST_RESULT_PART1_FILE_2);
    }

    #[test]
    fn test_part2() {
        let result = run_part2(TEST_FILE_1);
        println!("Result part2 on example : {result}");
        assert_eq!(result, TEST_RESULT_PART2_FILE_1);
    }

    #[test]
    fn test_example3_part2() {
        let result = run_part2(TEST_FILE_3);
        println!("Result part2 on example : {result}");
        assert_eq!(result, TEST_RESULT_PART2_FILE_3);
    }
}
