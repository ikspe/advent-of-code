use aoc_common::read_file::read_lines_as_reader;
use clap::Parser;
use log::{debug, error, info, trace};

// https://doc.rust-lang.org/cargo/reference/environment-variables.html
//const PKG_VERSION: &str = env!("CARGO_PKG_VERSION");
const AOC_YEAR: u32 = 2024;
const PKG_NAME: &str = env!("CARGO_PKG_NAME");
//const PKG_REPOSITORY: &str = env!("CARGO_PKG_REPOSITORY");

fn default_input_path() -> String {
    // TODO : this function should be moved to crate "common"
    format!(
        "{}/git/advent-of-code-inputs/{}/{}/input.txt",
        std::env::var("HOME").expect("error getting variable HOME"),
        AOC_YEAR,
        PKG_NAME
    )
    .to_string()
}

/// compute puzzle of the day
#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
#[command(after_help = "To select log level, use env var RUST_LOG=level")]
struct Args {
    /// run only part 1 or part 2
    #[arg(short, long)]
    part: Option<usize>,

    /// Input file (default to input file downloaded in input repo)
    #[arg(short, long)]
    input: Option<String>,
}

#[derive(Debug, Clone)]
struct OpCodeComputer {
    register_a: u64,
    register_b: u64,
    register_c: u64,
    program: Vec<u8>,
}

impl OpCodeComputer {
    pub fn parse_file(file_path: &str) -> Self {
        let mut reader: std::io::Lines<std::io::BufReader<std::fs::File>> =
            read_lines_as_reader(file_path).expect("error reading file");
        let registers = {
            let mut registers: Vec<u64> = vec![];
            for _ in 0..3 {
                let line = reader.next().unwrap().unwrap();
                let mut split_lines = line.split(": ");
                assert!(split_lines.next().is_some());
                registers.push(split_lines.next().unwrap().parse::<u64>().unwrap());
            }
            (registers[0], registers[1], registers[2])
        };
        let line = reader.next().unwrap().unwrap();
        assert!(line.is_empty());
        let line = reader.next().unwrap().unwrap();
        let mut split_lines = line.split(": ");
        assert!(split_lines.next().is_some());
        let opcodes: Vec<_> = split_lines
            .next()
            .unwrap()
            .split(",")
            .map(|x| x.parse::<u8>().unwrap())
            .collect();
        Self {
            register_a: registers.0,
            register_b: registers.1,
            register_c: registers.2,
            program: opcodes,
        }
    }

    pub fn run(&mut self) -> Vec<u8> {
        let mut instruction_pointer: usize = 0;
        let mut output: Vec<u8> = vec![];
        loop {
            trace!(
                "run() registers : {} / {} / {}  - instruction_pointer = {}",
                self.register_a,
                self.register_b,
                self.register_c,
                instruction_pointer
            );
            if (instruction_pointer + 1) >= self.program.len() {
                debug!(
                    "run() instruction_pointer = {}  -> halt",
                    instruction_pointer
                );
                break;
            }
            let opcode = self.program[instruction_pointer];
            let literal_operand = self.program[instruction_pointer + 1];
            let combo_operand = match literal_operand {
                0 | 1 | 2 | 3 => literal_operand as u64,
                4 => self.register_a,
                5 => self.register_b,
                6 => self.register_c,
                7 => {
                    error!("reserved literal_operand : 7 ... not happening in valid programs!!");
                    0 // abritrary chosen, it should not happen...
                }
                _ => panic!(),
            };
            debug!(
                "run() instruction_pointer = {}  -> opcode = {} / literal_operand = {} / combo_operand = {}",
                instruction_pointer, opcode, literal_operand, combo_operand
            );
            instruction_pointer += 2;
            match opcode {
                0 => {
                    // adv
                    let tmp = self.register_a / 2u64.pow(combo_operand as u32);
                    trace!(
                        "run() [adv] {} / 2.pow({})   =>  {}  (to register A)",
                        self.register_a,
                        combo_operand,
                        tmp
                    );
                    self.register_a = tmp;
                }
                1 => {
                    // bxl
                    let tmp = self.register_b ^ (literal_operand as u64);
                    trace!(
                        "run() [bxl] {:#b} (= {}) / {:#b} (= {})   =>  {:#b} (= {})  (to register B)",
                        self.register_b,
                        self.register_b,
                        literal_operand as u64,
                        literal_operand as u64,
                        tmp,
                        tmp,
                    );
                    self.register_b = tmp;
                }
                2 => {
                    // bst
                    let tmp = combo_operand % 8;
                    trace!("run() [bst]  {}  (to register B)", tmp);
                    self.register_b = tmp;
                }
                3 => {
                    // jnz
                    if self.register_a == 0 {
                        // do nothing
                        trace!("run() [jnz] not jumping");
                    } else {
                        trace!(
                            "run() [jnz]     jumping : instruction_pointer = {}",
                            literal_operand as usize
                        );
                        instruction_pointer = literal_operand as usize;
                    }
                }
                4 => {
                    // bxc
                    let tmp = self.register_b ^ self.register_c;
                    trace!(
                        "run() [bxc] {:#b} (= {}) / {:#b} (= {})   =>  {:#b} (= {})  (to register B)",
                        self.register_b,
                        self.register_b,
                        self.register_c,
                        self.register_c,
                        tmp,
                        tmp,
                    );
                    self.register_b = tmp;
                }
                5 => {
                    // out
                    trace!("run() [out] {}", (combo_operand % 8) as u8);
                    output.push((combo_operand % 8) as u8);
                }
                6 => {
                    // bdv
                    let tmp = self.register_a / 2u64.pow(combo_operand as u32);
                    trace!(
                        "run() [bdv] {} / 2.pow({})   =>  {}  (to register B)",
                        self.register_a,
                        combo_operand,
                        tmp
                    );
                    self.register_b = tmp;
                }
                7 => {
                    // cdv
                    let tmp = self.register_a / 2u64.pow(combo_operand as u32);
                    trace!(
                        "run() [cdv] {} / 2.pow({})   =>  {}  (to register C)",
                        self.register_a,
                        combo_operand,
                        tmp
                    );
                    self.register_c = tmp;
                }
                _ => panic!("unexpected literal_operand = {}", literal_operand),
            }
            trace!("");
        }
        info!("output = {:?}", output);
        output
    }

    pub fn compute_input_part2(input_attempt: &Vec<u8>) -> u64 {
        let mut res: u64 = 0;
        for it_val in input_attempt {
            res = (res * 8) + *it_val as u64;
        }
        res
    }
}

fn run_part1(file_path: &str) -> String {
    debug!("starting part 1");
    let mut computer = OpCodeComputer::parse_file(file_path);
    info!("computer = {:?}", computer);
    let output = computer.run();
    let result = output
        .iter()
        .map(|x| x.to_string())
        .collect::<Vec<_>>()
        .join(",");
    debug!("finished part 1");
    result
    // 675213517 => That's not the right answer.
    // 6,7,5,2,1,3,5,1,7 => yeah , it's better by reading the problem :facepalm:
}

fn run_part2(file_path: &str) -> i64 {
    // we know the number we look for is something matchin
    // sum(A(n) * 8 ^ n) for n in 0..=15
    // and we look for the smallest solution
    debug!("starting part 2");
    let mut computer: OpCodeComputer = OpCodeComputer::parse_file(file_path);
    let expected_solution = computer.program.clone();
    println!("expected_solution = {:?}", expected_solution);
    // let reverse_solution: Vec<_> = expected_solution.iter().rev().map(|x| *x).collect();
    // println!("reverse_solution = {:?}", reverse_solution);

    let mut input_attempt: Vec<u8> = vec![0];
    loop {
        let attempt_reg_a = OpCodeComputer::compute_input_part2(&input_attempt);
        computer.register_a = attempt_reg_a;
        let output = computer.run();
        debug!("================");
        debug!("    input_attempt = {:?}", input_attempt);
        debug!("    attempt_reg_a = {:?}", attempt_reg_a);
        debug!("    output        = {:?}", output);
        if output == expected_solution {
            return attempt_reg_a as i64;
        }
        // compare the N first elements of both arrays
        if expected_solution[expected_solution.len() - output.len()..] == output {
            debug!(
                "    found matching {} last elements => adding new element 0 ...",
                output.len()
            );
            input_attempt.push(0);
            continue;
        }
        // not matching elem
        // => try to increment last element
        // if too big, pop it and try again, until we find a matching one
        debug!("    <starting to alterate last element...>");
        loop {
            let last_elem = input_attempt.last_mut().unwrap();
            *last_elem += 1;
            if *last_elem < 8 {
                debug!("    <    incremented last element>");
                break;
            }
            debug!("    <    pop last element>");
            input_attempt.pop();
            if input_attempt.is_empty() {
                panic!("we should never reach state with empty array")
            }
        }
    }
    // 216549846240959 => That's not the right answer; your answer is too high.
}

fn main() {
    env_logger::init();
    let args = Args::parse();
    let input_file = args.input.unwrap_or_else(default_input_path);
    println!("input_file = {input_file:?}");
    if args.part == None || args.part == Some(1) {
        let res = run_part1(&input_file);
        println!("Result part1 : {res}");
    }
    if args.part == None || args.part == Some(2) {
        let res = run_part2(&input_file);
        println!("Result part2 : {res}");
    }
}

#[cfg(test)]
mod tests {
    // print output :
    // cargo test -- --show-output

    use super::*;

    const TEST_FILE_1: &str = "example1.txt";
    const TEST_FILE_2: &str = "example2.txt";

    const TEST_RESULT_PART1_FILE_1: &str = "4,6,3,5,6,3,5,2,1,0";
    const TEST_RESULT_PART2_FILE_2: i64 = 117440;

    #[test]
    fn test_part1() {
        let result = run_part1(TEST_FILE_1);
        println!("Result part1 on example : {result}");
        assert_eq!(result, TEST_RESULT_PART1_FILE_1);
    }

    #[test]
    fn test_part2() {
        let result = run_part2(TEST_FILE_2);
        println!("Result part2 on example : {result}");
        assert_eq!(result, TEST_RESULT_PART2_FILE_2);
    }
}
