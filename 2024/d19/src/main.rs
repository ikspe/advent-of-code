use std::collections::HashSet;

use aoc_common::read_file::read_lines_as_reader;
use clap::Parser;
use log::{debug, error, info, trace};

// https://doc.rust-lang.org/cargo/reference/environment-variables.html
//const PKG_VERSION: &str = env!("CARGO_PKG_VERSION");
const AOC_YEAR: u32 = 2024;
const PKG_NAME: &str = env!("CARGO_PKG_NAME");
//const PKG_REPOSITORY: &str = env!("CARGO_PKG_REPOSITORY");

fn default_input_path() -> String {
    // TODO : this function should be moved to crate "common"
    format!(
        "{}/git/advent-of-code-inputs/{}/{}/input.txt",
        std::env::var("HOME").expect("error getting variable HOME"),
        AOC_YEAR,
        PKG_NAME
    )
    .to_string()
}

/// compute puzzle of the day
#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
#[command(after_help = "To select log level, use env var RUST_LOG=level")]
struct Args {
    /// run only part 1 or part 2
    #[arg(short, long)]
    part: Option<usize>,

    /// Input file (default to input file downloaded in input repo)
    #[arg(short, long)]
    input: Option<String>,
}

#[derive(Debug)]
struct HotSpringBranding {
    allowed_patterns: Vec<String>,
    candidate_designs: Vec<String>,
}

impl HotSpringBranding {
    pub fn parse_file(file_path: &str) -> Self {
        let mut reader: std::io::Lines<std::io::BufReader<std::fs::File>> =
            read_lines_as_reader(file_path).expect("error reading file");
        let line = reader.next().unwrap().unwrap();
        let allowed_patterns: Vec<_> = line.split(", ").map(|x| x.to_string()).collect();
        let line = reader.next().unwrap().unwrap();
        assert!(line.is_empty());
        let mut candidate_designs: Vec<String> = vec![];
        loop {
            candidate_designs.push(if let Some(line) = reader.next() {
                line.unwrap()
            } else {
                break;
            });
        }
        // not required, we have checked both example + real input file
        // // sanity check: verify there are no duplicate elements
        // let set_patterns: HashSet<String> = HashSet::from_iter(allowed_patterns.iter().cloned());
        // info!(
        //     "allowed_patterns.len = {} ; set_patterns.len = {}",
        //     allowed_patterns.len(),
        //     set_patterns.len()
        // );
        // assert_eq!(set_patterns.len(), allowed_patterns.len());
        Self {
            allowed_patterns,
            candidate_designs,
        }
    }
    pub fn count_possible_designs(&self, part1: bool) -> u64 {
        let mut result: u64 = 0;
        for it_design in self.candidate_designs.iter() {
            let it_result = Self::count_one_design_combinations(&it_design, &self.allowed_patterns);
            debug!(
                "design = {} : found {} different ways",
                it_design, it_result
            );
            if it_result == 0 {
                continue;
            }
            if part1 {
                result += 1;
            } else {
                result += it_result;
            }
        }
        result
    }
    fn count_one_design_combinations(design: &String, all_allowed_patterns: &Vec<String>) -> u64 {
        let mut matches_at_index = Vec::with_capacity(design.len() + 1);
        matches_at_index.resize(design.len() + 1, 0u64);
        matches_at_index[0] = 1;

        for idx in 0..design.len() {
            let match_count_curr_idx = matches_at_index[idx];
            let design_from_curr_idx: &str = &design[idx..];
            for it_pattern in all_allowed_patterns {
                if design_from_curr_idx.starts_with(it_pattern) {
                    matches_at_index[idx + it_pattern.len()] += match_count_curr_idx;
                }
            }
        }
        matches_at_index[design.len()]
    }
}

fn run_part1(file_path: &str) -> i64 {
    debug!("starting part 1");
    let hot_spring_branding = HotSpringBranding::parse_file(file_path);
    debug!("{:?}", hot_spring_branding);
    let result = hot_spring_branding.count_possible_designs(true);
    debug!("finished part 1");
    result as i64
    // 400 => That's not the right answer.
}

fn run_part2(file_path: &str) -> i64 {
    debug!("starting part 2");
    let hot_spring_branding = HotSpringBranding::parse_file(file_path);
    debug!("{:?}", hot_spring_branding);
    let result = hot_spring_branding.count_possible_designs(false);
    debug!("finished part 2");
    result as i64
}

fn main() {
    env_logger::init();
    let args = Args::parse();
    let input_file = args.input.unwrap_or_else(default_input_path);
    println!("input_file = {input_file:?}");
    if args.part == None || args.part == Some(1) {
        let res = run_part1(&input_file);
        println!("Result part1 : {res}");
    }
    if args.part == None || args.part == Some(2) {
        let res = run_part2(&input_file);
        println!("Result part2 : {res}");
    }
}

#[cfg(test)]
mod tests {
    // print output :
    // cargo test -- --show-output

    use super::*;

    const TEST_FILE_1: &str = "example1.txt";
    // const TEST_FILE_2: &str = "example2.txt";

    const TEST_RESULT_PART1_FILE_1: i64 = 6;
    const TEST_RESULT_PART2_FILE_1: i64 = 16;

    #[test]
    fn test_part1() {
        let result = run_part1(TEST_FILE_1);
        println!("Result part1 on example : {result}");
        assert_eq!(result, TEST_RESULT_PART1_FILE_1);
    }

    #[test]
    fn test_part2() {
        let result = run_part2(TEST_FILE_1);
        println!("Result part2 on example : {result}");
        assert_eq!(result, TEST_RESULT_PART2_FILE_1);
    }
}
