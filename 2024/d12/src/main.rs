use std::collections::HashSet;

use aoc_common::{direction::Direction, read_file::read_lines_as_reader};
use clap::Parser;
use log::{debug, trace};

// https://doc.rust-lang.org/cargo/reference/environment-variables.html
//const PKG_VERSION: &str = env!("CARGO_PKG_VERSION");
const AOC_YEAR: u32 = 2024;
const PKG_NAME: &str = env!("CARGO_PKG_NAME");
//const PKG_REPOSITORY: &str = env!("CARGO_PKG_REPOSITORY");

fn default_input_path() -> String {
    // TODO : this function should be moved to crate "common"
    format!(
        "{}/git/advent-of-code-inputs/{}/{}/input.txt",
        std::env::var("HOME").expect("error getting variable HOME"),
        AOC_YEAR,
        PKG_NAME
    )
    .to_string()
}

/// compute puzzle of the day
#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
#[command(after_help = "To select log level, use env var RUST_LOG=level")]
struct Args {
    /// run only part 1 or part 2
    #[arg(short, long)]
    part: Option<usize>,

    /// Input file (default to input file downloaded in input repo)
    #[arg(short, long)]
    input: Option<String>,
}

type Position = (i32, i32);
type RegionDesc = (HashSet<Position>, char);
#[derive(Debug)]
struct WorldMap {
    // each region is defined by its position and its type of plant
    regions: Vec<RegionDesc>,
    pub max: Position,
}

impl WorldMap {
    pub fn parse_file(file_path: &str) -> Self {
        let reader: std::io::Lines<std::io::BufReader<std::fs::File>> =
            read_lines_as_reader(file_path).expect("error reading file");
        let list_lines: Vec<String> = reader.into_iter().map(|x| x.unwrap()).collect();
        WorldMap::parse_lines(list_lines)
    }
    fn parse_lines(lines: Vec<String>) -> Self {
        let mut regions: Vec<RegionDesc> = vec![];
        // for each position, check if belongs to existing regions ?
        for (it_y, it_line) in lines.iter().enumerate() {
            for (it_x, val) in it_line.chars().enumerate() {
                // we only check LEFT and TOP
                //    as the 2 other directions WILL be checked
                //    when iterating further elements
                let pos_curr = (it_x as i32, it_y as i32);
                let pos_top_and_left = vec![
                    (it_x as i32, it_y as i32 - 1),
                    (it_x as i32 - 1, it_y as i32),
                ];
                let region_idx_neigbours_same_region = {
                    let mut region_idx_neigbours_same_region: HashSet<usize> = HashSet::new();
                    for it_pos_other in pos_top_and_left {
                        for (region_idx, previous_region) in regions.iter().enumerate() {
                            if val != previous_region.1 {
                                continue;
                            }
                            if previous_region.0.contains(&it_pos_other) {
                                region_idx_neigbours_same_region.insert(region_idx);
                                break;
                            }
                        }
                    }
                    region_idx_neigbours_same_region
                };
                match region_idx_neigbours_same_region.len() {
                    0 => {
                        trace!(
                            "pos {:?}  ({}) : 0 neighbours : create new region",
                            pos_curr,
                            val
                        );
                        let mut new_region: RegionDesc = (HashSet::new(), val);
                        new_region.0.insert(pos_curr);
                        regions.push(new_region);
                    }
                    1 => {
                        trace!(
                            "pos {:?}  ({}) : 1 neighbour  : add to region",
                            pos_curr,
                            val
                        );
                        let region_idx = region_idx_neigbours_same_region.iter().next().unwrap();
                        regions[*region_idx].0.insert(pos_curr);
                    }
                    2 => {
                        trace!(
                            "pos {:?}  ({}) : 2 neighbours : merge 2 regions (+ add element)",
                            pos_curr,
                            val
                        );
                        let (new_region, copy_regions) = {
                            let mut new_region: RegionDesc = (HashSet::new(), val);
                            new_region.0.insert(pos_curr);
                            let mut copy_regions: Vec<RegionDesc> = vec![];
                            for (region_idx, it_region) in regions.into_iter().enumerate() {
                                if region_idx_neigbours_same_region.contains(&region_idx) {
                                    new_region.0.extend(it_region.0);
                                } else {
                                    copy_regions.push(it_region);
                                }
                            }
                            (new_region, copy_regions)
                        };
                        regions = copy_regions;
                        regions.push(new_region);
                    }
                    _ => panic!("pos {:?}  ({}) : SHALL NOT HAPPEN (bug)", pos_curr, val),
                }
            }
        }
        debug!("Finished parsing : regions.len() = {}", regions.len());
        for it_region in regions.iter() {
            debug!("    it_region area = {}", it_region.0.len());
        }
        Self {
            regions,
            max: (lines[0].len() as i32, lines.len() as i32),
        }
    }
    fn compute_one_region_perimeter(&self, region: &RegionDesc) -> usize {
        //easy algo :
        // - for each position : find number of neighbour => perimeter = (4 - neighbours.count())
        // - sum perimeter all positions
        let mut result: usize = 0;
        for it_pos in region.0.iter() {
            let possible_neighbours = vec![
                (it_pos.0 - 1, it_pos.1),
                (it_pos.0 + 1, it_pos.1),
                (it_pos.0, it_pos.1 - 1),
                (it_pos.0, it_pos.1 + 1),
            ];
            let count_real_neighbours: usize = possible_neighbours
                .iter()
                .map(|it_neighbour| region.0.contains(&it_neighbour) as usize)
                .sum();
            result += (4 - count_real_neighbours);
        }
        result
    }
    pub fn compute_total_prices_fences(&self) -> usize {
        self.regions
            .iter()
            .map(|x| x.0.len() * self.compute_one_region_perimeter(x))
            .sum()
    }
    fn compute_one_region_number_of_sides(&self, region: &RegionDesc) -> usize {
        // algorithm :
        // - for each position : find eventual neighbours => find limits identified by (position, Direction)
        // - for all limits : aggregate : try finding if belongs to 0/1/2 limits
        // - get the limits count

        debug!("================");
        let mut set_region_cell_borders: HashSet<(Position, Direction)> = HashSet::new();
        for it_cell in region.0.iter() {
            for it_dir in Direction::iterator() {
                let is_border = if let Some(neighbor) = it_dir.move_pos(&it_cell, &self.max) {
                    !region.0.contains(&neighbor)
                } else {
                    true
                };
                if is_border {
                    set_region_cell_borders.insert((*it_cell, (*it_dir).clone()));
                }
            }
        }
        // now reassemble the borders in segments
        trace!(
            "region {} : set_region_cell_borders = {:?}",
            region.1,
            set_region_cell_borders
        );
        let mut number_of_sides = 0;
        while !set_region_cell_borders.is_empty() {
            let (segment_begin, segment_border_direction) =
                set_region_cell_borders.iter().next().cloned().unwrap();
            set_region_cell_borders.remove(&(segment_begin, segment_border_direction));
            trace!(
                "region {} : segment_begin = {:?} / {:?}",
                region.1,
                segment_begin,
                segment_border_direction
            );
            let (first_direction, second_direction) = if matches!(
                segment_border_direction,
                Direction::North | Direction::South
            ) {
                (Direction::East, Direction::West)
            } else {
                (Direction::North, Direction::South)
            };
            // heading to first_direction, then second_direction
            for it_direction in [first_direction, second_direction] {
                let mut curr_cell = segment_begin.clone();
                loop {
                    let next_cell =
                        if let Some(next_cell) = it_direction.move_pos(&curr_cell, &self.max) {
                            next_cell
                        } else {
                            break;
                        };
                    if !set_region_cell_borders.contains(&(next_cell, segment_border_direction)) {
                        break;
                    }
                    set_region_cell_borders.remove(&(next_cell, segment_border_direction));
                    curr_cell = next_cell;
                }
            }
            number_of_sides += 1;
        }
        debug!(
            "region {} : {} * {} => price = {}",
            region.1,
            number_of_sides,
            region.0.len(),
            number_of_sides * region.0.len()
        );
        // number_of_sides * region.0.len()
        number_of_sides
    }
    pub fn compute_total_prices_fences_bulk_discount(&self) -> usize {
        self.regions
            .iter()
            .map(|x| x.0.len() * self.compute_one_region_number_of_sides(x))
            .sum()
    }
}

fn run_part1(file_path: &str) -> i64 {
    debug!("starting part 1");
    let world_map = WorldMap::parse_file(file_path);
    let result = world_map.compute_total_prices_fences();
    debug!("finished part 1");
    result as i64
}

fn run_part2(file_path: &str) -> i64 {
    debug!("starting part 2");
    let world_map = WorldMap::parse_file(file_path);
    let result = world_map.compute_total_prices_fences_bulk_discount();
    debug!("finished part 2");
    result as i64
}

fn main() {
    env_logger::init();
    let args = Args::parse();
    let input_file = args.input.unwrap_or_else(default_input_path);
    println!("input_file = {input_file:?}");
    if args.part == None || args.part == Some(1) {
        let res = run_part1(&input_file);
        println!("Result part1 : {res}");
    }
    if args.part == None || args.part == Some(2) {
        let res = run_part2(&input_file);
        println!("Result part2 : {res}");
    }
}

#[cfg(test)]
mod tests {
    // print output :
    // cargo test -- --show-output

    use super::*;

    const TEST_FILE_1: &str = "example1.txt";
    // const TEST_FILE_2: &str = "example2.txt";

    const TEST_RESULT_PART1_FILE_1: i64 = 1930;
    const TEST_RESULT_PART2_FILE_1: i64 = 1206;

    #[test]
    fn test_part1() {
        let result = run_part1(TEST_FILE_1);
        println!("Result part1 on example : {result}");
        assert_eq!(result, TEST_RESULT_PART1_FILE_1);
    }

    #[test]
    fn test_part2() {
        let result = run_part2(TEST_FILE_1);
        println!("Result part2 on example : {result}");
        assert_eq!(result, TEST_RESULT_PART2_FILE_1);
    }
}
