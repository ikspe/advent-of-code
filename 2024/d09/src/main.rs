use aoc_common::read_file::read_lines_as_reader;
use clap::Parser;
use log::{debug, error, trace};

// https://doc.rust-lang.org/cargo/reference/environment-variables.html
//const PKG_VERSION: &str = env!("CARGO_PKG_VERSION");
const AOC_YEAR: u32 = 2024;
const PKG_NAME: &str = env!("CARGO_PKG_NAME");
//const PKG_REPOSITORY: &str = env!("CARGO_PKG_REPOSITORY");

fn default_input_path() -> String {
    // TODO : this function should be moved to crate "common"
    format!(
        "{}/git/advent-of-code-inputs/{}/{}/input.txt",
        std::env::var("HOME").expect("error getting variable HOME"),
        AOC_YEAR,
        PKG_NAME
    )
    .to_string()
}

/// compute puzzle of the day
#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
#[command(after_help = "To select log level, use env var RUST_LOG=level")]
struct Args {
    /// run only part 1 or part 2
    #[arg(short, long)]
    part: Option<usize>,

    /// Input file (default to input file downloaded in input repo)
    #[arg(short, long)]
    input: Option<String>,
}

// #[derive(Debug)]
// enum DiskObject {
//     FileBlock { len: u32, id_num: u32 },
//     FreeSpace { len: u32 },
// }

#[derive(Debug, Clone)]
enum DiskObject {
    FileBlock { id_num: u32 },
    FreeSpace,
}

#[derive(Debug)]
struct DiskLayout {
    blocks: Vec<DiskObject>,
}

impl DiskLayout {
    pub fn parse_file(file_path: &str) -> Self {
        let mut reader: std::io::Lines<std::io::BufReader<std::fs::File>> =
            read_lines_as_reader(file_path).expect("error reading file");
        let disk_layout = DiskLayout::parse_line(reader.next().unwrap().unwrap());
        assert!(reader.next().is_none());
        disk_layout
    }
    pub fn parse_line(line: String) -> Self {
        let mut blocks: Vec<DiskObject> = vec![];
        let mut next_block_type = DiskObject::FileBlock { id_num: 0 };
        let mut next_id_num = 0;
        const RADIX: u32 = 10;
        for val in line.chars() {
            let block_len = val.to_digit(RADIX).unwrap();
            let current_block = match next_block_type {
                DiskObject::FileBlock { id_num: _ } => {
                    next_block_type = DiskObject::FreeSpace;
                    // next_id_num unchanged
                    DiskObject::FileBlock {
                        id_num: next_id_num,
                    }
                }
                DiskObject::FreeSpace => {
                    next_block_type = DiskObject::FileBlock {
                        id_num: next_id_num,
                    };
                    next_id_num += 1;
                    DiskObject::FreeSpace
                }
            };
            for _ in 0..block_len {
                blocks.push(current_block.clone());
            }
        }
        Self { blocks }
    }

    fn debug_disk_layout(&self) -> String {
        let mut display = "".to_string();
        for obj in self.blocks.iter() {
            match obj {
                DiskObject::FileBlock { id_num } => {
                    if *id_num <= 9 {
                        display.push_str(&id_num.to_string());
                    } else {
                        display.push_str("X");
                    }
                }
                DiskObject::FreeSpace => display.push_str("."),
            }
        }
        display
    }

    fn degrag_per_block_one_step(&mut self) -> bool {
        let mut first_idx_free: Option<usize> = None;
        let mut last_idx_block: Option<usize> = None;
        for (idx, val) in self.blocks.iter().enumerate() {
            match (val, first_idx_free) {
                (DiskObject::FreeSpace, None) => first_idx_free = Some(idx),
                (DiskObject::FileBlock { id_num: _ }, _) => last_idx_block = Some(idx),
                _ => {}
            }
        }

        trace!(
            "Found first free idx = {:?} / last_idx_block = {:?}",
            first_idx_free,
            last_idx_block
        );
        let first_idx_free = first_idx_free.unwrap();
        let last_idx_block = last_idx_block.unwrap();
        if last_idx_block < first_idx_free {
            return false;
        }
        self.blocks.swap(first_idx_free, last_idx_block);
        return true;
    }

    pub fn defrag_per_block(&mut self) {
        // this is not optimized : we could save positions between 1 defrag step to the next one
        debug!("defrag_per_block() start : {:?}", self);
        loop {
            if !self.degrag_per_block_one_step() {
                break;
            }
        }
        debug!("defrag_per_block() end   : {:?}", self);
    }
    fn find_empty_blocks_contiguous_from_start(&self, block_len: usize) -> Option<usize> {
        let mut start_current_free_block: Option<usize> = None;
        for (pos_idx, obj) in self.blocks.iter().enumerate() {
            match obj {
                DiskObject::FileBlock { id_num: _ } => {
                    start_current_free_block = None;
                }
                DiskObject::FreeSpace => {
                    if start_current_free_block.is_none() {
                        start_current_free_block = Some(pos_idx)
                    }
                    if pos_idx + 1 - start_current_free_block.unwrap() >= block_len {
                        return start_current_free_block;
                    }
                }
            }
        }
        None
    }
    pub fn defrag_whole_files(&mut self) {
        debug!(
            "defrag_whole_files() start   : {}",
            self.debug_disk_layout()
        );
        let mut list_all_files_idx_pos_len: Vec<(u32, usize, usize)> = vec![];
        let mut previous_obj = DiskObject::FreeSpace;
        let mut current_file_begin = 0; //for last file
        for (pos_idx, obj) in self.blocks.iter().enumerate() {
            match (&previous_obj, obj) {
                (
                    DiskObject::FileBlock {
                        id_num: id_num_prev,
                    },
                    DiskObject::FileBlock {
                        id_num: id_num_curr,
                    },
                ) => {
                    if id_num_prev != id_num_curr {
                        list_all_files_idx_pos_len.push((
                            *id_num_prev,
                            current_file_begin,
                            pos_idx - current_file_begin,
                        ));
                        current_file_begin = pos_idx;
                    }
                }
                (
                    DiskObject::FileBlock {
                        id_num: id_num_prev,
                    },
                    DiskObject::FreeSpace,
                ) => {
                    list_all_files_idx_pos_len.push((
                        *id_num_prev,
                        current_file_begin,
                        pos_idx - current_file_begin,
                    ));
                }
                (DiskObject::FreeSpace, DiskObject::FileBlock { id_num: _ }) => {
                    current_file_begin = pos_idx;
                }
                (DiskObject::FreeSpace, DiskObject::FreeSpace) => {}
            }
            previous_obj = obj.clone();
        }
        // eventual last file
        match previous_obj {
            DiskObject::FileBlock { id_num } => {
                list_all_files_idx_pos_len.push((
                    id_num,
                    current_file_begin,
                    self.blocks.len() - current_file_begin,
                ));
            }
            DiskObject::FreeSpace => {}
        }
        debug!(
            "defrag_whole_files()         : list_all_files_idx_pos_len = {:?}",
            list_all_files_idx_pos_len
        );
        for file_infos in list_all_files_idx_pos_len.iter().rev() {
            if let Some(pos_free_blocks) =
                self.find_empty_blocks_contiguous_from_start(file_infos.2)
            {
                if pos_free_blocks < file_infos.1 {
                    for i in 0..file_infos.2 {
                        self.blocks.swap(pos_free_blocks + i, file_infos.1 + i);
                    }
                }
            } else {
            }
            debug!(
                "defrag_whole_files()         : {}",
                self.debug_disk_layout()
            );
        }
        debug!(
            "defrag_whole_files() end     : {}",
            self.debug_disk_layout()
        );
        // 6237038358297 => That's not the right answer; your answer is too low.
        // 6237075041489
    }
    pub fn compute_checksum(&self) -> usize {
        let mut result = 0;
        for (idx, block) in self.blocks.iter().enumerate() {
            match block {
                DiskObject::FileBlock { id_num } => result += idx * (*id_num) as usize,
                DiskObject::FreeSpace => {} //for part 1, we could break but for part 2, we need not to break
            }
        }
        result
    }
}

fn run_part1(file_path: &str) -> i64 {
    debug!("starting part 1");
    let mut disk_layout = DiskLayout::parse_file(file_path);
    disk_layout.defrag_per_block();
    let result: usize = disk_layout.compute_checksum();
    debug!("finished part 1");
    result as i64
}

fn run_part2(file_path: &str) -> i64 {
    debug!("starting part 2");
    let mut disk_layout = DiskLayout::parse_file(file_path);
    disk_layout.defrag_whole_files();
    let result: usize = disk_layout.compute_checksum();
    debug!("finished part 2");
    result as i64
}

fn main() {
    env_logger::init();
    let args = Args::parse();
    let input_file = args.input.unwrap_or_else(default_input_path);
    println!("input_file = {input_file:?}");
    if args.part == None || args.part == Some(1) {
        let res = run_part1(&input_file);
        println!("Result part1 : {res}");
    }
    if args.part == None || args.part == Some(2) {
        let res = run_part2(&input_file);
        println!("Result part2 : {res}");
    }
}

#[cfg(test)]
mod tests {
    // print output :
    // cargo test -- --show-output

    use super::*;

    const TEST_FILE_1: &str = "example1.txt";
    // const TEST_FILE_2: &str = "example2.txt";

    const TEST_RESULT_PART1_FILE_1: i64 = 1928;
    const TEST_RESULT_PART2_FILE_1: i64 = 2858;

    #[test]
    fn test_parsing() {
        let disk_layout = DiskLayout::parse_line("12345".to_string());
        println!("{:?}", disk_layout);
        assert_eq!(disk_layout.blocks.len(), 1 + 2 + 3 + 4 + 5);
        let mut iter_block = disk_layout.blocks.into_iter();
        // "1"
        for _ in 0..1 {
            assert!(matches!(
                iter_block.next().unwrap(),
                DiskObject::FileBlock { id_num: 0 }
            ));
        }
        // "2"
        for _ in 0..2 {
            assert!(matches!(iter_block.next().unwrap(), DiskObject::FreeSpace));
        }
        // "3"
        for _ in 0..3 {
            assert!(matches!(
                iter_block.next().unwrap(),
                DiskObject::FileBlock { id_num: 1 }
            ));
        }
        // "4"
        for _ in 0..4 {
            assert!(matches!(iter_block.next().unwrap(), DiskObject::FreeSpace));
        }
        // "5"
        for _ in 0..5 {
            assert!(matches!(
                iter_block.next().unwrap(),
                DiskObject::FileBlock { id_num: 2 }
            ));
        }
        assert!(iter_block.next().is_none());

        let disk_layout = DiskLayout::parse_line("90909".to_string());
        println!("{:?}", disk_layout);
        assert_eq!(disk_layout.blocks.len(), 9 + 9 + 9);
        let mut iter_block = disk_layout.blocks.into_iter();
        // "9"
        for _ in 0..9 {
            assert!(matches!(
                iter_block.next().unwrap(),
                DiskObject::FileBlock { id_num: 0 }
            ));
        }
        // "0"
        // for _ in 0..0 {
        //     assert!(matches!(iter_block.next().unwrap(), DiskObject::FreeSpace));
        // }
        // "9"
        for _ in 0..9 {
            assert!(matches!(
                iter_block.next().unwrap(),
                DiskObject::FileBlock { id_num: 1 }
            ));
        }
        // // "0"
        // for _ in 0..0 {
        //     assert!(matches!(iter_block.next().unwrap(), DiskObject::FreeSpace));
        // }
        // "9"
        for _ in 0..9 {
            assert!(matches!(
                iter_block.next().unwrap(),
                DiskObject::FileBlock { id_num: 2 }
            ));
        }
        assert!(iter_block.next().is_none());
    }

    #[test]
    fn test_part1() {
        let result = run_part1(TEST_FILE_1);
        println!("Result part1 on example : {result}");
        assert_eq!(result, TEST_RESULT_PART1_FILE_1);
    }

    #[test]
    fn test_part2() {
        let result = run_part2(TEST_FILE_1);
        println!("Result part2 on example : {result}");
        assert_eq!(result, TEST_RESULT_PART2_FILE_1);
    }
}
