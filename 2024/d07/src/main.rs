use std::vec;

use aoc_common::read_file::read_lines_as_reader;
use clap::Parser;
use log::{debug, error, info, trace};

// https://doc.rust-lang.org/cargo/reference/environment-variables.html
//const PKG_VERSION: &str = env!("CARGO_PKG_VERSION");
const AOC_YEAR: u32 = 2024;
const PKG_NAME: &str = env!("CARGO_PKG_NAME");
//const PKG_REPOSITORY: &str = env!("CARGO_PKG_REPOSITORY");

fn default_input_path() -> String {
    // TODO : this function should be moved to crate "common"
    format!(
        "{}/git/advent-of-code-inputs/{}/{}/input.txt",
        std::env::var("HOME").expect("error getting variable HOME"),
        AOC_YEAR,
        PKG_NAME
    )
    .to_string()
}

/// compute puzzle of the day
#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
#[command(after_help = "To select log level, use env var RUST_LOG=level")]
struct Args {
    /// run only part 1 or part 2
    #[arg(short, long)]
    part: Option<usize>,

    /// Input file (default to input file downloaded in input repo)
    #[arg(short, long)]
    input: Option<String>,
}

// `Sized` is necessary to be able to return `Option<Self>`
trait OperatorTrait: Sized {
    // return = None => no remaining operator to try
    fn next(&self) -> Option<Self>;
    fn apply(&self, lhs: u64, rhs: u64) -> u64;
    fn first_operator() -> Self;
}

#[derive(Debug)]
enum OperatorPart1 {
    Add,
    Multiply,
}

impl OperatorTrait for OperatorPart1 {
    fn next(&self) -> Option<Self> {
        match self {
            OperatorPart1::Add => Some(OperatorPart1::Multiply),
            OperatorPart1::Multiply => None,
        }
    }

    fn apply(&self, lhs: u64, rhs: u64) -> u64 {
        match self {
            OperatorPart1::Add => lhs + rhs,
            OperatorPart1::Multiply => lhs * rhs,
        }
    }
    fn first_operator() -> Self {
        OperatorPart1::Add // arbitrary decided (but if we change that; we need to change next())
    }
}

#[derive(Debug)]
enum OperatorPart2 {
    Add,
    Multiply,
    Concatenate,
}

impl OperatorTrait for OperatorPart2 {
    fn next(&self) -> Option<Self> {
        match self {
            OperatorPart2::Add => Some(OperatorPart2::Multiply),
            OperatorPart2::Multiply => Some(OperatorPart2::Concatenate),
            OperatorPart2::Concatenate => None,
        }
    }

    fn apply(&self, lhs: u64, rhs: u64) -> u64 {
        match self {
            OperatorPart2::Add => lhs + rhs,
            OperatorPart2::Multiply => lhs * rhs,
            OperatorPart2::Concatenate => format!("{}{}", lhs, rhs).parse::<u64>().unwrap(),
        }
    }
    fn first_operator() -> Self {
        OperatorPart2::Add // arbitrary decided (but if we change that; we need to change next())
    }
}

#[derive(Debug)]
struct CalibrationEquation {
    result: u64,
    operands: Vec<u64>,
}

impl CalibrationEquation {
    pub fn parse_line(line: String) -> Self {
        let mut line_split = line.split(": ");
        let lhs = line_split.next().unwrap();
        let rhs = line_split.next().unwrap();
        assert!(line_split.next().is_none());

        Self {
            result: lhs.parse::<u64>().unwrap(),
            operands: rhs
                .split_whitespace()
                .into_iter()
                .map(|x| x.parse::<u64>().unwrap())
                .collect(),
        }
    }

    pub fn is_equation_possibly_true<Operator: OperatorTrait + std::fmt::Debug>(&self) -> bool {
        // add (+) and multiply (*)
        // always evaluated left-to-right, not according to precedence rules

        if self.operands.len() == 1 && self.operands[0] == self.result {
            return true;
        }

        //first operator = None => canari
        // we could also decide it to be Add/Multiply and check stack with len = 0, but this would be more complex
        let mut stack_current_state: Vec<(u64, Option<Operator>)> = vec![(self.operands[0], None)];
        loop {
            let last_elem = stack_current_state
                .last()
                .expect("found empty stack => should not happen !");
            let stack_len = stack_current_state.len();
            debug!(
                "    stack_current_state (len = {}) = {:?}",
                stack_len, stack_current_state
            );
            let need_rewind = if stack_len == self.operands.len() {
                if last_elem.0 == self.result {
                    return true;
                }
                true
            } else if last_elem.0 > self.result {
                true // optimisation : we know this is a dead end
            } else {
                false
            };
            if need_rewind {
                loop {
                    let last_elem = stack_current_state.pop().unwrap();
                    let stack_len = stack_current_state.len();
                    debug!(
                        "    rewind: pop last element : {:?} (len = {})",
                        last_elem, stack_len
                    );
                    let next_operator = if let Some(last_operator) = last_elem.1 {
                        if let Some(next_operator) = last_operator.next() {
                            next_operator
                        } else {
                            continue; // continue rewiding
                        }
                    } else {
                        debug!("    rewind : reached 1st element with operator = None => stop");
                        return false;
                    };
                    let next_operand = self.operands[stack_len];
                    let last_elem = stack_current_state
                        .last()
                        .expect("found empty stack => should not happen !");
                    debug!(
                        "    rewind : replacing previous last with next operator : executing {:?}({} , {})...",
                        next_operator, last_elem.0, next_operand
                    );
                    stack_current_state.push((
                        next_operator.apply(last_elem.0, next_operand),
                        Some(next_operator),
                    ));
                    break;
                }
                continue;
            }
            let next_operand = self.operands.get(stack_len).unwrap();
            let next_operator = Operator::first_operator();
            debug!(
                "    trying with next operator : executing {:?}({} , {})...",
                next_operator, last_elem.0, next_operand
            );
            stack_current_state.push((
                next_operator.apply(last_elem.0, *next_operand),
                Some(next_operator),
            ));
        }
    }
}

fn run_part1(file_path: &str) -> i64 {
    debug!("starting part 1");
    let mut result: u64 = 0;

    let reader: std::io::Lines<std::io::BufReader<std::fs::File>> =
        read_lines_as_reader(file_path).expect("error reading file");
    for line in reader {
        let equation = CalibrationEquation::parse_line(line.unwrap());
        info!("equation = {:?}", equation);
        if equation.is_equation_possibly_true::<OperatorPart1>() {
            info!("Found equation possibly true : {:?}", equation);
            result += equation.result;
        }
    }
    debug!("finished part 1");
    result as i64
}

fn run_part2(file_path: &str) -> i64 {
    debug!("starting part 2");
    let mut result: u64 = 0;

    let reader: std::io::Lines<std::io::BufReader<std::fs::File>> =
        read_lines_as_reader(file_path).expect("error reading file");
    for line in reader {
        let equation = CalibrationEquation::parse_line(line.unwrap());
        info!("equation = {:?}", equation);
        if equation.is_equation_possibly_true::<OperatorPart1>() {
            info!(
                "Found equation possibly true (via operator part 1) : {:?}",
                equation
            );
            result += equation.result;
        } else if equation.is_equation_possibly_true::<OperatorPart2>() {
            info!(
                "Found equation possibly true (via operator part 2) : {:?}",
                equation
            );
            result += equation.result;
        }
    }
    debug!("finished part 2");
    result as i64
}

fn main() {
    env_logger::init();
    let args = Args::parse();
    let input_file = args.input.unwrap_or_else(default_input_path);
    println!("input_file = {input_file:?}");
    if args.part == None || args.part == Some(1) {
        let res = run_part1(&input_file);
        println!("Result part1 : {res}");
    }
    if args.part == None || args.part == Some(2) {
        let res = run_part2(&input_file);
        println!("Result part2 : {res}");
    }
}

#[cfg(test)]
mod tests {
    // print output :
    // cargo test -- --show-output

    use super::*;

    const TEST_FILE_1: &str = "example1.txt";
    // const TEST_FILE_2: &str = "example2.txt";

    const TEST_RESULT_PART1_FILE_1: i64 = 3749;
    const TEST_RESULT_PART2_FILE_1: i64 = 11387;

    #[test]
    fn test_part1() {
        let result = run_part1(TEST_FILE_1);
        println!("Result part1 on example : {result}");
        assert_eq!(result, TEST_RESULT_PART1_FILE_1);
    }

    #[test]
    fn test_part2() {
        let result = run_part2(TEST_FILE_1);
        println!("Result part2 on example : {result}");
        assert_eq!(result, TEST_RESULT_PART2_FILE_1);
    }
}
