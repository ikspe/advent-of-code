use aoc_common::log::debug;
use aoc_common::read_file::read_lines_as_reader;
use clap::Parser;

// https://doc.rust-lang.org/cargo/reference/environment-variables.html
//const PKG_VERSION: &str = env!("CARGO_PKG_VERSION");
const AOC_YEAR: u32 = 2024;
const PKG_NAME: &str = env!("CARGO_PKG_NAME");
//const PKG_REPOSITORY: &str = env!("CARGO_PKG_REPOSITORY");

fn default_input_path() -> String {
    // TODO : this function should be moved to crate "common"
    format!(
        "{}/git/advent-of-code-inputs/{}/{}/input.txt",
        std::env::var("HOME").expect("error getting variable HOME"),
        AOC_YEAR,
        PKG_NAME
    )
    .to_string()
}

/// compute puzzle of the day
#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
#[command(after_help = "To select log level, use env var RUST_LOG=level")]
struct Args {
    /// run only part 1 or part 2
    #[arg(short, long)]
    part: Option<usize>,

    /// Input file (default to input file downloaded in input repo)
    #[arg(short, long)]
    input: Option<String>,
}

fn run_part1(file_path: &str) -> i64 {
    debug!("starting part 1");
    let mut result: u32 = 0;
    todo!();
    debug!("finished part 1");
    result as i64
}

fn run_part2(file_path: &str) -> i64 {
    debug!("starting part 2");
    let mut result: u32 = 0;
    todo!();
    debug!("finished part 2");
    result as i64
}

fn main() {
    env_logger::init();
    let args = Args::parse();
    let input_file = args.input.unwrap_or_else(default_input_path);
    println!("input_file = {input_file:?}");
    if args.part == None || args.part == Some(1) {
        let res = run_part1(&input_file);
        println!("Result part1 : {res}");
    }
    if args.part == None || args.part == Some(2) {
        let res = run_part2(&input_file);
        println!("Result part2 : {res}");
    }
}

#[cfg(test)]
mod tests {
    // print output :
    // cargo test -- --show-output

    use super::*;

    const TEST_FILE_1: &str = "example1.txt";
    // const TEST_FILE_2: &str = "example2.txt";

    fn get_result_part1_file1() -> i64 {
        todo!()
    }
    fn get_result_part2_file1() -> i64 {
        todo!()
    }

    #[test]
    fn test_part1() {
        let result = run_part1(TEST_FILE_1);
        let expected_result = get_result_part1_file1();
        println!("Result part1 on example : {result} / expected result = {expected_result}");
        assert_eq!(result, expected_result);
    }

    #[test]
    fn test_part2() {
        let result = run_part2(TEST_FILE_1);
        let expected_result = get_result_part2_file1();
        println!("Result part2 on example : {result} / expected result = {expected_result}");
        assert_eq!(result, expected_result);
    }
}
