use std::{collections::HashMap, usize};

use aoc_common::read_file::read_lines_as_reader;
use clap::Parser;
use log::{debug, error, trace};

// https://doc.rust-lang.org/cargo/reference/environment-variables.html
//const PKG_VERSION: &str = env!("CARGO_PKG_VERSION");
const AOC_YEAR: u32 = 2024;
const PKG_NAME: &str = env!("CARGO_PKG_NAME");
//const PKG_REPOSITORY: &str = env!("CARGO_PKG_REPOSITORY");

fn default_input_path() -> String {
    // TODO : this function should be moved to crate "common"
    format!(
        "{}/git/advent-of-code-inputs/{}/{}/input.txt",
        std::env::var("HOME").expect("error getting variable HOME"),
        AOC_YEAR,
        PKG_NAME
    )
    .to_string()
}

/// compute puzzle of the day
#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
#[command(after_help = "To select log level, use env var RUST_LOG=level")]
struct Args {
    /// run only part 1 or part 2
    #[arg(short, long)]
    part: Option<usize>,

    /// Input file (default to input file downloaded in input repo)
    #[arg(short, long)]
    input: Option<String>,
}

const ITERATIONS_PART1: usize = 25;
const ITERATIONS_PART2: usize = 75;

#[derive(Debug)]
struct Stones {
    stones: HashMap<String, usize>,
}

impl Stones {
    pub fn parse_file(file_path: &str) -> Self {
        let mut reader: std::io::Lines<std::io::BufReader<std::fs::File>> =
            read_lines_as_reader(file_path).expect("error reading file");
        let line = reader.next().unwrap().unwrap();
        assert!(reader.next().is_none());
        let raw_stones: Vec<String> = line.split_whitespace().map(|x| x.to_string()).collect();
        let mut stones: HashMap<String, usize> = HashMap::new();
        for it_stone in raw_stones {
            if let Some(val) = stones.get_mut(&it_stone) {
                *val = *val + 1;
            } else {
                stones.insert(it_stone, 1);
            }
        }
        Self { stones }
    }
    fn blink_once(&mut self) {
        let mut new_stones: HashMap<String, usize> = HashMap::new();
        for (it_stone, count_stone) in self.stones.iter() {
            // we could optimize the insertion, but this is not the main point, so let's not bother
            let vec_tokens = if it_stone == "0" {
                vec!["1".to_string()]
            } else if it_stone.len() % 2 == 0 {
                let (tok1, tok2) = it_stone.split_at(it_stone.len() / 2);
                // The new numbers don't keep extra leading zeroes
                // FIXME : no need to parse as int, we could simply analyze string
                vec![
                    tok1.parse::<u64>().unwrap().to_string(),
                    tok2.parse::<u64>().unwrap().to_string(),
                ]
            } else {
                vec![(it_stone.parse::<u64>().unwrap() * 2024).to_string()]
            };
            for token in vec_tokens {
                if let Some(val) = new_stones.get_mut(&token) {
                    *val = *val + count_stone;
                } else {
                    new_stones.insert(token, *count_stone);
                }
            }
        }
        self.stones = new_stones;
    }
    pub fn blink_many(&mut self, count: usize) {
        for i in 0..count {
            self.blink_once();
            debug!(
                "    After {} blinks : stones count = {}",
                i,
                self.stones.len()
            );
            debug!("    After {} blinks : stones       = {:?}", i, self.stones);
        }
    }
    pub fn count(&self) -> usize {
        let mut result: usize = 0;
        for count in self.stones.values() {
            result += count
        }
        result
    }
}

fn run_part1(file_path: &str) -> i64 {
    debug!("starting part 1");
    let mut stones = Stones::parse_file(file_path);
    debug!("stones = {:?}", stones);
    stones.blink_many(ITERATIONS_PART1);
    let result = stones.count();
    debug!("finished part 1");
    result as i64
}

fn run_part2(file_path: &str) -> i64 {
    debug!("starting part 2");
    let mut stones = Stones::parse_file(file_path);
    debug!("stones = {:?}", stones);
    stones.blink_many(ITERATIONS_PART2);
    let result = stones.count();
    debug!("finished part 2");
    result as i64
}

fn main() {
    env_logger::init();
    let args = Args::parse();
    let input_file = args.input.unwrap_or_else(default_input_path);
    println!("input_file = {input_file:?}");
    if args.part == None || args.part == Some(1) {
        let res = run_part1(&input_file);
        println!("Result part1 : {res}");
    }
    if args.part == None || args.part == Some(2) {
        let res = run_part2(&input_file);
        println!("Result part2 : {res}");
    }
}

#[cfg(test)]
mod tests {
    // print output :
    // cargo test -- --show-output

    use super::*;

    const TEST_FILE_0: &str = "example0.txt";
    const TEST_FILE_1: &str = "example1.txt";
    // const TEST_FILE_2: &str = "example2.txt";

    const TEST_RESULT_PART1_FILE_1: i64 = 55312;
    // const TEST_RESULT_PART2_FILE_1: i64 = 48;

    #[test]
    fn test_simple() {
        let mut stones = Stones::parse_file(TEST_FILE_0);
        debug!("stones = {:?}", stones);
        stones.blink_many(1);
        let result = stones.stones.len();
        println!("After 1 iterations : {result}");
        assert_eq!(result, 7);
    }
    #[test]
    fn test_part1() {
        let result = run_part1(TEST_FILE_1);
        println!("Result part1 on example : {result}");
        assert_eq!(result, TEST_RESULT_PART1_FILE_1);
    }

    #[test]
    fn test_part2() {
        let result = run_part2(TEST_FILE_1);
        println!("Result part2 on example : {result}");
        println!("[WFT] NO EXAMPLE FOR PART 2")
        // assert_eq!(result, TEST_RESULT_PART2_FILE_1);
    }
}
