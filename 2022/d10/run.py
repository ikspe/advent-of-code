import argparse
import time


def _is_interesting_cycle_part1(cycle: int):
    return 20 <= cycle <= 220 and cycle % 40 == 20


def main(is_part1: bool, input_file: str, verbose_level=0):
    register = 1
    cycle_count = 0
    cycle_by_cycle_num = cycle_count
    intersting_cycles_part1 = []

    all_register_positions = []

    with open(input_file) as f:
        for line in f:
            tokens = line.strip().split()
            operator = tokens[0]
            if verbose_level >= 3:
                print(f"[DEBUG] line = {line.strip()} => operator = {operator} - other tokens = {tokens[1:]}")
            previous_register = register
            if operator == "noop":
                cycle_count += 1
            elif operator == "addx":
                register += int(tokens[1])
                cycle_count += 2
            else:
                raise ValueError(f"line {line}")
            while cycle_by_cycle_num != cycle_count:
                all_register_positions.append(previous_register)
                cycle_by_cycle_num += 1
                if _is_interesting_cycle_part1(cycle_by_cycle_num):
                    intersting_cycles_part1.append((cycle_by_cycle_num, previous_register))


    if is_part1:
        if verbose_level >= 1:
            print(f"[INFO] intersting_cycles : {intersting_cycles_part1}")
            print(f"[INFO] signal strengths : {[x[0] * x[1] for x in intersting_cycles_part1]}")
        sum_signal_strength = sum([x[0] * x[1] for x in intersting_cycles_part1])
        print(f"Result : {sum_signal_strength}")
    else:
        width = 40
        pixels_part2 = []
        for cycle in range(6 * width):
            idx = cycle % width
            if all_register_positions[cycle]-1 <= idx <= all_register_positions[cycle]+1:
                pixels_part2.append("#")
            else:
                pixels_part2.append(".")

        for line_idx in range(6):
            print("".join(pixels_part2[line_idx*width:(line_idx+1)*width]))


if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")

    # parse the arguments
    args = parser.parse_args()

    # run !
    res = None
    time_begin = time.time()
    try:
        res = main(args.part1, args.input, verbose_level=args.verbose)
    finally:
        time_end = time.time()
        print(f"Result = {res}")
        print(f"Computation time = {time_end - time_begin} s")
