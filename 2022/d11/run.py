import argparse
import math
import time
import typing


class Monkey:
    LINE_ID_PREFIX = "Monkey "
    LINE_ITEMS_PREFIX = "  Starting items: "
    LINE_OPERATION_PREFIX = "  Operation: "
    LINE_TEST_DIVISABLE_PREFIX = "  Test: divisible by "
    LINE_TEST_TRUE_PREFIX = "    If true: throw to monkey "
    LINE_TEST_FALSE_PREFIX = "    If false: throw to monkey "

    id: int = None
    items: typing.List[int] = None
    operation_str: str = None
    operation_fn: callable = None
    test_divisable_by: int = None
    test_if_true_throw_to: int = None
    test_if_false_throw_to: str = None

    count_inspected_items = 0

    @classmethod
    def new_from_lines(cls, list_lines: typing.List[str]):
        """
Monkey 0:
  Starting items: 79, 98
  Operation: new = old * 19
  Test: divisible by 23
    If true: throw to monkey 2
    If false: throw to monkey 3
        """
        # print(f"[DEBUG] new_from_lines() : list_lines = {list_lines}")
        iter_lines = iter(list_lines)
        new_monkey = object.__new__(cls)

        line = next(iter_lines)
        assert line.startswith(Monkey.LINE_ID_PREFIX) and line.endswith(":")
        new_monkey.id = int(line[len(Monkey.LINE_ID_PREFIX):-1])

        line = next(iter_lines)
        assert line.startswith(Monkey.LINE_ITEMS_PREFIX)
        new_monkey.items = [int(t.strip()) for t in line[len(Monkey.LINE_ITEMS_PREFIX):].split(",")]

        line = next(iter_lines)
        assert line.startswith(Monkey.LINE_OPERATION_PREFIX)
        new_monkey.operation_str = line[len(Monkey.LINE_OPERATION_PREFIX):]
        new_monkey.operation_fn = cls._generate_operation_function(new_monkey.operation_str)

        line = next(iter_lines)
        assert line.startswith(Monkey.LINE_TEST_DIVISABLE_PREFIX)
        new_monkey.test_divisable_by = int(line[len(Monkey.LINE_TEST_DIVISABLE_PREFIX):])

        line = next(iter_lines)
        assert line.startswith(Monkey.LINE_TEST_TRUE_PREFIX)
        new_monkey.test_if_true_throw_to = int(line[len(Monkey.LINE_TEST_TRUE_PREFIX):])

        line = next(iter_lines)
        assert line.startswith(Monkey.LINE_TEST_FALSE_PREFIX)
        new_monkey.test_if_false_throw_to = int(line[len(Monkey.LINE_TEST_FALSE_PREFIX):])

        return new_monkey

    @classmethod
    def _generate_operation_function(cls, formula):
        tokens = formula.split("=")
        assert len(tokens) == 2
        assert tokens[0].strip() == "new"
        tokens = tokens[1].strip().split()
        assert len(tokens) == 3
        assert tokens[0] == "old"
        operator = tokens[1]
        other = tokens[2]
        if operator == "*":
            if other == "old":
                return lambda val: val * val
            else:
                other = int(other)
                return lambda val: val * other
        elif operator == "+":
            other = int(other)
            return lambda val: val + other
        else:
            raise ValueError(f"Unknown formula type : {formula}")


    def __str__(self) -> str:
        return f"Monkey<id={self.id} items = {self.items} operation='{self.operation_str}' test_div={self.test_divisable_by} if_true={self.test_if_true_throw_to} if false={self.test_if_false_throw_to}>"

    def inspect_items(self, relief_lower_worry: bool):
        items_copy = self.items.copy()
        self.items.clear()
        result_throwing = []
        for worry_level in items_copy:
            # inspect
            worry_level = self.operation_fn(worry_level)
            # relief
            if relief_lower_worry:
                worry_level = worry_level // 3
            # throw
            if worry_level % self.test_divisable_by == 0:
                result_throwing.append((self.test_if_true_throw_to, worry_level))
            else:
                result_throwing.append((self.test_if_false_throw_to, worry_level))
        self.count_inspected_items = self.count_inspected_items + len(result_throwing)
        return result_throwing


class AllMonkeys:
    def __init__(self, verbose_level: int):
        self.verbose_level = verbose_level
        self._all_monkeys: typing.List[Monkey] = []
        self._round_count = 0

    def add_monkey_from_lines(self, list_lines: typing.List[str]):
        monkey = Monkey.new_from_lines(list_lines)
        if self.verbose_level >= 1:
            print(f"[INFO] found {monkey}")
        assert monkey.id == len(self._all_monkeys)
        self._all_monkeys.append(monkey)

    def get_monkeys_test_lcm(self):
        all_monkeys_test_val = [monkey.test_divisable_by for monkey in self._all_monkeys]
        res_lcm = math.lcm(*all_monkeys_test_val)
        if self.verbose_level >= 1:
            print(f"[INFO] all_monkeys_test_val = {all_monkeys_test_val} => lcm = {res_lcm}")
        return res_lcm

    def exec_round(self, relief_lower_worry: bool, all_monkeys_test_lcm: typing.Optional[int]):
        for monkey in self._all_monkeys:
            result_throwing = monkey.inspect_items(relief_lower_worry=relief_lower_worry)
            for idx, worry_level in result_throwing:
                if all_monkeys_test_lcm:  # TODO : probably doesn't change part1 algo to force using lcm all the time...
                    worry_level %= all_monkeys_test_lcm  # least common multiple of monkeys divisible check
                monkey_dest = self._all_monkeys[idx]
                monkey_dest.items.append(worry_level)
        self._round_count += 1

    def print_state(self):
        print(f"After round {self._round_count:2}, the monkeys are holding items with these worry levels:")
        for monkey in self._all_monkeys:
            print(f"Monkey {monkey.id}: {', '.join([str(x) for x in monkey.items])}")
        print()

    def get_count_inspected_items(self):
        count_inspected_items = []
        for monkey in self._all_monkeys:
            count_inspected_items.append((monkey.id, monkey.count_inspected_items))
        return count_inspected_items


def main(is_part1: bool, input_file: str, verbose_level=0):
    all_monkeys = AllMonkeys(verbose_level=verbose_level)
    with open(input_file) as f:
        current_lines: typing.List[str] = []
        for line in f:
            line = line.rstrip()
            if not line:
                all_monkeys.add_monkey_from_lines(current_lines)
                current_lines = []
                continue
            current_lines.append(line)
            continue
        all_monkeys.add_monkey_from_lines(current_lines)

    if verbose_level >= 2:
        all_monkeys.print_state()

    all_monkeys_test_lcm = all_monkeys.get_monkeys_test_lcm()

    if is_part1:
        max_round_count = 20
        relief_lower_worry = True
        all_monkeys_test_lcm = None
    else:
        max_round_count = 10000
        relief_lower_worry = False

    for round_num in range(1, max_round_count + 1):
        all_monkeys.exec_round(relief_lower_worry, all_monkeys_test_lcm)
        if verbose_level >= 3 or (verbose_level >= 2 and (round_num in (1, 20) or round_num % 1000 == 0)):
            print(f"==== After round {round_num} ====")
            count_inspected_items = all_monkeys.get_count_inspected_items()
            for idx, count in count_inspected_items:
                print(f"Monkey {idx}: inspected items {count} times.")
            print()
        if verbose_level >= 3:
            all_monkeys.print_state()

    count_inspected_items = all_monkeys.get_count_inspected_items()
    if verbose_level >= 1:
        print(f"==== Final state (after round {round_num}) ====")
        for idx, count in count_inspected_items:
            print(f"Monkey {idx}: inspected items {count} times.")
        print()

    count_ordered = sorted(count_inspected_items, key=lambda x: x[1], reverse=True)
    print(f"Result (monkey business) = {count_ordered[0][1] * count_ordered[1][1]} ({count_ordered[0][1]} * {count_ordered[1][1]})")


if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")

    # parse the arguments
    args = parser.parse_args()

    # run !
    res = None
    time_begin = time.time()
    try:
        res = main(args.part1, args.input, verbose_level=args.verbose)
    finally:
        time_end = time.time()
        print(f"Result = {res}")
        print(f"Computation time = {time_end - time_begin} s")
