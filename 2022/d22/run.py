import argparse
import enum
import time


class Direction(enum.IntEnum):
    RIGHT = 0
    DOWN = 1
    LEFT = 2
    UP = 3

_DIRECTIONS_TURNING_RIGHT = {Direction.RIGHT: Direction.DOWN, Direction.DOWN: Direction.LEFT, Direction.LEFT: Direction.UP, Direction.UP: Direction.RIGHT}
_DIRECTIONS_TURNING_LEFT = {v: k for k, v in _DIRECTIONS_TURNING_RIGHT.items()}
_DIRECTIONS_FACING = {Direction.RIGHT: ">", Direction.DOWN: "v", Direction.LEFT: "<", Direction.UP: "^"}

def _split_actions_line(line_action: str):
    actions = []
    current_token = ""
    for t in line_action:
        if t.isnumeric():
            current_token += t
        else:
            if current_token:
                actions.append(current_token)
                current_token = ""
            actions.append(t)
    if current_token:
        actions.append(current_token)
    return actions


class _Board:
    def __init__(self, tiles: dict, verbose_level: int) -> None:
        self.tiles = tiles
        self._min_x = min([x for x, _ in self.tiles])
        self._max_x = max([x for x, _ in self.tiles])
        self._min_y = min([y for _, y in self.tiles])
        self._max_y = max([y for _, y in self.tiles])
        self._current_pos: tuple = None
        self._current_direction = None
        self.verbose_level = verbose_level

    def set_init_pos(self):
        for x in range(self._max_x):
            tile = self.tiles.get((x, 0))
            if tile == ".":
                self._current_pos = (x, 0)
                break
        assert self._current_pos
        self._current_direction = Direction.RIGHT
        self.tiles[self._current_pos] = _DIRECTIONS_FACING[self._current_direction]

    def exec_action(self, action):
        if action == "R":
            self._current_direction = _DIRECTIONS_TURNING_RIGHT[self._current_direction]
            self.tiles[self._current_pos] = _DIRECTIONS_FACING[self._current_direction]
            return
        elif action == "L":
            self._current_direction = _DIRECTIONS_TURNING_LEFT[self._current_direction]
            self.tiles[self._current_pos] = _DIRECTIONS_FACING[self._current_direction]
            return

        for _ in range(int(action)):
            next_pos, next_dir = self._find_next_pos_and_dir()
            if self.tiles[next_pos] == "#":
                break
            self._current_pos = next_pos
            self._current_direction = next_dir
            self.tiles[self._current_pos] = _DIRECTIONS_FACING[self._current_direction]

    def _find_next_pos_and_dir(self):
        raise NotImplementedError

    def _find_immediate_next_pos(self, pos: tuple, direction: Direction):
        x, y = pos
        # abstract class : return position that need to be processed given the cube being in 2D or 3D
        if direction == Direction.RIGHT:
            return x+1, y
        if direction == Direction.DOWN:
            return x, y+1
        if direction == Direction.LEFT:
            return x-1, y
        if direction == Direction.UP:
            return x, y-1
        raise ValueError(f"Incorrect direction {direction}")

    def draw(self):
        lines = [[" " for _ in range(self._min_x, self._max_x + 1)] for _ in range(self._min_y, self._max_y + 1)]
        for (x, y), tile in self.tiles.items():
            lines[y][x] = tile
        print("\n".join(["".join(l) for l in lines]))

    def get_password_part1(self):
        x, y = self._current_pos
        if self.verbose_level > 1:
            print(f"[DEBUG] self._current_pos = {self._current_pos} / self._current_direction = {self._current_direction}")
        return 1000 * (y+1) + 4 * (x+1) + {Direction.RIGHT: 0, Direction.DOWN: 1, Direction.LEFT: 2, Direction.UP: 3}[self._current_direction]


class Board2D(_Board):

    def _find_immediate_next_pos(self, pos: tuple, direction: Direction):
        x, y = super()._find_immediate_next_pos(pos, direction)
        # in python, modulo gives positive result, even for negative numbers
        return x%(self._max_x+1), y%(self._max_y+1)

    def _find_next_pos_and_dir(self):
        next_pos = self._find_immediate_next_pos(self._current_pos, self._current_direction)
        while next_pos not in self.tiles:
            next_pos = self._find_immediate_next_pos(next_pos, self._current_direction)
        # 2D : direction doesn't change
        return next_pos, self._current_direction


class Board3D(_Board):
    def __init__(self, tiles: dict, verbose_level: int) -> None:
        _Board.__init__(self, tiles=tiles, verbose_level=verbose_level)

        self._transitions_to = {}  # dict : key = (position orig, direction orig), value = (position next, direction next) 

        # let's go for hardcoding transitions... no shame
        if len(self.tiles) == 6 * 50 * 50:
            cube_edge_len = 50
            assert self._max_x == 3 * cube_edge_len - 1

            # fold on A
            for i in range(cube_edge_len):
                self._create_transition(pos_a=(cube_edge_len, i), pos_b=(0, 3*cube_edge_len-1-i),
                    dir_a1=Direction.LEFT, dir_b1=Direction.RIGHT, dir_b2=Direction.LEFT, dir_a2=Direction.RIGHT)
                #pos_a = (cube_edge_len, i)
                #pos_b = (0, 3*cube_edge_len-1-i)
                #self._transitions_to[(pos_a, Direction.LEFT)] = (pos_b, Direction.RIGHT)
                #self._transitions_to[(pos_b, Direction.LEFT)] = (pos_a, Direction.RIGHT)

            # fold on B
            for i in range(cube_edge_len):
                self._create_transition(pos_a=(cube_edge_len, cube_edge_len+i), pos_b=(i, 2*cube_edge_len),
                    dir_a1=Direction.LEFT, dir_b1=Direction.DOWN, dir_b2=Direction.UP, dir_a2=Direction.RIGHT)
                #pos_a = (cube_edge_len, cube_edge_len+i)
                #pos_b = (cube_edge_len-1-i, 2*cube_edge_len)
                #self._transitions_to[(pos_a, Direction.LEFT)] = (pos_b, Direction.DOWN)
                #self._transitions_to[(pos_b, Direction.UP)] = (pos_a, Direction.RIGHT)

            # fold on C
            for i in range(cube_edge_len):
                self._create_transition(pos_a=(cube_edge_len+i, 0), pos_b=(0, 3*cube_edge_len+i),
                    dir_a1=Direction.UP, dir_b1=Direction.RIGHT, dir_b2=Direction.LEFT, dir_a2=Direction.DOWN)
                #pos_a = (cube_edge_len+i, 0)
                #pos_b = (0, 3*cube_edge_len+i)
                #self._transitions_to[(pos_a, Direction.UP)] = (pos_b, Direction.RIGHT)
                #self._transitions_to[(pos_b, Direction.LEFT)] = (pos_a, Direction.DOWN)

            # fold on D
            for i in range(cube_edge_len):
                self._create_transition(pos_a=(3*cube_edge_len-1, i), pos_b=(2*cube_edge_len-1, 3*cube_edge_len-1-i),
                    dir_a1=Direction.RIGHT, dir_b1=Direction.LEFT, dir_b2=Direction.RIGHT, dir_a2=Direction.LEFT)
                #pos_a = (3*cube_edge_len-1, i)
                #pos_b = (2*cube_edge_len-1, 3*cube_edge_len-1-i)
                #self._transitions_to[(pos_a, Direction.RIGHT)] = (pos_b, Direction.LEFT)
                #self._transitions_to[(pos_b, Direction.RIGHT)] = (pos_a, Direction.LEFT)

            # fold on E
            for i in range(cube_edge_len):
                self._create_transition(pos_a=(2*cube_edge_len+i, cube_edge_len-1), pos_b=(2*cube_edge_len-1, cube_edge_len+i),
                    dir_a1=Direction.DOWN, dir_b1=Direction.LEFT, dir_b2=Direction.RIGHT, dir_a2=Direction.UP)
                #pos_a = (2*cube_edge_len+i, cube_edge_len-1)
                #pos_b = (2*cube_edge_len-1, cube_edge_len+i)
                #self._transitions_to[(pos_a, Direction.DOWN)] = (pos_b, Direction.LEFT)
                #self._transitions_to[(pos_b, Direction.RIGHT)] = (pos_a, Direction.UP)

            # fold on F
            for i in range(cube_edge_len):
                self._create_transition(pos_a=(cube_edge_len+i, 3*cube_edge_len-1), pos_b=(cube_edge_len-1, 3*cube_edge_len+i),
                    dir_a1=Direction.DOWN, dir_b1=Direction.LEFT, dir_b2=Direction.RIGHT, dir_a2=Direction.UP)
                #pos_a = (cube_edge_len+i, 3*cube_edge_len-1)
                #pos_b = (cube_edge_len-1, 3*cube_edge_len+i)
                #self._transitions_to[(pos_a, Direction.DOWN)] = (pos_b, Direction.LEFT)
                #self._transitions_to[(pos_b, Direction.RIGHT)] = (pos_a, Direction.UP)

            # fold on G
            for i in range(cube_edge_len):
                self._create_transition(pos_a=(2*cube_edge_len+i, 0), pos_b=(i, 4*cube_edge_len-1),
                    dir_a1=Direction.UP, dir_b1=Direction.UP, dir_b2=Direction.DOWN, dir_a2=Direction.DOWN)
                #pos_a = (2*cube_edge_len+i, 0)
                #pos_b = (cube_edge_len-1-i, 4*cube_edge_len-i)  # double ERROR !!
                #self._transitions_to[(pos_a, Direction.UP)] = (pos_b, Direction.UP)
                #self._transitions_to[(pos_b, Direction.DOWN)] = (pos_a, Direction.DOWN)

        elif self._max_x == 15:
            cube_edge_len = 4
            raise NotImplementedError
        else:
            raise ValueError(f"Not supporting this cube pattern")

    def _find_immediate_next_pos(self, pos: tuple, direction: Direction):
        return super()._find_immediate_next_pos(pos, direction)

    def _find_next_pos_and_dir(self):
        transition = self._transitions_to.get((self._current_pos, self._current_direction))
        if self.verbose_level >= 4:
            print(f"[DEBUG] moving 1... current position = {self._current_pos} / direction = {self._current_direction}")
        if transition is not None:
            next_pos, next_dir = transition
        else:
            next_pos = self._find_immediate_next_pos(self._current_pos, self._current_direction)
            assert next_pos in self.tiles
            next_dir = self._current_direction
        return next_pos, next_dir

    def run_part_2_unit_tests(self):
        if self.verbose_level <= 1:
            return
        if self.verbose_level >= 2:
            print(f"[DEBUG] starting part 2 unit tests")
        self._run_part_2_unit_tests()
        if self.verbose_level >= 2:
            print(f"[DEBUG] finished part 2 unit tests")

    def _create_transition(self, pos_a, pos_b, dir_a1, dir_b1, dir_b2, dir_a2):
        self._transitions_to[(pos_a, dir_a1)] = (pos_b, dir_b1)
        self._transitions_to[(pos_b, dir_b2)] = (pos_a, dir_a2)

    def _check_transition(self, pos_a, pos_b, dir_a1, dir_b1, dir_b2, dir_a2):
        pos, dir = self._transitions_to[(pos_a, dir_a1)]
        assert ((pos, dir) == (pos_b, dir_b1)), (pos, dir)
        pos, dir = self._transitions_to[(pos_b, dir_b2)]
        assert ((pos, dir) == (pos_a, dir_a2)), (pos, dir)


    def _run_part_2_unit_tests(self):
        if len(self.tiles) != 6 * 50 * 50:
            raise NotImplementedError  # check this is not the example

        assert len(self._transitions_to) == 14 * 50

        # fold on A
        self._check_transition(pos_a=(50, 0), pos_b=(0, 149),
            dir_a1=Direction.LEFT, dir_b1=Direction.RIGHT, dir_b2=Direction.LEFT, dir_a2=Direction.RIGHT)
        self._check_transition(pos_a=(50, 49), pos_b=(0, 100),
            dir_a1=Direction.LEFT, dir_b1=Direction.RIGHT, dir_b2=Direction.LEFT, dir_a2=Direction.RIGHT)

        # fold on B
        self._check_transition(pos_a=(50, 50), pos_b=(0, 100),
            dir_a1=Direction.LEFT, dir_b1=Direction.DOWN, dir_b2=Direction.UP, dir_a2=Direction.RIGHT)
        self._check_transition(pos_a=(50, 99), pos_b=(49, 100),
            dir_a1=Direction.LEFT, dir_b1=Direction.DOWN, dir_b2=Direction.UP, dir_a2=Direction.RIGHT)

        # fold on C
        self._check_transition(pos_a=(50, 0), pos_b=(0, 150),
            dir_a1=Direction.UP, dir_b1=Direction.RIGHT, dir_b2=Direction.LEFT, dir_a2=Direction.DOWN)
        self._check_transition(pos_a=(99, 0), pos_b=(0, 199),
            dir_a1=Direction.UP, dir_b1=Direction.RIGHT, dir_b2=Direction.LEFT, dir_a2=Direction.DOWN)

        # fold on D
        self._check_transition(pos_a=(149, 0), pos_b=(99, 149),
            dir_a1=Direction.RIGHT, dir_b1=Direction.LEFT, dir_b2=Direction.RIGHT, dir_a2=Direction.LEFT)
        self._check_transition(pos_a=(149, 49), pos_b=(99, 100),
            dir_a1=Direction.RIGHT, dir_b1=Direction.LEFT, dir_b2=Direction.RIGHT, dir_a2=Direction.LEFT)

        # fold on E
        self._check_transition(pos_a=(100, 49), pos_b=(99, 50),
            dir_a1=Direction.DOWN, dir_b1=Direction.LEFT, dir_b2=Direction.RIGHT, dir_a2=Direction.UP)
        self._check_transition(pos_a=(149, 49), pos_b=(99, 99),
            dir_a1=Direction.DOWN, dir_b1=Direction.LEFT, dir_b2=Direction.RIGHT, dir_a2=Direction.UP)

        # fold on F
        self._check_transition(pos_a=(50, 149), pos_b=(49, 150),
            dir_a1=Direction.DOWN, dir_b1=Direction.LEFT, dir_b2=Direction.RIGHT, dir_a2=Direction.UP)
        self._check_transition(pos_a=(99, 149), pos_b=(49, 199),
            dir_a1=Direction.DOWN, dir_b1=Direction.LEFT, dir_b2=Direction.RIGHT, dir_a2=Direction.UP)

        # fold on G
        self._check_transition(pos_a=(100, 0), pos_b=(0, 199),
            dir_a1=Direction.UP, dir_b1=Direction.UP, dir_b2=Direction.DOWN, dir_a2=Direction.DOWN)
        self._check_transition(pos_a=(149, 0), pos_b=(49, 199),
            dir_a1=Direction.UP, dir_b1=Direction.UP, dir_b2=Direction.DOWN, dir_a2=Direction.DOWN)

def main(is_part1: bool, input_file: str, verbose_level=0):
    with open(input_file) as f:
        lines = f.read().splitlines()
        assert not lines[-2]

    tiles = {}
    for idx_y, it_line in enumerate(lines[:-2]):
        for idx_x, it_tile in enumerate(it_line):
            if it_tile != " ":
                tiles[(idx_x, idx_y)] = it_tile

    if is_part1:
        print(f"[TRACE] creating board 2D")
        board = Board2D(tiles=tiles, verbose_level=verbose_level)
    else:
        print(f"[TRACE] creating board 3D")
        board = Board3D(tiles=tiles, verbose_level=verbose_level)
        board.run_part_2_unit_tests()

    board.set_init_pos()
    del tiles
    list_actions = _split_actions_line(lines[-1])

    if verbose_level >= 3:
        print(f"[DEBUG] board ({len(board.tiles)} tiles):")
        board.draw()
        print(f"[DEBUG] actions : {list_actions}")


    for action in list_actions:
        if verbose_level >= 2:
            print(f"[DEBUG] executing action : {action}")
        board.exec_action(action)
        if verbose_level >= 2:
            board.draw()

    password = board.get_password_part1()
    print(f"Result (password) = {password}")
    return password


if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")

    # parse the arguments
    args = parser.parse_args()

    # run !
    res = None
    time_begin = time.time()
    try:
        res = main(args.part1, args.input, verbose_level=args.verbose)
    finally:
        time_end = time.time()
        print(f"Result = {res}")
        print(f"Computation time = {time_end - time_begin} s")
