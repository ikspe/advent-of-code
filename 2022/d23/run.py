import argparse
import enum
import time
import typing


PosType = typing.Tuple[int, int]


class MoveDirection(enum.IntEnum):
    NORTH = 0
    WEST = 1
    SOUTH = 2
    EAST = 3

def get_next_direction(prev_direction):
    if prev_direction is None or prev_direction == MoveDirection.EAST:
        return MoveDirection.NORTH
    if prev_direction == MoveDirection.NORTH:
        return MoveDirection.SOUTH
    elif prev_direction == MoveDirection.SOUTH:
        return MoveDirection.WEST
    elif prev_direction == MoveDirection.WEST:
        return MoveDirection.EAST
    raise ValueError(f"Invalid prev_direction {prev_direction}")

def find_positions_to_check(pos: PosType, direction: MoveDirection):
    x, y = pos
    if direction == MoveDirection.NORTH:
        return [(x+i, y-1) for i in range(-1, 2)], (x, y-1)
    elif direction == MoveDirection.WEST:
        return [(x-1, y+i) for i in range(-1, 2)], (x-1, y)
    elif direction == MoveDirection.SOUTH:
        return [(x+i, y+1) for i in range(-1, 2)], (x, y+1)
    elif direction == MoveDirection.EAST:
        return [(x+1, y+i) for i in range(-1, 2)], (x+1, y)
    raise ValueError(f"Invalid direction {direction}")

def find_all_adjacent_positions(pos: PosType):
    x, y = pos
    return [(x+i, y+j) for i in range(-1,2) for j in range(-1,2) if (i,j)!=(0,0)]


class ElvesPositions:
    def __init__(self, verbose_level: int) -> None:
        self.positions: typing.Set[PosType] = set()
        self.verbose_level = verbose_level

    def add_elves(self, pos: PosType):
        self.positions.add(pos)

    def find_min_max_positions(self):
        min_x = min(pos[0] for pos in self.positions)
        max_x = max(pos[0] for pos in self.positions)
        min_y = min(pos[1] for pos in self.positions)
        max_y = max(pos[1] for pos in self.positions)
        return min_x, max_x, min_y, max_y

    def exec_round(self, directions_to_try: typing.List[MoveDirection]):
        count_moved_elves = 0
        orig_elves_count = len(self.positions)

        if self.verbose_level >= 4:
            print(f"Moving, elves try first to go to directions : {directions_to_try}")

        suggested_moves = dict()
        elves_staying: typing.Set[PosType] = set()

        # first half : check where each elve should go
        if self.verbose_level >= 4:
            print(f"[DEBUG] part 1")
        for pos in self.positions:
            adjacent_positions = find_all_adjacent_positions(pos)
            elve_moved = False
            if any(other_pos in self.positions for other_pos in adjacent_positions):
                for direction in directions_to_try:
                    positions_to_check, move_goal = find_positions_to_check(pos, direction)
                    elve_moved = all(other_pos not in self.positions for other_pos in positions_to_check)
                    if elve_moved:
                        moving_to_same_pos = suggested_moves.get(move_goal)
                        if not moving_to_same_pos:
                            moving_to_same_pos = []
                            suggested_moves[move_goal] = moving_to_same_pos
                        moving_to_same_pos.append(pos)
                        if self.verbose_level >= 4:
                            print(f"[DEBUG] elve at pos {pos} proposes moving {direction.name} => new pos = {move_goal}")
                        break
                if not elve_moved:
                    elves_staying.add(pos)
                    if self.verbose_level >= 4:
                        print(f"[DEBUG] elve at pos {pos} is NOT moving => staying at = {pos} (no available space)")
            else:
                elves_staying.add(pos)
                if self.verbose_level >= 4:
                    print(f"[DEBUG] elve at pos {pos} is NOT moving => staying at = {pos} (no adjacent elves)")
        
        # 2nd part : really move the elves
        if self.verbose_level >= 4:
            print(f"[DEBUG] part 2")
        self.positions.clear()
        self.positions.update(elves_staying)
        for move_goal, move_orig in suggested_moves.items():
            if len(move_orig) == 1:
                # yes, elve moves
                self.positions.add(move_goal)
                count_moved_elves += 1
            else:
                # several elves want to go at the same direction, so they all stay at their place
                for it_orig_pos in move_orig:
                    self.positions.add(it_orig_pos)
                    if self.verbose_level >= 4:
                        print(f"[DEBUG] elve at pos {it_orig_pos} can't move to {move_goal} : several elves want to move to same place => staying at pos = {it_orig_pos}")
        
        # final check
        assert len(self.positions) == orig_elves_count
        return count_moved_elves

    def draw(self, boundaries=None):
        if boundaries is not None:
            min_x, max_x, min_y, max_y = boundaries
        else:
            min_x, max_x, min_y, max_y = self.find_min_max_positions()
        all_tiles = [["." for _ in range(min_x, max_x + 1)] for _ in range(min_y, max_y + 1)]
        for x, y in self.positions:
            all_tiles[y - min_y][x - min_x] = "#"
        print("\n".join(["".join(line_tiles) for line_tiles in all_tiles]))


def main(is_part1: bool, input_file: str, verbose_level=0):
    positions = ElvesPositions(verbose_level=verbose_level)
    with open(input_file) as f:
        for idx_y, line in enumerate(f.read().splitlines()):
            for idx_x, data in enumerate(line):
                if data == "#":
                    positions.add_elves((idx_x, idx_y))

    if is_part1:
        max_rounds = 10
    else:
        max_rounds = None

    draw_boundaries = None

    if verbose_level >= 3:
        print(f"== Initial State ==")
        positions.draw(draw_boundaries)
        print()

    direction = None
    round_num = 0
    while True:
        if max_rounds is not None and round_num >= max_rounds:
            break
        round_num += 1
        direction = get_next_direction(direction)
        if verbose_level >= 3:
            print(f"== Start of Round {round_num} (first direction = {direction.name}) ==")
        direction_to_try = [direction]
        for _ in range(3):
            direction_to_try.append(get_next_direction(direction_to_try[-1]))

        moving_elves = positions.exec_round(direction_to_try)
        if verbose_level >= 3:
            print(f"== End of Round {round_num} (first direction = {direction.name}) ==")
            positions.draw(draw_boundaries)
            print()

        if max_rounds is None and moving_elves == 0:
            if verbose_level >= 1:
                print(f"[INFO] no elves moved at round = {round_num}")
            break

    min_x, max_x, min_y, max_y = positions.find_min_max_positions()
    nb_tiles = (max_x - min_x + 1) * (max_y - min_y + 1)
    nb_occupied_positions = len(positions.positions)
    print(f"[INFO] nb free tiles = nb_tiles - nb_occupied_positions = {nb_tiles} - {nb_occupied_positions} = {nb_tiles - nb_occupied_positions}")
    if is_part1:
        print(f"[INFO] Result (nb free tiles) = {nb_tiles - nb_occupied_positions}")
        return nb_tiles - nb_occupied_positions
    else:
        print(f"[INFO] Result (number of the first round where no Elf moves) = {round_num}")
        return round_num


if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")

    # parse the arguments
    args = parser.parse_args()

    # run !
    res = None
    time_begin = time.time()
    try:
        res = main(args.part1, args.input, verbose_level=args.verbose)
    finally:
        time_end = time.time()
        print(f"Result = {res}")
        print(f"Computation time = {time_end - time_begin} s")
