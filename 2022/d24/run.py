import argparse
import enum
import time
import typing


class Direction(enum.IntEnum):
    RIGHT = 0
    DOWN = 1
    LEFT = 2
    UP = 3
    
_DIRECTIONS_TO_STR = {Direction.RIGHT: ">", Direction.DOWN: "v", Direction.LEFT: "<", Direction.UP: "^"}
_DIRECTIONS_FROM_STR = {v: k for k, v in _DIRECTIONS_TO_STR.items()}

PosType = typing.Tuple[int, int]

class WorldMap:

    _WORLD_AT_MOMENT = {}  # key : moment ; value : world
    verbose_level: int = 0

    def __init__(self, walls: typing.Set[PosType], blizzards: typing.Dict[PosType, typing.List[Direction]], max_x, max_y) -> None:
        self.walls = walls
        self.blizzards = blizzards  # key : position ; value = list : directions
        self._min_x = self._min_y = 0
        self._max_x = max_x
        self._max_y = max_y
        
        self.starting_pos = (self._min_x + 1, self._min_y)
        self.end_pos = (self._max_x - 1, self._max_y)

    @classmethod
    def from_lines(cls, list_lines: typing.List[str]):
        nb_lines = len(list_lines)
        nb_cols = len(list_lines[0])

        walls = set()
        blizzards = {}  # key : position ; value = list : 
        
        for idx_y, line in enumerate(list_lines):
            for idx_x, data in enumerate(line):
                pos = (idx_x, idx_y)
                if data == "#":
                    walls.add(pos)
                elif data == ".":
                    pass
                else:
                    direction = _DIRECTIONS_FROM_STR[data]
                    blizzards_at_pos = blizzards.get(pos)
                    if not blizzards_at_pos:
                        blizzards_at_pos = []
                        blizzards[pos] = blizzards_at_pos
                    blizzards_at_pos.append(direction)

        world = WorldMap(walls=walls, blizzards=blizzards, max_x=nb_cols-1, max_y=nb_lines-1)
        return world

    def generate_world_next_moment(self):
        new_blizzards = {}
        # move blizzards
        for (x, y), blizzards_at_pos in self.blizzards.items():
            for it_direction in blizzards_at_pos:
                if it_direction == Direction.DOWN:
                    new_pos = (x, y+1)
                    if new_pos[1] >= self._max_y:
                        new_pos = (x, self._min_y+1)
                elif it_direction == Direction.LEFT:
                    new_pos = (x-1, y)
                    if new_pos[0] <= self._min_x:
                        new_pos = (self._max_x-1, y)
                elif it_direction == Direction.UP:
                    new_pos = (x, y-1)
                    if new_pos[1] <= self._min_y:
                        new_pos = (x, self._max_y-1)
                elif it_direction == Direction.RIGHT:
                    new_pos = (x+1, y)
                    if new_pos[0] >= self._max_x:
                        new_pos = (self._min_x+1, y)
                else:
                    raise ValueError(f"{x}/{y}/{it_direction}")
                new_blizzards_at_pos = new_blizzards.get(new_pos)
                if not new_blizzards_at_pos:
                    new_blizzards_at_pos = []
                    new_blizzards[new_pos] = new_blizzards_at_pos
                new_blizzards_at_pos.append(it_direction)
        # create new world with blizzard positions
        return WorldMap(walls=self.walls.copy(), blizzards=new_blizzards, max_x=self._max_x, max_y=self._max_y)
    
    @classmethod
    def register_world_at_moment(cls, epoch: int, world):
        world: WorldMap = world
        assert epoch not in cls._WORLD_AT_MOMENT
        if cls.verbose_level >= 2:
            print(f"[DEBUG] registering world for moment = {epoch}")
        cls._WORLD_AT_MOMENT[epoch] = world
    
    @classmethod
    def get_world_at_moment(cls, epoch):
        assert isinstance(epoch, int) and epoch >= 0
        world: WorldMap = cls._WORLD_AT_MOMENT.get(epoch)
        if world is None:
            previous_world: WorldMap = cls.get_world_at_moment(epoch - 1)
            world = previous_world.generate_world_next_moment()
            cls.register_world_at_moment(epoch, world)
        return world

    def draw(self, expedition: PosType):
        if expedition:
            assert self.is_expedition_position_legit(expedition)
        assert expedition not in self.walls
        assert expedition not in self.blizzards
        all_lines = [["." for _ in range(self._min_x, self._max_x + 1)] for _ in range(self._min_y, self._max_y + 1)]
        for pos in self.walls:
            all_lines[pos[1]][pos[0]] = "#"
        for pos, blizzards_at_pos in self.blizzards.items():
            if len(blizzards_at_pos) > 1:
                all_lines[pos[1]][pos[0]] = str(len(blizzards_at_pos))
            else:
                all_lines[pos[1]][pos[0]] = _DIRECTIONS_TO_STR[blizzards_at_pos[0]]
        if expedition:
            all_lines[expedition[1]][expedition[0]] = "E"
        print("\n".join(["".join(it_line) for it_line in all_lines]))

    def is_expedition_position_legit(self, expedition: PosType):
        if expedition in self.walls:
            return False
        if expedition in self.blizzards:
            return False
        if not self._min_x <= expedition[0] <= self._max_x:
            return False
        if not self._min_y <= expedition[1] <= self._max_y:
            return False
        return True

def _get_possible_score_part1(ending_point, position, moment) -> int:
    # score : current time + remaining manhattan distance
    return moment + abs(position[0] - ending_point[0]) + abs(position[1] - ending_point[1])

def compute_part1(orig_time: int, starting_point: PosType, ending_point: PosType):
    # A-star algo

    all_next_candidates: typing.List[typing.Tuple[PosType, int, int, list]] = [
        (starting_point, orig_time, _get_possible_score_part1(ending_point=ending_point, position=starting_point, moment=orig_time), [starting_point])
    ]  # list : tuple(position, world time, possible score, list positions)
    encountered_positions_at_time = {(starting_point, orig_time)}
    
    while all_next_candidates:
        all_next_candidates = sorted(all_next_candidates, key=lambda x: x[2])
        position, moment, score, parcours = all_next_candidates.pop(0)
        if position == ending_point:
            print(f"[INFO] found best possible score reaching position {position} at moment {moment}")
            return moment
        if WorldMap.verbose_level >= 3:
            print(f"Minute {moment} (current max score = {score}):")
            WorldMap.get_world_at_moment(moment).draw(position)
        x, y = position
        next_moment = moment+1
        next_world = WorldMap.get_world_at_moment(next_moment)
        for next_pos in [(x, y), (x-1, y), (x+1, y), (x, y-1), (x, y+1)]:
            if not next_world.is_expedition_position_legit(next_pos):
                if WorldMap.verbose_level >= 4:
                    print(f"[DEBUG] position = {position} => next_pos {next_pos} at {next_moment} : skipped (position NOT legit)")
                continue
            if (next_pos, next_moment) in encountered_positions_at_time:
                if WorldMap.verbose_level >= 4:
                    print(f"[DEBUG] position = {position} => next_pos {next_pos} at {next_moment} : skipped (position already encountered)")
                continue
            if WorldMap.verbose_level >= 4:
                print(f"[DEBUG] position = {position} => next_pos {next_pos} at {next_moment} : kept")
            all_next_candidates.append((next_pos, next_moment,
                _get_possible_score_part1(ending_point=ending_point, position=next_pos, moment=next_moment), parcours.copy() + [next_pos]))
            encountered_positions_at_time.add((next_pos, next_moment))
    
    raise ValueError(f"[ERROR] no more candidate position, but still not found any good path :-(")

def main(is_part1: bool, input_file: str, verbose_level=0):
    orig_time = 0
    WorldMap.verbose_level = verbose_level
    with open(input_file) as f:
        all_lines = f.read().splitlines()
        orig_world = WorldMap.from_lines(all_lines)
        WorldMap.register_world_at_moment(orig_time, orig_world)
        starting_point = (1, 0)
        ending_point = (len(all_lines[-1]) - 2, len(all_lines) - 1)
    
    if verbose_level >= 3:
        print(f"starting_point = {starting_point} / ending_point = {ending_point} / len(all_lines) = {len(all_lines)} / len(all_lines[-1]) = {len(all_lines[-1])}")
        print(f"orig_world : {orig_world._min_x} -> {orig_world._max_x} / {orig_world._min_y} -> {orig_world._max_y}")

    if verbose_level >= 1:
        print(f"Initial state:")
        orig_world.draw(None)
        print()

    nb_steps_end_part1 = compute_part1(orig_time=orig_time, starting_point=starting_point, ending_point=ending_point)
    if verbose_level >= 1:
        print(f"[INFO] nb steps end part 1 = {nb_steps_end_part1}")
    if is_part1:
        return nb_steps_end_part1

    nb_steps_end_gathering_snack = compute_part1(orig_time=nb_steps_end_part1, starting_point=ending_point, ending_point=starting_point)
    if verbose_level >= 1:
        print(f"[INFO] nb steps part gathering food = {nb_steps_end_gathering_snack}")

    nb_steps_end_part2 = compute_part1(orig_time=nb_steps_end_gathering_snack, starting_point=starting_point, ending_point=ending_point)
    if verbose_level >= 1:
        print(f"[INFO] nb steps part 2 = {nb_steps_end_part2}")
    print(f"Results (nb steps total) = {nb_steps_end_part2}")
    return nb_steps_end_part2

if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")

    # parse the arguments
    args = parser.parse_args()

    # run !
    res = None
    time_begin = time.time()
    try:
        res = main(args.part1, args.input, verbose_level=args.verbose)
    finally:
        time_end = time.time()
        print(f"Result = {res}")
        print(f"Computation time = {time_end - time_begin} s")
