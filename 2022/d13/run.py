import argparse
import time
import typing


class _Packet:

    @classmethod
    def is_well_ordered(cls, left, right, indent=0):
        # print(" " * indent + f"- Compare {left} vs {right}")
        if isinstance(left, PacketInt) and isinstance(right, PacketInt):
            if left._value < right._value:
                # print(" " * (indent + 2) + f"=> True")
                return True
            elif left._value > right._value:
                # print(" " * (indent + 2) + f"=> False")
                return False
            else:
                # print(" " * (indent + 2) + f"=> None")
                return None
        elif isinstance(left, PacketList) and isinstance(right, PacketList):
            for idx in range(min(len(left._members), len(right._members))):
                cmp = cls.is_well_ordered(left._members[idx], right._members[idx], indent=indent+2)
                if cmp is True:
                    return True
                elif cmp is False:
                    return False
                # else (None) => continue
            if len(left._members) < len(right._members):
                return True
            elif len(left._members) > len(right._members):
                return False
            return None
        elif isinstance(left, PacketInt):
            left2 = PacketList()
            left2.append(left)
            # print(" " * indent + f"- Mixed types: convert left to {left2} and retry comparison")
            return cls.is_well_ordered(left2, right, indent=indent+2)
        else:  # => elif isinstance(right, PacketInt):
            right2 = PacketList()
            right2.append(right)
            # print(" " * indent + f"- Mixed types: convert right to {right2} and retry comparison")
            return cls.is_well_ordered(left, right2, indent=indent+2)

    def __lt__(self, right):
        return self.is_well_ordered(self, right, 0)


class PacketList(_Packet):
    def __init__(self):
        self._members: typing.List[_Packet] = []

    def append(self, packet: _Packet):
        self._members.append(packet)

    def __str__(self):
        return "[" + ",".join([str(x) for x in self._members]) + "]"

class PacketInt(_Packet):
    def __init__(self, val: int):
        self._value = val

    def __str__(self):
        return str(self._value)


def _split_tokens_line(line: str):
    list_tokens = []
    for t in line:
        if t in (",", "[", "]"):
            list_tokens.append(t)
            list_tokens.append("")
        else:
            list_tokens[-1] += t
    return [x for x in list_tokens if x not in ("", ",")]


def _parse_line(line: str):
    line = line.strip()
    if not line:
        return None
    tokens = _split_tokens_line(line)
    stack_indent_level: typing.List[PacketList] = []
    top_packet: PacketList = None
    for t in tokens:
        if t == "[":
            new_list = PacketList()
            if not stack_indent_level:
                assert top_packet is None
                top_packet = new_list
            else:
                stack_indent_level[-1].append(new_list)
            stack_indent_level.append(new_list)
            continue
        elif t == "]":
            stack_indent_level.pop()
        else:
            member = PacketInt(int(t))
            stack_indent_level[-1].append(member)
    assert top_packet
    return top_packet


def main(is_part1: bool, input_file: str, verbose_level=0):
    with open(input_file) as f:
        list_pairs_packets: typing.typing[typing.List[PacketList]] = []
        current_pair = None
        for line in f:

            packet = _parse_line(line)
            if packet is None:
                assert current_pair and len(current_pair) == 2
                current_pair = None
                continue

            if current_pair is None:
                current_pair = []
                list_pairs_packets.append(current_pair)
            current_pair.append(packet)
            continue

    if verbose_level >= 2:
        print(f"[DEBUG] printing packets :")
        for pair in list_pairs_packets:
            assert len(pair) == 2
            for packet in pair:
                print(packet)
            print()

    if is_part1:
        pairs_well_ordered = []
        for pair_idx, pair in enumerate(list_pairs_packets):
            if verbose_level >= 1:
                print(f"==== Pair {pair_idx+1} ====")
            assert len(pair) == 2
            cmp = (pair[0] < pair[1])
            if cmp:
                pairs_well_ordered.append(pair_idx+1)
            if verbose_level >= 1:
                print(f"=> pair in the {'right' if cmp else 'wrong'} order")
                print()

            print(f"Pairs well ordered = {pairs_well_ordered}")
            print(f"Result (sum indexes pairs well ordered) = {sum(pairs_well_ordered)}")
    else:
        all_packets: typing.List[_Packet] = []
        for pair in list_pairs_packets:
            all_packets.extend(pair)

        # add divider packets :
        list_dividers = []
        for str_divider in ["[[2]]", "[[6]]"]:
            packet = _parse_line(str_divider)
            list_dividers.append(packet)
        all_packets.extend(list_dividers)

        all_packets_sorted = sorted(all_packets)
        if verbose_level >= 1:
            print("[INFO] all packets sorted :")

        position_dividers = []
        for idx, packet in enumerate(all_packets_sorted):
            if verbose_level >= 1:
                print(packet)
            if packet in list_dividers:
                position_dividers.append(idx+1)
                if verbose_level >= 1:
                    print(f"    => found divider at position {idx+1}")

        assert len(position_dividers) == 2
        print(f"Result (decoder key) = {position_dividers[0] * position_dividers[1]} (= {position_dividers[0]} * {position_dividers[1]})")


if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")

    # parse the arguments
    args = parser.parse_args()

    # run !
    res = None
    time_begin = time.time()
    try:
        res = main(args.part1, args.input, verbose_level=args.verbose)
    finally:
        time_end = time.time()
        print(f"Result = {res}")
        print(f"Computation time = {time_end - time_begin} s")
