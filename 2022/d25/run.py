import argparse
import time


_DATA_TEST_0 = """
  Decimal          SNAFU
        1              1
        2              2
        3             1=
        4             1-
        5             10
        6             11
        7             12
        8             2=
        9             2-
       10             20
       15            1=0
       20            1-0
     2022         1=11-2
    12345        1-0---0
314159265  1121-1110-1=0
"""

_DATA_TEST_1 = """
 SNAFU  Decimal
1=-0-2     1747
 12111      906
  2=0=      198
    21       11
  2=01      201
   111       31
 20012     1257
   112       32
 1=-1=      353
  1-12      107
    12        7
    1=        3
   122       37
"""

_DICT_SNAFU_VAL = {"2": 2, "1": 1, "0": 0, "-": -1, "=": -2}
_DICT_VAL_TO_SNAFU = {v: k for k, v in _DICT_SNAFU_VAL.items()}

def _convert_snafu_to_decimal(snafu_input: str) -> int:
    result = 0
    for snagit in snafu_input:
        result = result * 5 + _DICT_SNAFU_VAL[snagit]
    return result

def _convert_decimal_to_snafu(val: int) -> str:
    res = ""
    while val:
        mod = (val + 2) % 5 - 2
        res = _DICT_VAL_TO_SNAFU[mod] + res
        val = (val - mod) // 5
    return res

def main(is_part1: bool, input_file: str, verbose_level=0):
    with open(input_file) as f:
        list_snafu_inputs = f.read().splitlines()
    
    if "example" in input_file:
        print(f"[DEBUG] executing unit-tests...")
        for test_input in [_DATA_TEST_0, _DATA_TEST_1]:
            test_input_tokens = [l.split() for l in test_input.splitlines() if l]
            assert len(test_input_tokens[0]) == 2
            if test_input_tokens[0][0] == "SNAFU":
                idx_col_snafu = 0
                idx_col_decimal = 1
            else:
                idx_col_snafu = 1
                idx_col_decimal = 0
            assert test_input_tokens[0][idx_col_snafu] == "SNAFU"
            assert test_input_tokens[0][idx_col_decimal] == "Decimal"
            for tokens in test_input_tokens[1:]:
                print(f"    Testing {tokens} ...")
                assert _convert_snafu_to_decimal(tokens[idx_col_snafu]) == int(tokens[idx_col_decimal])
                assert _convert_decimal_to_snafu(int(tokens[idx_col_decimal])) == tokens[idx_col_snafu]
        print(f"[DEBUG] finished unit-tests...")
    
    if is_part1:
        pass
    else:
        raise NotImplementedError

    sum_all_nums = 0
    for snafu_str in list_snafu_inputs:
        sum_all_nums += _convert_snafu_to_decimal(snafu_str)
    
    res = _convert_decimal_to_snafu(sum_all_nums)
    print(f"[INFO] sum of all numbers = (decimal) {sum_all_nums} / (snafu) {res}")
    return res

if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")

    # parse the arguments
    args = parser.parse_args()

    # run !
    res = None
    time_begin = time.time()
    try:
        res = main(args.part1, args.input, verbose_level=args.verbose)
    finally:
        time_end = time.time()
        print(f"Result = {res}")
        print(f"Computation time = {time_end - time_begin} s")
