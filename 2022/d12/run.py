import argparse
import time


def main(is_part1: bool, input_file: str, verbose_level=0):
    with open(input_file) as f:
        start_pos = None
        end_pos = None
        all_heights = {}
        line_count = 0
        col_count = None
        for line in f:
            for col_idx, alt in enumerate(line.strip()):
                pos = (line_count, col_idx)
                if alt == "S":
                    start_pos = pos
                    alt = "a"
                elif alt == "E":
                    end_pos = pos
                    alt = "z"
                all_heights[pos] = ord(alt) - ord("a")
                assert 0 <= all_heights[pos] < 26

                col_count = col_idx + 1
            line_count += 1

    if verbose_level >= 2:
        print(f"[DEBUG] line_count = {line_count} / col_count = {col_count}")
        print(f"[DEBUG] start_pos = {start_pos}")
        print(f"[DEBUG] end_pos =   {end_pos}")

    if is_part1:
        shortest_path = _find_shortest_path_from_pos(all_heights, start_pos, end_pos, verbose_level, line_count, col_count)
        print(f"Result (shortest path for end_pos = {end_pos}) = {shortest_path}")
    else:
        shortest_path = None
        best_start_pos = None
        for start_pos, alt in all_heights.items():
            if alt != 0:
                continue
            local_shortest_path = _find_shortest_path_from_pos(all_heights, start_pos, end_pos, verbose_level, line_count, col_count)
            if local_shortest_path is None:
                continue
            elif (shortest_path is None) or (local_shortest_path < shortest_path):
                shortest_path = local_shortest_path
                best_start_pos = start_pos
        print(f"Result (shortest path for end_pos = {end_pos} - found from start_pos + {start_pos}) = {shortest_path}")

def _find_shortest_path_from_pos(all_heights, start_pos, end_pos, verbose_level: int, line_count, col_count):
    assert all_heights[start_pos] == 0

    shortest_path = {}
    shortest_path[start_pos] = 0
    next_candidates = [start_pos]

    while next_candidates:
        current_pos = next_candidates.pop(0)
        current_alt = all_heights[current_pos]
        current_path_length = shortest_path[current_pos]

        if verbose_level >= 2:
            print(f"[DEBUG] evaluating position {current_pos} - altitude = {current_alt} - path length = {current_path_length} - next_candidates = {next_candidates}")

        if current_pos == end_pos:
            if verbose_level >= 1:
                print(f"[INFO] found end pos = {current_pos} at shortest length = {current_path_length}")
                return current_path_length

        for neigh_pos in [x for x in
                          [
                              (current_pos[0]+1, current_pos[1]),
                              (current_pos[0]-1, current_pos[1]),
                              (current_pos[0], current_pos[1]+1),
                              (current_pos[0], current_pos[1]-1),
                              ] if 0 <= x[0] < line_count and 0 <= x[1] < col_count]:
            if verbose_level >= 3:
                print(f"[DEBUG]    current_pos = {current_pos} (alt {current_alt} / path length = {current_path_length}) - evaluating neigbour {neigh_pos} at altitude {all_heights[neigh_pos]}", end="")
            if neigh_pos in shortest_path:
                if verbose_level >= 3:
                    print(f"    -> neighbour has already a shortest path")
                continue  # we already have a shorter path to come to this position
            if neigh_pos in next_candidates:
                if verbose_level >= 3:
                    print(f"    -> neighbour is already in next_candidates ; it has a path = {shortest_path[neigh_pos]}")
                continue
            if all_heights[neigh_pos] <= current_alt + 1:
                if verbose_level >= 3:
                    print(f"    -> neighbour added in next_candidates with path = {current_path_length + 1}")
                next_candidates.append(neigh_pos)
                shortest_path[neigh_pos] = current_path_length + 1
            else:
                if verbose_level >= 3:
                    print(f"    > neighbour has incorrect altitude (current = {current_alt} ; neighbour = {all_heights[neigh_pos]})")

    if verbose_level >= 2:
        print(f"[DEBUG] impossible to find path from start_pos {start_pos} to end_pos {end_pos}")
    return None


if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")

    # parse the arguments
    args = parser.parse_args()

    # run !
    res = None
    time_begin = time.time()
    try:
        res = main(args.part1, args.input, verbose_level=args.verbose)
    finally:
        time_end = time.time()
        print(f"Result = {res}")
        print(f"Computation time = {time_end - time_begin} s")
