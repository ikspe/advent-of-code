import argparse
import time


def main(is_part1: bool, input_file: str, verbose_level=0):
    with open(input_file) as f:
        data = f.read().strip()

    if is_part1:
        size_start_of_packet = 4
    else:
        size_start_of_packet = 14

    # find start-of-packet marker : 4 characters, all different
    tmp_start_of_packet = []
    processed_char_start_of_packet = None
    for idx, char_at_idx in enumerate(data):
        tmp_start_of_packet += [char_at_idx]
        if len(tmp_start_of_packet) > size_start_of_packet:
            tmp_start_of_packet.pop(0)
        if len(set(tmp_start_of_packet)) == size_start_of_packet:
            processed_char_start_of_packet = idx + 1
            break

    assert processed_char_start_of_packet is not None
    if verbose_level >= 1:
        print(f"[INFO] processed_char_start_of_packet = {processed_char_start_of_packet} : tmp_start_of_packet = {tmp_start_of_packet}")

    print(f"Result = processed_char_start_of_packet = {processed_char_start_of_packet}")


if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")

    # parse the arguments
    args = parser.parse_args()

    # run !
    res = None
    time_begin = time.time()
    try:
        res = main(args.part1, args.input, verbose_level=args.verbose)
    finally:
        time_end = time.time()
        print(f"Result = {res}")
        print(f"Computation time = {time_end - time_begin} s")
