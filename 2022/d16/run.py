import argparse
import time
import typing


class Valve:
    def __init__(self, name: str, flow_rate: int, tunnels_to_valves: typing.List[str]):
        self.name = name
        self.flow_rate = flow_rate
        self.tunnels_to_valves = tunnels_to_valves
        self.is_open = False

    _STR_VALVE = "Valve "
    _STR_FLOW_RATE = " has flow rate="
    _STR_TUNNEL_1 = "; tunnel leads to valve "
    _STR_TUNNELS_MULTIPLE = "; tunnels lead to valves "

    @classmethod
    def new_from_line(cls, line):
        """Valve AA has flow rate=0; tunnels lead to valves DD, II, BB"""
        line = line.strip()
        tokens = line.split(cls._STR_FLOW_RATE)
        assert len(tokens) == 2, (line, tokens)
        assert tokens[0].startswith(cls._STR_VALVE), (line, tokens)
        name = tokens[0].removeprefix(cls._STR_VALVE)
        if cls._STR_TUNNELS_MULTIPLE in tokens[1]:
            # multiple tunnels
            tokens = tokens[1].split(cls._STR_TUNNELS_MULTIPLE)
            assert len(tokens) == 2, (line, tokens)
            flow_rate = int(tokens[0])
            tunnels_to_valves = tokens[1].split(", ")
        else:
            # single tunnel
            assert cls._STR_TUNNEL_1 in tokens[1], (line, tokens)
            tokens = tokens[1].split(cls._STR_TUNNEL_1)
            assert len(tokens) == 2, (line, tokens)
            flow_rate = int(tokens[0])
            tunnels_to_valves = [tokens[1]]
        assert name not in tunnels_to_valves
        return Valve(name=name, flow_rate=flow_rate, tunnels_to_valves=tunnels_to_valves)


class Parcours:
    def __init__(self, list_current_pos: list, open_valves: set) -> None:
        self.current_score = 0  # optimization : when opening a valve, compute corresponding score until the end
        self.opened_valves = open_valves
        self.list_current_pos = list_current_pos.copy()
        self.memory_positions = [[x] for x in list_current_pos]

    def copy(self):
        new = Parcours(self.list_current_pos, self.opened_valves.copy())
        new.current_score = self.current_score
        new.memory_positions = [l.copy() for l in self.memory_positions]
        return new

    def __str__(self):
        return f"Parcours at positions {self.list_current_pos} : score = {self.current_score} - positions = {self.memory_positions}"

class ValveSystem:
    def __init__(self, verbose_level: int) -> None:
        self.all_valves: typing.Dict[str, Valve] = {}
        self.verbose_level = verbose_level
        self._distances_between_valves: typing.Dict[typing.Tuple[str, str], int] = {}  # key : tuple(start valve, dest valve)

    def insert_valve(self, valve: Valve):
        assert valve.name not in self.all_valves
        self.all_valves[valve.name] = valve

    def check_consistency(self):
        assert "AA" in self.all_valves  # starting point
        for name, valve in self.all_valves.items():
            assert name == valve.name
            assert name not in valve.tunnels_to_valves
            for it_tunnel in valve.tunnels_to_valves:
                assert it_tunnel in self.all_valves

    def list_open_valves(self):
        open_valves = []
        pressure = 0
        for valve in self.all_valves.values():
            if valve.is_open:
                open_valves.append(valve.name)
                pressure += valve.flow_rate
        return open_valves, pressure

    def find_paths_to_valves(self, start_pos: str):
        if self.verbose_level >= 3:
            print(f"[DEBUG] find_paths_to_valves() - start_pos = {start_pos}")
        paths_to_valves: typing.Dict[str, list] = {start_pos: []}  # 1st possibility : stay here, not moving

        candidates = [[x] for x in self.all_valves[start_pos].tunnels_to_valves]  # now start thinking moving to other tunnels

        while candidates:
            current_valve_path = candidates.pop(0)
            current_valve = self.all_valves[current_valve_path[-1]]
            paths_to_valves[current_valve.name] = current_valve_path  # save it

            for it_tunnel in current_valve.tunnels_to_valves:
                if it_tunnel in paths_to_valves:
                    continue
                new_path = [x for x in current_valve_path]
                new_path.append(it_tunnel)
                candidates.append(new_path)

        return paths_to_valves

    def compute_all_distances(self):
        for valve_name in self.all_valves:
            paths = self.find_paths_to_valves(valve_name)
            for it_dest_valve_name, it_path in paths.items():
                key = (valve_name, it_dest_valve_name)
                assert key not in self._distances_between_valves
                self._distances_between_valves[key] = len(it_path)
        assert len(self._distances_between_valves) == len(self.all_valves) ** 2

    def _find_possible_next_parcours(self, initial_parcours: Parcours, remaining_time: int):

        candidates_parcours = [initial_parcours]

        for explorer_idx in range(len(initial_parcours.list_current_pos)):
            previous_candidate_parcours = candidates_parcours
            candidates_parcours: typing.List[Parcours] = []

            for it_parcours in previous_candidate_parcours:
                valve = self.all_valves[it_parcours.list_current_pos[explorer_idx]]
                # optimization : valve.flow_rate > 0
                if valve.flow_rate > 0 and valve.name not in it_parcours.opened_valves:
                    new_parcours = it_parcours.copy()
                    new_parcours.opened_valves.add(valve.name)
                    new_parcours.current_score += remaining_time * valve.flow_rate
                    new_parcours.memory_positions[explorer_idx].append(True)
                    candidates_parcours.append(new_parcours)
                for it_tunnel in valve.tunnels_to_valves:
                    new_parcours = it_parcours.copy()
                    new_parcours.list_current_pos[explorer_idx] = it_tunnel
                    new_parcours.memory_positions[explorer_idx].append(it_tunnel)
                    candidates_parcours.append(new_parcours)
        return candidates_parcours


    def find_all_parcours(self, list_start_pos, eruption_time: int, open_valves=None, max_parcours=None):

        # the key point is to find iteratively all possible parcours at a given time,
        # but shrink the list to only solutions that can eventually reach a good score

        if open_valves is None:
            open_valves = set()

        list_current_parcours: typing.List[Parcours] = [Parcours(list_start_pos, open_valves)]

        for current_time in range(1, eruption_time + 1):
        #for current_time in range(1, 7 + 1):
            list_old_parcours = list_current_parcours
            list_current_parcours: typing.List[Parcours] = []
            for it_parcours in list_old_parcours:
                candidates_parcours: typing.List[Parcours] = self._find_possible_next_parcours(it_parcours, remaining_time=eruption_time - current_time)

                # optimization to avoid dying with too many parcours : avoid duplicate parcours. if already same parcours (=same state), keep the one with best score
                for it_candidate_parcours in candidates_parcours:
                    found_existing_parcours = None
                    for p in list_current_parcours:
                        if set(p.list_current_pos) == set(it_candidate_parcours.list_current_pos) and it_candidate_parcours.opened_valves.issubset(p.opened_valves):
                            found_existing_parcours = p
                            break

                    if not found_existing_parcours:
                        list_current_parcours.append(it_candidate_parcours)
                        if self.verbose_level >= 3:
                            print(f"[DEBUG] added parcours {it_candidate_parcours}")
                    elif found_existing_parcours.current_score < it_candidate_parcours.current_score:
                        if self.verbose_level >= 3:
                            print(f"[DEBUG] added parcours {it_candidate_parcours} - dropped parcours {found_existing_parcours}")
                        list_current_parcours.remove(found_existing_parcours)
                        list_current_parcours.append(it_candidate_parcours)

            if self.verbose_level >= 2:
                max_score = max([p.current_score for p in list_current_parcours])
                print(f"==== Minute {current_time} - number of parcours before shrink = {len(list_current_parcours)} - current max score = {max_score} ====")
                del max_score
            # really important : shrink list to avoid keeping clearly non optimized parcours
            list_current_parcours, current_best_navive_possible_score = self._shrink_list_parcours(list_parcours=list_current_parcours, remaining_time=eruption_time - current_time)
            if self.verbose_level >= 2:
                max_score = max([p.current_score for p in list_current_parcours])
                print(f"==== Minute {current_time} - number of parcours after  shrink = {len(list_current_parcours)} (reducing to max = {max_parcours}) - current max score = {max_score} / current_best_navive_possible_score = {current_best_navive_possible_score} ====")
                del max_score
            if max_parcours is not None:
                list_current_parcours = sorted(list_current_parcours, key=lambda p: p.current_score, reverse=True)[:max_parcours]
            if self.verbose_level >= 3:
                for p in list_current_parcours:
                    print(f"    {p}")

        sorted_list_current_parcours = sorted(list_current_parcours, key=lambda p: p.current_score, reverse=True)
        max_score = sorted_list_current_parcours[0].current_score
        print(f"Result = max possible score = {max_score}")

        if self.verbose_level >= 2:
            sorted_parcours = sorted(list_current_parcours, key=lambda p: p.current_score, reverse=True)
            print(f"Result = max possible score = {sorted_parcours[0].current_score} - positions = {sorted_parcours[0].memory_positions}")
        
        return sorted_list_current_parcours[0]

    def _shrink_list_parcours(self, list_parcours: typing.List[Parcours], remaining_time: int):
        # - find the best current score of all parcours
        # - compute the best possible "naive" final score for every parcours. (assuming we can go in all directions in the mean time)
        # - remove all parcours that will never reach the best current score

        current_best_navive_possible_score = None
        current_best_score = max([p.current_score for p in list_parcours])
        new_list_parcours: typing.List[Parcours] = []
        for it_parcours in list_parcours:
            parcours_naive_possible_score = it_parcours.current_score
            for valve in self.all_valves.values():
                if valve.name in it_parcours.opened_valves:
                    continue
                distance = min([self._distances_between_valves[(current_pos, valve.name)] for current_pos in it_parcours.list_current_pos])
                if distance > remaining_time:
                    continue
                parcours_naive_possible_score += (remaining_time - distance) * valve.flow_rate
            if current_best_navive_possible_score is None or parcours_naive_possible_score > current_best_navive_possible_score:
                current_best_navive_possible_score = parcours_naive_possible_score
            if parcours_naive_possible_score < current_best_score:
                continue

            new_list_parcours.append(it_parcours)
        return new_list_parcours, current_best_navive_possible_score

def _find_cul_de_sac(parcours: Parcours, valve_system: ValveSystem, max_iterations: int = None):
    # look 
    open_valves_solo = {}
    current_pos = parcours.memory_positions[0][0]
    nb_steps_before_cul_de_sac = 0
    if max_iterations is not None:
        max_iterations += 1
    for action in parcours.memory_positions[0][1:max_iterations]:
        print(f"action = {action} ; current_pos = {current_pos}")
        if action is True:
            open_valves_solo[current_pos] = nb_steps_before_cul_de_sac + 1

            # TODO : new return condition : look for first returned value, probably the one with highest score
            return nb_steps_before_cul_de_sac + 1, open_valves_solo, current_pos
        else:

            # TODO : original condition : look for cul-de-sac
            #valve = valve_system.all_valves[current_pos]
            #if len(valve.tunnels_to_valves) == 1:
            #    return nb_steps_before_cul_de_sac, open_valves_solo, current_pos # found cul de sac : valve with tunnel to only 1 valve

            current_pos = action
        nb_steps_before_cul_de_sac += 1
    
    raise ValueError(f"Not found any cul de sac :-(")
    

def main(is_part1: bool, input_file: str, verbose_level=0):

    with open(input_file) as f:
        valve_system = ValveSystem(verbose_level=verbose_level)
        for line in f:
            valve_system.insert_valve(Valve.new_from_line(line))

    if verbose_level >= 1:
        print(f"[INFO] finished parsing valves")
        print(f"[INFO] valves count = {len(valve_system.all_valves)} ; names = {' '.join(n for n in valve_system.all_valves)}")

    valve_system.compute_all_distances()

    list_start_pos = ["AA"]
    if is_part1:
        eruption_time = 30
        return valve_system.find_all_parcours(list_start_pos, eruption_time=eruption_time).current_score

    # idea part 2 :
    # - make simulation like part 1 to find best score (on 26 iterations)
    # - consider correct the N first steps = steps to 1st dead-end valve (1 tunnel) with opening => it gives path explorer 1
    # - make another simulation by having closed the valves of explorer 1 for N first steps => it gives path explorer 2
    # - knowing N first steps of explorer 1 and 2, start simulation at that point
    eruption_time = 26

    list_start_pos = ["AA", "AA"]
    # tried max_parcours = 1000 => bad result / 2000 => same bad result. Found correct result with 3000
    # it's not exact computation, but good enough approximation removing very bad parcours
    best_parcours_together = valve_system.find_all_parcours(list_start_pos, eruption_time=eruption_time, max_parcours=3000)

    """
    best_parcours_part2_solo = valve_system.find_all_parcours(list_start_pos, eruption_time=eruption_time)

    nb_steps_before_cul_de_sac, open_valves_solo, pos_solo = _find_cul_de_sac(best_parcours_part2_solo, valve_system)
    if verbose_level >= 1:
        print(f"[INFO] Found parcours with {eruption_time} iterations : reaching cul de sac after {nb_steps_before_cul_de_sac} iterations ; valves opened = {open_valves_solo} / pos = {pos_solo}")

    best_parcours_elephant_solo_part2 = valve_system.find_all_parcours(list_start_pos, eruption_time=eruption_time, open_valves=set(open_valves_solo.keys()))

    nb_steps_elephant_before_cul_de_sac, open_valves_elephant_solo, _ = _find_cul_de_sac(best_parcours_elephant_solo_part2, valve_system, max_iterations=nb_steps_before_cul_de_sac)
    if verbose_level >= 1:
        print(f"[INFO] Found elephant parcours with {eruption_time} iterations : reaching cul de sac after {nb_steps_elephant_before_cul_de_sac} iterations ; valves opened = {open_valves_elephant_solo}")

    # now, compute corresponding score for those 2 solo parcours

    score_solo = 0
    for valve_name, nb_steps in open_valves_solo.items():
        score_solo += (eruption_time - nb_steps) * valve_system.all_valves[valve_name].flow_rate
    print(f"Score solo : {score_solo}")

    score_elephant_solo = 0
    for valve_name, nb_steps in open_valves_elephant_solo.items():
        score_elephant_solo += (eruption_time - nb_steps) * valve_system.all_valves[valve_name].flow_rate
    print(f"Score elephant solo : {score_elephant_solo}")

    # find elephant starting position
    pos_elephant = list_start_pos[0]
    for action in best_parcours_elephant_solo_part2.memory_positions[0][1:nb_steps_before_cul_de_sac+1]:
        if action is True:
            pass
        else:
            pos_elephant = action

    list_start_pos = [pos_solo, pos_elephant]
    print(f"Starting positions : {list_start_pos} with initial score {score_solo + score_elephant_solo}")

    open_valves = set(open_valves_solo.keys()).union(set(open_valves_elephant_solo.keys()))

    best_parcours_together = valve_system.find_all_parcours(list_start_pos, eruption_time=eruption_time-nb_steps_before_cul_de_sac,
        open_valves=open_valves, max_parcours=3000)
    """

if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")

    # parse the arguments
    args = parser.parse_args()

    # run !
    res = None
    time_begin = time.time()
    try:
        res = main(args.part1, args.input, verbose_level=args.verbose)
    finally:
        time_end = time.time()
        print(f"Result = {res}")
        print(f"Computation time = {time_end - time_begin} s")
