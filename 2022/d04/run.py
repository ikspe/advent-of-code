import argparse
import time


def main(is_part1: bool, input_file: str, verbose_level=0):
    with open(input_file) as f:
        list_pairs = []
        for line in f:
            line = line.strip()
            if line:
                list_pairs.append(line.split(","))
        count_overlap = 0
    for pair1, pair2 in list_pairs:
        pair1_start, pair1_end = (int(x) for x in pair1.split("-"))
        pair2_start, pair2_end = (int(x) for x in pair2.split("-"))
        if verbose_level >= 3:
            print(f"[DEBUG] pair1 = {(pair1_start, pair1_end)} / pair2 = {pair2_start, pair2_end}")
        is_overlap = False
        if is_part1:
            if  (pair1_start <= pair2_start <= pair2_end <= pair1_end) or \
                (pair2_start <= pair1_start <= pair1_end <= pair2_end):
                    is_overlap = True  # full overlap
        else:
            if  pair2_start <= pair1_start <= pair2_end or \
                pair2_start <= pair1_end   <= pair2_end or \
                pair1_start <= pair2_start <= pair1_end or \
                pair1_start <= pair2_end   <= pair1_end:
                    is_overlap = True  # full or partial overlap
        if is_overlap:
            count_overlap += 1
            if verbose_level >= 2:
                print(f"[DEBUG] overlap detected for pair1 = {(pair1_start, pair1_end)} / pair2 = {pair2_start, pair2_end}")
    print(f"Result = count assignment pairs ovelaping = {count_overlap}")


if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")

    # parse the arguments
    args = parser.parse_args()

    # run !
    res = None
    time_begin = time.time()
    try:
        res = main(args.part1, args.input, verbose_level=args.verbose)
    finally:
        time_end = time.time()
        print(f"Result = {res}")
        print(f"Computation time = {time_end - time_begin} s")
