import argparse
import time
import typing



"""

* Idea 1 :

geode-robot: G1 ore, G2 obsidian
obsidian-robot : Ob1 ore, Ob2 clay
clay-robot : C1 ore
ore-robot: Or1 ore (not used in part 1)

ore-coefficients (G1, Ob1, C1, Or1 are between 2 and 4)

Nb rounds: N (24)

Max nb geodes M requires max geode robots during several rounds
- MG (geode-robots) requires G1*MG ore + G2*MG obsidian
- MOb (obisian-robots) to collect G2*MG obsidian during several rounds. It requires Ob1*MOb ore + Ob2*MOb clay
- MC (clay-robots) to collect Ob2*MOb clay during several rounds. It requires C1 ore

=> cost in ore : G1*MG+Ob1*Mob+C1 ore

* Idea 2 :




"""




class Blueprint:

    ALL_POSSIBLE_MATERIALS = ["ore", "clay", "obsidian", "geode"]

    def __init__(self, num: int, costs: typing.Dict[str, typing.Dict[str, int]]) -> None:
        self.num = num
        self.costs = costs
        print("\n  ".join(self._str_lines()))

    @classmethod
    def from_line(cls, line: str):
        blueprint, robots_details = line.strip().split(":")
        assert blueprint.startswith("Blueprint ")
        blueprint_num = int(blueprint.removeprefix("Blueprint "))
        robots_costs = {}
        for it_robot_details in robots_details.split(" Each "):
            if not it_robot_details:
                continue
            target, cost = it_robot_details.split(" robot costs ")
            assert target in cls.ALL_POSSIBLE_MATERIALS, f"target {target} not in {cls.ALL_POSSIBLE_MATERIALS}"
            assert cost.endswith(".")
            cost = cost.removesuffix(".")
            cost = {k: int(v) for v, k in [x.split() for x in cost.split(" and ")]}
            robots_costs[target] = cost
            assert all([c in cls.ALL_POSSIBLE_MATERIALS for c in cost])

        return Blueprint(num=blueprint_num, costs=robots_costs)

    def _str_lines(self):
        lines = [f"Blueprint {self.num}:"]
        for target, costs in self.costs.items():
            lines.append(f"Each {target} robot costs " + " and ".join([f"{v} {k}" for k, v in costs.items()]) + ".")
        return lines

    def __str__(self) -> str:
        #return f"Blueprint {self.num}:" + " Each ".join([] + [f"{target} robot costs " + " and ".join([f"{x} {y}" for x, y in costs]) + "." for target, costs in self.costs.items()])
        return " ".join(self._str_lines())

"""

class MyDevices:
    def __init__(self, verbose_level: int) -> None:
        self.robots = {}
        self.artifacts = {}
        self.list_blueprints: typing.List[Blueprint] = []
        self.verbose_level = verbose_level
        self._generator_next_robot_to_build = iter(Blueprint.ALL_POSSIBLE_MATERIALS)
        self._next_robot_to_build = next(self._generator_next_robot_to_build)
        self._next_robot_to_build = next(self._generator_next_robot_to_build)  # we don't want to build ore-collecting robots at first
        self._previous_robot_bo_build = self._next_robot_to_build

    def execute_blueprint(self, blueprint_num):
        blueprint = self.list_blueprints[blueprint_num-1]
        new_robots = {}
        if self.verbose_level >= 3:
            print(f"[DEBUG] Next robot to build = {self._next_robot_to_build}-collecting / current = {self._previous_robot_bo_build}")
            print(f"[DEBUG] Blueprint cost = {blueprint.costs}")
            print(f"[DEBUG] Status : robots = {self.robots} / artifacts = {self.artifacts}")
        for target in [self._next_robot_to_build, self._previous_robot_bo_build]:
            if self.verbose_level >= 3:
                print(f"[DEBUG]     trying to build robot for {target}")
            costs = blueprint.costs[target]
            cost_ok = True
            for k, v in costs.items():
                if self.artifacts.get(k, 0) < v:
                    cost_ok = False
                    continue
            if not cost_ok:
                continue
            for k, v in costs.items():
                self.artifacts[k] -= v
            new_robots[target] = new_robots.get(target, 0) + 1
            if target == self._next_robot_to_build:
                try:
                    self._next_robot_to_build, self._previous_robot_bo_build = next(self._generator_next_robot_to_build), self._next_robot_to_build  # now we want to build next robot
                    if self.verbose_level >= 3:
                        print(f"[DEBUG]     Now interested in building robot {self._next_robot_to_build}-collecting")
                except StopIteration:
                    pass
            if self.verbose_level >= 1:
                print(f"Spend " + "and ".join([f"{v} {k}" for k, v in costs.items()]) + f" to start building an {target}-collecting robot.")
            break
        collected_artifacts = {k: v for k, v in self.robots.items()}
        if self.verbose_level >= 3:
            print(f"[DEBUG] Status : robots = {self.robots} / artifacts = {self.artifacts} / collected_artifacts = {collected_artifacts}")
        for k, v in collected_artifacts.items():
            self.artifacts[k] = self.artifacts.get(k, 0) + v
            if self.verbose_level >= 2:
                print(f"{v} {k}-collecting robot collects {v} {k}; you now have {self.artifacts[k]} {k}.")
        for k, v in new_robots.items():
            self.robots[k] = self.robots.get(k, 0) + v
            if self.verbose_level >= 2:
                print(f"The {v} new {k}-collecting robot is ready; you now have {self.robots[k]} of them.")

def main(is_part1: bool, input_file: str, verbose_level=0):
    my_devices = MyDevices(verbose_level=verbose_level)
    with open(input_file) as f:
        for line in f:
            my_devices.list_blueprints.append(Blueprint.from_line(line))
            assert str(my_devices.list_blueprints[-1]) == line.strip()

    if verbose_level >= 2:
        print(f"[DEBUG] found {len(my_devices.list_blueprints)} blueprints :")
        for b in my_devices.list_blueprints:
            print(f"{b}")
        print(f"[DEBUG] done printing blueprints!")

    if is_part1:
        my_devices.robots = {"ore": 1}
        max_time = 24
    else:
        raise NotImplementedError

    for current_time in range(1, max_time+1):
        if verbose_level >= 1:
            print()
            print(f"== Minute {current_time} ==")
        my_devices.execute_blueprint(1)
"""

class CollectState:
    def __init__(self, robots: dict, artifacts: dict) -> None:
        self.robots = robots
        self.artifacts = artifacts

    def __eq__(self, other: object) -> bool:
        other: CollectState = other
        return self.robots == other.robots and self.artifacts == other.artifacts

    def __str__(self) -> str:
        return f"CollectState<robots = {self.robots} , artifacts = {self.artifacts}>"

class Computing:

    def __init__(self, list_blueprints: typing.List[Blueprint], verbose_level: int) -> None:
        self.list_blueprints = list_blueprints
        self.verbose_level = verbose_level

    def _clear_worse_states(self, list_states: typing.List[CollectState], remaining_rounds: int):
        if self.verbose_level >= 2:
            print(f"[DEBUG] _clear_worse_states(remaining_rounds={remaining_rounds}) - len(list_states) = {len(list_states)}")

        filtered_states: typing.List[CollectState] = []
        # first filter : check if some states are present twice, or if some states are subsets of other
        for it_state in list_states:
            if self.verbose_level >= 4:
                print(f"[DEBUG] checking to add current state = {it_state}")
            found_better_state = False
            for other_state in filtered_states.copy():
                # current_is_better_somewhere = all([other_state.robots.get(it_robot, 0) - it_state.robots.get(it_robot, 0) for it_robot in Blueprint.ALL_POSSIBLE_MATERIALS])
                current_is_better_somewhere = False
                other_is_better_somewhere = False
                for it_robot in Blueprint.ALL_POSSIBLE_MATERIALS:
                    robot_diff = other_state.robots.get(it_robot, 0) - it_state.robots.get(it_robot, 0)
                    if robot_diff < 0:
                        current_is_better_somewhere = True
                    elif robot_diff > 0:
                        other_is_better_somewhere = True
                for it_artifact in Blueprint.ALL_POSSIBLE_MATERIALS:
                    artifact_diff = other_state.artifacts.get(it_artifact, 0) - it_state.artifacts.get(it_artifact, 0)
                    if artifact_diff < 0:
                        current_is_better_somewhere = True
                    elif artifact_diff > 0:
                        other_is_better_somewhere = True
                if current_is_better_somewhere and other_is_better_somewhere:
                    continue
                elif current_is_better_somewhere:
                    if self.verbose_level >= 4:
                        print(f"[DEBUG]     found worse state  : {other_state} compared to current = {it_state}")
                    filtered_states.remove(other_state)
                    continue
                elif other_is_better_somewhere:
                    found_better_state = True
                    if self.verbose_level >= 4:
                        print(f"[DEBUG]     found better state : {other_state} compared to current = {it_state}")
                    break
                else:
                    found_better_state = True
                    if self.verbose_level >= 4:
                        print(f"[DEBUG]     found same   state : {other_state} compared to current = {it_state}")
                    break
            if not found_better_state:
                if self.verbose_level >= 4:
                    print(f"[DEBUG]     no    better state : adding current = {it_state}")
                filtered_states.append(it_state)
        if self.verbose_level >= 2:
            print(f"[DEBUG] _clear_worse_states(remaining_rounds={remaining_rounds}) - after first filter : len(filtered_states) = {len(filtered_states)}")
        # end of first filter

        # second filter :
        # - get current best score (here, score that will be sure to have at end of time)
        # - for each state, assume it's possible to build 1 geode robot each round, compute best score, check if lower that current best score
        current_best_score = max([s.artifacts.get("geode", 0) + it_state.robots.get("geode", 0) * remaining_rounds for s in filtered_states])
        if self.verbose_level >= 2:
            print(f"[DEBUG] _clear_worse_states(remaining_rounds={remaining_rounds}) - before second filter : current_best_score = {current_best_score}")
        list_naive_possible_score = []
        for it_state in filtered_states:
            list_naive_possible_score.append((it_state, it_state.artifacts.get("geode", 0) + it_state.robots.get("geode", 0) * remaining_rounds + (remaining_rounds * (remaining_rounds - 1) // 2)))
        
        filtered_states = [it_state for it_state, naive_score in list_naive_possible_score if naive_score >= current_best_score]

        if self.verbose_level >= 2:
            print(f"[DEBUG] _clear_worse_states(remaining_rounds={remaining_rounds}) - after second filter : len(filtered_states) = {len(filtered_states)}")

        # end of second filter

        return filtered_states

    _CLEANING_SCORE_ARIFACTS = {"ore": 1, "clay": 10, "obsidian": 10 * 20, "geode": 10 * 20 ** 2}  # input : max coeff 20 from clay to obsidian, then to geode
    def _clear_worse_states2(self, list_states: typing.List[CollectState], remaining_rounds: int, max_nb_states: int):
        if self.verbose_level >= 2:
            print(f"[DEBUG] _clear_worse_states2(remaining_rounds={remaining_rounds}) - starting = {len(list_states)}, max_nb_states = {max_nb_states}")
        if max_nb_states >= len(list_states):
            return list_states  # we keep them all

        list_states_score_simulation = []
        for state in list_states:
            score_simulation =  sum([self._CLEANING_SCORE_ARIFACTS[k] * v for k, v in state.artifacts.items()]) + \
                                sum([self._CLEANING_SCORE_ARIFACTS[k] * v * remaining_rounds for k, v in state.robots.items()])
            list_states_score_simulation.append((state, score_simulation))
        list_states_score_simulation = sorted(list_states_score_simulation, key=lambda x: x[1], reverse=True)
        return [x[0] for x in list_states_score_simulation][:max_nb_states] 
    
    @classmethod
    def _find_possible_robots_to_buy(cls, blueprint: Blueprint, state: CollectState):
        #print(f"_find_possible_robots_to_buy() starting...")
        #print(f"            blueprint = {blueprint}")
        #print(f"            state : {state}")

        has_obsidian_robots = state.robots.get("obsidian", 0) > 0
        has_geode_robots = state.robots.get("geode", 0) > 0

        # list : robots to buy, remaining artifacts
        wish_list_robots = [({}, state.artifacts.copy())]
        for it_robot, it_costs in blueprint.costs.items():

            #print(f"                trying robots : {it_robot} - wish_list_robots = {wish_list_robots}")

            if it_robot == "ore" and has_obsidian_robots:
                continue  # optimization (assumption) : no need new ore robots when we already have obsidian robots
            if it_robot == "clay" and has_geode_robots:
                continue  # optimization (assumption) : no need new clay robots when we already have geode robots

            tmp_new_wish_list = []
            for it_wish_list_robots, it_wish_list_artifacts in wish_list_robots:
                nb_possible_robots = min([it_wish_list_artifacts.get(artifact, 0) // cost for artifact, cost in it_costs.items()])
                nb_possible_robots = min(nb_possible_robots, 1)  # robots can be bought only 1 by 1of each category (assumption)
                for nb_robots in range(nb_possible_robots + 1):
                    new_artifacts = it_wish_list_artifacts.copy()
                    new_robots = it_wish_list_robots.copy()
                    if nb_robots > 0:
                        for artifact, cost in it_costs.items():
                            new_artifacts[artifact] -= nb_robots * cost
                        new_robots.update({it_robot: nb_robots})
                    tmp_new_wish_list.append((new_robots, new_artifacts))
            wish_list_robots = tmp_new_wish_list
        return [x[0] for x in wish_list_robots]

        """
        list_possible_bought_robots = [{}]  # always possible NOT to buy any robot
        for it_robot, it_cost in blueprint.costs.items():
            tmp_list_to_buy = []
            if it_robot == "ore":
                # don't build more ore robots that the max ore needed
                if it_state.robots[it_robot] >= max([c.get("ore") for k, c in blueprint.costs.items() if k != it_robot]):
                    continue
                if it_state.artifacts.get("ore", 0) <= blueprint.costs["ore"]["ore"]:
                    continue
            else:
                cost_ok = True
                for item, item_price in it_cost.items():
                    if it_state.artifacts.get(item, 0) < item_price:
                        cost_ok = False
                        break
                if not cost_ok:
                    continue
            list_possible_bought_robots.append({it_robot: 1})
        return list_possible_bought_robots
        """

    def _compute_part1_one_blueprint(self, blueprint: Blueprint, max_time: int, init_robots: dict):
        list_states = [CollectState(robots=init_robots.copy(), artifacts={})]
        current_time = 0
        while current_time < max_time:
            current_time += 1
            if self.verbose_level >= 2:
                print(f"[DEBUG] == Minute {current_time} - starting - nb possible states = {len(list_states)} ==")
            skipped_states = 0

            new_states = []
            for it_state in list_states:

                if current_time < max_time:
                    list_possible_bought_robots = self._find_possible_robots_to_buy(blueprint=blueprint, state=it_state)
                else:
                    list_possible_bought_robots = [{}]  # always possible NOT to buy any robot
                
                #print(f"    ==> possible robots to buy : {list_possible_bought_robots}")
                if self.verbose_level >= 3:
                    print(f"[DEBUG] trying to extend state {it_state} ... Possible built = {list_possible_bought_robots}")
                for it_possible_bought_robots in list_possible_bought_robots:
                    tmp_new_state = CollectState(robots=it_state.robots.copy(), artifacts=it_state.artifacts.copy())
                    # pay for buying new robots : decrease stocks
                    for it_robot, it_robot_count in it_possible_bought_robots.items():
                        for item, item_price in blueprint.costs[it_robot].items():
                            tmp_new_state.artifacts[item] = tmp_new_state.artifacts.get(item, 0) - it_robot_count * item_price
                            assert tmp_new_state.artifacts[item] >= 0
                    # gather artifacts with current robots
                    for it_robot, it_robot_count in tmp_new_state.robots.items():
                        tmp_new_state.artifacts[it_robot] = tmp_new_state.artifacts.get(it_robot, 0) + it_robot_count
                    # add robots to current robots
                    for it_robot, it_robot_count in it_possible_bought_robots.items():
                        tmp_new_state.robots[it_robot] = tmp_new_state.robots.get(it_robot, 0) + it_robot_count
                    # save state (check if not yet present)
                    if tmp_new_state not in new_states:
                        new_states.append(tmp_new_state)
                    else:
                        skipped_states += 1
            #list_states = new_states
            list_states = self._clear_worse_states(new_states, remaining_rounds=max_time-current_time)
            list_states = self._clear_worse_states2(list_states, remaining_rounds=max_time-current_time, max_nb_states=500)
            if self.verbose_level >= 3:
                print(f"[DEBUG] List of states :")
                for s in list_states:
                    print(f"    {s}")
            if self.verbose_level >= 2:
                print(f"[DEBUG] == Minute {current_time} - ending   - nb possible states = {len(list_states)} / nb unfiltered possible states = {len(new_states)} / skipped states = {skipped_states} ==")
                print()
            
            #if current_time >= 6:
            #    raise NotImplementedError

        sorted_states = sorted(list_states, key=lambda s: s.artifacts.get("geode", 0), reverse=True)
        best_state = sorted_states[0]
        max_geodes = best_state.artifacts.get("geode", 0)
        if self.verbose_level >= 1:
            print(f"[INFO] blueprint {blueprint.num} : max geodes = {max_geodes} ({best_state})")
        return max_geodes

    def compute_part1(self, max_time: int, init_robots: dict):
        #self._compute_part1_one_blueprint(self.list_blueprints[0], max_time=max_time, init_robots=init_robots)
        #raise NotImplementedError

        best_scores = []
        for blueprint in self.list_blueprints:
            max_geodes = self._compute_part1_one_blueprint(blueprint, max_time=max_time, init_robots=init_robots)
            best_scores.append((blueprint.num, max_geodes))
        
        return sum([x*y for x, y in best_scores])

def main(is_part1: bool, input_file: str, verbose_level=0):
    list_blueprints = []
    with open(input_file) as f:
        for line in f:
            list_blueprints.append(Blueprint.from_line(line))
            assert str(list_blueprints[-1]) == line.strip()
    
    computing = Computing(list_blueprints=list_blueprints, verbose_level=verbose_level)
    del list_blueprints

    if verbose_level >= 2:
        print(f"[DEBUG] found {len(computing.list_blueprints)} blueprints :")
        for b in computing.list_blueprints:
            print(f"{b}")
        print(f"[DEBUG] done printing blueprints!")

    if is_part1:
        init_robots = {"ore": 1}
        max_time = 24
    else:
        raise NotImplementedError

    return computing.compute_part1(max_time=max_time, init_robots=init_robots)

if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")

    # parse the arguments
    args = parser.parse_args()

    # run !
    res = None
    time_begin = time.time()
    try:
        res = main(args.part1, args.input, verbose_level=args.verbose)
    finally:
        time_end = time.time()
        print(f"Result = {res}")
        print(f"Computation time = {time_end - time_begin} s")
