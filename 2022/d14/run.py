import argparse
import enum
import time
import typing


class CellContent:
    ROCK = 1
    SAND = 2


class UniverseContent:

    SAND_POURING_POS = (500, 0)

    def __init__(self, verbose_level: int):
        self._content: typing.Dict[tuple, CellContent] = {}
        self._min_x = None
        self._max_x = None
        self._min_y = None
        self._max_y = None
        self._floor_enabled = False
        self.verbose_level = verbose_level

    @property
    def _floor_y(self):
        if not self._floor_enabled:
            return None
        return self._max_y + 2

    def fill_one_pos(self, pos: tuple, content=CellContent.ROCK):
        x, y = pos
        self._content[pos] = content
        if self._min_x is None or x < self._min_x:
            self._min_x = x
        if self._max_x is None or x > self._max_x:
            self._max_x = x
        if self._min_y is None or y < self._min_y:
            self._min_y = y
        if self._max_y is None or y > self._max_y:
            self._max_y = y

    def enable_floor(self):
        self._floor_enabled = True

    def process_sand_unit(self):
        """
        return :
            - None if sans is resting at pouring position (can't pour anymore)
            - True if sand is resting at the end
            - False if it flows to the abyss
        """

        """
        return tuple : (sand resting, sand position)
            - sand resting : True if sand is resting at the end, False if it flows to the abyss
            - sand position : None if sand flowing to the abyss, otherwise sand position resting
        """

        sand_x, sand_y = self.SAND_POURING_POS
        while True:

            # order of this list matters, first position has to be tried before the next one, and so !
            possible_future_positions = [
                (sand_x, sand_y + 1),  # down one step if possible
                (sand_x - 1, sand_y + 1),  # one step down and to the left
                (sand_x + 1, sand_y + 1),  # one step down and to the right
            ]

            if self._floor_enabled:
                if sand_y + 1 >= self._floor_y:
                    assert sand_y + 1 == self._floor_y
                    if self.verbose_level >= 2:
                        print(f"[DEBUG] sand resting on floor")
                    possible_future_positions = []  # force sand to rest
            else:
                if sand_y >= self._max_y:
                    if self.verbose_level >= 2:
                        print(f"[DEBUG] sand flowing to the abyss")
                    return False  # stop processing sand : it reaches the abyss

            sand_resting = True
            for possible_pos in possible_future_positions:
                if possible_pos in self._content:
                    continue
                sand_resting = False
                sand_x, sand_y = possible_pos
                break

            if sand_resting:
                if self.verbose_level >= 2:
                    print(f"[DEBUG] sand resting at {(sand_x, sand_y)}")
                self._content[(sand_x, sand_y)] = CellContent.SAND
                if (sand_x, sand_y) == self.SAND_POURING_POS:
                    return None
                return True

    def __len__(self):
        return len(self._content)

    def items(self):
        return self._content.items()

    def print_content(self):
        for pos, content in self.items():
            print(f"[DEBUG] cell {pos} : {content}")
        print(f"[DEBUG] floor enabled = {self._floor_enabled} - floor y = {self._floor_y}")

def main(is_part1: bool, input_file: str, verbose_level=0):

    with open(input_file) as f:
        universe_content = UniverseContent(verbose_level=verbose_level)
        for line in f:
            previous_x_y = None
            for pos in line.strip().split(" -> "):
                x, y = [int(a) for a in pos.split(",")]
                if previous_x_y is not None:
                    if previous_x_y[0] == x:
                        for it_y in range(min(y, previous_x_y[1]), max(y, previous_x_y[1]) + 1):
                            universe_content.fill_one_pos((x, it_y), CellContent.ROCK)
                    elif previous_x_y[1] == y:
                        for it_x in range(min(x, previous_x_y[0]), max(x, previous_x_y[0]) + 1):
                            universe_content.fill_one_pos((it_x, y), CellContent.ROCK)
                    else:
                        raise ValueError(f"error line {line} at pos {pos} for previous_x_y = {previous_x_y}")
                else:
                    universe_content.fill_one_pos((x, y), CellContent.ROCK)
                previous_x_y = x, y

    if is_part1:
        pass
    else:
        if verbose_level >= 1:
            print(f"[INFO] enabling universe infinite floor")
        universe_content.enable_floor()

    if verbose_level >= 2:
        print(f"[DEBUG] cells with content : {len(universe_content)}")
    if verbose_level >= 2:
        universe_content.print_content()

    count_sand_resting = 0
    while True:
        sand_resting = universe_content.process_sand_unit()
        if sand_resting:
            count_sand_resting += 1
        else:
            break
        if verbose_level >= 2:
            universe_content.print_content()

    if is_part1:
        print(f"Result (units of sand come to rest before sand starts flowing into the abyss) = {count_sand_resting}")
    else:
        count_sand_resting += 1  # trick
        print(f"Result (units of sand come to rest) = {count_sand_resting}")


if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")

    # parse the arguments
    args = parser.parse_args()

    # run !
    res = None
    time_begin = time.time()
    try:
        res = main(args.part1, args.input, verbose_level=args.verbose)
    finally:
        time_end = time.time()
        print(f"Result = {res}")
        print(f"Computation time = {time_end - time_begin} s")
