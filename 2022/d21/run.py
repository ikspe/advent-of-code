import argparse
import time


def main(is_part1: bool, input_file: str, verbose_level=0):

    with open(input_file) as f:
        all_formula = {}
        for line in f:
            name, formula = line.strip().split(": ")
            all_formula[name] = formula.split()

    is_part2_with_dichotomia = True
    is_part2_with_plots = False

    if is_part1:
        res = resolve_yelling(all_formula.copy(), verbose_level=verbose_level)
        assert res[0]
        res = res[1]
        print(f"Result (yelling of 'root'): {res}")
        return res
    else:
        assert len(all_formula["root"]) == 3
        all_formula["root"][1] = "="

        if is_part2_with_dichotomia:
            return part2_with_dichotomia(all_formula, verbose_level=verbose_level)
        elif is_part2_with_plots:
            return part2_with_plots(all_formula, verbose_level=verbose_level)
        else:
            return part2_with_analysis(all_formula, verbose_level=verbose_level)

def part2_with_dichotomia(all_formula: dict, verbose_level):
        # proceed with dichotomia
        # - first find upper bound for which res lower_bound != res upper_bound
        # - then refine with dichotomia

        results_dichotimia = {}
        assert "humn" in all_formula

        max_upper_bound = 2 ** 64

        lower_bound = 0
        if verbose_level >= 2:
            print(f"Searching value for input = {lower_bound} ...")
        all_formula["humn"] = [str(lower_bound)]
        res = resolve_yelling(all_formula.copy(), verbose_level=verbose_level)
        assert res[0]
        res = res[1]
        results_dichotimia[lower_bound] = res
        if verbose_level >= 2:
            print(f"Dichotomia search for input {lower_bound} : result = {res}")
            print()
        if res == 0:
            if verbose_level >= 1:
                print(f"Result (found directly with lower bound : {lower_bound} => {res}) : {lower_bound}")
            return res

        # dichotomia : find upper bound
        current_input = 1
        while True:
            if verbose_level >= 2:
                print(f"Searching value for input = {current_input} ...")
            all_formula["humn"] = [str(current_input)]
            res = resolve_yelling(all_formula.copy(), verbose_level=verbose_level)
            assert res[0]
            res = res[1]
            results_dichotimia[current_input] = res
            if verbose_level >= 2:
                print(f"Dichotomia search for input {current_input} : result = {res}")
                print()
            if res == 0:
                if verbose_level >= 1:
                    print(f"Result (found searching upper bound : {current_input} => {res}) : {current_input}")
                return res
            if res != results_dichotimia[lower_bound]:
                break
            current_input *= 2
            if current_input > max_upper_bound:
                raise ValueError(f"upper_bound for dichotomia exceeded max value : {current_input}")

        import time
        upper_bound = current_input
        while True:
            if verbose_level >= 4:
                time.sleep(1)
            current_input = (lower_bound + upper_bound) // 2
            if verbose_level >= 2:
                print(f"Searching value for input = {current_input} (bounds = {lower_bound} , {upper_bound}) ...")
            if current_input in (lower_bound, upper_bound):
                if verbose_level >= 1:
                    print(f"[ERROR] Dichotimia failed : current_input ({current_input}) == lower_bound ({lower_bound}) or upper_bound ({upper_bound})")
                    for k, v in sorted(results_dichotimia.items()):
                        print(f"    {k:5} => {v}")
                raise ValueError(f"Dichotimia failed : current_input = {current_input} , lower_bound ({lower_bound}) , upper_bound ({upper_bound}). Current results = {results_dichotimia}")
            if current_input in results_dichotimia:
                res = results_dichotimia[current_input]
            else:
                all_formula["humn"] = [str(current_input)]
                res = resolve_yelling(all_formula.copy(), verbose_level=verbose_level)
                assert res[0]
                res = res[1]
                results_dichotimia[current_input] = res
                if verbose_level >= 2:
                    print(f"Dichotomia search for input {current_input} : result = {res}")
                    print()
            if res == 0:
                if verbose_level >= 1:
                    print(f"Result (found searching value : {current_input} => {res}) : {current_input}")
                return res
            if results_dichotimia[lower_bound] == res:
                lower_bound = current_input
            else:
                upper_bound = current_input
        raise NotImplementedError(f"This code should never be reached")

def part2_with_plots(all_formula: dict, verbose_level):
    import matplotlib.pyplot as plt

    assert "humn" in all_formula
    x_values = []
    y_values = []
    found_result = None
    for current_input in range(0, 10**4, 1):
        all_formula["humn"] = [str(current_input)]
        res = resolve_yelling(all_formula.copy(), verbose_level=verbose_level)
        assert res[0]
        x_values.append(current_input)
        y_values.append(res[1])
        print(f"input {current_input:5} => {res[1]}")
        if res[1] == 0:
            found_result = current_input
    if found_result is not None:
        print(f"Found result : {found_result}")
    plt.scatter(x_values, y_values)
    plt.show()

def part2_with_analysis(all_formula: dict, verbose_level):

    UNKNOWN_SYMBOL = "????"

    assert "humn" in all_formula
    all_formula["humn"] = [UNKNOWN_SYMBOL]

    found_result, result, remaining_formula, known_yelling = resolve_yelling(all_formula.copy(), verbose_level=verbose_level)
    assert not found_result
    print(f"Remaining formula :")
    for k, v in remaining_formula.items():
        print(f"  {k}: " + " ".join(v))

    remaining_algebric_formula = {k: v.copy() for k, v in remaining_formula.items()}
    known_tokens = {k: [str(v)] for k, v in known_yelling.items()}
    print(f"Known tokens :")
    for k, v in known_tokens.items():
        print(f"  {k}: " + " ".join(v))

    while remaining_algebric_formula:
        if verbose_level >= 2:
            print()
            print(f"Iterating over all algebric formulas...")
        nb_remaining_items = len(remaining_algebric_formula)
        for name, formula in remaining_algebric_formula.copy().items():
            print(f"Iterating over formula {name} : (old) {formula}")
            new_formula = []
            for item in formula:
                if item in known_tokens:
                    expanded_item = known_tokens[item]
                    if len(expanded_item) > 1:
                        expanded_item = ["("] + expanded_item + [")"]
                    new_formula.extend(expanded_item)
                else:
                    new_formula.append(item)
            remaining_algebric_formula[name] = new_formula
            print(f"Iterating over formula {name} : (new) {new_formula}")
            tmp_formula_operands = [item for item in new_formula if item not in ["(", ")", "+", "*", "-", "/", "=", UNKNOWN_SYMBOL] and not item.isnumeric()]
            if all([item in known_tokens for item in tmp_formula_operands]):
                if verbose_level >= 2:
                    print(f"[DEBUG] found formula where all items are known : ({name}) {new_formula}")
                del remaining_algebric_formula[name]
                known_tokens[name] = new_formula
        if nb_remaining_items == len(remaining_algebric_formula):
            if verbose_level >= 2:
                print(f"[DEBUG] impossible to find any more know tokens => stopping")
            break

    if verbose_level >= 2:
        print(f"Algebric formulas :")
        for k, v in remaining_algebric_formula.items():
            print(f"  {k}: " + " ".join([str(t) for t in v]))
        print(f"Known tokens :")
        for k, v in known_tokens.items():
            print(f"  {k}: " + " ".join(v))

    print(f"Final equation to solve:")
    formula = known_tokens["root"]
    print(" ".join(formula))


def resolve_yelling(all_formula: dict, verbose_level):
    known_yelling = {}

    # optimize : first step is to simply populate from already know values
    #for name, formula in all_formula.copy().items():
    #    if len(formula) == 0 and formula[0].isnumeric():
    #        del all_formula[name]
    #        known_yelling[name] = int(all_formula)

    while all_formula:
        nb_remaining_items = len(all_formula)
        for name, formula in all_formula.copy().items():
            if len(formula) == 1:
                formula = formula + ["+", "0"]
            assert len(formula) == 3, (name, formula)
            operands = []
            for val in [formula[0], formula[2]]:
                if val.isnumeric():
                    operands.append(int(val))
                elif val in known_yelling:
                    operands.append(known_yelling[val])
                else:
                    break
            if len(operands) != 2:
                continue
            operator = formula[1]
            if operator == "+":
                res = operands[0] + operands[1]
            elif operator == "*":
                res = operands[0] * operands[1]
            elif operator == "-":
                res = operands[0] - operands[1]
            elif operator == "/":
                res = operands[0] // operands[1]
            elif operator == "=":
                if operands[0] < operands[1]:
                    res = -1
                elif operands[0] > operands[1]:
                    res = 1
                else:
                    res = 0
            else:
                raise ValueError(f"{name}: invalid operator {operator}")
            if verbose_level >= 3:
                print(f"[DEBUG] found yelling for {name} : {' '.join(formula)} = {res}")
            del all_formula[name]
            known_yelling[name] = res

        if nb_remaining_items == len(all_formula):
            if verbose_level >= 2:
                print(f"[DEBUG] stopped iterating : no more know value")
            break

        if "root" in known_yelling:
            break

    res = known_yelling.get("root")

    return (res is not None), res, all_formula, known_yelling

if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")

    # parse the arguments
    args = parser.parse_args()

    # run !
    res = None
    time_begin = time.time()
    try:
        res = main(args.part1, args.input, verbose_level=args.verbose)
    finally:
        time_end = time.time()
        print(f"Result = {res}")
        print(f"Computation time = {time_end - time_begin} s")
