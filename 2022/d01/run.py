import argparse
import time


def main(is_part1: bool, input_file: str, verbose_level=0):
    with open(input_file) as f:
        elves_list = []
        current_elve = []
        for line in f:
            line = line.strip()
            if not line:
                elves_list.append(current_elve)
                current_elve = []
                continue
            current_elve.append(int(line))
            continue
        elves_list.append(current_elve)
        del current_elve

    food_each_elve = [sum(x) for x in elves_list]
    if verbose_level >= 1:
        for idx, food in enumerate(food_each_elve):
            print(f"[DEBUG] food elve {idx:3x} = {food}")
    if is_part1:
        elves_top_count = 1
    else:
        elves_top_count = 3
    food_each_elve = sorted(food_each_elve, reverse=True)
    top_food_elves = food_each_elve[:elves_top_count]
    print(f"Top {elves_top_count} elves having more food are {top_food_elves} => total :")
    print(sum(top_food_elves))


if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")

    # parse the arguments
    args = parser.parse_args()

    # run !
    res = None
    time_begin = time.time()
    try:
        res = main(args.part1, args.input, verbose_level=args.verbose)
    finally:
        time_end = time.time()
        print(f"Result = {res}")
        print(f"Computation time = {time_end - time_begin} s")
