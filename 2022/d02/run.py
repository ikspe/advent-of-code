import argparse
import time


LIST_MY_OPP_WINNING_ACTIONS = [("A", "C"), ("B", "A"), ("C", "B")]
LIST_MY_OPP_LOOSING_ACTIONS = [(y, x) for x, y in LIST_MY_OPP_WINNING_ACTIONS]
DICT_MY_OPP_WINNING_ACTIONS = {x: y for x, y in LIST_MY_OPP_WINNING_ACTIONS}
DICT_MY_OPP_LOOSING_ACTIONS = {y: x for x, y in LIST_MY_OPP_WINNING_ACTIONS}
DICT_MY_OPP_TIGHT_ACTIONS = {x: x for x, _ in LIST_MY_OPP_WINNING_ACTIONS}

print(f"DICT_MY_OPP_LOOSING_ACTIONS = {DICT_MY_OPP_LOOSING_ACTIONS}")


def main(is_part1: bool, input_file: str, verbose_level=0):
    with open(input_file) as f:
        all_rounds = []
        for line in f:
            all_rounds.append(line.strip().split())
            assert len(all_rounds[-1]) == 2
    total_score = 0
    for op_action, token2 in all_rounds:
        if is_part1:
            my_action = {"X": "A", "Y": "B", "Z": "C"}[token2]
        else:
            # X means you need to lose, Y means you need to end the round in a draw, and Z means you need to win.
            my_action = {"X": DICT_MY_OPP_WINNING_ACTIONS, "Y": DICT_MY_OPP_TIGHT_ACTIONS, "Z": DICT_MY_OPP_LOOSING_ACTIONS}[token2][op_action]
            """"
            if token2 == "X":
                # my_action = {"A": "C", "B": "A", "C": "B"}[op_action]
                my_action = DICT_MY_OPP_WINNING_ACTIONS[op_action]
            elif token2 == "Y":
                # my_action = op_action
                my_action = DICT_MY_OPP_TIGHT_ACTIONS[op_action]
            elif token2 == "Z":
                # my_action = {"C": "A", "A": "B", "B": "C"}[op_action]
                my_action = DICT_MY_OPP_LOOSING_ACTIONS[op_action]
            else:
                raise ValueError(f"Invalid expected result {token2} from original {(op_action, token2)}")
            """
        score_my_action = {"A": 1, "B": 2, "C": 3}[my_action]
        # A : Rock / B : Paper / C : Scirrors
        if op_action == my_action:
            score_outcome = 3  # draw
        elif (op_action, my_action) in LIST_MY_OPP_WINNING_ACTIONS:
            score_outcome = 0
        elif (op_action, my_action) in LIST_MY_OPP_LOOSING_ACTIONS:
            score_outcome = 6
        else:
            raise ValueError(f"non managed case : {(op_action, my_action)} from original = {(op_action, token2)}")
        if verbose_level >= 1:
            print(f"[DEBUG] line score : {score_my_action + score_outcome} = {score_my_action} + {score_outcome}    (my action = {my_action} / op action = {op_action} / token2 = {token2})")
        total_score += (score_my_action + score_outcome)
    print(f"Total score : {total_score}")


if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")

    # parse the arguments
    args = parser.parse_args()

    # run !
    res = None
    time_begin = time.time()
    try:
        res = main(args.part1, args.input, verbose_level=args.verbose)
    finally:
        time_end = time.time()
        print(f"Result = {res}")
        print(f"Computation time = {time_end - time_begin} s")
