import argparse
import time
import typing


class RockBlockGenerator:
    """
####

.#.
###
.#.

..#
..#
###

#
#
#
#

##
##
    """
    _LIST_SHAPES = [
        # Warning : y growing corresponds to going upper and upper
        [(i, 0) for i in range(4)],
        [(1, 0), (0, 1), (1, 1), (2, 1), (1, 2)],
        # [(2, 0), (2, 1), (2, 2), (1, 2), (0, 2)], # 3rd form if y direction was bottom
        [(0, 0), (1, 0), (2, 0), (2, 1), (2, 2)],  # 3rd form with y direction to top (only directional shape)
        [(0, i) for i in range(4)],
        [(i, j) for i in range(0, 2) for j in range(0, 2)]
    ]
    def __iter__(self):
        cycle_length = len(self._LIST_SHAPES)
        idx = 0
        while True:
            yield self._LIST_SHAPES[idx], idx
            idx = (idx + 1) % cycle_length


class JetGasGenerator:
    def __init__(self, jet_gas_input: str) -> None:
        self._data = jet_gas_input

    def __iter__(self):
        cycle_length = len(self._data)
        idx = 0
        while True:
            yield self._data[idx], idx
            idx = (idx + 1) % cycle_length

class Chamber:
    # vertical chamber is exactly seven units wide
    _CHAMBER_WIDTH = 7

    def __init__(self, jet_gas_input: str, verbose_level: int) -> None:
        self._occupied_cells: typing.Set[tuple] = {(i, 0) for i in range(self._CHAMBER_WIDTH)}  # floor
        self._jet_gas_iter = iter(JetGasGenerator(jet_gas_input=jet_gas_input))
        self.verbose_level = verbose_level
        Chamber._debug_instance = self

    def find_max_y(self) -> int:
        y_cells_max = None
        for x, y in self._occupied_cells:
            if y_cells_max is None or y > y_cells_max:
                y_cells_max = y
        return y_cells_max

    def _get_block_init_shift(self):
        # Each rock appears so that its left edge is two units away from the left wall
        # and its bottom edge is three units above the highest rock in the room (or the floor, if there isn't one).

        # block has :
        # - all its (x, y) verifying 0 <= x < cls._CHAMBER_WIDTH and y >= 0
        # - at least one cell with x == 0
        # - at least one cell with y == 0
        current_max_y = self.find_max_y()
        return 2, current_max_y + 3 + 1

    @classmethod
    def _get_shifted_block(cls, block: typing.List[tuple], shift: tuple):
        new_block = []
        shift_x, shift_y = shift
        for x, y in block:
            new_block.append((x + shift_x, y + shift_y))
        return new_block

    def _is_block_allowed(self, block):
        for pos in block:
            if not 0 <= pos[0] < self._CHAMBER_WIDTH:
                return False
            if pos in self._occupied_cells:
                return False
        return True

    def insert_block(self, block: typing.List[tuple]):
        block = self._get_shifted_block(block, shift=self._get_block_init_shift())

        if self.verbose_level >= 3:
            print(f"A new rock begins falling:")
            self.draw(block=block)
            print()

        jet_gas_cycle_idx = None

        while True:
            # check if jet gas moves the block
            jet_gas_direction, jet_gas_cycle_idx = next(self._jet_gas_iter)
            if jet_gas_direction == ">":
                shifted_block = self._get_shifted_block(block, (1, 0))
                str_direction = "right"
            elif jet_gas_direction == "<":
                shifted_block = self._get_shifted_block(block, (-1, 0))
                str_direction = "left"
            else:
                raise ValueError(f"Invalid direction : {jet_gas_direction}")

            if self._is_block_allowed(shifted_block):
                block = shifted_block
            else:
                str_direction += ", but nothing happens"
            if self.verbose_level >= 3:
                print(f"Jet of gas pushes rock {str_direction}:")
                self.draw(block=block)
                print()

            # check if block can fall
            shifted_block = self._get_shifted_block(block, (0, -1))
            if self._is_block_allowed(shifted_block):
                block = shifted_block
                if self.verbose_level >= 3:
                    print(f"Rock falls 1 unit:")
                    self.draw(block=block)
                    print()
            else:
                break

        # block is resting : mark cells as occupied
        for pos in block:
            self._occupied_cells.add(pos)
        if self.verbose_level >= 3:
            print(f"Rock falls 1 unit, causing it to come to rest:")
            self.draw()
            print()

        return min(x for x, _ in block), max(y for _, y in block) == self.find_max_y(), jet_gas_cycle_idx  # return : tuple(block left position, is block at top, jet_gas_cycle_idx)

    def draw(self, block: typing.List[tuple] = None, block_already_shifted=True):
        y_max = self.find_max_y()
        if block_already_shifted:
            pass
        else:
            block = self._get_shifted_block(block, self._get_block_init_shift())
        if block:
            y_max = max(y_max, *[y for _, y in block])
        lines = [["." for _ in range(self._CHAMBER_WIDTH)] for _ in range(y_max + 1)]

        # occupied rocks
        for x, y in self._occupied_cells:
            lines[y][x] = "#"

        # floor
        for i in range(self._CHAMBER_WIDTH):
            lines[0][i] = "-"

        # block
        if block:
            for x, y in block:
                lines[y][x] = "@"

        for l in reversed(lines[1:]):
            print("|" + "".join(l) + "|")
        print("+" + "".join(lines[0]) + "+")


    @classmethod
    def audit_block(cls, block):
        matching_first_col = False
        matching_bottom_line = False
        for x, y in block:
            assert 0 <= x < cls._CHAMBER_WIDTH
            assert y >= 0
            if x == 0:
                matching_first_col = True
            if y == 0:
                matching_bottom_line = True
        assert matching_first_col
        assert matching_bottom_line


def _test_print_shapes():

    iter_block_generator = iter(RockBlockGenerator())

    for _ in range(11):
        block = next(iter_block_generator)
        Chamber.audit_block(block)
        matrix = [[" " for _ in range(4)] for _ in range(4)]
        print(block)
        for x, y in block:
            matrix[y][x] = "#"
        print("\n".join(["".join(l) for l in reversed(matrix)]))
        print()


def main(is_part1: bool, input_file: str, verbose_level=0):
    with open(input_file) as f:
        jet_gas_input = None
        for line in f:
            jet_gas_input = line.strip()
            break

    assert jet_gas_input

    #print(f"[DEBUG] testing shape generation...")
    #_test_print_shapes()
    #print(f"[DEBUG] Done !")

    if verbose_level >= 1:
        print(f"[INFO] creating chamber...")
    chamber = Chamber(jet_gas_input=jet_gas_input, verbose_level=verbose_level)
    iter_block_generator = iter(RockBlockGenerator())
    if verbose_level >= 1:
        print(f"[INFO] created!")

    part2_dict_block_cycle_to_height = {}
    part2_dict_result = None  # tuple : (dict key, dict previous val, dict new val)
    history_tower_height = []

    if is_part1:
        target_blocks_count = 2022
    else:
        target_blocks_count = 1000000000000


    #if verbose_level >= 2:
    #    print(f"[DEBUG] inserting one block to test")
    #chamber.insert_block(next(iter_block_generator))
    #if verbose_level >= 2:
    #    print(f"[DEBUG] inserted!")

    for block_num, (block, block_cycle_idx) in enumerate(iter_block_generator):
        if block_num >= target_blocks_count:
            break
        if verbose_level == 2:
            print(f"[DEBUG] inserting one block...")
            chamber.draw(block=block, block_already_shifted=False)
        block_left_pos, block_at_top, jet_gas_cycle_idx = chamber.insert_block(block)
        history_tower_height.append(chamber.find_max_y())
        # ugly, hardcode the found cycle length = 1767 to avoid similarities with first blocks that may have side-effects of reaching floor so not correct alignment, leading to incorrect cycle measurements
        if not is_part1 and block_at_top and block_num >= 1767:
            dict_key = (block_left_pos, block_cycle_idx, jet_gas_cycle_idx)
            dict_val = (block_num + 1, chamber.find_max_y())
            if dict_key in part2_dict_block_cycle_to_height:
                part2_dict_result = (dict_key, part2_dict_block_cycle_to_height[dict_key], dict_val)
                break
            part2_dict_block_cycle_to_height[dict_key] = dict_val


    if verbose_level >= 2:
        print(f"[DEBUG] Final picture:")
        chamber.draw()

    max_y = chamber.find_max_y()
    if is_part1:
        if verbose_level >= 1:
            print(f"[INFO] Result (max y block at iteration {target_blocks_count}) = {max_y}")
        return max_y
    else:
        if part2_dict_result is None:
            print(f"[WARNING] very suprisingly, we have iterated over the total , probably an error somewhere... Result (max y block at iteration {target_blocks_count}) = {max_y}")
            return max_y

        print(f"[DEBUG] len(history_tower_height) = {len(history_tower_height)}")
        print(f"[DEBUG] found part2_dict_result = {part2_dict_result}")
        dict_key, dict_previous_val, dict_new_val = part2_dict_result

        cycle_duration = dict_new_val[0] - dict_previous_val[0]
        cycle_height_increase = dict_new_val[1] - dict_previous_val[1]
        nb_cycles = (target_blocks_count - dict_previous_val[0]) // cycle_duration
        print(f"[DEBUG] found cycle_duration = {dict_new_val[0]} - {dict_previous_val[0]} = {cycle_duration} => nb_cycles = {nb_cycles} / cycle_height_increase = {dict_new_val[1]} - {dict_previous_val[1]} = {cycle_height_increase}")
        remaining_iterations = (target_blocks_count - dict_previous_val[0]) % cycle_duration
        history_idx_to_take_remaining = dict_previous_val[0] + remaining_iterations
        height_remaining_value = history_tower_height[history_idx_to_take_remaining - 1]

        print(f"[DEBUG] (approximate value) tower height after {nb_cycles} cycles = {nb_cycles * cycle_height_increase + dict_previous_val[1]} = {nb_cycles} * {cycle_height_increase} + {dict_previous_val[1]}")
        print(f"[DEBUG] Remaining iterations to compute : ({target_blocks_count} - {dict_previous_val[0]}) % {cycle_duration} = {remaining_iterations}")
        print(f"[DEBUG] Remaining value at iteration {remaining_iterations} : {height_remaining_value}")

        result = height_remaining_value + nb_cycles * cycle_height_increase
        print(f"[DEBUG] Result (Tower at iteration {target_blocks_count}) = {height_remaining_value} + {nb_cycles} * {cycle_height_increase} = {result}")

        assert dict_previous_val[1] == history_tower_height[dict_previous_val[0] - 1]
        return result


if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")

    # parse the arguments
    args = parser.parse_args()

    # run !
    res = None
    time_begin = time.time()
    try:
        res = main(args.part1, args.input, verbose_level=args.verbose)
    finally:
        time_end = time.time()
        print(f"Result = {res}")
        print(f"Computation time = {time_end - time_begin} s")
