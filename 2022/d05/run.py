import argparse
import enum
import time



class _ParsingState(enum.IntEnum):
    CRATES = 0
    CRATES_COUNT = 1
    MOVE_INSTRUCTIONS = 2


def _print_crates_stacks(crates_stacks):
    print(f"[DEBUG] crates_stacks:")
    for it_stack in crates_stacks:
        print(f"[DEBUG] (len ={len(it_stack):3}) {' '.join(it_stack)}")


def _parse_file(input_file: str, verbose_level=0):
    with open(input_file) as f:
        parsing_state = _ParsingState.CRATES
        line_crates = []
        move_instructions = []
        line_crates_naming = None
        for line in f:
            line, line_unstripped = line.rstrip(), line

            # parsing state transitions
            if parsing_state == _ParsingState.CRATES and not line.strip().startswith("["):
                parsing_state = _ParsingState.CRATES_COUNT
            elif parsing_state == _ParsingState.CRATES_COUNT and not line:
                parsing_state = _ParsingState.MOVE_INSTRUCTIONS
                continue

            # line treatment
            if parsing_state == _ParsingState.CRATES:
                line_crates.append(line)
            elif parsing_state == _ParsingState.CRATES_COUNT:
                line_crates_naming = line_unstripped
            elif parsing_state == _ParsingState.MOVE_INSTRUCTIONS:
                assert line
                move_instructions.append(line)

    if verbose_level >= 3:
        print("[DEBUG] line_crates :")
        for line in line_crates:
            print(line)
        print("[DEBUG] line_crates_naming :")
        print(line_crates_naming)
        print("[DEBUG] move_instructions :")
        for line in move_instructions:
            print(line)
        print("[DEBUG] <end lines>")

    # now find the crates stack positions, using line "line_crates_naming"
    crates_str_idx_positions = []
    start_pos = None
    for idx, char_idx in enumerate(line_crates_naming):
        if verbose_level >= 3:
            print(f"[DEBUG] line_crates_naming : idx = {idx} , char_idx = '{char_idx}'")
        if start_pos is None:
            if char_idx.isalnum():
                start_pos = idx
        else:
            if not char_idx.isalnum():
                crates_str_idx_positions.append((start_pos, idx))
                start_pos = None

    if verbose_level >= 2:
        print(f"[DEBUG] crates_str_idx_positions:")
        for start_pos, end_pos in crates_str_idx_positions:
            print(f"[DEBUG] {start_pos} -> {end_pos}")
    assert len(crates_str_idx_positions) == len(line_crates_naming.strip().split())
    assert [int(x) for x in line_crates_naming.strip().split()] == [x for x in range(1, len(crates_str_idx_positions) + 1)]

    del line_crates_naming

    # now transform crate lines into crates stacks (starting from bottom line)
    crates_stacks = [[] for _ in range(len(crates_str_idx_positions))]

    for line in reversed(line_crates):
        if verbose_level >= 2:
            print(f"[DEBUG] parsing crates from line: '{line}'")
        for crate_idx, (start_pos, end_pos) in enumerate(crates_str_idx_positions):
            crate = line[start_pos:end_pos]
            if verbose_level >= 3:
                print(f"[DEBUG] crate_idx = {crate_idx} : pos {start_pos} -> {end_pos} : crate = '{crate}'")
            if not crate.strip():
                continue

            assert len(crate) > 0 and line[start_pos - 1] == "[" and line[end_pos] == "]", f"bad crate '{crate} for line '{line}' at positions {start_pos} -> {end_pos}"
            crates_stacks[crate_idx].append(crate)

    if verbose_level >= 2:
        _print_crates_stacks(crates_stacks)

    return crates_stacks, move_instructions


def main(is_part1: bool, input_file: str, verbose_level=0):
    crates_stacks, move_instructions = _parse_file(input_file=input_file, verbose_level=verbose_level)

    for instructions in move_instructions:
        tokens_instructions = instructions.split()
        assert len(tokens_instructions) == 6
        assert tuple(tokens_instructions[idx] for idx in (0, 2, 4)) == ("move", "from", "to"), tokens_instructions
        mv_count, mv_from, mv_to = tuple(int(tokens_instructions[idx]) for idx in (1, 3, 5))
        if verbose_level >= 1:
            print(f"[INFO] move {mv_count} from {mv_from} to {mv_to}")
        if is_part1:
            # CrateMover 9000 => one by one
            for _ in range(mv_count):
                crates_stacks[mv_to -1 ].append(crates_stacks[mv_from - 1].pop())
        else:
            # CrateMover 9001 => the ability to pick up and move multiple crates at once
            moved_crates = crates_stacks[mv_from - 1][-mv_count:]
            crates_stacks[mv_to -1 ].extend(moved_crates)
            del crates_stacks[mv_from - 1][-mv_count:]
        if verbose_level >= 2:
            _print_crates_stacks(crates_stacks)

    message_top_stacks = []
    for it_stack in crates_stacks:
        message_top_stacks.append(it_stack[-1])
    print(f"Result = message top stacks = {''.join(message_top_stacks)}")


if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")

    # parse the arguments
    args = parser.parse_args()

    # run !
    res = None
    time_begin = time.time()
    try:
        res = main(args.part1, args.input, verbose_level=args.verbose)
    finally:
        time_end = time.time()
        print(f"Result = {res}")
        print(f"Computation time = {time_end - time_begin} s")
