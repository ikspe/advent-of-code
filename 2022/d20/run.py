import argparse
import time
import typing


class ChainedItem:
    def __init__(self, val: int):
        self.val = val
        self.prev_item: ChainedItem = None
        self.next_item: ChainedItem = None
        self.first = False

    def navigate(self, nb_steps: int):
        curr = self
        assert nb_steps >= 0
        #if nb_steps == 0:
        #    return self
        for _ in range(abs(nb_steps)):
            #if nb_steps > 0:
                curr = curr.next_item
            #else:
            #    curr = curr.prev_item
        return curr

    def _move_between(self, new_prev, new_next):
        new_prev: ChainedItem = new_prev
        new_next: ChainedItem = new_next
        assert self not in (new_prev, new_next)
        assert new_prev != new_next

        if self.first:
            # it doesn't really change anything since positions are relative to element 0...
            self.next_item.first = True
            self.first = False
        #elif new_next.first:
        #    # that is a decision made by AoC creator, it could have been different...
        #    new_next.first = False
        #    self.first = True

        # print(f"{self} moves between {new_prev} and {new_next}:")

        self.next_item.prev_item, self.prev_item.next_item = self.prev_item, self.next_item
        new_prev.next_item = self
        new_next.prev_item = self
        self.prev_item = new_prev
        self.next_item = new_next

    #def move_before(self, item):
    #    item: ChainedItem = item
    #    if item == self or item.prev_item == self:
    #        print(f"{self} does not move:")
    #        return
    #    self._move_between(item.prev_item, item)

    def move_after(self, item):
        item: ChainedItem = item
        if item == self or item.next_item == self:
            #print(f"{self} does not move:")
            return
        self._move_between(item, item.next_item)

    def __str__(self):
        return str(self.val)

def get_first_elem(list_all_items: list) -> ChainedItem:
    tmp_first_elem = [x for x in list_all_items if x.first]
    assert len(tmp_first_elem) == 1, len(tmp_first_elem)
    return tmp_first_elem[0]

def get_chained_list(item_first: ChainedItem):
    list_items = [item_first]
    item = item_first.next_item
    while item != item_first:
        list_items.append(item)
        item = item.next_item
    return list_items

def print_items(list_all_items: list):
    first_elem = get_first_elem(list_all_items)
    list_elems = get_chained_list(first_elem)
    print(", ".join([str(x) for x in list_elems]))
    print()

def main(is_part1: bool, input_file: str, verbose_level=0):

    # parse the list, not creating links between items
    list_all_items: typing.List[ChainedItem] = []
    with open(input_file) as f:
        for line in f:
            line = line.strip()
            if not line:
                continue
            list_all_items.append(ChainedItem(val=int(line)))

    if is_part1:
        decryption_key = 1
        repeat_count = 1
    else:
        decryption_key = 811589153
        repeat_count = 10

    list_all_items[0].first = True
    items_count = len(list_all_items)

    # chain the items
    for idx, item in enumerate(list_all_items):
        prev_item = list_all_items[idx - 1]
        next_item = list_all_items[(idx + 1) % items_count]
        item.prev_item = prev_item
        item.next_item = next_item
        prev_item.next_item = item
        next_item.prev_item = item
        item.val *= decryption_key

    if verbose_level >= 2:
        print(f"Initial arrangement:")
        print_items(list_all_items)

    for _ in range(repeat_count):
        for item in list_all_items:
            steps = item.val % (items_count - 1)
            other_item: ChainedItem = item.navigate(steps)
            #other_item: ChainedItem = item.navigate(item.val % len(list_all_items))
            item.move_after(other_item)  # in python, modulo is always positive
            #if item.val > 0:
            #    item.move_after(other_item)
            #else:
            #    item.move_before(other_item)
            if verbose_level >= 2:
                print_items(list_all_items)

    if True:
        tmp_list_elem_0 = [x for x in list_all_items if x.val == 0]
        assert len(tmp_list_elem_0) == 1
        element_0 = tmp_list_elem_0[0]
        assert element_0.val == 0

        result_items = []
        for relative_idx in [1000, 2000, 3000]:
            result_items.append(element_0.navigate(relative_idx % items_count))
        result_values = [x.val for x in result_items]
        print(f"[INFO] found items : {result_values}")
        result = sum(result_values)
        print(f"Result = {result}")
        return result


if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")

    # parse the arguments
    args = parser.parse_args()

    # run !
    res = None
    time_begin = time.time()
    try:
        res = main(args.part1, args.input, verbose_level=args.verbose)
    finally:
        time_end = time.time()
        print(f"Result = {res}")
        print(f"Computation time = {time_end - time_begin} s")
