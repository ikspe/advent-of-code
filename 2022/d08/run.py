import argparse
import math
import time

class _Global:
    verbose_level = 0


def _get_scenic_score(all_trees, line_idx, col_idx):
    this_tree = all_trees[line_idx][col_idx]
    line = all_trees[line_idx]

    scores_direction = []

    for direction, direction_idx_range in [
        ("left", range(col_idx - 1, -1, -1)),
        ("right", range(col_idx + 1, len(line))),
    ]:
        count_visible = 0
        reached_end = True
        for idx in direction_idx_range:
            count_visible += 1
            if line[idx] >= this_tree:
                reached_end = False
                break
        scores_direction.append((count_visible, reached_end))

    for direction, direction_idx_range in [
        ("top", range(line_idx - 1, -1, -1)),
        ("bottom", range(line_idx + 1, len(all_trees))),
    ]:
        count_visible = 0
        reached_end = True
        for idx in direction_idx_range:
            count_visible += 1
            if all_trees[idx][col_idx] >= this_tree:
                reached_end = False
                break
        scores_direction.append((count_visible, reached_end))

    scenic_score = math.prod(x for x, _ in scores_direction)
    is_visible = any(y for _, y in scores_direction)
    if _Global.verbose_level >= 2:
        print(f"[DEBUG]      line {line_idx} , col {col_idx} : scenic score :{scenic_score:3} / is_visible = {str(is_visible):5}   (visible by direction = {scores_direction})")
    return scenic_score, is_visible


def main(is_part1: bool, input_file: str):
    all_trees = []
    with open(input_file) as f:
        for line in f:
            all_trees.append([int(x) for x in line.strip()])
    if _Global.verbose_level >= 2:
        print(f"[DEBUG] Trees :")
        for line in all_trees:
            print(f"{''.join([str(x) for x in line])}")
        print(f"[DEBUG] Done!")

    line_count = len(all_trees)
    col_count = len(all_trees[0])  # all lines have same number of columns
    if _Global.verbose_level >= 3:
        print(f"[DEBUG] line_count = {line_count}")
        print(f"[DEBUG] col_count = {col_count}")
        print()

    max_scenic_score = 0
    count_tree_visible_interior = 0
    # don't compute trees on 1st line, last line, they don't have any view
    for line_idx, line in enumerate(all_trees[1:-1]):
        line_idx += 1
        for col_idx in range(1, col_count -1):
            if _Global.verbose_level >= 3:
                print(f"[DEBUG] computing line {line_idx} , col {col_idx} : tree height = {line[col_idx]}")
            scenic_score, is_visible = _get_scenic_score(all_trees, line_idx, col_idx)
            if scenic_score > max_scenic_score:
                max_scenic_score = scenic_score
            if is_visible:
                count_tree_visible_interior += 1

    count_tree_visible_edges = len(all_trees) * 2 + len(all_trees[0]) * 2 - 4  # all trees on edge are visible
    if _Global.verbose_level >= 2:
        print(f"[DEBUG] trees visible on edge : {count_tree_visible_edges}")
        print(f"[DEBUG] trees visible on interior : {count_tree_visible_interior}")
    count_tree_visible = count_tree_visible_edges + count_tree_visible_interior
    print(f"Result part 1 (number of trees visible) : {count_tree_visible}")
    print(f"Result part 2 (highest scenic score) : {max_scenic_score}")


if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")

    # parse the arguments
    args = parser.parse_args()

    # run !
    res = None
    time_begin = time.time()
    try:
        _Global.verbose_level = args.verbose
        res = main(args.part1, args.input)
    finally:
        time_end = time.time()
        print(f"Result = {res}")
        print(f"Computation time = {time_end - time_begin} s")
