import argparse
import time


ITEM_PRIORITIES = dict(
    [(chr(x + ord("a")), x +  1) for x in range(0, 26)] +
    [(chr(x + ord("A")), x + 27) for x in range(0, 26)])
assert ITEM_PRIORITIES["a"] == 1
assert ITEM_PRIORITIES["b"] == 2
assert ITEM_PRIORITIES["z"] == 26
assert ITEM_PRIORITIES["A"] == 27
assert ITEM_PRIORITIES["B"] == 28
assert ITEM_PRIORITIES["Z"] == 52

def main(is_part1: bool, input_file: str, verbose_level=0):
    with open(input_file) as f:
        list_items_rucksacks = []
        for line in f:
            line = line.strip()
            line_len = len(line)
            list_items_rucksacks.append((line[:line_len//2], line[line_len//2:]))
            assert len(list_items_rucksacks[-1][0]) == len(list_items_rucksacks[-1][1])
            continue

    if is_part1:
        sum_priorities = 0
        for comp1, comp2 in list_items_rucksacks:
            item = set(comp1).intersection(set(comp2))
            if verbose_level >= 3:
                print(f"[DEBUG] item in common = {item}")
            assert len(item) == 1
            item = list(item)[0]
            if verbose_level >= 2:
                print(f"[DEBUG] item in common = {item} => priority = {ITEM_PRIORITIES[item]}")
            sum_priorities += ITEM_PRIORITIES[item]
        print(f"Final result : {sum_priorities}")
    else:
        sum_badge_priorities = 0
        ruckstacks_of_the_group = []
        for comp1, comp2 in list_items_rucksacks:
            ruckstacks_of_the_group.append(comp1 + comp2)
            if len(ruckstacks_of_the_group) == 3:
                item = set(ruckstacks_of_the_group[0]).intersection(set(ruckstacks_of_the_group[1])).intersection(set(ruckstacks_of_the_group[2]))
                if verbose_level >= 3:
                    print(f"[DEBUG] item in common = {item} for elves = {ruckstacks_of_the_group}")
                assert len(item) == 1
                item = list(item)[0]
                if verbose_level >= 2:
                    print(f"[DEBUG] item in common = {item} => priority = {ITEM_PRIORITIES[item]}")
                sum_badge_priorities += ITEM_PRIORITIES[item]
                ruckstacks_of_the_group = []
        print(f"Final result : {sum_badge_priorities}")


if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")

    # parse the arguments
    args = parser.parse_args()

    # run !
    res = None
    time_begin = time.time()
    try:
        res = main(args.part1, args.input, verbose_level=args.verbose)
    finally:
        time_end = time.time()
        print(f"Result = {res}")
        print(f"Computation time = {time_end - time_begin} s")
