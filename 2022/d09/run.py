import argparse
import time


def _move_head(pos, direction):
    x, y = pos
    if direction == "R":
        return x + 1, y
    elif direction == "L":
        return x - 1, y
    elif direction == "U":
        return x, y - 1
    elif direction == "D":
        return x, y + 1
    else:
        raise ValueError

def _catchup_tail(pos_head, pos_tail):
    x_head, y_head = pos_head
    x_tail, y_tail = pos_tail
    if abs(x_head - x_tail) <= 1 and abs(y_head - y_tail) <= 1:
        return x_tail, y_tail
    if x_head == x_tail:
        y_tail += (y_head - y_tail) // abs(y_tail - y_head)
    elif y_head == y_tail:
        x_tail += (x_head - x_tail) // abs(x_tail - x_head)
    else:
        # move diagonally:
        y_tail += (y_head - y_tail) // abs(y_tail - y_head)
        x_tail += (x_head - x_tail) // abs(x_tail - x_head)
    return x_tail, y_tail

def _print_2d(rope, x_offset, y_offset, x_count, y_count):
    positions_2d = [["." for _ in range(x_count)] for _ in range(y_count)]
    positions_2d[y_offset][x_offset] = "s"  # start
    for idx, pos in reversed(list(enumerate(rope))):
        positions_2d[y_offset + pos[1]][x_offset + pos[0]] = ("H" if idx == 0 else ("T" if idx == len(rope) - 1 else str(idx)))
    for line in positions_2d:
        print("".join(line))


def main(is_part1: bool, input_file: str, verbose_level=0):
    with open(input_file) as f:
        list_inputs = []
        for line in f:
            line = line.strip()
            # print(f"read line = {line}")
            if not line:
                continue
            tokens = line.split()
            list_inputs.append((tokens[0], int(tokens[1])))

    if verbose_level >= 2:
        print(f"List moves :")
        for x, y in list_inputs:
            print(f"{x} : {y}")

    rope = [(0, 0) for _ in range(2 if is_part1 else 10)]

    #print_x_count, print_y_count = 20, 20
    #print_x_offset, print_y_offset = 0, print_y_count - 1
    #
    #_print_2d(rope, print_x_offset, print_y_offset, print_x_count, print_y_count)

    tail_positions_visited = set()
    debug_nb_iterations = 0
    for direction, nb_steps in list_inputs:
        if verbose_level >= 2:
            print(f"[DEBUG] move {direction} {nb_steps}")
        for _ in range(nb_steps):
            if verbose_level >= 2:
                print(f"[DEBUG]    move {direction}")
            rope[0] = _move_head(rope[0], direction)
            for idx in range(len(rope) - 1):
                rope[idx+1] = _catchup_tail(rope[idx], rope[idx+1])
                if verbose_level >= 2:
                    print(f"[DEBUG]        pos[{idx}] = {rope[idx]} / pos[{idx+1}] = {rope[idx+1]}")
                assert abs(rope[idx][0] - rope[idx+1][0]) <= 1 and abs(rope[idx][1] - rope[idx+1][1]) <= 1
            tail_positions_visited.add(rope[-1])
            #_print_2d(rope, print_x_offset, print_y_offset, print_x_count, print_y_count)
        debug_nb_iterations += 1
        #if debug_nb_iterations >= 2:
        #    raise ValueError(f"WARNING, exiting loop for debug purpose ; visited positions = {len(tail_positions_visited)}")

    print(f"Result (nb of tail positions visited) = {len(tail_positions_visited)}")


if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")

    # parse the arguments
    args = parser.parse_args()

    # run !
    res = None
    time_begin = time.time()
    try:
        res = main(args.part1, args.input, verbose_level=args.verbose)
    finally:
        time_end = time.time()
        print(f"Result = {res}")
        print(f"Computation time = {time_end - time_begin} s")
