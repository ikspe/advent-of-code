import argparse
import enum
import time
import typing


class _AocFileAbstract:
    def __init__(self, parent, name):
        self.name = name
        self.parent: _AocFileAbstract = parent

    def print_sub_tree(self, *args, **kwargs):
        raise NotImplementedError

    @property
    def subtree_items_sizes(self) -> typing.Tuple[int, typing.Tuple]:
        """
        Return a tuple : (total file size, list : [tuple(dir object, size)])
        """
        raise NotImplementedError


class AocDir (_AocFileAbstract):
    def __init__(self, parent, name):
        assert (parent is not None and name != "/") or (parent is None and name == "/")
        _AocFileAbstract.__init__(self, parent, name)
        self.childs: typing.Dict[str, _AocFileAbstract] = {}

    def add_child(self, child: _AocFileAbstract):
        self.childs[child.name] = child

    def get_from_name(self, name):
        if name == "..":
            assert self.parent
            return self.parent
        return self.childs[name]

    def print_sub_tree(self, indentation_level: int):
        name = "/" if not self.parent else self.name
        print(f"{' ' * indentation_level} - {name} (dir)")
        for f in self.childs.values():
            f.print_sub_tree(indentation_level + 2)
    @property
    def subtree_items_sizes(self) -> typing.Tuple[int, typing.Tuple[_AocFileAbstract, int]]:
        list_dir_sizes = []
        my_total_size = 0
        for child in self.childs.values():
            child_size, child_sublist = child.subtree_items_sizes
            my_total_size += child_size
            list_dir_sizes += child_sublist

        list_dir_sizes.append([self, my_total_size])

        return my_total_size, list_dir_sizes

class AocPlainFile(_AocFileAbstract):
    def __init__(self, parent, name, size):
        assert parent is not None
        _AocFileAbstract.__init__(self, parent, name)
        self.size = size

    def print_sub_tree(self, indentation_level: int):
        print(f"{' ' * indentation_level} - {self.name} (file, size = {self.size})")

    @property
    def subtree_items_sizes(self) -> typing.Tuple[int, typing.Tuple[_AocFileAbstract, int]]:
        return self.size, []

class AocFileSystem:
    def __init__(self):
        self._root_dir = AocDir(None, "/")
        self._current_dir: typing.Optional[AocDir] = None

    def cd(self, target_path: str):
        if target_path == "/":
            self._current_dir = self._root_dir
        else:
            assert not target_path.startswith("/")
            for tokens in target_path.split("/"):
                self._current_dir = self._current_dir.get_from_name(tokens)

    @property
    def root_dir(self):
        return self._root_dir

    @property
    def current_dir(self):
        return self._current_dir

    def print_tree(self):
        self._root_dir.print_sub_tree(0)


class InputState(enum.IntEnum):
    NONE = 0
    LS = 1
    OTHER_COMMAND = 2


def main(is_part1: bool, input_file: str, verbose_level=0):
    # build file system from input
    if verbose_level >= 1:
        print(f"[INFO] Parsing input file to build filesystem...")
    file_system = AocFileSystem()
    input_state = InputState.NONE
    with open(input_file) as f:
        for line in f:
            line = line.replace("\n", "").replace("\r", "")
            if verbose_level >= 3:
                print(f"[DEBUG] treating input line '{line}'")
            line_prefix, line_details = line.split(maxsplit=1)
            if line_prefix == "$":
                # execute command
                if line_details == "ls":
                    input_state = InputState.LS
                    continue
                input_state = InputState.OTHER_COMMAND
                command, target = line_details.split(maxsplit=1)
                if command == "cd":
                    file_system.cd(target)
                    continue
                raise ValueError(f"Unknown command '{command}' for line '{line}' ; line_details = '{line_details} ; target = '{target}'")
            else:
                assert input_state == InputState.LS
                if line_prefix == "dir":
                    new_file = AocDir(parent=file_system.current_dir, name=line_details)
                else:
                    # plain file
                    new_file = AocPlainFile(parent=file_system.current_dir, name=line_details, size=int(line_prefix))
                file_system.current_dir.add_child(new_file)

    if verbose_level >= 1:
        print(f"[INFO] Done !")

    if verbose_level >= 2:
        print(f"[INFO] Printing filesystem...")
        file_system.print_tree()
        print(f"[INFO] Done !")

    if verbose_level >= 1:
        print(f"[INFO] Gathering directory sizes...")
    root_dir_size, all_dir_sizes = file_system.root_dir.subtree_items_sizes
    if verbose_level >= 1:
        print(f"[INFO] Done !")
    if verbose_level >= 2:
        print(f"[INFO] Directory sizes :")
        for dir_obj, dir_size in all_dir_sizes:
            print(f"dir {'/' if dir_obj is file_system.root_dir else dir_obj.name} size = {dir_size}")
        print(f"Total root dir ('/') size = {root_dir_size}")

    if is_part1:
        result = 0
        for dir_obj, dir_size in all_dir_sizes:
            if dir_size <= 100000:
                result += dir_size
        print(f"Result : {result}")
    else:
        total_system_size = 70000000
        required_unused_space_size = 30000000

        current_unused_space_size = total_system_size - root_dir_size
        if verbose_level >= 1:
            print(f"[INFO] total_system_size = {total_system_size} / required_unused_space_size = {required_unused_space_size}" +
                  f" / root_dir_size = {root_dir_size} / => current_unused_space_size = {current_unused_space_size}")
        matching_dirs = [(dir_obj, dir_size) for dir_obj, dir_size in all_dir_sizes if current_unused_space_size + dir_size >= required_unused_space_size]
        matching_dirs_sizes_sorted = sorted(matching_dirs, key=lambda x: x[1])
        if verbose_level >= 1:
            print(f"Found {len(matching_dirs_sizes_sorted)} candidate for deletion :")
            for dir_obj, dir_size in matching_dirs_sizes_sorted:
                print(f"dir '{'/' if dir_obj is file_system.root_dir else dir_obj.name}' size = {dir_size}")
        print(f"Result = size of smallest dir to delete = {matching_dirs_sizes_sorted[0][1]}")

if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")

    # parse the arguments
    args = parser.parse_args()

    # run !
    res = None
    time_begin = time.time()
    try:
        res = main(args.part1, args.input, verbose_level=args.verbose)
    finally:
        time_end = time.time()
        print(f"Result = {res}")
        print(f"Computation time = {time_end - time_begin} s")
