import argparse
import time
import typing


class Sensor:
    _SENSOR_PREFIX = "Sensor at"
    _CLOSEST_BEACON_STR = ": closest beacon is at"
    _X_POS_PREFIX = " x="
    _Y_POS_PREFIX = " y="

    def __init__(self, line: str):
        line = line.strip()
        print(f"Starting parsing line {line} ...")
        assert line.startswith(self._SENSOR_PREFIX)
        sensor, beacon = line.removeprefix(self._SENSOR_PREFIX).split(self._CLOSEST_BEACON_STR)

        positions = []
        for coords in [sensor, beacon]:
            x_str, y_str = coords.split(",")
            assert x_str.startswith(self._X_POS_PREFIX)
            assert y_str.startswith(self._Y_POS_PREFIX)
            positions.append([int(x_str.removeprefix(self._X_POS_PREFIX)), int(y_str.removeprefix(self._Y_POS_PREFIX))])
        self._sensor_pos = tuple(positions[0])
        self._closest_beacon_pos = tuple(positions[1])

        # optimization : better static member than computed property
        self.coverage_distance = abs(self._sensor_pos[0] - self._closest_beacon_pos[0]) + abs(self._sensor_pos[1] - self._closest_beacon_pos[1])

    @property
    def sensor_pos(self):
        return self._sensor_pos

    @property
    def closest_beacon_pos(self):
        return self._closest_beacon_pos

    #def coverage_distance(self) -> int:
    #    return abs(self._sensor_pos[0] - self._closest_beacon_pos[0]) + abs(self._sensor_pos[1] - self._closest_beacon_pos[1])

    def __str__(self) -> str:
        return self._SENSOR_PREFIX + self._CLOSEST_BEACON_STR.join(
            [",".join([self._X_POS_PREFIX + str(x), self._Y_POS_PREFIX + str(y)]) for x, y in [self._sensor_pos, self._closest_beacon_pos]])

def count_line_pos_no_beacon(list_sensors: typing.List[Sensor], line_idx, verbose_level: int):
    beacons_pos = set()
    covered_pos = set()
    for sensor in list_sensors:
        if verbose_level >= 2:
            print(f"[DEBUG] parsing sensor {sensor.sensor_pos} - coverage_distance = {sensor.coverage_distance}")
        if sensor.closest_beacon_pos[1] == line_idx:
            beacons_pos.add(sensor.closest_beacon_pos)
            if verbose_level >= 3:
                print(f"[DEBUG]     Beacon found at position {sensor.closest_beacon_pos}")
        if sensor.sensor_pos[1] - sensor.coverage_distance <= line_idx <= sensor.sensor_pos[1] + sensor.coverage_distance:
            relative_y_beacon = abs(sensor.sensor_pos[1] - line_idx)
            remaining_x_coverage = sensor.coverage_distance - relative_y_beacon
            assert remaining_x_coverage >= 0
            for x in range(sensor.sensor_pos[0]-remaining_x_coverage, sensor.sensor_pos[0]+remaining_x_coverage+1):
                covered_pos.add((x, line_idx))
                if verbose_level >= 3:
                    print(f"[DEBUG]     Found covered point at position {(x, line_idx)}")
    if verbose_level >= 2:
        print(f"[DEBUG] line_idx {line_idx} : nb beacons = {len(beacons_pos)} / covered positions = {len(covered_pos)} / diff = {len(covered_pos.difference(beacons_pos))}")
    return len(covered_pos.difference(beacons_pos))

def part2_graphical_solution(list_sensors: typing.List[Sensor], x_max, y_max, verbose_level: int):
    # graphical solution or die (4000000x4000000 grid can't be handled, neither finding exact mathematical solution with inequations and abs())

    import matplotlib.pyplot as plt

    plt.axis([0, x_max, 0, y_max])
    #for sensor in list_sensors:
    #    plt.scatter(sensor.sensor_pos[0], sensor.sensor_pos[1], s=10000 ,  facecolors='none', edgecolors='blue' )
    for sensor in list_sensors:
        sensor_pos = sensor.sensor_pos
        coverage_distance = sensor.coverage_distance
        sensors_edge_points = []
        sensors_edge_points.append((sensor_pos[0]-coverage_distance, sensor.sensor_pos[1]))
        sensors_edge_points.append((sensor_pos[0], sensor_pos[1]-coverage_distance))
        sensors_edge_points.append((sensor_pos[0]+coverage_distance, sensor.sensor_pos[1]))
        sensors_edge_points.append((sensor_pos[0], sensor_pos[1]+coverage_distance))

        x1 = [sensors_edge_points[idx-1][0] for idx in range(0, len(sensors_edge_points))]
        y1 = [sensors_edge_points[idx-1][1] for idx in range(0, len(sensors_edge_points))]
        x2 = [sensors_edge_points[idx-0][0] for idx in range(0, len(sensors_edge_points))]
        y2 = [sensors_edge_points[idx-0][1] for idx in range(0, len(sensors_edge_points))]
        plt.plot(x1, y1, x2, y2, marker = 'o')

        ## TODO : better range with x1, y1 for idx in range(-1, len(sensors_edge_points))
        #for idx_point in range(len(sensors_edge_points)):
        #    previous_point = sensors_edge_points[idx_point-1]
        #    point = sensors_edge_points[idx_point]
        #    x1, y1 = [previous_point[0]], [previous_point[1]]
        #    x2, y2 = [point[0]], [point[1]]
        #    plt.plot(x1, y1, x2, y2, marker = 'o')

    #reverse y-axis
    plt.gca().invert_yaxis()

    plt.show()


def part2_solution_analytics(list_sensors: typing.List[Sensor], x_max, y_max, verbose_level: int):
    # find points on edge of each sensor and try if they are covered by other sensor or not

    all_edge_points = dict()  # better dict than set so we keep the order
    for sensor in list_sensors:
        sensor_pos = sensor.sensor_pos
        coverage_distance = sensor.coverage_distance
        for x_edge_start, y_edge_start, x_coeff, y_coeff in [
            (sensor_pos[0] - coverage_distance - 1, sensor_pos[1], 1, 1),
            (sensor_pos[0], sensor_pos[1] + coverage_distance + 1, 1, -1),
            (sensor_pos[0] + coverage_distance + 1, sensor_pos[1], -1, -1),
            (sensor_pos[0], sensor_pos[1] - coverage_distance - 1, -1, 1),
        ]:
            if verbose_level >= 2:
                print(f"{str(sensor):80}  ==>  coverage_distance = {str(coverage_distance):5} => x_min, y_min, x_coeff, y_coeff = {(x_edge_start, y_edge_start, x_coeff, y_coeff)}")
            for idx in range(coverage_distance + 1):
                x = x_edge_start + idx * x_coeff
                y = y_edge_start + idx * y_coeff
                if      (x > x_max and x_coeff > 0) or (x < 0 and x_coeff < 0) or \
                        (y > y_max and y_coeff > 0) or (y < 0 and y_coeff < 0):
                    break
                if 0 <= x <= x_max and 0 <= y <= y_max:
                    all_edge_points[(x, y)] = None
                    #if verbose_level >= 3:
                    #    print(f"[DEBUG] adding  edge point at {(x, y)}")
                #else:
                #    if verbose_level >= 3:
                #        print(f"[DEBUG] skipped edge point at {(x, y)} : out of bounds")

    if verbose_level >= 1:
        print(f"[INFO] number of edge points = {len(all_edge_points)}")
    if verbose_level >= 3:
        for pos in all_edge_points:
            print(f"[DEBUG] edge point at {pos}")
    if verbose_level >= 1:
        print(f"[INFO] detecting points...")
    for edge_point in all_edge_points:
        point_detected = False
        for sensor in list_sensors:
            dist = abs(edge_point[0] - sensor.sensor_pos[0]) + abs(edge_point[1] - sensor.sensor_pos[1])
            if dist <= sensor.coverage_distance:
                if verbose_level >= 2:
                    print(f"[DEBUG] skipping point {str(edge_point):10} : dist ({str(dist):5}) < coverage {sensor.coverage_distance:5} for sensor {sensor}")
                point_detected = True
                break
        if not point_detected:
            return edge_point
    raise ValueError("Could not find any matching point")


def main(is_part1: bool, input_file: str, verbose_level=0):
    with open(input_file) as f:
        list_sensors: typing.List[Sensor] = []
        for line in f:
            list_sensors.append(Sensor(line))

    if verbose_level >= 1:
        print(f"[DEBUG] sensors parsed : {len(list_sensors)}")
        for sensor in list_sensors:
            print(f"{str(sensor):80}  ==>  coverage distance = {sensor.coverage_distance}")

    if is_part1:
        # yeah, this test on file name is ugly, but so is the challenge...
        if "example" in input_file:
            line_idx = 10
        else:
            line_idx = 2000000
        pos_no_beacon = count_line_pos_no_beacon(list_sensors, line_idx, verbose_level=verbose_level)
        print(f"Result (line {line_idx} : positions without beacon) = {pos_no_beacon}")
    else:
        # yeah, this test on file name is ugly, but so is the challenge...
        if "example" in input_file:
            x_max = y_max = 20
        else:
            x_max = y_max = 4000000
        GRAPHICAL_SOLUTION = False
        if GRAPHICAL_SOLUTION:
            part2_graphical_solution(list_sensors=list_sensors, x_max=x_max, y_max=y_max, verbose_level=verbose_level)
        else:
            res_x, res_y = part2_solution_analytics(list_sensors=list_sensors, x_max=x_max, y_max=y_max, verbose_level=verbose_level)
            res = res_x * 4000000 + res_y
            print(f"Result (tuning frequency) = {res}  (= {res_x} * 4000000 + {res_y})")
            """
[INFO] number of edge points = 56593737
[INFO] detecting points...
Result (tuning frequency) = 12274327017867  (= 3068581 * 4000000 + 3017867)
Result = None
Computation time = 121.30685710906982 s
            """


if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")

    # parse the arguments
    args = parser.parse_args()

    # run !
    res = None
    time_begin = time.time()
    try:
        res = main(args.part1, args.input, verbose_level=args.verbose)
    finally:
        time_end = time.time()
        print(f"Result = {res}")
        print(f"Computation time = {time_end - time_begin} s")
