import argparse
import time


class LavaCubes:
    def __init__(self, all_cubes: set, verbose_level: int) -> None:
        self.all_cubes = all_cubes
        self.verbose_level = verbose_level

        self.min_x = self.max_x = self.min_y = self.max_y = self.min_z = self.max_z = None
        for x, y, z in self.all_cubes:
            if self.min_x is None or x < self.min_x:
                self.min_x = x
            if self.max_x is None or x > self.max_x:
                self.max_x = x
            if self.min_y is None or y < self.min_y:
                self.min_y = y
            if self.max_y is None or y > self.max_y:
                self.max_y = y
            if self.min_z is None or z < self.min_z:
                self.min_z = z
            if self.max_z is None or z > self.max_z:
                self.max_z = z

    def is_pos_space_to_exterior(self, start_cube):
        if start_cube in self.all_cubes:
            return False
        treated_cubes = set()
        next_candidates = [start_cube]
        treated_cubes.add(start_cube)
        while next_candidates:
            pos = next_candidates.pop(0)
            x, y, z = pos
            if not self.min_x < x < self.max_x:
                return True
            if not self.min_y < y < self.max_y:
                return True
            if not self.min_z < z < self.max_z:
                return True
            for adjacent_cube in [
                (x-1, y, z),
                (x+1, y, z),
                (x, y-1, z),
                (x, y+1, z),
                (x, y, z-1),
                (x, y, z+1),
            ]:
                if adjacent_cube in treated_cubes:
                    continue
                if adjacent_cube in self.all_cubes:
                    continue
                next_candidates.append(adjacent_cube)
                treated_cubes.add(adjacent_cube)

        return False


def main(is_part1: bool, input_file: str, verbose_level=0):
    with open(input_file) as f:
        all_cubes = set()
        for line in f:
            x, y, z = [int(t) for t in line.strip().split(",")]
            all_cubes.add((x, y, z))

    if is_part1:
        return part1_surface(all_cubes, verbose_level)
    else:
        return part2_surface(all_cubes, verbose_level)

def part1_surface(all_cubes, verbose_level: int):

    count_exposed_faces = 0
    for x, y, z in all_cubes:
        for adjacent_cube in [
            (x-1, y, z),
            (x+1, y, z),
            (x, y-1, z),
            (x, y+1, z),
            (x, y, z-1),
            (x, y, z+1),
        ]:
            if adjacent_cube not in all_cubes:
                count_exposed_faces += 1

    print(f"Result (number of exposed faces) = {count_exposed_faces}")
    return count_exposed_faces

def part2_surface(all_cubes, verbose_level: int):

    lava_cubes = LavaCubes(all_cubes, verbose_level)

    count_exposed_faces = 0
    for x, y, z in all_cubes:
        for adjacent_cube in [
            (x-1, y, z),
            (x+1, y, z),
            (x, y-1, z),
            (x, y+1, z),
            (x, y, z-1),
            (x, y, z+1),
        ]:
            if lava_cubes.is_pos_space_to_exterior(adjacent_cube):
                count_exposed_faces += 1

    print(f"Result (exterior surface of your scanned lava droplet) = {count_exposed_faces}")
    return count_exposed_faces


if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")

    # parse the arguments
    args = parser.parse_args()

    # run !
    res = None
    time_begin = time.time()
    try:
        res = main(args.part1, args.input, verbose_level=args.verbose)
    finally:
        time_end = time.time()
        print(f"Result = {res}")
        print(f"Computation time = {time_end - time_begin} s")
